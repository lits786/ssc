<?php

use Illuminate\Database\Eloquent\SoftDeletingTrait;

class Luku extends Eloquent{
	use SoftDeletingTrait;
	
	protected $table = 'luku';
	protected $dates = ['date'];

	public static function boot(){
        parent::boot();
        Luku::updated(function($luku){
		    $userId = Auth::id();
		    $luku->activities()->attach($userId,['comment'=>'Edited']);
		});
		Luku::created(function($luku){
		    $userId = Auth::id();
		    $luku->activities()->attach($userId,['comment'=>'Created']);
		});
		Luku::deleted(function($luku){
		    $userId = Auth::id();
		    $luku->activities()->attach($userId,['comment'=>'Deleted']);
		});
    }

	public function activities(){
		return $this->morphToMany('User', 'trackable', 'activities_track')
					->withPivot('comment')
					->withTimestamps();
	}

	public function files(){
		return $this->hasMany('LukuFile');
	}

	public function currency(){
		return $this->belongsTo('Currency');
	}

	public function percentage(){
		$percentage = $this->ssc_contribution *100 / $this->total_amount;
		return number_format($percentage,2).'%';
	}

	public function selfPercentage(){
		$percentage = $this->self_contribution *100 / $this->total_amount;
		return $percentage =  number_format($percentage,2).'%';
	}

	public function totalAmount(){
		return $this->currency->name.$this->total_amount;
	}

	public function selfContribution(){
		if(!$this->self_contribution){
			return 0;
		}
		return $this->currency->name.$this->self_contribution;
	}

	public function sscContribution(){
		if(!$this->ssc_contribution){
			return 0;
		}
		return $this->currency->name.$this->ssc_contribution;
	}
}