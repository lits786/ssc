<?php



class LoanApplication extends Eloquent{



	protected $dates = ['date_started', 'date_completed', 'registration_date', 'declaration_date'];

	public static function boot(){
        parent::boot();
        LoanApplication::updated(function($application){
		    $userId = Auth::id();
		    $application->activities()->attach($userId,['comment'=>'Edited']);
		});
		LoanApplication::created(function($application){
		    $userId = Auth::id();
		    $application->activities()->attach($userId,['comment'=>'Created']);
		});
		LoanApplication::deleted(function($application){
		    $userId = Auth::id();
		    $application->activities()->attach($userId,['comment'=>'Deleted']);
		});
    }

	public function activities(){
		return $this->morphToMany('User', 'trackable', 'activities_track');
	}

	public function setDateStartedAttribute($value){

		$this->attributes['date_started'] = date_create_from_format('d/m/Y', $value);

	}



	public function setDateCompletedAttribute($value){

		$this->attributes['date_completed'] = date_create_from_format('d/m/Y', $value);

	}



	public function setRegistrationDateAttribute($value){

		$this->attributes['registration_date'] = date_create_from_format('d/m/Y', $value);

	}



	public function setDeclarationDateAttribute($value){

		$this->attributes['declaration_date'] = date_create_from_format('d/m/Y', $value);

	}

}