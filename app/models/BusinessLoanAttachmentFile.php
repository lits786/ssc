<?php

use Illuminate\Database\Eloquent\SoftDeletingTrait;

class BusinessLoanAttachmentFile extends Eloquent{
	use SoftDeletingTrait;
	
	protected $table = 'business_loan_attachment_files';
	public $timestamps = false;
	protected $dates = ['deadline']; 
}