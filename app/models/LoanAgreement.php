<?php



class LoanAgreement extends Eloquent{

	public static function boot(){
        parent::boot();
        LoanAgreement::updated(function($agreement){
		    $userId = Auth::id();
		    $agreement->activities()->attach($userId,['comment'=>'Edited']);
		});
		LoanAgreement::created(function($agreement){
		    $userId = Auth::id();
		    $agreement->activities()->attach($userId,['comment'=>'Created']);
		});
		LoanAgreement::deleted(function($agreement){
		    $userId = Auth::id();
		    $agreement->activities()->attach($userId,['comment'=>'Deleted']);
		});
    }

	public function activities(){
		return $this->morphToMany('User', 'trackable', 'activities_track');
	}

	public function setDateOfAgreementAttribute($value){

		$this->attributes['date_of_agreement'] = date_create_from_format('d/m/Y', $value);

	} 

}

