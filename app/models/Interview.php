<?php

use Illuminate\Database\Eloquent\SoftDeletingTrait;

class Interview extends Eloquent{
	use SoftDeletingTrait;
	
	protected $dates = ['date', 'next_date','next_interview'];
	protected $touches = [];

	public static function boot(){
        parent::boot();
        Interview::updated(function($interview){
		    $userId = Auth::id();
		    $interview->activities()->attach($userId,['comment'=>'Edited']);
		});
		Interview::created(function($interview){
		    $userId = Auth::id();
		    $interview->activities()->attach($userId,['comment'=>'Created']);
		});
		Interview::deleted(function($interview){
		    $userId = Auth::id();
		    $interview->activities()->attach($userId,['comment'=>'Deleted']);
		});
    }

	public function activities(){
		return $this->morphToMany('User', 'trackable', 'activities_track')
					->withPivot('comment')
					->withTimestamps();
	}
}
