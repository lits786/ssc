<?php 

class Spouse extends Eloquent{

	protected $table = 'spouses';
	protected $dates = ['date_of_birth'];

	public function income(){
		return $this->hasOne('Income', 'spouse_id');
	}
}