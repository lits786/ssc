<?php 

class Income extends Eloquent{
	protected $table = 'incomes';

	public function incomeType(){
		return $this->belongsTo('IncomeType');
	}
}
