<?php





class BusinessPlan extends Eloquent{

	protected $dates = ['start_date'];

	public static function boot(){
        parent::boot();
        BusinessPlan::updated(function($plan){
		    $userId = Auth::id();
		    $plan->activities()->attach($userId,['comment'=>'Edited']);
		});
		BusinessPlan::created(function($plan){
		    $userId = Auth::id();
		    $plan->activities()->attach($userId,['comment'=>'Created']);
		});
		BusinessPlan::deleted(function($plan){
		    $userId = Auth::id();
		    $plan->activities()->attach($userId,['comment'=>'Deleted']);
		});
    }

	public function activities(){
		return $this->morphToMany('User', 'trackable', 'activities_track');
	}

}