<?php

use Illuminate\Database\Eloquent\SoftDeletingTrait;

class Rent extends Eloquent{
	use SoftDeletingTrait;
	
	protected $table = 'rent';
	protected $dates = ['date'];

	public static function boot(){
        parent::boot();
        Rent::updated(function($rent){
		    $userId = Auth::id();
		    $rent->activities()->attach($userId,['comment'=>'Edited']);
		});
		Rent::created(function($rent){
		    $userId = Auth::id();
		    $rent->activities()->attach($userId,['comment'=>'Created']);
		});
		Rent::deleted(function($rent){
		    $userId = Auth::id();
		    $rent->activities()->attach($userId,['comment'=>'Deleted']);
		});
    }

	public function activities(){
		return $this->morphToMany('User', 'trackable', 'activities_track')
					->withPivot('comment')
					->withTimestamps();
	}
	
	public function files(){
		return $this->hasMany('RentFile');
	}

	public function currency(){
		return $this->belongsTo('Currency');
	}

	public function percentage(){
		$ta = $this->total_amount != 0 ? $this->total_amount : 1;
		$percentage = $this->ssc_contribution *100 / $ta;
		return number_format($percentage,2).'%';
	}

	public function selfPercentage(){
		$ta = $this->total_amount != 0 ? $this->total_amount : 1;
		$percentage = $this->self_contribution *100 / $ta;
		return $percentage =  number_format($percentage,2).'%';
	}

	public function totalAmount(){
		return $this->currency->name.' '.$this->total_amount;
	}

	public function selfContribution(){
		return $this->currency->name.' '.$this->self_contribution;
	}

	public function sscContribution(){
		return $this->currency->name.' '.$this->ssc_contribution;
	}
}