<?php

use Illuminate\Database\Eloquent\SoftDeletingTrait;

class BusinessLoanPayment extends Eloquent{
	use SoftDeletingTrait;
	
	protected $dates = ['deadline'];

	public static function boot(){
        parent::boot();
        BusinessLoanPayment::updated(function($payment){
		    $userId = Auth::id();
		    $payment->activities()->attach($userId,['comment'=>'Edited']);
		});
		BusinessLoanPayment::created(function($payment){
		    $userId = Auth::id();
		    $payment->activities()->attach($userId,['comment'=>'Created']);
		});
		BusinessLoanPayment::deleted(function($payment){
		    $userId = Auth::id();
		    $payment->activities()->attach($userId,['comment'=>'Deleted']);
		});
    }

	public function activities(){
		return $this->morphToMany('User', 'trackable', 'activities_track')
					->withPivot('comment')
					->withTimestamps();
	}

	public function businessLoanAgreement(){
		return $this->belongsTo('BusinessLoanAgreement','business_loan_code', 'loan_code');
	}
}