<?php

class Relationship extends Eloquent{
	public function other(){
		return $this->hasMany('Other', 'relationship_id');
	}
}