<?php
class UserType extends Eloquent{

	public function files(){
		return $this->hasMany('SSCFile','user_type_assigned');
	}
	
	public function permissions(){
		return $this->belongsToMany('Permission');
	}
	
	public function hasPermission($permission){
		$permissions = $this->permissions()->lists('id');

		if(in_array($permission, $permissions)){
			return true;
		}
		return false;
	}
}