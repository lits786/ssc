<?php

use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletingTrait;

class SSCFile extends Eloquent{
	use SoftDeletingTrait;

	protected $table = 'files';
	protected $dates = ['date_paid', 'date'];

	public static function boot(){
        parent::boot();
        SSCFile::updated(function($file){
		    $userId = Auth::id();
		    $file->activities()->attach($userId,['comment'=>'Edited']);
		});
		SSCFile::created(function($file){
		    $userId = Auth::id();
		    $file->activities()->attach($userId,['comment'=>'Created']);
		});
		SSCFile::deleted(function($file){
		    $userId = Auth::id();
		    $file->activities()->attach($userId,['comment'=>'Deleted']);
		});
    }

	public function activities(){
		return $this->morphToMany('User', 'trackable', 'activities_track')
					->withPivot('comment')
					->withTimestamps();;
	}

	// Relationships
	public function person(){
		return $this->hasOne('Person','file_id');
	}

	public function remarks(){
		return $this->hasOne('Remark', 'file_id');
	}

	public function interviews(){
		return $this->hasMany('Interview', 'file_id');
	}

	public function userType(){
		return $this->belongsTo('UserType', 'user_type_assigned');
	}

	public function businessPlans(){
		return $this->hasMany('BusinessPlan', 'file_id');
	}

	public function rents($currency_id=0){
		if($currency_id == 0){
			return $this->hasMany('Rent', 'file_id');
		}else{
			return $this->hasMany('Rent', 'file_id')
						->where('currency_id', $currency_id);
		}
	}

	public function medicals($currency_id=0){
		if($currency_id == 0){
			return $this->hasMany('Medical', 'file_id');
		}else{
			return $this->hasMany('Medical', 'file_id')
						->where('currency_id', $currency_id);
		}
	}

	public function luku($currency_id=0){
		if($currency_id == 0){
			return $this->hasMany('Luku', 'file_id');
		}else{
			return $this->hasMany('Luku', 'file_id')
						->where('currency_id', $currency_id);
		}
	}

	public function transport($currency_id=0){
		if($currency_id == 0){
			return $this->hasMany('Transport', 'file_id');
		}else{
			return $this->hasMany('Transport', 'file_id')
						->where('currency_id', $currency_id);
		}
	}

	public function cashAllowance($currency_id=0){
		if($currency_id == 0){
			return $this->hasMany('CashAllowance', 'file_id');
		}else{
			return $this->hasMany('CashAllowance', 'file_id')
						->where('currency_id', $currency_id);
		}
	}

	public function maidSupport($currency_id=0){
		if($currency_id == 0){
			return $this->hasMany('MaidSupport', 'file_id');
		}else{
			return $this->hasMany('MaidSupport', 'file_id')
						->where('currency_id', $currency_id);
		}
	}
	public function visit(){
		
			return $this->hasMany('Visit', 'file_id');
		
	}
	public function dawasco($currency_id=0){
		if($currency_id == 0){
			return $this->hasMany('Dawasco', 'file_id');
		}else{
			return $this->hasMany('Dawasco', 'file_id')
						->where('currency_id', $currency_id);
		}
	}

	public function schoolAssistances($currency_id=0){
		if($currency_id == 0){
			return $this->hasMany('SchoolAssistance', 'file_id');
		}else{
			return $this->hasMany('SchoolAssistance', 'file_id')
						->where('currency_id', $currency_id);
		}
	}
	public function generalFileNo()
	{
			
			return 'SSC'.sprintf("%04d", $this->id);
	}
	
	public function midFileNumber($assigned_to)
	{
		if($assigned_to == 2)
		{	
			$u_id = UserType::find($assigned_to);
			
			$num = $u_id->num + 1;
			
			$u_id->num =  $num;
			$u_id->save();
			return 'SSC/T0/'.sprintf("%04d",$num);
		}
		elseif($assigned_to == 3)
		{	
			$u_id = UserType::find($assigned_to);
			
			$num = $u_id->num + 1;
			
			$u_id->num =  $num;
			$u_id->save();
			return 'SSC/T1/'.sprintf("%04d", $num);
		}
		elseif($assigned_to == 4)
		{	
			$u_id = UserType::find($assigned_to);
			
			$num = $u_id->num + 1;
			
			$u_id->num =  $num;
			$u_id->save();
			return 'SSC/T2/'.sprintf("%04d", $num);
		}
		elseif($assigned_to == 5)
		{
			$u_id = UserType::find($assigned_to);
			
			$num = $u_id->num + 1;
			
			$u_id->num =  $num;
			$u_id->save();
			return 'SSC/OR/'.sprintf("%04d", $num);
			
			
			
			
		}
		elseif($assigned_to == 6)
		{
			$u_id = UserType::find($assigned_to);
			
			$num = $u_id->num + 1;
			
			$u_id->num =  $num;
			$u_id->save();
			
			return 'SSC/FA/'.sprintf("%04d",$num);
		}
		elseif($assigned_to == 7)
		{
			$u_id = UserType::find($assigned_to);
			
			$num = $u_id->num + 1;
			
			$u_id->num =  $num;
			$u_id->save();
			return 'SSC/CB/'.sprintf("%04d", $this->id);
		}
		
	}

	public function orTypeFileNumber($type = 0)
	{
		if($type == 1)
			{
				$u_id = OutreachType::find($type);
			
				$num = $u_id->num + 1;
				
				$u_id->num =  $num;
				$u_id->save();
				return 'SSC/OR/US/'.sprintf("%04d",$num);
			}
			elseif($type == 2)
			{
				$u_id = OutreachType::find($type);
			
				$num = $u_id->num + 1;
				
				$u_id->num =  $num;
				$u_id->save();
				return 'SSC/OR/BF/'.sprintf("%04d",$num);
			}
			elseif($type == 3)
			{
				$u_id = OutreachType::find($type);
			
				$num = $u_id->num + 1;
				
				$u_id->num =  $num;
				$u_id->save();
				return 'SSC/OR/OPH/'.sprintf("%04d",$num);
			}
			elseif($type == 4)
			{
				$u_id = OutreachType::find($type);
			
				$num = $u_id->num + 1;
				
				$u_id->num =  $num;
				$u_id->save();
				return 'SSC/OR/BK/'.sprintf("%04d",$num);
			}else
			{
			return "-";
			}
	}
	public function formatFileNumber($assigned_to = 0,$type= null){
		// return 'SSC'.date('Y').'-'.sprintf("%05d", $this->id);
		
		if($assigned_to == 2)
		{	
			$u_id = UserType::find($assigned_to);
			
			$num = $u_id->num + 1;
			
			$u_id->num =  $num;
			$u_id->save();
			return 'SSC/T0/'.sprintf("%04d",$num);
		}
		elseif($assigned_to == 3)
		{	
			$u_id = UserType::find($assigned_to);
			
			$num = $u_id->num + 1;
			
			$u_id->num =  $num;
			$u_id->save();
			return 'SSC/T1/'.sprintf("%04d", $num);
		}
		elseif($assigned_to == 4)
		{	
			$u_id = UserType::find($assigned_to);
			
			$num = $u_id->num + 1;
			
			$u_id->num =  $num;
			$u_id->save();
			return 'SSC/T2/'.sprintf("%04d", $num);
		}
		elseif($assigned_to == 5)
		{
			if($type == 1)
			{
				$u_id = OutreachType::find($type);
			
				$num = $u_id->num + 1;
				
				$u_id->num =  $num;
				$u_id->save();
				return 'SSC/OR/US/'.sprintf("%04d",$num);
			}
			elseif($type == 2)
			{
				$u_id = OutreachType::find($type);
			
				$num = $u_id->num + 1;
				
				$u_id->num =  $num;
				$u_id->save();
				return 'SSC/OR/BF/'.sprintf("%04d",$num);
			}
			elseif($type == 3)
			{
				$u_id = OutreachType::find($type);
			
				$num = $u_id->num + 1;
				
				$u_id->num =  $num;
				$u_id->save();
				return 'SSC/OR/OPH/'.sprintf("%04d",$num);
			}
			elseif($type == 4)
			{
				$u_id = OutreachType::find($type);
			
				$num = $u_id->num + 1;
				
				$u_id->num =  $num;
				$u_id->save();
				return 'SSC/OR/BK/'.sprintf("%04d",$num);
			}
			
		}
		elseif($assigned_to == 6)
		{
			$u_id = DB::table('user_types')->where('id',$assigned_to)-> value('num');
			$num = $u_id + 1;
			$user_type = UserType::find($assigned_to);
			$user_type->num =  $num;
			
			return 'SSC/FA/'.sprintf("%04d",$num);
		}
		elseif($assigned_to == 7)
		{
			$u_id = DB::table('user_types')->where('id',$assigned_to)-> value('num');
			$num = $u_id + 1;
			$user_type = UserType::find($assigned_to);
			$user_type->num =  $num;
			return 'SSC/CB/'.sprintf("%04d", $this->id);
		}
		else
		{
			$u_id = UserType::find(1);
			
			$num = $u_id->num + 1;
			
			$u_id->num =  $num;
			$u_id->save();
			return 'SSC'.sprintf("%04d", $num);
		}
		

	}

	public function totalRentStatement(){
		$currency_ids = Currency::lists('id');
		$table = '<table class="table table-hover table-striped table-bordered"><thead><tr><th>CURRENCY</th><th>TOTAL AMOUNT</th><th>SELF CONTRIBUTION</th><th>SSC CONTRIBUTION</th></tr></thead>';
		$table .= '<tbody>';

		foreach ($currency_ids as $currency_id) {
			$currency = Currency::find($currency_id);
			$amount = $this->rents($currency_id)->sum('total_amount');
			if($amount){
				$selfPercentage = $this->rents($currency_id)->sum('self_contribution') *100/$amount;
				$selfPercentage = number_format($selfPercentage,2).'%';
				$selfPercentage = '('.$selfPercentage.')';

				$sscPercentage = $this->rents($currency_id)->sum('ssc_contribution') *100/$amount;
				$sscPercentage = number_format($sscPercentage,2).'%';
				$sscPercentage = '('.$sscPercentage.')';

				$table .= '<tr>';
				$table .= '<td>'.$currency->name.'</td>';
				$table .= '<td>'.$amount.'</td>';
				$table .= '<td>'.$this->rents($currency_id)->sum('self_contribution').$selfPercentage.'</td>';
				$table .= '<td>'.$this->rents($currency_id)->sum('ssc_contribution').$sscPercentage.'</td>';
				$table .= '</tr>';
			}
		}

		$table .= '</tbody></table>';
		return $table;
	}

	public function totalLukuStatement(){
		$currency_ids = Currency::lists('id');
		$table = '<table class="table table-hover table-striped table-bordered"><thead><tr><th>CURRENCY</th><th>TOTAL AMOUNT</th><th>SELF CONTRIBUTION</th><th>SSC CONTRIBUTION</th></tr></thead>';
		$table .= '<tbody>';

		foreach ($currency_ids as $currency_id) {
			$currency = Currency::find($currency_id);
			$amount = $this->luku($currency_id)->sum('total_amount');
			if($amount){
				$selfPercentage = $this->luku($currency_id)->sum('self_contribution') *100/$amount;
				$selfPercentage = number_format($selfPercentage,2).'%';
				$selfPercentage = '('.$selfPercentage.')';

				$sscPercentage = $this->luku($currency_id)->sum('ssc_contribution') *100/$amount;
				$sscPercentage = number_format($sscPercentage,2).'%';
				$sscPercentage = '('.$sscPercentage.')';

				$table .= '<tr>';
				$table .= '<td>'.$currency->name.'</td>';
				$table .= '<td>'.$amount.'</td>';
				$table .= '<td>'.$this->luku($currency_id)->sum('self_contribution').$selfPercentage.'</td>';
				$table .= '<td>'.$this->luku($currency_id)->sum('ssc_contribution').$sscPercentage.'</td>';
				$table .= '</tr>';
			}
		}

		$table .= '</tbody></table>';
		return $table;
	}

	public function totalTransportStatement(){
		$currency_ids = Currency::lists('id');
		$table = '<table class="table table-hover table-striped table-bordered"><thead><tr><th>CURRENCY</th><th>TOTAL AMOUNT</th><th>SELF CONTRIBUTION</th><th>SSC CONTRIBUTION</th></tr></thead>';
		$table .= '<tbody>';

		foreach ($currency_ids as $currency_id) {
			$currency = Currency::find($currency_id);
			$amount = $this->transport($currency_id)->sum('total_amount');
			if($amount){
				$selfPercentage = $this->transport($currency_id)->sum('self_contribution') *100/$amount;
				$selfPercentage = number_format($selfPercentage,2).'%';
				$selfPercentage = '('.$selfPercentage.')';

				$sscPercentage = $this->transport($currency_id)->sum('ssc_contribution') *100/$amount;
				$sscPercentage = number_format($sscPercentage,2).'%';
				$sscPercentage = '('.$sscPercentage.')';

				$table .= '<tr>';
				$table .= '<td>'.$currency->name.'</td>';
				$table .= '<td>'.$amount.'</td>';
				$table .= '<td>'.$this->transport($currency_id)->sum('self_contribution').$selfPercentage.'</td>';
				$table .= '<td>'.$this->transport($currency_id)->sum('ssc_contribution').$sscPercentage.'</td>';
				$table .= '</tr>';
			}
		}

		$table .= '</tbody></table>';
		return $table;
	}

	public function totalMaidSupportStatement(){
		$currency_ids = Currency::lists('id');
		$table = '<table class="table table-hover table-striped table-bordered"><thead><tr><th>CURRENCY</th><th>TOTAL AMOUNT</th><th>SELF CONTRIBUTION</th><th>SSC CONTRIBUTION</th></tr></thead>';
		$table .= '<tbody>';

		foreach ($currency_ids as $currency_id) {
			$currency = Currency::find($currency_id);
			$amount = $this->maidSupport($currency_id)->sum('total_amount');
			if($amount){
				$selfPercentage = $this->maidSupport($currency_id)->sum('self_contribution') *100/$amount;
				$selfPercentage = number_format($selfPercentage,2).'%';
				$selfPercentage = '('.$selfPercentage.')';

				$sscPercentage = $this->maidSupport($currency_id)->sum('ssc_contribution') *100/$amount;
				$sscPercentage = number_format($sscPercentage,2).'%';
				$sscPercentage = '('.$sscPercentage.')';

				$table .= '<tr>';
				$table .= '<td>'.$currency->name.'</td>';
				$table .= '<td>'.$amount.'</td>';
				$table .= '<td>'.$this->maidSupport($currency_id)->sum('self_contribution').$selfPercentage.'</td>';
				$table .= '<td>'.$this->maidSupport($currency_id)->sum('ssc_contribution').$sscPercentage.'</td>';
				$table .= '</tr>';
			}
		}

		$table .= '</tbody></table>';
		return $table;
	}

	public function totalCashAllowanceStatement(){
		$currency_ids = Currency::lists('id');
		$table = '<table class="table table-hover table-striped table-bordered"><thead><tr><th>CURRENCY</th><th>TOTAL AMOUNT</th><th>SELF CONTRIBUTION</th><th>SSC CONTRIBUTION</th></tr></thead>';
		$table .= '<tbody>';

		foreach ($currency_ids as $currency_id) {
			$currency = Currency::find($currency_id);
			$amount = $this->cashAllowance($currency_id)->sum('total_amount');
			if($amount){
				$selfPercentage = $this->cashAllowance($currency_id)->sum('self_contribution') *100/$amount;
				$selfPercentage = number_format($selfPercentage,2).'%';
				$selfPercentage = '('.$selfPercentage.')';

				$sscPercentage = $this->cashAllowance($currency_id)->sum('ssc_contribution') *100/$amount;
				$sscPercentage = number_format($sscPercentage,2).'%';
				$sscPercentage = '('.$sscPercentage.')';

				$table .= '<tr>';
				$table .= '<td>'.$currency->name.'</td>';
				$table .= '<td>'.$amount.'</td>';
				$table .= '<td>'.$this->cashAllowance($currency_id)->sum('self_contribution').$selfPercentage.'</td>';
				$table .= '<td>'.$this->cashAllowance($currency_id)->sum('ssc_contribution').$sscPercentage.'</td>';
				$table .= '</tr>';
			}
		}

		$table .= '</tbody></table>';
		return $table;
	}

	public function totalDawascoStatement(){
		$currency_ids = Currency::lists('id');
		$table = '<table class="table table-hover table-striped table-bordered"><thead><tr><th>CURRENCY</th><th>TOTAL AMOUNT</th><th>SELF CONTRIBUTION</th><th>SSC CONTRIBUTION</th></tr></thead>';
		$table .= '<tbody>';

		foreach ($currency_ids as $currency_id) {
			$currency = Currency::find($currency_id);
			$amount = $this->dawasco($currency_id)->sum('total_amount');
			if($amount){
				$selfPercentage = $this->dawasco($currency_id)->sum('self_contribution') *100/$amount;
				$selfPercentage = number_format($selfPercentage,2).'%';
				$selfPercentage = '('.$selfPercentage.')';

				$sscPercentage = $this->dawasco($currency_id)->sum('ssc_contribution') *100/$amount;
				$sscPercentage = number_format($sscPercentage,2).'%';
				$sscPercentage = '('.$sscPercentage.')';

				$table .= '<tr>';
				$table .= '<td>'.$currency->name.'</td>';
				$table .= '<td>'.$amount.'</td>';
				$table .= '<td>'.$this->dawasco($currency_id)->sum('self_contribution').$selfPercentage.'</td>';
				$table .= '<td>'.$this->dawasco($currency_id)->sum('ssc_contribution').$sscPercentage.'</td>';
				$table .= '</tr>';
			}
		}

		$table .= '</tbody></table>';
		return $table;
	}

	public function totalMedicalStatement(){
		$currency_ids = Currency::lists('id');
		$table = '<table class="table table-hover table-striped table-bordered"><thead><tr><th>CURRENCY</th><th>TOTAL AMOUNT</th><th>SELF CONTRIBUTION</th><th>SSC CONTRIBUTION</th></tr></thead>';
		$table .= '<tbody>';

		foreach ($currency_ids as $currency_id) {
			$currency = Currency::find($currency_id);
			$amount = $this->medicals($currency_id)->sum('total_amount');
			if($amount){
				$selfPercentage = $this->medicals($currency_id)->sum('self_contribution') *100/$amount;
				$selfPercentage = number_format($selfPercentage,2).'%';
				$selfPercentage = '('.$selfPercentage.')';

				$sscPercentage = $this->medicals($currency_id)->sum('ssc_contribution') *100/$amount;
				$sscPercentage = number_format($sscPercentage,2).'%';
				$sscPercentage = '('.$sscPercentage.')';

				$table .= '<tr>';
				$table .= '<td>'.$currency->name.'</td>';
				$table .= '<td>'.$amount.'</td>';
				$table .= '<td>'.$this->medicals($currency_id)->sum('self_contribution').$selfPercentage.'</td>';
				$table .= '<td>'.$this->medicals($currency_id)->sum('ssc_contribution').$sscPercentage.'</td>';
				$table .= '</tr>';
			}
		}

		$table .= '</tbody></table>';
		return $table;
	}

	public function totalSchoolAssistanceStatement(){
		$currency_ids = Currency::lists('id');
		$table = '<table class="table table-hover table-striped table-bordered"><thead><tr><th>CURRENCY</th><th>TOTAL AMOUNT</th><th>SELF CONTRIBUTION</th><th>SSC CONTRIBUTION</th></tr></thead>';
		$table .= '<tbody>';

		foreach ($currency_ids as $currency_id) {
			$currency = Currency::find($currency_id);
			$amount = $this->schoolAssistances($currency_id)->sum('total_amount');
			if($amount){
				$selfPercentage = $this->schoolAssistances($currency_id)->sum('self_contribution') *100/$amount;
				$selfPercentage = number_format($selfPercentage,2).'%';
				$selfPercentage = '('.$selfPercentage.')';

				$sscPercentage = $this->schoolAssistances($currency_id)->sum('ssc_contribution') *100/$amount;
				$sscPercentage = number_format($sscPercentage,2).'%';
				$sscPercentage = '('.$sscPercentage.')';


				$table .= '<tr>';
				$table .= '<td>'.$currency->name.'</td>';
				$table .= '<td>'.$amount.'</td>';
				$table .= '<td>'.$this->schoolAssistances($currency_id)->sum('self_contribution').$selfPercentage.'</td>';
				$table .= '<td>'.$this->schoolAssistances($currency_id)->sum('ssc_contribution').$sscPercentage.'</td>';
				$table .= '</tr>';
			}
		}

		$table .= '</tbody></table>';
		return $table;
	}


	public function nextInterviewDate(){
		$interview = $this->interviews()->orderBy('next_date','DESC')->first();
		$today = new Carbon;

		if($interview){
			// $date = $interview->next_date;
			$date = $interview->next_interview;
			if($today->diffInDays($interview->next_date)<=7){
				return "<span style='color:red;'>".$date->format('d-M-Y')."</span>";
			}
			return "<span>".$date->format('d-M-Y')."</span>";
		}

		return '-';
	}	

	public function concludedStatus(){
		$concluded = '<span>Concluded</span>';
		if($this->concluded=='no'){
			return '<span style="color:red;">Not Concluded</span>';
		}

		if(in_array('no', $this->interviews()->lists('concluded'))){
			return '<span style="color:red;">Not Concluded</span>';
		}

		return $concluded;
	}
}