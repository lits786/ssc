<?php

class Student extends Eloquent{
	
	public function grade()
	{
		return $this->belongsTo(Grade::class);
	}
}