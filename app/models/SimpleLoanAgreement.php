<?php

use Illuminate\Database\Eloquent\SoftDeletingTrait;

class SimpleLoanAgreement extends Eloquent{
	use SoftDeletingTrait;
	
	protected $table = 'simple_loan_agreements';
	public $timestamps = false;
	protected $dates = ['deadline'];

	public static function boot(){
        parent::boot();
        SimpleLoanAgreement::updated(function($agreement){
		    $userId = Auth::id();
		    $agreement->activities()->attach($userId,['comment'=>'Edited']);
		});
		SimpleLoanAgreement::created(function($agreement){
		    $userId = Auth::id();
		    $agreement->activities()->attach($userId,['comment'=>'Created']);
		});
		SimpleLoanAgreement::deleted(function($agreement){
		    $userId = Auth::id();
		    $agreement->activities()->attach($userId,['comment'=>'Deleted']);
		});
    }

	public function activities(){
		return $this->morphToMany('User', 'trackable', 'activities_track')
					->withPivot('comment')
					->withTimestamps();
	}

	public function currency(){
		return $this->belongsTo('Currency');
	}

	public function getAmount(){
		return $this->currency->name.' '.$this->amount;
	}

	public function payments(){
		return $this->hasMany('SimpleLoanPayment', 'simple_loan_code', 'loan_code');
	}

	public function amountRemaining(){
		$payed_amount = $this->payments->sum('amount');
		$amount_remaining = $this->amount - $payed_amount;
		return $amount_remaining;
	}
}