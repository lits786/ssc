<?php

use Illuminate\Database\Eloquent\SoftDeletingTrait;

class CapacityBuilding extends Eloquent{
	use SoftDeletingTrait;
	
	protected $table = 'capacity_building';
	protected $dates = ['date'];

	public static function boot(){
        parent::boot();
        CapacityBuilding::updated(function($capacity){
		    $userId = Auth::id();
		    $capacity->activities()->attach($userId,['comment'=>'Edited']);
		});
		CapacityBuilding::created(function($capacity){
		    $userId = Auth::id();
		    $capacity->activities()->attach($userId,['comment'=>'Created']);
		});
		CapacityBuilding::deleted(function($capacity){
		    $userId = Auth::id();
		    $capacity->activities()->attach($userId,['comment'=>'Deleted']);
		});

		CapacityBuilding::deleting(function($capacity){
		    $images = $capacity->images;
		    foreach ($images as $image) {
		    	$image->delete();
		    }
		});
    }

	public function activities(){
		return $this->morphToMany('User', 'trackable', 'activities_track')
					->withPivot('comment')
					->withTimestamps();
	}

	

	public function file(){
		return $this->belongsTo('SSCFile');
	}

	public function images(){
		return $this->hasMany('CapacityBuildingImage');
	}
}