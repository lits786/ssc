<?php



class LoanGuarantee extends Eloquent{

	protected $table = 'loan_guarantee';

	protected $dates = ['date_of_loan_agreement'];

	public static function boot(){
        parent::boot();
        LoanGuarantee::updated(function($guarantee){
		    $userId = Auth::id();
		    $guarantee->activities()->attach($userId,['comment'=>'Edited']);
		});
		LoanGuarantee::created(function($guarantee){
		    $userId = Auth::id();
		    $guarantee->activities()->attach($userId,['comment'=>'Created']);
		});
		LoanGuarantee::deleted(function($guarantee){
		    $userId = Auth::id();
		    $guarantee->activities()->attach($userId,['comment'=>'Deleted']);
		});
    }

	public function activities(){
		return $this->morphToMany('User', 'trackable', 'activities_track');
	}

}