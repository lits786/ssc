<?php

use Illuminate\Database\Eloquent\SoftDeletingTrait;

class SimpleLoanPayment extends Eloquent{
	use SoftDeletingTrait;
	
	protected $dates = ['deadline'];

	public static function boot(){
        parent::boot();
        SimpleLoanPayment::updated(function($payment){
		    $userId = Auth::id();
		    $payment->activities()->attach($userId,['comment'=>'Edited']);
		});
		SimpleLoanPayment::created(function($payment){
		    $userId = Auth::id();
		    $payment->activities()->attach($userId,['comment'=>'Created']);
		});
		SimpleLoanPayment::deleted(function($payment){
		    $userId = Auth::id();
		    $payment->activities()->attach($userId,['comment'=>'Deleted']);
		});
    }

	public function activities(){
		return $this->morphToMany('User', 'trackable', 'activities_track')
					->withPivot('comment')
					->withTimestamps();
	}

	public function simpleLoanAgreement(){
		return $this->belongsTo('SimpleLoanAgreement','simple_loan_code', 'loan_code');
	}
}