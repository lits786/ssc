<?php

use Illuminate\Database\Eloquent\SoftDeletingTrait;

class Will extends Eloquent{
	use SoftDeletingTrait;
	
	public $timestamps = false;
	protected $table = 'wills';

	public static function boot()
    {
        parent::boot();

        Will::deleting(function($will){
		    File::delete($will->path);
		});
    }

    public function activities(){
		return $this->morphToMany('User', 'trackable', 'activities_track')
					->withPivot('comment')
					->withTimestamps();
	}



}