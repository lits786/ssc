<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;
	use SoftDeletingTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password', 'remember_token');

	
	public function assignedFiles(){
		return $this->hasMany('SSCFile', 'user_type_assigned', 'user_type');	
	}

	
	public function type(){
		return $this->belongsTo('UserType', 'user_type');
	}

	public function interviewActivities()
    {
        return $this->morphedByMany('Interview', 'trackable');
    }

	
}
