<?php 

class Residence extends Eloquent{
	protected $table = 'residence';


	public function residenceType(){
		return $this->belongsTo('ResidenceType');
	}

	public function residenceOwnershipType(){
		return $this->belongsTo('ResidenceOwnershipType');
	}	
}