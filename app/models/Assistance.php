<?php
class Assistance extends Eloquent{
	protected $table = 'assistances';

	public function summary(){
		$output = '';

		if($this->rent){
			$output .= 'Rent: '.$this->rent.'<br/>';
		}
		if($this->school_fees){
			$output .= 'School fees: '.$this->school_fees.'<br/>';
		}
		if($this->medical){
			$output .= 'Medical: '.$this->medical.'<br/>';
		}
		if($this->electricity){
			$output .= 'Electricity: '.$this->electricity.'<br/>';
		}
		if($this->water){
			$output .= 'Water: '.$this->water.'<br/>';
		}
		if($this->others){
			$output .= 'Others: '.$this->others.'<br/>';
		}
		if($this->others){
			$output .= 'Ramadhan: '.$this->ramadhan.'<br/>';
		}
		if($this->others){
			$output .= 'Monthly allocate: '.$this->monthly_allocate.'<br/>';
		}

		return $output;
	}
}