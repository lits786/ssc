<?php

class Person extends Eloquent{
	protected $dates = ['date_of_birth', 'passport_expiry_date'];
	
	public function residence(){
		return $this->hasOne('Residence','person_id');
	}

	public function address(){
		return $this->hasOne('Address','person_id');
	}

	public function income(){
		return $this->hasOne('Income', 'person_id');
	}

	public function spouse(){
		return $this->hasOne('Spouse', 'person_id');
	}

	public function assistance(){
		return $this->hasOne('Assistance', 'person_id');
	}

	public function children(){
		return $this->hasMany('Child', 'person_id');
	}
	
	public function other(){
		return $this->hasMany('Other', 'person_id');
	}
}