<?php

class Other extends Eloquent{
	protected $dates = ['date_of_birth'];

	public function shortSummary(){
		return $this->full_name.', Born: '
					.$this->date_of_birth->format('d/m/Y')
					.', '.$this->occupation
					.', '.$this->gender();
	}

	public function gender(){
		if($this->gender=='F'){
			return "Female";
		}
		return 'Male';
	}
	
	public function relationship(){
		return $this->belongsTo('Relationship');
	}
}