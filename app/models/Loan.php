<?php
class Loan extends Eloquent{
	protected $dates = ['deadline'];

	public function payments(){
		return $this->hasMany('LoanPayment');
	}

	public function currency(){
		return $this->belongsTo('Currency');
	}
	
	public function amountRemaining(){
		$amount_paid = $this->payments()->sum('amount');
		$amount_remaining = $this->amount - $amount_paid;
		return $amount_remaining;
	}
}