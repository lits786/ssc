<?php

class CapacityBuildingImage extends Eloquent{
	public $timestamps = false;

	public static function boot(){
        parent::boot();

        CapacityBuildingImage::deleting(function($image){
		    File::delete($image->path);
		});
    }
}