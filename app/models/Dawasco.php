<?php

use Illuminate\Database\Eloquent\SoftDeletingTrait;

class Dawasco extends Eloquent{
	use SoftDeletingTrait;
	
	protected $table = 'dawasco';
	protected $dates = ['date'];

	public static function boot(){
        parent::boot();
        Dawasco::updated(function($dawasco){
		    $userId = Auth::id();
		    $dawasco->activities()->attach($userId,['comment'=>'Edited']);
		});
		Dawasco::created(function($dawasco){
		    $userId = Auth::id();
		    $dawasco->activities()->attach($userId,['comment'=>'Created']);
		});
		Dawasco::deleted(function($dawasco){
		    $userId = Auth::id();
		    $dawasco->activities()->attach($userId,['comment'=>'Deleted']);
		});
    }

	public function activities(){
		return $this->morphToMany('User', 'trackable', 'activities_track')
					->withPivot('comment')
					->withTimestamps();
	}

	public function files(){
		return $this->hasMany('DawascoFile');
	}

	public function currency(){
		return $this->belongsTo('Currency');
	}

	public function percentage(){
		$ta = $this->total_amount != 0 ? $this->total_amount : 1;
		$percentage = $this->ssc_contribution *100 / $ta;
		return number_format($percentage,2).'%';
	}

	public function selfPercentage(){
		$ta = $this->total_amount != 0 ? $this->total_amount : 1;
		$percentage = $this->self_contribution *100 / $ta;
		return $percentage =  number_format($percentage,2).'%';
	}

	public function totalAmount(){
		return $this->currency->name.''.$this->total_amount;
	}

	public function selfContribution(){
		if(!$this->self_contribution){
			return 0;
		}
		return $this->currency->name.$this->self_contribution;
	}

	public function sscContribution(){
		if(!$this->ssc_contribution){
			return 0;
		}
		return $this->currency->name.$this->ssc_contribution;
	}
}