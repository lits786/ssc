<?php

use Illuminate\Database\Eloquent\SoftDeletingTrait;

class SchoolAssistance extends Eloquent{
	use SoftDeletingTrait;
	
	protected $table = 'school_assistances';
	protected $dates = ['date_of_birth'];

	public static function boot(){
        parent::boot();
        SchoolAssistance::updated(function($assistance){
		    $userId = Auth::id();
		    $assistance->activities()->attach($userId,['comment'=>'Edited']);
		});
		SchoolAssistance::created(function($assistance){
		    $userId = Auth::id();
		    $assistance->activities()->attach($userId,['comment'=>'Created']);
		});
		SchoolAssistance::deleted(function($assistance){
		    $userId = Auth::id();
		    $assistance->activities()->attach($userId,['comment'=>'Deleted']);
		});
    }

	public function activities(){
		return $this->morphToMany('User', 'trackable', 'activities_track')
					->withPivot('comment')
					->withTimestamps();
	}

	public function setDateOfBirthAttribute($value){
		$this->attributes['date_of_birth'] = date_create_from_format('d/m/Y', $value);
	}

	public function setDateAttribute($value){
		$this->attributes['date'] = date_create_from_format('d/m/Y', $value);
	}

	public function school(){
		return $this->belongsTo('School');
	}

	public function currency(){
		return $this->belongsTo('Currency');
	}

	public function percentage(){
		$percentage = $this->ssc_contribution *100 / $this->total_amount;
		return number_format($percentage,2).'%';
	}

	public function selfPercentage(){
		$percentage = $this->self_contribution *100 / $this->total_amount;
		return $percentage =  number_format($percentage,2).'%';
	}

	public function totalAmount(){
		return $this->currency->name.' '.$this->total_amount;
	}

	public function selfContribution(){
		if(!$this->self_contribution){
			return 0;
		}
		return $this->currency->name.' '.$this->self_contribution;
	}
	public function sscContribution(){
		if(!$this->ssc_contribution){
			return 0;
		}
		return $this->currency->name.' '.$this->ssc_contribution;
	}
}