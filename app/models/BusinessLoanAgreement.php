<?php

use Illuminate\Database\Eloquent\SoftDeletingTrait;
use Carbon\Carbon;

class BusinessLoanAgreement extends Eloquent{
	use SoftDeletingTrait;
	
	protected $table = 'business_loan_agreements';
	public $timestamps = false;
	protected $dates = ['deadline'];

	public static function boot(){
        parent::boot();
        BusinessLoanAgreement::updated(function($agreement){
		    $userId = Auth::id();
		    $agreement->activities()->attach($userId,['comment'=>'Edited']);
		});
		BusinessLoanAgreement::created(function($agreement){
		    $userId = Auth::id();
		    $agreement->activities()->attach($userId,['comment'=>'Created']);
		});
		BusinessLoanAgreement::deleted(function($agreement){
		    $userId = Auth::id();
		    $agreement->activities()->attach($userId,['comment'=>'Deleted']);
		});
    }

	public function activities(){
		return $this->morphToMany('User', 'trackable', 'activities_track')
					->withPivot('comment')
					->withTimestamps();
	}

	public function currency(){
		return $this->belongsTo('Currency');
	}

	// public function getAmountAttribute($amount){
	// 	return $this->currency->name.' '.number_format($amount);
	// }

	public function getAmount(){
		return $this->currency->name.' '.number_format($this->amount);
	}
	
	public function payments(){
		return $this->hasMany('BusinessLoanPayment', 'business_loan_code', 'loan_code');
	}

	public function attachments()
	{
		return $this->hasMany('BusinessLoanAttachment', 'business_loan_agreement_id', 'id');
	}

	public function amountRemaining(){
		$payed_amount = $this->payments->sum('amount');
		$amount_remaining = $this->amount - $payed_amount;
		return $amount_remaining;
	}

	public function getAmountRemaining(){
		$payed_amount = $this->payments->sum('amount');
		$amount_remaining = $this->amount - $payed_amount;
		return $this->currency->name.' '.number_format($amount_remaining);
	}

	public function getDateAttribute($date)
	{
		return Carbon::parse($date)->format('d/m/Y');
	}
}