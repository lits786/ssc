<?php

use Illuminate\Database\Eloquent\SoftDeletingTrait;

class BusinessLoanAttachment extends Eloquent{
	use SoftDeletingTrait;
	
	protected $table = 'business_loan_attachments';
	public $timestamps = false;
	protected $dates = ['deadline'];
 
	public function files()
	{
		return $this->hasMany('BusinessLoanAttachmentFile', 'business_loan_attachment_id', 'id');
	}
}