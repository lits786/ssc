<?php



class LoanPayment extends Eloquent{

	public static function boot(){
        parent::boot();
        LoanPayment::updated(function($payment){
		    $userId = Auth::id();
		    $payment->activities()->attach($userId,['comment'=>'Edited']);
		});
		LoanPayment::created(function($payment){
		    $userId = Auth::id();
		    $payment->activities()->attach($userId,['comment'=>'Created']);
		});
		LoanPayment::deleted(function($payment){
		    $userId = Auth::id();
		    $payment->activities()->attach($userId,['comment'=>'Deleted']);
		});
    }

	public function activities(){
		return $this->morphToMany('User', 'trackable', 'activities_track');
	}

}