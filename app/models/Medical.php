<?php

use Illuminate\Database\Eloquent\SoftDeletingTrait;

class Medical extends Eloquent{
	use SoftDeletingTrait;
	
	protected $table = 'medicals';
	protected $dates = ['date'];

	public static function boot(){
        parent::boot();
        Medical::updated(function($medical){
		    $userId = Auth::id();
		    $medical->activities()->attach($userId,['comment'=>'Edited']);
		});
		Medical::created(function($medical){
		    $userId = Auth::id();
		    $medical->activities()->attach($userId,['comment'=>'Created']);
		});
		Medical::deleted(function($medical){
		    $userId = Auth::id();
		    $medical->activities()->attach($userId,['comment'=>'Deleted']);
		});
    }

	public function activities(){
		return $this->morphToMany('User', 'trackable', 'activities_track')
					->withPivot('comment')
					->withTimestamps();
	}

	public function files(){
		return $this->hasMany('MedicalFile');
	}

	public function currency(){
		return $this->belongsTo('Currency');
	}

	public function percentage(){
		if($this->total_amount>0)
			$percentage = $this->ssc_contribution *100 / $this->total_amount;
		else {
			$percentage = 0;
		}
		return number_format($percentage,2).'%';
	}

	public function selfPercentage(){ 
		if($this->total_amount>0)
			$percentage = $this->self_contribution *100 / $this->total_amount;
		else {
			$percentage = 0;
		}
		return $percentage =  number_format($percentage,2).'%';
	}

	public function totalAmount(){
		return $this->currency->name.' '.$this->total_amount;
	}

	public function selfContribution(){
		return $this->currency->name.' '.$this->self_contribution;
	}

	public function sscContribution(){
		return $this->currency->name.' '.$this->ssc_contribution;
	}
}