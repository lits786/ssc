@extends('layouts.main')
@section('content')
<div class="content-header">
    <div class="leftside-content-header">
        <ul class="breadcrumbs">
            <li><i class="fa fa-home" aria-hidden="true"></i><a href="#">Change Password</a></li>
        </ul>
    </div>
</div>
<div class="row animated fadeInUp">
    <div class="col-sm-12 col-lg-12">
        <div class="row">
            <div class="col-sm-12 col-md-12">
                {{-- <h4 class="section-subtitle">Interview</h4> --}}
                <div class="panel">
                    <div class="panel-content">
                        <div class="row">
                            <div class="col-md-12">
                                <form id="user-form" data-id="0" enctype="multipart/form-data">
                                            
                                    <div class="row form-group">
                                        <div class="col-sm-4">
                                            <label for="w2-username" class="control-label">Old Password<span class="required">*</span></label>
                                            <input type="password" class="form-control" id="old_password" name="old_password" required>
                                        </div>
                                        <div class="col-sm-4">
                                            <label for="w2-username" class="control-label">New Password<span class="required">*</span></label>
                                            <input type="password" class="form-control" id="new_password" name="new_password" required>
                                        </div>
                                        <div class="col-sm-4">
                                            <label for="w2-username" class="control-label">Confirm Password<span class="required">*</span></label>
                                            <input type="password" class="form-control" id="con_password" name="con_password" required>
                                        </div>
                                    </div>                                              
                                    <div class="row form-group ">
                                        <div class="col-sm-12">
                                            <button type="submit" class="btn btn-primary">Submit</button>
                                        </div>
                                    </div>
                                </form>                               
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('page-scripts')
<script src="/vendor/twitter-bootstrap-wizard/jquery.bootstrap.wizard.js"></script>
<script src="/vendor/jquery-validation/jquery.validate.min.js"></script>
<script src="/javascripts/examples/forms/wizard.js"></script>
<script type="text/javascript">
    jQuery('.datepicker').datepicker({format: "d/m/yyyy",});
</script>
<script type="text/javascript">
    var user_form = $('#user-form');
   
    user_form.on('submit', function( event ) {
        if(Validate()){
            event.preventDefault();
            var form = $(this);
            var formData = new FormData(this);
            $.ajax({
                url: '/new-password/save',
                type: 'POST',
                dataType: 'json',
                data: formData,
                cache: false,
                contentType: false,
                processData: false
            })
            .done(function(re) {

                if(re == 1)
                {
                    toastr.warning('Your current password does not matches with the password you provided. Please try again.');
                }
                else if(re == 2)
                {
                    toastr.warning('New Password cannot be same as your current password. Please choose a different password.');
                }
                else if(re == 3)
                {
                    toastr.success('Password changed successfully!');
                    successNotification();
                }  

                clearUserForm();
            })
            .fail(function() {
                console.log("error");
            });
        }
        
    });
    function clearUserForm(){
        user_form.trigger('reset');
    }

    function Validate() {
        var password = $('#new_password').val();
        var confirmPassword = $('#con_password').val();
        if (password != confirmPassword) {
            toastr.error("Passwords do not match.");
            return false;
        }
        return true;
    }
</script>
@endsection