@extends('layouts.main')



@section('content')

<div class="row animated fadeInUp">
    <div class="col-sm-12 col-lg-12">
        <div class="row">
            <div class="col-sm-12 col-md-12">
                {{-- <h4 class="section-subtitle">Interview</h4> --}}
                <div class="panel">
                    <div class="panel-content">
                        <table class="table table-hover table-striped view" id="all-interviews-table">
                        	<thead>
                        		{{-- <tr>
                        			<th>FILE NO.</th>
                        			<th>NAME OF APPLICANT</th>
                        		</tr> --}}
                        	</thead>

                        	<tbody>
                            <tr>
                                  <td>Date</td>
                                  <td>{{$transport->date->format('d M Y')}}</td>
                            </tr>
                            <tr>
                                  <td>Amount</td>
                                  <td>{{$transport->totalAmount()}}</td>
                            </tr>
                            <tr>
                                  <td>Self Contribution</td>
                                  <td>{{$transport->selfContribution()}}</td>
                            </tr>
                            <tr>
                                  <td>Remarks</td>
                                  <td>{{$transport->remarks}}</td>
                            </tr>
                            
                            <tr>
                              @if(count($files))
                                <td>Files</td>
                                <td>
                                  @foreach($files as $file)
                                    <a href="{{url('/'.$file->path)}}">File</a><br/>
                                  @endforeach
                                </td>
                              @endif
                            </tr>
                        	</tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection