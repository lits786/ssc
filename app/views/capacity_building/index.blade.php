@extends('layouts.main')
@section('content')
<div class="content-header">
    <div class="leftside-content-header">
        <ul class="breadcrumbs">
            <li><i class="fa fa-home" aria-hidden="true"></i><a href="#">Capacity building</a></li>
        </ul>
    </div>
</div>
<div class="row animated fadeInUp">
    <div class="col-sm-12 col-lg-12">
        <div class="row">
            <div class="col-sm-12 col-md-12">
                {{-- <h4 class="section-subtitle">Interview</h4> --}}
                <div class="panel">
                    <div class="panel-content">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="tabs">
                                    <ul class="nav nav-tabs">
                                        <li class="active"><a href="#add-capacity" data-toggle="tab">Add</a></li>
                                        <li><a href="#all-capacities" data-toggle="tab">All</a></li>
                                    </ul>
                                    <div class="tab-content">
                                        <div class="tab-pane fade in active" id="add-capacity">
                                            <form id="capacity-building-form" data-id="{{$capacity_id}}" enctype="multipart/form-data">
                                            	<div class="row form-group">
                                            		<div class="col-md-12">
					                                    <div class="dropzone" id="dropzone-images"></div>
					                                </div>
                                            	</div>
                                            	@if($capacity)
                                            	<div class="row form-group">
                                            		@foreach($capacity->images as $image)
                                            		<div class="col-md-4" class="image-box-1">
                                            			<img src="{{asset('/'.$image->path)}}" style="width:100%;" />
                                                        <a class="btn btn-primary delete-image" href="{{url('/capacity-building-image/delete/'.$image->id)}}">delete</a>
                                            		</div>
                                            		@endforeach
                                            	</div>
                                            	@endif
                                                
                                                <div class="row form-group">                       
                                                    <div class="col-sm-6">
                                                        <label for="">Title</label>
			                                            <input type="text" class="form-control" name="title" @if($capacity)value="{{$capacity->title}}"@endif>
                                                    </div>   
                                                    <div class="col-sm-6">
                                                        <label class="control-label">Date<span class="required">*</span></label>
                                                        <div class="input-group">
                                                            <span class="input-group-addon x-primary"><i class="fa fa-calendar"></i></span>
                                                            <input type="text" class="form-control datepicker" name="date" @if($capacity)value="{{$capacity->date->format('d/m/Y')}}"@endif>
                                                        </div>
                                                    </div>                                                 
                                                </div>

                                                <div class="row form-group">
                                                	<div class="col-sm-12">
                                                		<label for="">Description</label>
			                                            <textarea class="form-control" rows="3"  name="description">@if($capacity){{$capacity->description}}@endif</textarea>
                                                	</div>
                                                </div>
                                                                                              
                                                <div class="row form-group ">
                                                    <div class="col-sm-12">
                                                        <button type="submit" class="btn btn-primary">Submit</button>
                                                        <button type="reset" class="btn btn-primary">Cancel</button>
                                                    </div>
                                                </div>
                                                <div class="progress">
                                                    <progress id="progressBar" value="0" max="100" style="width:300px;"></progress>
                                                    <h3 id="status"></h3>
                                                    <p id="loaded_n_total"></p>
                                                </div> 
                                            </form>
                                        </div>
                                        
                                        <div class="tab-pane fade" id="all-capacities">
                                            <table class="table table-hover table-striped" id="all-capacities-table">
                                                <thead>
                                                    <tr>
                                                        <th>NAME</th>
                                                        <th>Description</th>
                                                        <th>ACTION</Sth>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('page-scripts')
<script src="{{asset('/vendor/twitter-bootstrap-wizard/jquery.bootstrap.wizard.js')}}"></script>
<script src="{{asset('/vendor/jquery-validation/jquery.validate.min.js')}}"></script>
<script src="{{asset('/javascripts/examples/forms/wizard.js')}}"></script>
<script type="text/javascript">
    jQuery('.datepicker').datepicker({format: "d/m/yyyy",});
</script>
<script type="text/javascript">
	var capacity_id = 0;
	var dropzone = new Dropzone("div#dropzone-images",{ 
            init: function() {
                this.on('maxfilesreached', function(file) {
                    //toastr.warning('Maximum number of allowed files reached', 'WARNING')
                });
                this.on("sending", function(file, xhr, formData) {
                  formData.append("capacity-id", capacity_id);
                });
                this.on("queuecomplete", function(file) {
                    this.removeAllFiles();
                    window.location.href="{{url('/capacity-building/form')}}";
                });

            },
            paramName: "image", 
            acceptedFiles: "image/*",
            dictDefaultMessage: 'upload images',
            maxFiles: 10,
            autoProcessQueue: false,
            parallelUploads: 15,
            addRemoveLinks: true,
            url: '{{url('/capacity-building/image/save')}}'
        }
    );

    var capacity_building = $('#capacity-building-form');
   
    capacity_building.on('submit', function( event ) {
        event.preventDefault();
        var form = $(this);
        var formData = new FormData(this);
        formData.append('id', form.data('id'));
        $.ajax({
            xhr: function() {
                var xhr = new window.XMLHttpRequest();
                xhr.upload.addEventListener("progress", progressHandler, false);
                xhr.addEventListener("load", completeHandler, false);
                xhr.addEventListener("error", errorHandler, false);
                xhr.addEventListener("abort", abortHandler, false);

                return xhr;
            },
            url: '{{url('/capacity-building/save')}}',
            type: 'POST',
            dataType: 'json',
            data: formData,
            cache: false,
            contentType: false,
            processData: false
        })
        .done(function(feedback) {
        	capacity_id = feedback.capacity_id;
            if(dropzone.getQueuedFiles().length){
    			dropzone.processQueue();
    		}else{
    			window.location.href="{{url('/capacity-building/form')}}";
    		}
            successNotification();
            clearCapacityBuildingFrom();
            all_capacities_table.draw(false);
        })
        .fail(function() {
            console.log("error");
        });
        function _(el) {
            return document.getElementById(el);
        }

        function progressHandler(event) {
            _("loaded_n_total").innerHTML = "Uploaded " + event.loaded + " bytes of " + event.total;
            var percent = (event.loaded / event.total) * 100;
            _("progressBar").value = Math.round(percent);
            _("status").innerHTML = Math.round(percent) + "% uploaded... please wait";
        }

        function completeHandler(event) {
            _("status").innerHTML = event.target.responseText;
            _("progressBar").value = 0;
        }

        function errorHandler(event) {
            _("status").innerHTML = "Upload Failed";
        }

        function abortHandler(event) {
            _("status").innerHTML = "Upload Aborted";
        }
    });

    $('button[type=reset]').click(function(event) {
        event.preventDefault();
        window.location.href = '{{url('/capacity-building/form')}}';
    });

    function clearCapacityBuildingFrom(){
        capacity_building.trigger('reset');
        capacity_building.data('id', 0);
    }

    $('.delete-image').click(function(event) {
        event.preventDefault();
        var link = $(this);
        var href = link.attr('href');

        $.ajax({
            url: href,
            type: 'POST',
            dataType: 'json',
            data: {},
        })
        .done(function() {
            console.log("success");
            link.closest('div').fadeOut();
        })
        .fail(function() {
            console.log("error");
        });
        
    });
</script>
<script type="text/javascript">
    var all_capacities_table =  $('#all-capacities-table');
    all_capacities_table = all_capacities_table.DataTable({ 
        "scrollX": true,               
        "processing": true,
        "serverSide": true,
        "ajax": "{{url('/capacity-building/all-capacities-table')}}",
        "pageLength":25,
        "ordering":false,
        "bAutoWidth": false,
        "oSearch": {"sSearch": ''},
        "columns": [
            {data: 'title', name: 'title'},
            {data: 'description', name: 'description'},
            {data: 'actions', name:'actions', orderable: false, searchable: false}

        ]
    });
    all_capacities_table.on('draw', function(event) {
        all_capacities_table.rows().every( function ( rowIdx, tableLoop, rowLoop ) {
            $(this.node()).find('.delete').on('click', function(event) {
                event.preventDefault(); 
                var link = $(this);                  
                var id = link.data('id');
                $.ajax({
                    url: "{{url('/capacity-building/delete/',"+id")}}",
                    type: 'POST',
                    dataType: 'json',
                    data: {},
                })
                .done(function() {
                    all_capacities_table.draw(false);
                })
                .fail(function() {
                    console.log("error");
                });
                
            });

            $(this.node()).find('.history').on('click', function(event) {
              event.preventDefault(); 
              var link = $(this);
              showHistory(link);                            
            });
        });
    });
</script>
@endsection