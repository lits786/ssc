@extends('layouts.main')



@section('content')

<div class="row animated fadeInUp">
    <div class="col-sm-12 col-lg-12">
        <div class="row">
            <div class="col-sm-12 col-md-12">
                {{-- <h4 class="section-subtitle">Interview</h4> --}}
                <div class="panel">
                    <div class="panel-content">
                        <table class="table table-hover table-striped view" id="all-interviews-table">
                        	<thead>
                        		{{-- <tr>
                        			<th>FILE NO.</th>
                        			<th>NAME OF APPLICANT</th>
                        		</tr> --}}
                        	</thead>

                        	<tbody>
                            <tr>
                              <td>File</td>
                              <td>{{$capacity->file->file_no}}</td>
                            </tr>
                            <tr>
                              <td>Title</td>
                              <td>{{$capacity->title}}</td>
                            </tr>
                            <tr>
                              <td>Date</td>
                              <td>{{$capacity->date->format('jS M Y')}}</td>
                            </tr>
                            <tr>
                              <td>Description</td>
                              <td>{{$capacity->description}}</td>
                            </tr>

                        	</tbody>
                        </table>
                        <div class="row form-group">
                          @foreach($capacity->images as $image)
                            <div class="col-md-4">
                              <img src="{{asset('/'.$image->path)}}" />
                            </div>
                          @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection