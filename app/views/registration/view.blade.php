@extends('layouts.main')



@section('content')

<div class="row animated fadeInUp">
    <div class="col-sm-12 col-lg-12">
        <div class="row">
            <div class="col-sm-12 col-md-12">
                {{-- <h4 class="section-subtitle">Interview</h4> --}}
                <div class="panel">
                    <div class="panel-content">
                        <table class="table table-hover table-striped view" id="all-interviews-table">
                        	<thead>
                        		{{-- <tr>
                        			<th>FILE NO.</th>
                        			<th>NAME OF APPLICANT</th>
                        		</tr> --}}
                        	</thead>
                        	<tbody>
                        		<tr>
                        			<td>FILE NO.</td>
                        			<td>{{$file->file_no}}</td>
                        		</tr>
                        		<tr>
                        			<td>Full Name</td>
                        			<td>{{$person->full_name}}</td>
                        		</tr>

                        		<tr>
                        			<td>Date of birth</td>
                        			<td>{{$person->date_of_birth->format('d/M/Y')}}</td>
                        		</tr>
                        		<tr>
                        			<td>Head of family</td>
                        			<td>{{$person->head_of_family}}</td>
                        		</tr>
                        		<tr>
                        			<td>Education level</td>
                        			<td>{{$person->education_level}}</td>
                        		</tr>
                        		<tr>
                        			<td>Residence phone number</td>
                        			<td>{{$address->residence_phone_number}}</td>
                        		</tr>
                        		<tr>
                        			<td>Office phone number</td>
                        			<td>{{$address->office_phone_number}}</td>
                        		</tr>
                        		<tr>
                        			<td>Mobile phone number</td>
                        			<td>{{$address->mobile_phone_number}}</td>
                        		</tr>
                        		<tr>
                        			<td>Postal address</td>
                        			<td>{{$address->postal_address}}</td>
                        		</tr>
                        		<tr>
                        			<td>Physical address</td>
                        			<td>{{$address->physical_address}}</td>
                        		</tr>
                        		<tr>
                        			<td>Email</td>
                        			<td>{{$address->emaiml}}</td>
                        		</tr>
                        		<tr>
                        			<td>Plot number</td>
                        			<td>{{$address->plot_number}}</td>
                        		</tr>
                        		<tr>
                        			<td>Block number</td>
                        			<td>{{$address->block_number}}</td>
                        		</tr>
                        		<tr>
                        			<td>Floor number</td>
                        			<td>{{$address->floor_number}}</td>
                        		</tr>
                        		<tr>
                        			<td>Street</td>
                        			<td>{{$address->street}}</td>
                        		</tr>
                        		<tr>
                        			<td>Nearest landmark</td>
                        			<td>{{$address->nearest_landmark}}</td>
                        		</tr>
                        		<tr>
                        			<td>Residence type</td>
                        			<td>{{$residence->residenceType->name}}</td>
                        		</tr>
                        		<tr>
                        			<td>Residence ownership</td>
                        			<td>{{$residence->residenceOwnershipType->name}}</td>
                        		</tr>
                        		<tr>
                        			<td>Owner name</td>
                        			<td>{{$residence->owner_name}}</td>
                        		</tr>
                        		<tr>
                        			<td>Rent per month</td>
                        			<td>{{$residence->rent_per_month}}</td>
                        		</tr>
                        		<tr>
                        			<td>Jamaat membership no.</td>
                        			<td>{{$residence->jamaat_membership_no}}</td>
                        		</tr>
                        		<tr>
                        			<td>Income type</td>
                        			<td>{{$income->incomeType->name}}</td>
                        		</tr>
                        		<tr>
                        			<td>Business name</td>
                        			<td>{{$income->businessName}}</td>
                        		</tr>
                        		<tr>
                        			<td>Location</td>
                        			<td>{{$income->location}}</td>
                        		</tr>
                        		<tr>
                        			<td>Business type</td>
                        			<td>{{$income->businessType}}</td>
                        		</tr>
                        		<tr>
                        			<td>Income per month</td>
                        			<td>{{$income->income_per_month}}</td>
                        		</tr>
                        		<tr>
                        			<td>Employers name</td>
                        			<td>{{$income->employers_name}}</td>
                        		</tr>
                        		<tr>
                        			<td>Ofice employed</td>
                        			<td>{{$income->office_employed}}</td>
                        		</tr>
                        		<tr>
                        			<td>Position employed</td>
                        			<td>{{$income->position_employed}}</td>
                        		</tr>
                        		<tr>
                        			<td>Salary per month</td>
                        			<td>{{$income->salary_per_month}}</td>
                        		</tr>
                        		<tr>
                        			<td>Spouse</td>
                        			<td>{{$spouse->full_name}}</td>
                        		</tr>
                        		<tr>
                        			<td>Spouse's date of birth</td>
                        			<td>{{$spouse->date_of_birth->format('d/M/Y')}}</td>
                        		</tr>
                        		<tr>
                        			<td>Spouse's occupation</td>
                        			<td>{{$spouse->occupation}}</td>
                        		</tr>

                        		@foreach($children as $child)
                        		<tr>
                        			<td>Child</td>
                        			<td>{{$child->shortSummary()}}</td>
                        		</tr>
                        		@endforeach


                        		<tr>
                        			<td>Assistance</td>
                        			<td>@if($assistance){{$assistance->summary()}}@endif</td>
                        		</tr>

                        		<tr>
                        			<td>Remarks by Member</td>
                        			<td>{{nl2br($remarks->by_member)}}</td>
                        		</tr>
                        		<tr>
                        			<td>Remarks by head of department</td>
                        			<td>{{nl2br($remarks->by_head_of_department)}}</td>
                        		</tr>
                        		<tr>
                        			<td>Remarks by chairman of welfare</td>
                        			<td>{{nl2br($remarks->by_chairman_of_welare)}}</td>
                        		</tr>



                        	</tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection