@extends('layouts.main')



@section('content')
<div class="content-header">
    <div class="leftside-content-header">
        <ul class="breadcrumbs">
            <li>
                <i class="fa fa-home" aria-hidden="true"></i>
                <a href="#">All registrations
                </a>
            </li>
        </ul>
    </div>
</div>

<div class="row animated fadeInUp">
    <div class="col-sm-12 col-lg-12">
        <div class="row">
            <div class="col-sm-12 col-md-12">
                {{-- <h4 class="section-subtitle">Interview</h4> --}}
                <div class="panel">
                    <div class="panel-content">
                        <table class="table table-hover table-striped" id="all-registrations-table">
                        	<thead>
                        		<tr>
                        			<th>FILE NO.</th>
                        			<th>NAME OF APPLICANT</th>
                        			<th>ACTIONS</th>
                        		</tr>
                        	</thead>
                        	<tbody>
                        		
                        	</tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection




@section('page-scripts')
<script type="text/javascript">
	var all_registrations_table =  $('#all-registrations-table');
    all_registrations_table = all_registrations_table.DataTable({                
        "processing": true,
        "serverSide": true,
        "ajax": "{{url('/registration/all-registered-data-table')}}",
        "pageLength":25,
        "ordering":false,
        "oSearch": {"sSearch": ''},
        "columns": [
            {data: 'file_no', name: 'file_no'},
            {data: 'full_name', name: 'people.full_name'},
            
            {data: 'actions', name: 'actions',orderable: false, searchable: false},
            // {data: 'search_slug', name: "search_slug", visible: false, searchable:true}
        ]
    });
</script>
@endsection