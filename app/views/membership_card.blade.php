<!DOCTYPE html>
<html >
<head>
  <meta charset="UTF-8">
  <title>Digital Business Card</title>
  
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">
  
  <link rel="stylesheet" href="paper.css">
  
  <link rel="stylesheet" href="card.css">
  <style>@page { size: A6 landscape }</style>
</head>

<body>
  <span id="click-me">Click to flip!</span>
<div class="A6 landscape container">

  <div id="card-front" class="card card-front frontpic sheet"> 
    
    <div class="" style="text-align:center; font-weight: 400; color: #fff; margin-top: 75px; font-size: 30px;"><b>SOCIAL SERVICES COMMITTEE</b></div>
    <div class="desc" style="font-weight: 150; margin-top:75px; color:#fff;"><b>MEMBERSHIP ID NO: </b></div>
    <div class="twitter-front" style="">
      <div id="twitter-front" class="left">Date of Issue: {{date('d-m-Y')}}</div>
      <div id="twitter-front" class="right">Date of Expiry: {{date('d-m-Y',strtotime(date("d-m-Y",time()) . " + 365 day"))}}</div>
    </div>
    <div class="" style="clear:both;">
      <img style="margin-top:30px; vertical-align:middle;" src="images/SSC.png" alt="logo" width="40" height="40" />
      <div class="fif" style="font-weight: 70; font-style: italic; text-align: center; margin-top: 25px; color: #fff; margin-left:70px;">Kindly note: This card should be presented at every visit.</div>
    </div>
    
    
  </div> 

  <div id="card-back" class="card card-back card-flip watermark sheet">
    <div class="initials initials-back" style="margin-top: 60px; color:#fff;"><b>REACHOUT & EMPOWER</b></div>
    <div class="links">
        <div class="desc desc-back">If lost or found please return to SSC office at Sabado Parking 9th Floor or contact 0799786122/3/4</div>
    </div>
  </div>

  <iframe name="print_frame" width="700" height="400" frameborder="0" src="about:blank"></iframe>

</div>
  <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>

    <script src="card.js"></script>

</body>
</html>
