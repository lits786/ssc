<!doctype html>
<html lang="en" class="fixed">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <title>SSC</title>
    <link rel="apple-touch-icon" sizes="120x120" href="{{ asset('favicon/apple-icon-120x120.png')}}">
    <link rel="icon" type="image/png" sizes="192x192" href="{{ asset('favicon/android-icon-192x192.png')}}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('favicon/favicon-32x32.png')}}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('favicon/favicon-16x16.png')}}">
    <script src="{{ asset('vendor/pace/pace.min.js')}}"></script>
    <link href="{{ asset('vendor/pace/pace-theme-minimal.css')}}" rel="stylesheet" />
    <link rel="stylesheet" href="{{ asset('vendor/bootstrap/css/bootstrap.css')}}">
    <link rel="stylesheet" href="{{ asset('vendor/font-awesome/css/font-awesome.css')}}">
    <link rel="stylesheet" href="{{ asset('vendor/animate.css/animate.css')}}">
    <link rel="stylesheet" href="{{ asset('vendor/toastr/toastr.min.css')}}">
    <link rel="stylesheet" href="{{ asset('vendor/magnific-popup/magnific-popup.css')}}">
    <link rel="stylesheet" href="{{ asset('vendor/bootstrap_date-picker/css/bootstrap-datepicker3.min.css')}}">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.13/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.1.1/min/dropzone.min.css" />
    <link rel="stylesheet" href="{{ asset('stylesheets/css/style.css')}}">
    <link rel="stylesheet" href="{{ asset('stylesheets/css/custom.css')}}">

    <style type="text/css">
        .submit-div{
            margin-top: 20px;
        }

        .submit-div button{
            float: right;
        }

        .btn.mr{
            margin-right: 10px;
        }

        .btn.ml{
            margin-left: 10px;
        }

        .row h4{
            margin-left: 15px;
        }
    </style>
</head>
<body>
<?php $user = Auth::user(); ?>
<div class="wrap">
    <div class="page-header">
        <div class="leftside-header">
            <div class="logo">
                <a href="/" class="on-click">
                {{-- <h2 style="font-size: 15px !important;">SSC-@if(Session::get('file_num')){{Session::get('file_num')}}@endif-@if(Session::get('file_assign')){{Session::get('file_assign')}}@endif</h2> --}}
                <h2 style="font-size: 15px !important;">@if(Session::get('file_num')){{'GENERAL FILE: '.Session::get('file_num')}}@endif @if(Session::get('file_assign')){{Session::get('file_assign')}}@endif</h2>
                </a>
            </div>
            <div id="menu-toggle" class="visible-xs toggle-left-sidebar" data-toggle-class="left-sidebar-open" data-target="html">
                <i class="fa fa-bars" aria-label="Toggle sidebar"></i>
            </div>
        </div>

        <div class="rightside-header">
            <div class="header-middle"></div>
            @if($user)            
            <div class="header-section" id="user-headerbox">
                <div class="user-header-wrap">                    
                    <div class="user-info">
                        <span class="user-name">{{$user->username}}</span>
                        <span class="user-profile">{{$user->type->name}}</span>
                    </div>
                </div>
            </div>
            <div class="header-separator"></div>
            <div class="header-section">
                <a href="/logout" data-toggle="tooltip" data-placement="left" title="Logout"><i class="fa fa-sign-out log-out" aria-hidden="true"></i></a>
            </div>
            @endif
        </div>
    </div>
    <div class="page-body">

        @if(Auth::check())
        <div class="left-sidebar">
            <!-- left sidebar HEADER -->
            <div class="left-sidebar-header">
                <div class="left-sidebar-title">Navigation</div>
                <div class="left-sidebar-toggle c-hamburger c-hamburger--htla hidden-xs" data-toggle-class="left-sidebar-collapsed" data-target="html">
                    <span></span>
                </div>
            </div>
            <!-- NAVIGATION -->
            <!-- ========================================================= -->
            <div id="left-nav" class="nano">
                <div class="nano-content">
                    <nav>
                        <ul class="nav" id="main-nav">
                            <!--HOME-->
                            <li class="active-item"><a href="{{url('/')}}"><i class="fa fa-home" aria-hidden="true"></i><span>Dashboard</span></a></li>
                                                        
                            @if($user->type->hasPermission(1))
                            <li class="has-child-item close-item">
                                <a><i class="fa fa-pie-chart" aria-hidden="true"></i><span>Registration</span> </a>
                                <ul class="nav child-nav level-1">
                                    
                                    {{--@if($user->active_file && Request::path() != '/')--}}
                                        <li><a href="{{url('/forms/registration',$user->active_file)}}">Details</a></li>
                                        <!-- <li><a href="/registration/all">All registered</a></li> -->
                                    {{--@elseif(!$user->active_file)--}}
                                        <li><a href="{{url('/forms/registration')}}">New</a></li>
                                    {{--@endif--}}
                                </ul>
                            </li>
                            <li class="has-child-item close-item">
                                <a><i class="fa fa-files-o" aria-hidden="true"></i><span>Subsidy</span></a>
                                <ul class="nav child-nav level-1">  
                                    <li><a href="{{url('/subsidy/students')}}">Add Student</a></li>
                                    <li><a href="{{url('/subsidy/students')}}/list">List of Students</a></li>
                                    <li><a href="{{url('/subsidy/grades')}}">Grades</a></li>
                                </ul>
                            </li>
                            @endif
                            @if($user->active_file && Request::path()!='/')
                            @if($user->type->hasPermission(15))
                            <li class="has-child-item close-item">
                                <a><i class="fa fa-film" aria-hidden="true"></i><span>Vouchers</span> </a>
                                <ul class="nav child-nav level-1">
                                    <li><a href="{{url('/forms/voucher')}}">New</a></li>
                                    <li><a href="{{url('/vouchers/all')}}">All vouchers</a></li>
                                </ul>
                            </li>
                            @endif
                            @endif
                            
				            @if($user->type->hasPermission(13))
                            <li class=""><a href="{{url('forms/visit')}}"><i class="fa fa-eye" aria-hidden="true"></i><span>Visit Form</span></a></li>
                            @endif

                            @if($user->active_file && Request::path()!='/')
                            <li class="">
                                <a href="{{url('/upload')}}"><i class="fa fa-file" aria-hidden="true"></i><span>Upload Files</span> </a>
                            </li>
                            @if($user->type->hasPermission(2))
                            <li class="has-child-item close-item">
                                <a><i class="fa fa-pie-chart" aria-hidden="true"></i><span>Interview</span> </a>
                                <ul class="nav child-nav level-1">
                                    <li><a href="{{url('/forms/interview')}}">New</a></li>
                                    <li><a href="{{url('/interview/all')}}">All interviews</a></li>
                                </ul>
                            </li>
                            @endif

                            @if($user->type->hasPermission(3))
                            <li class="">
                                <a href="{{url('/forms/school-assistance')}}"><i class="fa fa-edit" aria-hidden="true"></i><span>Education</span> </a>
                            </li>
                            @endif

                            @if($user->type->hasPermission(4))
                            <li class="">
                                <a  href="{{url('/forms/medical')}}"><i class="fa fa-eyedropper" aria-hidden="true"></i><span>Medical</span> </a>
                                {{-- <ul class="nav child-nav level-1">
                                    <li><a href="/forms/medical">Local</a></li>
                                    <li><a href="#">Foreign</a></li>
                                    <li><a href="/medical/all">All</a></li>
                                </ul> --}}
                            </li>
                            @endif
				
                            @if($user->type->hasPermission(5))
                            
                            <li class="has-child-item close-item">
                                <a><i class="fa fa-files-o" aria-hidden="true"></i><span>Business Loan</span></a>
                                <ul class="nav child-nav level-1">                                
                                    <li><a href="{{url('/business-loan/loan-agreement')}}">Loan agreement</a></li>
                                    <li><a href="{{url('/business-loan/loan-payments')}}">Loan payments</a></li>
                                </ul>
                            </li> 
                            @endif

                            @if($user->type->hasPermission(6))
                            <li class=""
                            >
                                <a href="{{url('/forms/rent')}}"><i class="fa fa-home" aria-hidden="true"></i><span>Rent</span> </a>
                                <!-- <ul class="nav child-nav level-1">
                                    <li><a href="/forms/rent">New</a></li>
                                    <li><a href="/rent/all">All</a></li>
                                </ul> -->
                            </li>
                            @endif

                            @if($user->type->hasPermission(7))
                            <li class="">
                                <a href="{{url('/forms/dawasco')}}"><i class="fa fa-asterisk" aria-hidden="true"></i><span>Dawasco</span> </a>
                                <!-- <ul class="nav child-nav level-1">
                                    <li><a href="/forms/dawasco">New</a></li>
                                    <li><a href="/dawasco/all">All</a></li>
                                </ul> -->
                            </li>
                            @endif

                            @if($user->type->hasPermission(8))
                            <li class="">
                                <a href="{{url('/forms/luku')}}"><i class="fa fa-institution" aria-hidden="true"></i><span>Luku</span> </a>
                               <!--  <ul class="nav child-nav level-1">
                                    <li><a href="/forms/luku">New</a></li>
                                    <li><a href="/luku/all">All</a></li>
                                </ul> -->
                            </li>
                            @endif
                            @if($user->type->hasPermission(17))
                            <li class="">
                                <a href="{{url('/forms/transport')}}"><i class="fa fa-plane" aria-hidden="true"></i><span>Transport</span> </a>
                               <!--  <ul class="nav child-nav level-1">
                                    <li><a href="/forms/luku">New</a></li>
                                    <li><a href="/luku/all">All</a></li>
                                </ul> -->
                            </li>
                            @endif
                            @if($user->type->hasPermission(18))
                            <li class="">
                                <a href="{{url('/forms/cash-allowance')}}"><i class="fa fa-server" aria-hidden="true"></i><span>Cash Allowance</span> </a>
                               <!--  <ul class="nav child-nav level-1">
                                    <li><a href="/forms/luku">New</a></li>
                                    <li><a href="/luku/all">All</a></li>
                                </ul> -->
                            </li>
                            @endif
                            @if($user->type->hasPermission(19))
                            <li class="">
                                <a href="{{url('/forms/maid-support')}}"><i class="fa fa-archive" aria-hidden="true"></i><span>Maid Support</span> </a>
                               <!--  <ul class="nav child-nav level-1">
                                    <li><a href="/forms/luku">New</a></li>
                                    <li><a href="/luku/all">All</a></li>
                                </ul> -->
                            </li>
                            @endif

                            <li class="">
                                <a  href="{{url('/capacity-building/form')}}"><i class="fa fa-thumb-tack" aria-hidden="true"></i><span>Capacity building</span> </a>
                            </li>

                            <li class="">
                                <a  href="{{url('/will-attachment/form')}}"><i class="fa fa-users" aria-hidden="true"></i><span>Will</span> </a>
                            </li>

                            <li class="has-child-item close-item">
                                <a><i class="fa fa-files-o" aria-hidden="true"></i><span>Simple Loan</span></a>
                                <ul class="nav child-nav level-1">                                
                                    <li><a href="{{url('/simple-loan/loan-agreement')}}">Loan agreement</a></li>
                                    <li><a href="{{url('/simple-loan/loan-payments')}}">Loan payments</a></li>
                                </ul>
                            </li>


                            @endif

                            @if($user->type->hasPermission(9))
                            <li class="has-child-item close-item">
                                <a><i class="fa fa-files-o" aria-hidden="true"></i><span>Reports</span></a>
                                <ul class="nav child-nav level-1">  
                                    <li><a href="{{url('/reports/registrations')}}">Registrations</a></li>                              
                                    <li><a href="{{url('/reports/interviews')}}">Interviews</a></li>
                                    <li><a href="{{url('/reports/education')}}">Education</a></li>
                                    <li><a href="{{url('/reports/medicals')}}">Medicals</a></li>
                                    <li><a href="{{url('/reports/rents')}}">Rents</a></li>
                                    <li><a href="{{url('/reports/dawasco')}}">Dawasco</a></li>
                                    <li><a href="{{url('/reports/luku')}}">Luku</a></li>
                                    <li><a href="{{url('/reports/simple-loans')}}">Simple loans</a></li>
                                    <li><a href="{{url('/reports/loans')}}">Loans</a></li>
                                </ul>
                            </li>
                            @endif                             

                            @if($user->type->hasPermission(10))
                            <li class="has-child-item close-item">
                                <a><i class="fa fa-files-o" aria-hidden="true"></i><span>Settings</span></a>
                                <ul class="nav child-nav level-1">                                
                                    <li><a href="{{url('/users')}}">Users</a></li>
                                    <li><a href="{{url('/security')}}">Security</a></li>
                                </ul>
                            </li>
                            @endif
                            <li class=""><a href="{{url('/new-password')}}"><i class="fa fa-key" aria-hidden="true"></i><span>Change Password</span></a></li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
        @endif
        <div class="content">
            @yield('content')
        </div>
        
        <a href="#" class="scroll-to-top"><i class="fa fa-angle-double-up"></i></a>
    </div>
</div>

<div id="plain-modal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body">
        
      </div>
      {{-- <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div> --}}
    </div>

  </div>
</div>
<script src="{{ asset('vendor/jquery/jquery-1.12.3.min.js')}}"></script>
<script src="{{ asset('vendor/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{ asset('vendor/nano-scroller/nano-scroller.js')}}"></script>
<script src="{{ asset('javascripts/template-script.min.js')}}"></script>
<script src="{{ asset('javascripts/template-init.min.js')}}"></script>
<script src="{{ asset('vendor/toastr/toastr.min.js')}}"></script>
<script src="{{ asset('vendor/bootstrap_date-picker/js/bootstrap-datepicker.min.js')}}"></script>
<script src="{{ asset('vendor/chart-js/chart.min.js')}}"></script>
<script src="{{ asset('vendor/magnific-popup/jquery.magnific-popup.min.js')}}"></script>
<script src="{{ asset('javascripts/printpage.min.js')}}"></script>
<script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.16/datatables.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.1.1/min/dropzone.min.js"></script> 
<script type="text/javascript">
    Dropzone.autoDiscover = false;
 
    function switchTabs(tab1, tab2){
        console.log('switching tabs');
        $(tab2).removeClass('active').removeClass('in');
        $('a[href="'+tab2+'"]').closest('li').removeClass('active');
        $(tab1).addClass('active').addClass('in');
        $('a[href="'+tab1+'"]').closest('li').addClass('active');
    }

    function showHistory(link){
        var href = link.attr('href');
        var modal = $('#plain-modal');
        modal.find()

        $.ajax({
            url: href,
            type: 'GET',
            dataType: 'html',
            data: {},
        })
        .done(function(feedback) {
            console.log('done')
            modal.find('.modal-title').text('File History');
            modal.find('.modal-body').html(feedback);
            modal.modal('show');
        })
        .fail(function() {
            console.log("error");
        });
            
    }
</script>
{{-- <script src="{{ url('javascripts/examples/dashboard.js')}}"></script> --}}
<script src="{{ url('javascripts/bootbox.min.js')}}"></script>

@yield('page-scripts');
<script>
function formatNumber(num) {
  return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
}

function onInputChange(target,span){
    var formattedNumber = formatNumber(target.value);
    if(formattedNumber == '')
    document.getElementById(span).innerHTML = ``; 
    else
    document.getElementById(span).innerHTML = `  (Tsh.${formattedNumber})`; 
}
</script>
</body>
</html>
