@extends('layouts.main')

<style type="text/css">
.user-types .row .name h4{
    margin-left: 0;
    text-align: left;
}    
</style>

@section('content')
<div class="content-header">
    <div class="leftside-content-header">
        <ul class="breadcrumbs">
            <li><i class="fa fa-home" aria-hidden="true"></i><a href="#">Dashboard</a></li>
        </ul>
    </div>
</div>
<div class="row animated fadeInUp">
    <div class="col-sm-12 col-lg-12">
        <div class="row user-types">
            <div class="col-sm-12">
            @if($user->type->name == 'Admin' || $user->type->name == 'Superadmin')
                @foreach($userTypes as $userType)
                @if($userType->name == 'Admin' || $userType->name == 'Superadmin')
                    <div class="" style="width:13%;display:inline-block;">
                        <div class="panel widgetbox wbox-2 bg-darker-2 color-light" style="height:70px;">
                            <a href="{{url('/user/files/')}}/{{$userType->id}}" class="user-files" onclick='event.preventDefault(); checkUserTypeId("{{url('/user/files/')}}/{{$userType->id}}",{{$userType->id}});'>
                                <div class="panel-content">
                                    <div class="row">
                                        <div class="col-xs-8 name">
                                            {{-- <span class="icon fa fa-envelope color-lighter-1"></span> --}}
                                            <h4>{{$userType->name}}</h4>
                                        </div>
                                        {{-- <div class="col-xs-4">
                                            <h4 class="subtitle color-lighter-1">Total Files</h4>
                                            <h1 class="title color-light"> 5</h1>
                                        </div> --}}
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                @else
                    <div class="" style="width:14%;display:inline-block;">
                        <div class="panel widgetbox wbox-2 color-light" style="height:70px;background:{{$userType->color}};">
                            <a href="{{url('/user/files/')}}/{{$userType->id}}" class="user-files" onclick='event.preventDefault(); checkUserTypeId("{{url('/user/files/')}}/{{$userType->id}}",{{$userType->id}});'>
                                <div class="panel-content">
                                    <div class="row">
                                        <div class="col-xs-8 name">
                                            {{-- <span class="icon fa fa-user color-darker-2"></span> --}}
                                            <h4>{{$userType->name}}</h4>
                                        </div>
                                        {{-- <div class="col-xs-4">
                                            <h4 class="subtitle color-darker-2">Total Files</h4>
                                            <h1 class="title color-w"> {{$userType->files->count()}}</h1>
                                        </div> --}}
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                @endif
                @endforeach
            @else
                <div class="" style="width:14%;display:inline-block;">
                    <div class="panel widgetbox wbox-2 color-light" style="height:70px;background:{{$user->type->color}};">
                        <a href="{{url('/user/files/')}}/{{$user->type->id}}" class="user-files" onclick='event.preventDefault(); checkUserTypeId("{{url('/user/files/')}}/{{$user->type->id}}",{{$user->type->id}});'>
                            <div class="panel-content">
                                <div class="row">
                                    <div class="col-xs-8 name">
                                        {{-- <span class="icon fa fa-user color-darker-2"></span> --}}
                                        <h4>{{$user->type->name}}</h4>
                                    </div>
                                    {{-- <div class="col-xs-4">
                                        <h4 class="subtitle color-darker-2">Total Files</h4>
                                        <h1 class="title color-w"> {{$userType->files->count()}}</h1>
                                    </div> --}}
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            @endif                            
            </div>
        </div> 

        @if(Auth::check())
        {{-- <div class="row">
        		<div class="col-sm-12">
        		<a href="#"><i class="fa fa-file-pdf-o fa-2x" aria-hidden="true"></i></a>
        		</div>
        </div> --}}
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-sm-12">
                <table class="table table-hover table-striped" id="files-table">
                    <thead>
                        <tr>
                            <th>GENERAL FILE #</th>
                            <!--<th>FILE #</th>
                            @if(Auth::user()->user_type == 5 or Auth::user()->user_type == 1 or Auth::user()->user_type == 10)
		            <th>FILE OR #</th>
                    @endif-->
                            <th>TYPE</th>
                            <th>MEMBER</th>
                            <th>NEXT INTERVIEW</th>
                            <!--<th>STATUS</th>-->
                            <th>ASSIGNED TO</th>
                            <th>ACTIONS</th>
                        </tr>
                    </thead>
                    <tbody>
                        
                    </tbody>
                </table>                
            </div>
        </div> 
        @endif             
    </div>
</div>

<div class="modal fade" id="login-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-body">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel" style="margin-left:40px;">Login to your Account</h4>
                </div>
                <div class="modal-body">
                    <div class="panel-body" style="padding: 40px;">
                        <form id="login-form" name="" class="nobottommargin" action="/login" method="post">
                            <div class="col_full">
                                <label for="login-form-username">Username</label>
                                <input type="text" name="username" value="" class="form-control not-dark" required/>
                            </div>
                            <div class="col_full">
                                <label for="login-form-password">Password:</label>
                                <input type="password" name="password" value="" class="form-control not-dark" required/>
                            </div>
                            <div class="col_full nobottommargin">
                                <button type="submit" class="button button-3d button-black nomargin"  value="login">Login</button>
                                {{-- <a href="#" class="fright">Forgot Password?</a>
                                <a href="/register" class="fright" style="margin-right:30px;">Register</a> --}}
                            </div>
                        </form>
                        <div class="line line-sm"></div>
                    </div>
                </div>
                {{-- <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Save changes</button>
                </div> --}}
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="move-file-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-body">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" style="margin-left:40px;">Move File</h4>
                </div>
                <div class="modal-body">
                    <div class="panel-body" style="padding: 40px;">
                        <form id="move-file-form" name="" class="nobottommargin" action="/login" method="post">
                            <div class="col_full">
                                <label for="login-form-username" class="control-label">Move File to</label>
                                <select name="user-type-assigned" class="form-control" required>
                                    @foreach($userTypes as $userType)
                                        <option value="{{$userType->id}}">{{$userType->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col_full nobottommargin">
                                <button type="submit" class="button button-3d button-black nomargin">Confirm</button>
                            </div>
                        </form>
                        <div class="line line-sm"></div>
                    </div>
                </div>
                {{-- <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Save changes</button>
                </div> --}}
            </div>
        </div>
    </div>
</div>
@endsection


@section('page-scripts')



@if(!Auth::check())
<script type="text/javascript">
    var loginModal = $('#login-modal');
    var loginForm = $('#login-form');
    var linkToFiles = '';

    $('.user-files').click(function(event) {
        event.preventDefault();
        var link = $(this);
        linkToFiles = link.attr('href');
        loginModal.modal('show');
    });

    loginForm.submit(function(event) {
        event.preventDefault();
        var formData = new FormData(this);
        formData.append('linkToFiles', linkToFiles);

        $.ajax({
            url: '/login',
            type: 'POST',
            dataType: 'json',
            data: formData,
            cache: false,
            processData: false,
            contentType: false
        })
        .done(function(feedback) {
            if(feedback.status==1){
                window.location.href=feedback.linkToFiles;
            }
            else 
            {
             toastr.error('not accessible!');
            }
            
        })
        .fail(function() {
            console.log("error");
        });
        
    });


</script>
@else
<script type="text/javascript">
	var loginModal = $('#login-modal');
	var loginForm = $('#login-form');
	var linkToFiles = '';
	loginForm.submit(function(event) {
        event.preventDefault();
        var formData = new FormData(this);
        formData.append('linkToFiles', linkToFiles);

        $.ajax({
            url: '/login',
            type: 'POST',
            dataType: 'json',
            data: formData,
            cache: false,
            processData: false,
            contentType: false
        })
        .done(function(feedback) {
            if(feedback.status==1){
                window.location.href=feedback.linkToFiles;
            }
            else
            {
            toastr.error('not accessible!');
            }
            console.log("success");
        })
        .fail(function() {
            console.log("error");
        });
        
    });
	function checkUserTypeId(linkToFile,userTypeId)
	{
		linkToFiles = linkToFile ;
		
		$.ajax({
	            url: "{{url('/users/checkUserTypeId')}}",
	            type: 'GET',
	            dataType: 'json',
	            
	            data: {
	            'userTypeId' : userTypeId,
	            'linkToFiles' : linkToFile 
	            }
	        })
	        .done(function(feedback) {
	            if(feedback.status==1){
	              window.location.href=linkToFiles ; 
	            
	            
	            }
	            else
	            {
	             
	            	loginModal.modal('show');
	            }
	            
	        })
	        .fail(function() {
	           
	        });
	}
	
    console.log('drawing the table');
    var files_table =  $('#files-table');
    var moveFileModal = $('#move-file-modal');                

    files_table = files_table.DataTable({   
        "scrollX": true,             
        "processing": true,
        "serverSide": true,
        "ajax": "{{url('/user/files-datatable/')}}/{{$userTypeId}}",
        "pageLength":25,
        "ordering":true,
        "bAutoWidth": false,
        "oSearch": {"sSearch": ''},
        "columns": [
            {data: 'file_no', name: 'file_no'}, 
            {data: 'file_type', name: 'file_type'}, 
           // {data: 'user_type_file_no', name: 'user_type_file_no'}, 
            @if(Auth::user()->user_type == 5 or Auth::user()->user_type == 1 or Auth::user()->user_type == 10)
           // {data: 'user_type_or_no', name: 'user_type_or_no'},
            @endif
            {data: 'person', name: 'people.full_name'}, 
            {data: 'next_date', name:'interviews.next_interview', orderable:false, searchable: false},
         //   {data: 'concludedStatus', name: 'concludedStatus',orderable: false, searchable: false},
            {data: 'assigned_to', name: 'assigned_to',orderable: false, searchable: false},
            {data: 'actions', name: 'actions',orderable: false, searchable: false},
            {data: 'date_of_birth', name: 'date_of_birth', visible: false},
            {data: 'mobile_phone_number', name: 'mobile_phone_number', visible: false}
        ]
    });


    files_table.on('draw', function(event) {
        files_table.rows().every( function ( rowIdx, tableLoop, rowLoop ) {
            $(this.node()).find('.delete').on('click', function(event) {
                event.preventDefault(); 
                var link = $(this);                  
                var id = link.data('id');

                $.ajax({
                    url: '/agreements/destroy',
                    type: 'POST',
                    dataType: 'json',
                    data: {id: id},
                })
                .done(function() {
                    files_table.draw(false);
                })
                .fail(function() {
                    console.log("error");
                });
                
            });

            $(this.node()).find('.close-file').on('click', function(event) {
                event.preventDefault(); 
                var link = $(this);                  
                var url = link.attr('href');
		 bootbox.confirm({
		        message: "Are You Sure?",
		        buttons: {
		            confirm: {
		                label: 'Ok',
		                className: 'btn-success'
		            },
		            cancel: {
		                label: 'Cancel',
		                className: 'btn-danger'
		            }
		        },
		        callback: function (result) {
		        	if(result)
		        	{
		        		
			                $.ajax({
			                    url: url,
			                    type: 'POST',
			                    dataType: 'json',
			                    data: {},
			                })
			                .done(function() {
			                    files_table.draw(false);
			                    successNotification();
			                })
			                .fail(function() {
			                    console.log("error");
			                });
		        	}   
		        }
		    });
                
            });
            
            $(this.node()).find('.open-file').on('click', function(event) {
                event.preventDefault(); 
                var link = $(this);                  
                var url = link.attr('href');
		
		
		    bootbox.confirm({
		        message: "Are You Sure?",
		        buttons: {
		            confirm: {
		                label: 'Ok',
		                className: 'btn-success'
		            },
		            cancel: {
		                label: 'Cancel',
		                className: 'btn-danger'
		            }
		        },
		        callback: function (result) {
		        	if(result)
		        	{
		        		
			                $.ajax({
			                    url: url,
			                    type: 'POST',
			                    dataType: 'json',
			                    data: {},
			                })
			                .done(function() {
			                    files_table.draw(false);
			                    successNotification();
			                })
			                .fail(function() {
			                    console.log("error");
			                });
		        	}   
		        }
		    });
               
                
            });

            $(this.node()).find('.move').on('click', function(event) {
                event.preventDefault();                 
                var link = $(this);                  
                var user_assigned = link.data('user_type');
                console.log(user_assigned);
                $('select[name=user-type-assigned]').val(user_assigned);
                $('#move-file-form').data('file_id',link.data('file_id'));

                moveFileModal.modal('show');
                console.log(user_assigned);

                                
            });

            $(this.node()).find('.history').on('click', function(event) {
              event.preventDefault(); 
              var link = $(this);
              showHistory(link);                            
            });


        });
    });

    $('#move-file-form').submit(function(event) {
        event.preventDefault();
        var form = $(this);
        var formData = new FormData(this);
        formData.append('file-id', form.data('file_id'));

        $.ajax({
            url: '/ssc-file/move',
            type: 'POST',
            dataType: 'json',
            data: formData,
            cache: false,
            contentType: false,
            processData: false
        })
        .done(function() {
            files_table.draw(false);
            moveFileModal.modal('hide');
            successNotification();
            console.log("success");
        })
        .fail(function() {
            console.log("error");
        });
        
    });
</script>
@endif


@endsection