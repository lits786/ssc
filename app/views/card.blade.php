<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <title>Multiple Sheets</title>

  <!-- Normalize or reset CSS with your favorite library -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/7.0.0/normalize.min.css">

  <!-- Load paper.css for happy printing -->
  <link rel="stylesheet" href="paper.css">
  <link rel="stylesheet" href="card.css">

  <!-- Set page size here: A5, A4 or A3 -->
  <!-- Set also "landscape" if you need -->
  <style>@page { size: card landscape }</style>
</head>

<!-- Set "A5", "A4" or "A3" for class name -->
<!-- Set also "landscape" if you need -->
<body class="card landscape">

  <!-- Each sheet element should have the class "sheet" -->
  <!-- "padding-**mm" is optional: you can set 10, 15, 20 or 25 -->
  <section class="sheet padding-10mm card-front frontpic" 
  style="background:url(../images/cardback.jpg) no-repeat;
  -webkit-background:url(../images/cardback.jpg) no-repeat;
  background-position: center;
  background-size: cover;
  top: 0;
  left: 0;
  bottom: 0;
  right: 0;">

    <div class="" style="text-align:center; font-weight: 400; color: #fff; margin-top: -6px; font-size: 16px;"><b>SOCIAL SERVICES COMMITTEE</b></div>
    <div class="desc" style="font-weight: 150; margin-top:25px; color:#fff;"><b>MEMBERSHIP ID NO: </b></div>
    <div class="twitter-front" style="">
        <div id="twitter-front" class="left">Date of Issue: {{date('d-m-Y')}}</div>
        <div id="twitter-front" class="right">Date of Expiry: {{date('d-m-Y',strtotime(date("d-m-Y",time()) . " + 365 day"))}}</div>
    </div>
    <div class="">
        <img style="margin-top:30px; margin-left:0px; padding-left:0px; vertical-align:middle;" src="images/SSC.png" alt="logo" width="40" height="40" />
        <div class="fif" style="font-weight: 70; font-style: italic; text-align: center; margin-top: 25px; color: #fff; margin-left:45px;">Kindly note: This card should be presented at every visit.</div>
    </div>

  </section>

  <!-- The second sheet -->
  <section class="sheet padding-10mm card-back" style="background:url(../images/cardback.jpg) no-repeat;
  -webkit-background:url(../images/cardback.jpg) no-repeat;
  background-position: center;
  background-size: cover;
  top: 0;
  left: 0;
  bottom: 0;
  right: 0;">
    
    <div class="initials initials-back" style="margin-top: 5px; color:#fff;"><b>REACHOUT & EMPOWER</b></div>
    <div class="links">
        <div class="desc desc-back">If lost or found please return to SSC office at Sabado Parking 9th Floor or contact 0799786122/3/4</div>
    </div>

  </section>

</body>

</html>
