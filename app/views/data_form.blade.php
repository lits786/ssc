<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	<style type="text/css">
	
		h2{
			color:#181818;
			font-family:Helvetica, Arial, sans-serif;
			font-size:20px;
			line-height: 20px;
			font-weight: normal;
		}
		p{
			color:#555;
			font-family:Helvetica, Arial, sans-serif;
			font-size:12px;
			line-height:160%;
		}
		
		#table-data {
			border-collapse: collapse;
			width: 100%;
		}

		#table-data th, #table-data td {
			text-align: left;
			padding: 8px;
		}

		#table-data tr:nth-child(even){background-color: #f2f2f2}

		#table-data th {
			background-color: #4CAF50;
			color: white;
		}
	</style>

</head>
<body>
    
    <h1 style="text-align: center; font-size: 23px; line-height: 23px;">SOCIAL SERVICE COMMITTEE OF KSIJ - DSM</h1>
    <h2 style="text-align: center;">{{$student->level}} / INSTALLMENT PLAN 2018</h2>
    <h2 style="text-align: center;">{{$student->student_name}} - {{$student->grade->name}}</h2>
    <h2 style="text-align: center;">STUDENT NO.: {{$student->student_no}}</h2>
    <h2 style="text-align: center;">S.NO.: {{$student->serial_no}}</h2>    
    <br /><hr> <br />
    <div class='movableContent'>
        <table id="table-data" class="table table-bordered" style="width: 100%;">
            <tr>
                <td>TOTAL FEES</td>
                <td>{{$student->actual_fees}}</td>
            </tr>
            <tr>
                <td>SUBSIDY</td>
                <td>{{$student->quarter}}</td>
            </tr>
            <tr>
                <td>BALANCE</td>
                <td>{{$student->total_fees}}</td>
            </tr>

            <tr><td></td><td></td></tr>

            <tr>
                <td>NO. OF INSTALLMENTS</td>
                <td>{{count($installments)}}</td>
            </tr>
            <?php $tot = 0; ?>
            @foreach($installments as $installment)
            <tr>
                <td>{{$installment->name}} - {{$installment->date}}</td>
                <td>{{$installment->amount}}</td>
            </tr>  
            <?php $tot = $tot + $installment->amount; ?>
            @endforeach          
            <tr>
                <td><b>TOTAL</b></td>
                <td><b>{{$tot}}</b></td>
            </tr>

        </table>
    </div>

    <p>
        <b>@if($student->new_student == 'yes'){{'Please note: Addmission fees are payable with the first installment.'}}@endif</b>
    </p>
    <p>
        <b>@if($student->exam_fees == 'yes'){{'Pleast note : Exam fees are payable with the first installment.'}}@endif</b>
    </p><br />

</body>
</html>
