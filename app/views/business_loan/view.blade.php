@extends('layouts.main')



@section('content') 
<?php
  //dd($agreement->attachments);
?>
<div class="row animated fadeInUp">
    <div class="col-sm-12 col-lg-12">
        <div class="row">
            <div class="col-sm-12 col-md-12">
                {{-- <h4 class="section-subtitle">Interview</h4> --}}
                <div class="panel">
                    <div class="panel-content">
                        <table class="table table-hover table-striped view" id="all-interviews-table">
                        	<thead>
                        		{{-- <tr>
                        			<th>FILE NO.</th>
                        			<th>NAME OF APPLICANT</th>
                        		</tr> --}}
                        	</thead>

                        	<tbody>
                            <tr>
                                  <td>Name</td>
                                  <td>{{$agreement->name}}</td>
                            </tr>
                            <tr>
                                  <td>Loan Code</td>
                                  <td>{{$agreement->loan_code}}</td>
                            </tr>
                            <tr>
                                  <td>Amount</td>
                                  <td>{{number_format($agreement->getAmount())}}</td>
                            </tr>
                            <tr>
                                  <td>Amount remaining</td>
                                  <td>{{$agreement->getAmountRemaining()}}</td>
                            </tr>
                            <tr>
                                  <td>Agreement date</td>
                                  <td>{{$agreement->date}}</td>
                            </tr>
                            <tr>
                                  <td>Deadline</td>
                                  <td>{{$agreement->deadline->format('d/m/Y')}}</td>
                            </tr>
                            <tr>
                              <td>ATTACHMENTS</td>
                              <td></td>
                            </tr>
                            @foreach ($agreement->attachments as $item)
                                <tr>
                                  <td>{{$item->description}}</td>
                                  <td>
                                    @foreach ($item->files as $file)
                                        <a href="{{url($file->file_url)}}">{{basename($file->file_url)}}</a><br>
                                    @endforeach
                                  </td>
                                </tr>
                            @endforeach
                        	</tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection