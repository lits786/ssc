@extends('layouts.main')



@section('content')

<div class="content-header">

    <div class="leftside-content-header">

        <ul class="breadcrumbs">

            <li><i class="fa fa-home" aria-hidden="true"></i><a href="#">Loan Application</a></li>

        </ul>

    </div>

</div>

<div class="row animated fadeInUp">

    <div class="col-sm-12 col-lg-12">

        <div class="row">
            <div class="col-sm-12 col-md-12">
                {{-- <h4 class="section-subtitle">Interview</h4> --}}
                <div class="panel">
                    <div class="panel-content">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="tabs">
                                    <ul class="nav nav-tabs">
                                        <li class="active"><a href="#new-application" data-toggle="tab">New
                                                Application</a></li>
                                        <li><a href="#all-applications" data-toggle="tab">All Applications</a></li>
                                    </ul>
                                    <div class="tab-content">
                                        <div class="tab-pane fade in active" id="new-application">
                                            <div class="form-wizard wizard-basic wizard-block wizard-list-tabs">
                                                <form id="loan-application-form" data-id='{{$id}}'>
                                                    <div class="tab-steps" style="display:none; ">
                                                        <ul>
                                                            <li class="active"><a href="#w2-tab1"
                                                                    data-toggle="tab">Personal</a></li>
                                                           

                                                        </ul>
                                                    </div>
                                                    <div class="tab-content">
                                                        <div class="tab-pane active" id="w2-tab1">
                                                            <div class="row" id="-info">
                                                                <div class="col-sm-3">
                                                                    <label class="control-label">Code<span
                                                                            class="required">*</span></label>
                                                                     <input type="text" class="form-control">
                                                                </div>
                                                                <div class="col-sm-3">
                                                                    <label class="control-label">Name<span
                                                                            class="required">*</span></label>
                                                                     <input type="text" class="form-control">
                                                                </div>
                                                                <div class="col-sm-3">
                                                                    <label class="control-label">Type of business<span
                                                                            class="required">*</span></label>
                                                                     <input type="text" class="form-control">
                                                                </div>
                                                                <div class="col-sm-3">
                                                                    <label class="control-label">Attach<span
                                                                            class="required">*</span></label>
                                                                     <input type="file" class="form-control" multiple>
                                                                </div>
                                                            </div>  
                                                            <div class="row" style="margin-top:15px;">
                                                                <div class="col-sm-12">
                                                                    <a class="btn btn-primary pull-right" id="add-child"> + </a>
                                                                </div>                                                                
                                                            </div>                                                            
                                                        </div> 
                                                    </div>
                                                    <div class="tab-buttons row">
                                                        {{-- <a class="btn btn-primary previous">Previous</a>
                                                        <a class="btn btn-primary next">Next</a> --}}
                                                        <button class="btn btn-primary finish">Submit</button>
                                                        <a class="btn btn-primary mr" href="{{url('/forms/business-loan-application')}}"
                                                            style="float:right;">Cancel</a>
                                                    </div>
                                                    <div class="progress">
                                                        <progress id="progressBar" value="0" max="100"
                                                            style="width:300px;"></progress>
                                                        <h3 id="status"></h3>
                                                        <p id="loaded_n_total"></p>
                                                    </div>
                                                </form>

                                            </div>
                                        </div>
                                        <div class="tab-pane fade" id="all-applications">
                                            <table class="table table-hover table-striped" id="all-applications-table">
                                                <thead>
                                                    <tr>
                                                        <th>Ref. No</th>
                                                        <th>ACTIONS</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('page-scripts')

<script src="{{asset('/vendor/twitter-bootstrap-wizard/jquery.bootstrap.wizard.js')}}"></script>
<script src="{{asset('/vendor/jquery-validation/jquery.validate.min.js')}}"></script>
<script src="{{asset('/javascripts/examples/forms/wizard.js')}}"></script>

<script type="text/javascript">
    jQuery('.datepicker').datepicker({
        format: "d/m/yyyy",
    });
</script>

<script type="text/javascript">
    // Submit registration form
    var application_form = $('#loan-application-form');
    //Submit form button
    var application_form_submit = $('#loan-application-form .tab-buttons .finish');
    //Validate
    var application_form_validate = buildValidation(application_form);
    application_form.bootstrapWizard({
        tabClass: 'steps',
        nextSelector: '.tab-buttons .next',
        previousSelector: '.tab-buttons .previous',
        onInit: function (activeTab, navigation) {
            this.totalTabs = navigation.children('li').length;
        },
        onNext: function () {
            //return validTabInfo(application_form,application_form_validate);
            return true;
        },
        onTabClick: function (activeTab, navigation, actualIndex, clickedIndex) {
            return onTabClick(clickedIndex, actualIndex, this);
        },
        onTabChange: function (activeTab, navigation, previousIndex, actualIndex) {
            inLastTab(actualIndex, this.totalTabs, application_form_submit);
        },
        onTabShow: function (activeTab, navigation, actualIndex) {
            updateValidateTabs(activeTab);
        }
    });
    //On Submit
    application_form.on('submit', function (event) {
        event.preventDefault();
        var form = $(this);
        var formData = new FormData(this);
        formData.append('id', form.data('id'));
        console.log(formData.get('id'));

        $.ajax({
                xhr: function () {
                    var xhr = new window.XMLHttpRequest();
                    xhr.upload.addEventListener("progress", progressHandler, false);
                    xhr.addEventListener("load", completeHandler, false);
                    xhr.addEventListener("error", errorHandler, false);
                    xhr.addEventListener("abort", abortHandler, false);

                    return xhr;
                },
                url: '{{url(' / loan - application / save ')}}',
                type: 'POST',
                dataType: 'json',
                data: formData,
                cache: false,
                contentType: false,
                processData: false
            })
            .done(function () {
                console.log("success");
                successNotification();
                // application_form[0].reset();
                application_form.bootstrapWizard('first');
            })
            .fail(function () {
                console.log("error");
            });

        function _(el) {
            return document.getElementById(el);
        }

        function progressHandler(event) {
            _("loaded_n_total").innerHTML = "Uploaded " + event.loaded + " bytes of " + event.total;
            var percent = (event.loaded / event.total) * 100;
            _("progressBar").value = Math.round(percent);
            _("status").innerHTML = Math.round(percent) + "% uploaded... please wait";
        }

        function completeHandler(event) {
            _("status").innerHTML = event.target.responseText;
            _("progressBar").value = 0;
        }

        function errorHandler(event) {
            _("status").innerHTML = "Upload Failed";
        }

        function abortHandler(event) {
            _("status").innerHTML = "Upload Aborted";
        }




        //if ( application_form.valid() ) {
        // successNotification();
        // application_form[0].reset();
        // application_form.bootstrapWizard('first');
        //}
    });
</script>


<script type="text/javascript">
    var all_applications_table = $('#all-applications-table');
    all_applications_table = all_applications_table.DataTable({
        "scrollX": true,
        "processing": true,
        "serverSide": true,
        "ajax": "{{url('/loan-application/all-applications-table')}}",
        "pageLength": 25,
        "ordering": false,
        "bAutoWidth": false,
        "oSearch": {
            "sSearch": ''
        },
        "columns": [{
                data: 'loan_id',
                name: 'loan_id'
            },
            // {data: 'currency', name: 'currency'},
            // {data: 'amount', name: 'amount'},
            // {data: 'remarks', name: 'remarks'},            
            {
                data: 'actions',
                name: 'actions',
                orderable: false,
                searchable: false
            }
        ]
    });


    all_applications_table.on('draw', function (event) {
        all_applications_table.rows().every(function (rowIdx, tableLoop, rowLoop) {
            $(this.node()).find('.delete').on('click', function (event) {
                event.preventDefault();
                var link = $(this);
                var id = link.data('id');

                $.ajax({
                        url: '{{url(' / dawasco / destroy ')}}',
                        type: 'POST',
                        dataType: 'json',
                        data: {
                            id: id
                        },
                    })
                    .done(function () {
                        all_applications_table.draw(false);
                    })
                    .fail(function () {
                        console.log("error");
                    });

            });


        });
        // all_applications_table.row.add({
        //         'date': 1,
        //         'amount': 2,
        //         'currency': 3,
        //         'remarks':4
        //     }).draw( false );
    });  
    var formIndex = 1;
    $('#add-form').click(function(event) {
        event.preventDefault();
        $.ajax({
            url: '{{url('/registration/child-sub-form')}}',
            type: 'GET',
            dataType: 'html',
            data: {child_index: childIndex},
        })
        .done(function(feedback) {
            $('#children-info').append(feedback);
            childIndex = childIndex + 1;
        })
        .fail(function() {
            console.log("error");
        });
        
    });
</script>
@endsection