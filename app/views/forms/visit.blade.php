@extends('layouts.main')



@section('content') 
<div class="content-header">

  <div class="leftside-content-header">

    <ul class="breadcrumbs">

      <li><i class="fa fa-home" aria-hidden="true"></i><a href="#">Visit Form {{Session::get('file-number', '')}}</a></li>

    </ul>

  </div>

</div>

<div class="row animated fadeInUp">

  <div class="col-sm-12 col-lg-12">

    <div class="row">
      <div class="col-sm-12 col-md-12">
        {{-- <h4 class="section-subtitle">Interview</h4> --}}
        <div class="panel">
            <div class="panel-content">
                <div class="tabs">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#add-visit" data-toggle="tab">Add Visit</a></li>
                       
                        <li><a href="#all-visit" data-toggle="tab">All Visit</a></li>
                    </ul>
                     <div class="tab-content">
                        <div class="tab-pane fade in active" id="add-visit">
                            <form id="visit-form"  enctype="multipart/form-data" data-id="{{($visit!=null)?$visit->id:0}}"> 
                	             <div class="row form-group">
                                            
                                            <div class="col-sm-6">
                                                <label  class="control-label">Date</label>
                                                <div class="input-group">
                                                    <span class="input-group-addon x-primary"><i class="fa fa-calendar"></i></span>
                                                    <input type="text" class="form-control datepicker"  name="date" value="{{($visit!=null)?$visit->date:''}}">
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <label  class="control-label">Full Name</label>
                                                <input type="text" class="form-control" name="full-name" value="{{($visit!=null)?$visit->name:''}}" >
                                            </div>
                                            <div class="col-sm-12">
                                                <label for="">Purpose of Visit</label>
                                                <textarea class="form-control" rows="3"  placeholder="Write a purpose" name="purpose" value="" >{{($visit!=null)?$visit->purpose:''}}</textarea>
                                            </div>
                                            <div class="col-sm-12">
                                                <label for="">Visited by comments</label>
                                                <textarea class="form-control" rows="3"  placeholder="Write a comments" name="comment" value="" >{{($visit!=null)?$visit->comment:''}}</textarea>
                                            </div>
                                            <div class="col-sm-12">
                                                <label for="">Action taken by commitee</label>
                                                <textarea class="form-control" rows="3"  placeholder="Write a commitee action" name="action" value="" >{{($visit!=null)?$visit->action:''}}</textarea>
                                            </div>
                                        </div>
                              
                                <div class="row form-group ">
                                    <div class="col-sm-12">
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        
                        <div class="tab-pane fade" id="all-visit">
                             <table class="table table-hover table-striped" id="all-visit-table">
		                                <thead>
		                                    <tr>
		                                        <th>DATE</th>
		                                        <th>NAME</th>
		                                        <th>PURPOSE OF VISIT</th>
		                                        <th>COMMITEE COMMENT</th>
		                                        <th>ACTIONS</th>
		                                    </tr>
		                                </thead>
		                                <tbody>
		                                    
		                                </tbody>
		                            </table>
                            
                        </div>
                    </div>
                </div>
          </div>
        </div>
      </div>

    </div>

  </div>

</div>

@endsection

@section('page-scripts')

<script src="{{asset('/vendor/twitter-bootstrap-wizard/jquery.bootstrap.wizard.js')}}"></script>
<script src="{{asset('/vendor/jquery-validation/jquery.validate.min.js')}}"></script>
<script src="{{asset('/javascripts/examples/forms/wizard.js')}}"></script>

<script type="text/javascript">
    jQuery('.datepicker').datepicker({format: "d/m/yyyy",});
</script>


<script type="text/javascript">
    $('#visit-form').submit(function(event) {
        event.preventDefault();
        var form = $(this);
        var formData = new FormData(this);
        console.log(form.data('id'));
        formData.append('id', form.data('id'));

        $.ajax({
            url: '{{url('/visit/save')}}',
            type: 'POST',
            dataType: 'json',
            data: formData,
            cache: false,
            processData: false,
            contentType: false
        })
        .done(function(feedback) {
            resetLukuForm();
            toastr.success('success', 'STATUS');
            window.location.href='{{url('/forms/visit')}}';
            console.log("success");
        })
        .fail(function() {
            console.log("error");
        });
        

    });

    function resetLukuForm(){
        var form = $('#visit-form');
        form.trigger('reset');
        form.find('input').val('');
        form.data('id',0);
    }
</script>

<script type="text/javascript">
    var all_visit_table =  $('#all-visit-table');
    all_visit_table = all_visit_table.DataTable({  
        "scrollX": false,              
        "processing": true,
        "serverSide": true,
        "ajax": "{{url('/visit/all-visit-data-table')}}",
        "pageLength":25,
        "ordering":false,
        "bAutoWidth":false,
        "oSearch": {"sSearch": ''},
        "columns": [
            {data: 'date', name: 'date'},
            {data: 'name', name: 'name'},
            {data: 'purpose', name: 'purpose'},
            {data: 'comment', name: 'comment'},
            {data: 'action', name: 'action'},
            
        ]
    });


    all_visit_table.on('draw', function(event) {
        all_visit_table.rows().every( function ( rowIdx, tableLoop, rowLoop ) {
            $(this.node()).find('.delete').on('click', function(event) {
                event.preventDefault(); 
                var link = $(this);                  
                var id = link.data('id');

                $.ajax({
                    url: '{{url('/visit/destroy')}}',
                    type: 'POST',
                    dataType: 'json',
                    data: {id: id},
                })
                .done(function() {
                    all_visit_table.draw(false);
                })
                .fail(function() {
                    console.log("error");
                });
                
            });

            $(this.node()).find('.history').on('click', function(event) {
              event.preventDefault(); 
              var link = $(this);
              showHistory(link);                            
            });


        });
        // all_luku_table.row.add({
        //         'date': 1,
        //         'amount': 2,
        //         'currency': 3,
        //         'remarks':4
        //     }).draw( false );
    });
</script>



@endsection