@extends('layouts.main')



@section('content')

<div class="content-header">

    <div class="leftside-content-header">

        <ul class="breadcrumbs">

            <li>
                <i class="fa fa-home" aria-hidden="true"></i>
                <a href="#">Registration Form 
                @if($file)
                {{$file->file_no}}
                {{$file_as = UserType::where('id',$file->user_type_assigned)->first()->name}} 
                {{-- {{Session::set('file_num', $file->file_no)}} --}}
                {{-- {{Session::set('file_num', $file->user_type_file_no)}}
                {{Session::set('file_assign', $file_as)}}
                {{Session::set('file_assigned_to', $file->user_type_assigned)}}  --}}
                {{Session::set('file_num',$file->file_no)}}
                @endif
                </a>
            </li>

        </ul>

    </div>

</div>

<div class="row animated fadeInUp">

    <div class="col-sm-12 col-lg-12">

        <div class="row">
            <div class="col-sm-12 col-md-12">
                {{-- <h4 class="section-subtitle">Interview</h4> --}}
                <div class="panel">
                    <div class="panel-content">
                        <div class="form-wizard wizard-basic wizard-block wizard-list-tabs">
                            <form  id="registration-form" class="registration-from" data-id="{{$file_id}}">
                                <div class="tab-steps">
                                    <ul>
                                        <li class="active"><a href="#w2-tab1" data-toggle="tab">Personal</a></li>
                                        <li><a href="#w2-tab2" data-toggle="tab">Residence</a></li>
                                        <li><a href="#w2-tab3" data-toggle="tab">Income</a></li>
                                        <li><a href="#w2-tab4" data-toggle="tab">Family</a></li>
                                        <li><a href="#w2-tab5" data-toggle="tab">Others</a></li>
                                        <li><a href="#w2-tab6" data-toggle="tab">Remarks</a></li>

                                    </ul>
                                </div>
                                <div class="tab-content">
                                    <div class="tab-pane active" id="w2-tab1">
                                        @if($user->user_type == 1 || $user->user_type == 10)
                                        <div class="row form-group">
                                            <div class="col-sm-6">
                                              <label class="control-label">Assign to</label>
                                              <select class="form-control" name="user-type-assigned">
                                                @foreach($user_types as $user_type)
                                                  <option value="{{$user_type->id}}" @if($user_type->id == Session::get('file_assigned_to')) selected @endif>{{$user_type->name}}</option>
                                                @endforeach          
                                               </select>
                                            </div>
                                            <div id="outreach-type" class="col-sm-6" style="display:none;">
                                              <label class="control-label">OutReach Type</label>
                                              <select class="form-control" name="outreach-type">
                                                
                                                @foreach($outreach_types as $outreach_type)
                                                  <option value="{{$outreach_type->id}}">{{$outreach_type->name}}</option>
                                                @endforeach          
                                               </select>
                                            </div>
                                            <div class="col-sm-6">
                                                <label  class="control-label">Date</label>
                                                <div class="input-group">
                                                    <span class="input-group-addon x-primary"><i class="fa fa-calendar"></i></span>
                                                    <input type="text" class="form-control datepicker"  name="date" @if($file && $file->date)value="{{$file->date->format('d/m/Y')}}"@endif>
                                                </div>
                                            </div>
                                        </div>
                                        @else
                                        <div class="row form-group">
                                            <div class="col-sm-6">
                                              <label class="control-label">Assign to</label>
                                              <select disabled="disabled" class="form-control" name="user-type-assigned">
                                                
                                                @foreach($user_types as $user_type)
                                                  <option value="{{$user_type->id}}" @if($user_type->id == $user->user_type) selected @endif>{{$user_type->name}}</option>
                                                @endforeach          
                                               </select>
                                            </div>
                                            @if($user->user_type == 5)
                                            
                                            	<div class="col-sm-6">
	                                              <label class="control-label">OutReach Type</label>
                                              <select class="form-control" name="outreach-type">
                                                
                                                @foreach($outreach_types as $outreach_type)
                                                  <option value="{{$outreach_type->id}}">{{$outreach_type->name}}</option>
                                                @endforeach          
                                               </select>
                                            </div>
                                            @endif
                                           
                                        </div>
                                        @endif
					                    <div class="row form-group">
                                            <div class="col-sm-6">
                                                <label for="w2-email" class="control-label">Total Family Members</label>
                                                <input type="text" class="form-control"  name="total-family" @if($file && $file->person) value="{{$file->person->total_family}}" @endif >
                                            </div>
                                            <div class="col-sm-6">
                                                <label for="w2-email" class="control-label">No. of adults</label>
                                                <input type="text" class="form-control"  name="total-adults" @if($file && $file->person) value="{{$file->person->total_adults}}" @endif >
                                            </div>
                                            <div class="col-sm-6">
                                                <label for="w2-email" class="control-label">Total kids(< 18 yrs)</label>
                                                <input type="text" class="form-control"  name="total-kids" @if($file && $file->person) value="{{$file->person->total_kids}}" @endif >
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col-sm-6">
                                                <label  class="control-label">Full Name(Required three names)</label>
                                                <input type="text" class="form-control" name="full-name" @if($file && !is_null($file->person))value="{{$file->person->full_name}}"@endif>
                                            </div>
                                            <div class="col-sm-6">
                                                <label  class="control-label">General File#</label>
                                                <input type="text" class="form-control" name="general-file" @if($file && !is_null($file->file_no))value="{{$file->file_no}}"@endif>
                                            </div>
                                            <div class="col-sm-3">
                                                <label for="exampleInputFile">Attach photograph</label>
                                                <input type="file" class="form-control-file" name='photo' aria-describedby="fileHelp">
                                            </div>
                                            <div class="col-sm-3">
                                                @if($file && $file->person)
                                                <img src="{{asset('/'.$file->person->image)}}" width="128">
                                                @endif
                                            </div>
                                        </div>
                                        
                                        <div class="row form-group">
                                        <div class="col-sm-6">
													<label  class="control-label">Maritual Status</label>
													
															<div class="radio">
															   <label>
                                                                  <input  type="radio"  name="maritual-status" value="Married" @if($file && !is_null($file->person)) @if( $file->person->maritual_status == "Married") checked @endif @endif
                                                                  > Married<br/>
																  <input  type="radio"  name="maritual-status" value="Single"@if($file && !is_null($file->person)) @if( $file->person->maritual_status == "Single") checked @endif @endif> Single<br/>
																  <input  type="radio"  name="maritual-status" value="Divorced" @if($file && !is_null($file->person)) @if( $file->person->maritual_status == "Divorced") checked @endif @endif> Divorced<br/>
																  <input  type="radio"  name="maritual-status" value="Widow"@if($file && !is_null($file->person)) @if( $file->person->maritual_status == "Widow") checked @endif @endif> Widow<br/>
																  <input  type="radio"  name="maritual-status" value="Other" @if($file && !is_null($file->person)) @if( $file->person->maritual_status == "Other") checked @endif @endif> Other<br/>
																  
															   </label>
															</div>
													
													</div>
													<div class="col-sm-6 maritual-status" style="display:none;">
                                                <label  class="control-label">Maritual Status Name</label>
                                                <input type="text" class="form-control"  name="maritual-status-name" @if($file && !is_null($file->person))value="{{$file->person->maritual_status_name}}"@endif>
                                            </div>
                                            <div class="col-sm-6 maritual-status" style="display:none;">
                                                <label  class="control-label">Comment</label>
                                                <textarea class="form-control" rows="3"  placeholder="Write a comment" name="maritual-status-comment" >@if($file && !is_null($file->person)){{$file->person->maritual_status_comment}}@endif</textarea>
                                               
                                            </div>
													
                                        </div>
                                        <div class="row form-group">
                                            <div class="col-sm-6">
                                                <label  class="control-label">Head of your family</label>
                                                <input type="text" class="form-control"  name="head-of-family" @if($file && !is_null($file->person))value="{{$file->person->head_of_family}}"@endif>
                                            </div>
                                            <div class="col-sm-6">
                                                <label  class="control-label">Date of Birth</label>
                                                <div class="input-group">
                                                    <span class="input-group-addon x-primary"><i class="fa fa-calendar"></i></span>
                                                    <input type="text" class="form-control datepicker"  name="date-of-birth" @if($file && !is_null($file->person) )value="{{$file->person->date_of_birth->format('d/m/Y')}}"@endif>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <label  class="control-label">Education Level</label>
                                                <input type="text" class="form-control"  name="education-level" @if($file && !is_null($file->person))value="{{$file->person->education_level}}"@endif>
                                            </div>
                                        </div>

                                        <div class="row form-group">
                                            <div class="col-sm-6">
                                                <label  class="control-label">Passport No.</label>
                                                <input type="text" class="form-control"  name="passport-number" @if($file && !is_null($file->person))value="{{$file->person->passport_number}}"@endif>
                                            </div>
                                            
                                             
                                            <div class="col-sm-6">
                                                <label  class="control-label">Voters id.</label>
                                                <input type="text" class="form-control"  name="voter-id" @if($file && !is_null($file->person) )value="{{$file->person->voter_id}}"@endif>
                                            </div>
                                             
                                            <div class="col-sm-6">
                                                <label  class="control-label">Driver Licence</label>
                                                <input type="text" class="form-control"  name="license-number" @if($file && !is_null($file->person))value="{{$file->person->driver_licence}}"@endif>
                                            </div>
                                            
                                            <div class="col-sm-6">
                                                <label  class="control-label">National id.</label>
                                                <input type="text" class="form-control"  name="national-id" @if($file && !is_null($file->person) )value="{{$file->person->national_id}}"@endif>
                                            </div>
                                            <div class="col-sm-6">
                                                <label  class="control-label">Nationality</label>
                                                <input type="text" class="form-control"  name="nationality" @if($file && !is_null($file->person) )value="{{$file->person->nationality}}"@endif>
                                            </div>
                                            <div class="col-sm-6">
                                                <label  class="control-label">Passport expiry date</label>
                                                <div class="input-group">
                                                    <span class="input-group-addon x-primary"><i class="fa fa-calendar"></i></span>
                                                    <input type="text" class="form-control datepicker"  name="passport-expiry-date" @if($file && !is_null($file->person) )value="{{$file->person->passport_expiry_date->format('d/m/Y')}}"@endif>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                        <div class="col-sm-3">
                                                <label for="passport-image">Scanned Passport</label>
                                                <input type="file" class="form-control-file" name='passport-image' aria-describedby="fileHelp">
                                            </div>
                                            @if($file && $file->person)
                                            <div class="col-sm-3">
                                                <img src="{{asset('/'.$file->person->passport_image)}}" width="128">
                                            </div>
                                            @endif
                                            
                                             
                                            <div class="col-sm-3">
                                                <label for="voter-image">Scanned Voters ID</label>
                                                <input type="file" class="form-control-file" name='voter-image' aria-describedby="fileHelp">
                                            </div>
                                            @if($file && $file->person)
                                                <div class="col-sm-3">
                                                    <img src="{{asset('/'.$file->person->voter_image)}}" width="128">
                                                </div>
                                            @endif
                                             
                                            <div class="col-sm-3">
                                                <label for="driver-licence-image">Scanned Driver Licence</label>
                                                <input type="file" class="form-control-file" name='driver-licence-image' aria-describedby="fileHelp">
                                            </div>
                                            @if($file && $file->person)
                                                <div class="col-sm-3">
                                                    <img src="{{asset('/'.$file->person->driver_image)}}" width="128">
                                                </div>
                                            @endif
                                            
                                            <div class="col-sm-3">
                                                <label for="national-image">Scanned National ID</label>
                                                <input type="file" class="form-control-file" name='national-image' aria-describedby="fileHelp">
                                            </div>
                                            @if($file && $file->person)
                                                <div class="col-sm-3">
                                                    <img src="{{asset('/'.$file->person->national_image)}}" width="128">
                                                </div>
                                            @endif
                                        </div>
                                        <div class="row form-group">
                                            <div class="col-sm-6">
                                                <label  class="control-label">Lawajam Details</label>
                                                <input type="text" class="form-control"  name="lawajam-details" @if($file)value="{{$file->lawajam_details}}"@endif>
                                            </div>
                                            <div class="col-sm-6">
                                                <label  class="control-label">Date paid</label>
                                                <div class="input-group">
                                                    <span class="input-group-addon x-primary"><i class="fa fa-calendar"></i></span>
                                                    <input type="text" class="form-control datepicker"  name="date-paid" @if($file && !is_null($file->date_paid))value="{{$file->date_paid->format('d/m/Y')}}"@endif>
                                                </div>
                                            </div>
                                             <div class="col-sm-6">
                                                <label  class="control-label">Lawajam Member ID</label>
                                                <input type="text" class="form-control"  name="lawajam-member-id" @if($file)value="{{$file->lawajam_member_id}}"@endif>
                                            </div>
                                            
                                            
                                        </div>
                                        <div class="row form-group">
                                            <div class="col-sm-6">
                                                <label  class="control-label">Residence Phone Number</label>
                                                <input type="text" class="form-control"  name="residence-phone" @if($file && !is_null($file->person)) @if(!is_null($file->person->address))value="{{$file->person->address->residence_phone_number}}" @endif @endif>
                                            </div>
                                            <div class="col-sm-6">
                                                <label  class="control-label">Office Phone Number</label>
                                                <input type="text" class="form-control"  name="office-phone" @if($file && !is_null($file->person)) @if(!is_null($file->person->address))value="{{$file->person->address->office_phone_number}}"@endif @endif>
                                            </div>
                                            <div class="col-sm-6">
                                                <label  class="control-label">Mobile Phone Number</label>
                                                <input type="text" class="form-control"  name="mobile-phone" @if($file && !is_null($file->person)) @if(!is_null($file->person->address))value="{{$file->person->address->mobile_phone_number}}"@endif @endif>
                                            </div>
                                            <div class="col-sm-6">
                                                <label  class="control-label">Postal Address</label>
                                                <input type="text" class="form-control"  name="postal-address" @if($file && !is_null($file->person))  @if(!is_null($file->person->address) )value="{{$file->person->address->postal_address}}"@endif @endif>
                                            </div>
                                            <div class="col-sm-6">
                                                <label  class="control-label">E-mail Address</label>
                                                <input type="text" class="form-control"  name="email" @if($file && !is_null($file->person)) @if($file && !is_null($file->person->address) )value="{{$file->person->address->email}}"@endif @endif>
                                            </div>
                                            <div class="col-sm-6">
                                                <label  class="control-label">Physical Address</label>
                                                <input type="text" class="form-control"  name="physical-address" @if($file && !is_null($file->person)) @if($file && !is_null($file->person->address) )value="{{$file->person->address->physical_address}}"@endif @endif>
                                            </div>
                                            <div class="col-sm-6">
                                                <label  class="control-label">Plot No.</label>
                                                <input type="text" class="form-control"  name="plot-no" @if($file && !is_null($file->person)) @if($file && !is_null($file->person->address) )value="{{$file->person->address->plot_number}}"@endif @endif>
                                            </div>
                                            <div class="col-sm-6">
                                                <label  class="control-label">Block No.</label>
                                                <input type="text" class="form-control"  name="block-no" @if($file && !is_null($file->person)) @if($file && !is_null($file->person->address) )value="{{$file->person->address->block_number}}"@endif @endif>
                                            </div>
                                            <div class="col-sm-6">
                                                <label  class="control-label">Floor No.</label>
                                                <input type="text" class="form-control"  name="floor-no" @if($file && !is_null($file->person)) @if($file && !is_null($file->person->address) )value="{{$file->person->address->floor_number}}"@endif @endif>
                                            </div>
                                            <div class="col-sm-6">
                                                <label  class="control-label">Street</label>
                                                <input type="text" class="form-control"  name="street" @if($file && !is_null($file->person)) @if($file && !is_null($file->person->address) )value="{{$file->person->address->street}}"@endif @endif>
                                            </div>
                                            <div class="col-sm-6">
                                                <label  class="control-label">Nearest Landmark</label>
                                                <input type="text" class="form-control"  name="nearest-landmark" @if($file && !is_null($file->person)) @if($file && !is_null($file->person->address) )value="{{$file->person->address->nearest_landmark}}"@endif @endif>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="w2-tab2">
                                        <div class="row form-group">
                                            <div class="col-sm-12">
                                                <label  class="control-label">Type of Residence</label>
                                                   @foreach($residence_types as $residence_type)
                                                    <div class="radio">
                                                        <label>
                                                            <input type="radio" name="type-of-residence" value="{{$residence_type->id}}" @if($file && !is_null($file->person)) @if($file && !is_null($file->person->residence)) @if($residence_type->id==$file->person->residence->residence_type_id) checked @endif @endif @endif> {{$residence_type->name}}
                                                        </label>
                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>
                                        
                                        <div class="row form-group" id="rented-residence-info" style="display:none;">
                                            <div class="col-sm-12">
                                                <label  class="control-label">Owner</label>
                                                @foreach($residence_ownership_types as $residence_ownership_type)
                                                    <div class="radio">
                                                        <label>
                                                            <input type="radio" name="residence-owner" value="{{$residence_ownership_type->id}}" @if($file && !is_null($file->person)) @if($file && !is_null($file->person->residence)) @if($file && $residence_ownership_type->id==$file->person->residence->residence_ownership_type_id) checked @endif @endif @endif> {{$residence_ownership_type->name}}
                                                        </label>
                                                    </div>
                                                @endforeach
                                            </div>
						                    <div class="col-sm-6">
                                                <label for="w2-email" class="control-label">Rent per month<span id="registration-rent-per-month"></span></label>
                                                <input type="text" class="form-control" onkeyup="onInputChange(this,'registration-rent-per-month');" name="rent-per-month" @if($file && !is_null($file->person))  @if($file && !is_null($file->person->residence))  value="{{$file->person->residence->rent_per_month}}" @endif @endif>
                                            </div>
						                    <div class="col-sm-6">
                                                <label for="w2-email" class="control-label">Owner name(if private)</label>
                                                <input type="text" class="form-control" name="private-owner-name" @if($file && !is_null($file->person))  @if($file && !is_null($file->person->residence))  value="{{$file->person->residence->private_owner_name}}" @endif @endif>
                                            </div>                                            
                                        </div> 
                                        <div class="row form-group" id="trustee-residence-info" style="display:none;"> 
						                    <div class="col-sm-6">
                                                <label for="w2-email" class="control-label">Rent<span id="registration-rent-amount"></span></label>
                                                <input type="text" class="form-control" onkeyup="onInputChange(this,'registration-rent-amount');" name="trustee-rent" @if($file && !is_null($file->person))  @if($file && !is_null($file->person->residence))  value="{{$file->person->residence->trustee_rent}}" @endif @endif>
                                            </div>
						                    <div class="col-sm-6">
                                                <label for="w2-email" class="control-label">Owner name</label>
                                                <input type="text" class="form-control" name="trustee-owner-name" @if($file && !is_null($file->person))  @if($file && !is_null($file->person->residence))  value="{{$file->person->residence->trustee_owner_name}}" @endif @endif>
                                            </div>                                            
                                        </div> 
                                    </div>
                                    <div class="tab-pane" id="w2-tab3">
                                        <div class="row form-group">
                                            <div class="col-sm-12">
                                                <label  class="control-label">Income activity</label>

                                                @foreach($income_types as $income_type)
                                                <div class="radio">
                                                    <label>
                                                        <input type="radio" name="income-type" value="{{$income_type->id}}" @if($file && !is_null($file->person)) @if($file && !is_null($file->person->income)) @if( $income_type->id==$file->person->income->income_type_id) checked @endif @endif @endif> {{$income_type->name}}
                                                    </label>
                                                </div>
                                                @endforeach
                                            </div>
                                        </div>
                                        <div class="row form-group" id="business-info" style="display:none;">
                                            <h2>Business Details</h2>
                                            <div class="col-sm-6">
                                                <label for="w2-email" class="control-label">Name of the business</label>
                                                <input type="text" class="form-control" name="business-name" @if($file && !is_null($file->person)) @if($file && !is_null($file->person->income)) value="{{$file->person->income->business_name}}" @endif @endif>
                                            </div>
                                            <div class="col-sm-6">
                                                <label for="w2-email" class="control-label">Location</label>
                                                <input type="text" class="form-control" name="location" @if($file && !is_null($file->person)) @if($file && !is_null($file->person->income)) value="{{$file->person->income->location}}" @endif @endif>
                                            </div>
                                            <div class="col-sm-6">
                                                <label for="w2-email" class="control-label">Type of business</label>
                                                <input type="text" class="form-control" name="business-type" @if($file && !is_null($file->person)) @if($file && !is_null($file->person->income)) value="{{$file->person->income->business_type}}" @endif @endif>
                                            </div>
                                            <div class="col-sm-6">
                                                <label for="w2-email" class="control-label">Income per month<span id="registration-income-per-month"></span></label>
                                                <input type="text" class="form-control" onkeyup="onInputChange(this,'registration-income-per-month');" name="income-per-month" @if($file && !is_null($file->person)) @if($file && !is_null($file->person->income)) value="{{$file->person->income->income_per_month}}" @endif @endif>
                                            </div>
                                            <div class="col-sm-6">
                                                <label for="w2-email" class="control-label">Other incentives<span id="registration-other-incentives"></span></label>
                                                <input type="text" class="form-control" onkeyup="onInputChange(this,'registration-other-incentives');"  name="other-incentives" @if($file && !is_null($file->person)) @if($file && !is_null($file->person->income)) value="{{$file->person->income->other_incentives}}" @endif @endif>
                                            </div>
                                        </div>                                        

                                        <div class="row form-group" id="employment-info" style="display:none;">
                                            <h2>Employment Details</h2>
                                            <div class="col-sm-6">
                                                <label for="w2-email" class="control-label">Name of office employed</label>
                                                <input type="text" class="form-control"  name="office-employed" @if($file && !is_null($file->person)) @if($file && !is_null($file->person->income)) value="{{$file->person->income->office_employed}}" @endif @endif>
                                            </div>
                                            <div class="col-sm-6">
                                                <label for="w2-email" class="control-label">Name of employer</label>
                                                <input type="text" class="form-control"  name="employers-name" @if($file && !is_null($file->person)) @if($file && !is_null($file->person->income)) value="{{$file->person->income->employers_name}}" @endif @endif>
                                            </div>
                                            <div class="col-sm-6">
                                                <label for="w2-email" class="control-label">Employed as (position)</label>
                                                <input type="text" class="form-control"  name="position-employed" @if($file && !is_null($file->person)) @if($file && !is_null($file->person->income)) value="{{$file->person->income->position_employed}}" @endif @endif>
                                            </div>
                                            <div class="col-sm-6">
                                                <label for="w2-email" class="control-label">Salary per month<span id="registration-salary-per-month"></span></label>
                                                <input type="text" class="form-control"onkeyup="onInputChange(this,'registration-salary-per-month');"  name="salary-per-month" @if($file && !is_null($file->person)) @if($file && !is_null($file->person->income)) value="{{$file->person->income->salary_per_month}}" @endif @endif>
                                            </div>
                                            <div class="col-sm-6">
                                                <label for="w2-email" class="control-label">Other incentives<span id="registration-other-incentives"></span></label>
                                                <input type="text" class="form-control"onkeyup="onInputChange(this,'registration-other-incentives');" name="other-incentives-employed" @if($file && !is_null($file->person)) @if($file && !is_null($file->person->income)) value="{{$file->person->income->other_incentives_employed}}" @endif @endif>
                                            </div>
                                        </div> 
                                        <div class="row form-group" id="unemployment-info" style="display:none;">
                                            <h2>Un-employment Details</h2>
                                            <div class="col-sm-6">
                                                <label for="w2-email" class="control-label">Name(supported by if any)</label>
                                                <input type="text" class="form-control"  name="unemployment-name" @if($file && !is_null($file->person)) @if($file && !is_null($file->person->income)) value="{{$file->person->income->unemployment_name}}" @endif @endif>
                                            </div>
                                            <div class="col-sm-6">
                                                <label for="w2-email" class="control-label">Relation</label>
                                                <input type="text" class="form-control"  name="unemployment-relation" @if($file && !is_null($file->person)) @if($file && !is_null($file->person->income)) value="{{$file->person->income->unemployment_relation}}" @endif @endif>
                                            </div>
                                            <div class="col-sm-6">
                                                <label for="w2-email" class="control-label">Amount<span id="registration-employment-details-amount"></span></label>
                                                <input type="text" class="form-control"onkeyup="onInputChange(this,'registration-employment-details-amount');"  name="unemployment-amount" @if($file && !is_null($file->person)) @if($file && !is_null($file->person->income)) value="{{$file->person->income->unemployment_amount}}" @endif @endif>
                                            </div>
                                            <div class="col-sm-6">
                                                <label for="w2-email" class="control-label">Comments</label>
                                                <textarea type="text" class="form-control"  name="unemployment-comments">@if($file && !is_null($file->person)) @if($file && !is_null($file->person->income)) {{$file->person->income->unemployment_comments}} @endif @endif</textarea>
                                            </div> 
                                        </div> 
                                    </div>
                                    <div class="tab-pane" id="w2-tab4">    
                                        
                                            <div class="row form-group">
                                            <div class="col-sm-6">
                                                <label for="w2-email" class="control-label">Name of spouse</label>
                                                <input type="text" class="form-control"  name="spouse-full-name" @if($file && !is_null($file->person)) @if($file && !is_null($file->person->spouse)) value="{{$file->person->spouse->full_name}}" @endif @endif>
                                            </div>
                                            <div class="col-sm-6">
                                                <label for="w2-email" class="control-label">Date of birth</label>
                                                <div class="input-group">
                                                    <span class="input-group-addon x-primary"><i class="fa fa-calendar"></i></span>
                                                    <input type="text" class="form-control datepicker"  name="spouse-date-of-birth" @if($file && !is_null($file->person)) @if($file && !is_null($file->person->spouse)) value="{{$file->person->spouse->date_of_birth->format('d/m/Y')}}" @endif @endif>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <label for="exampleInputFile">Attach photograph</label>
                                                <input type="file" class="form-control form-control-file" name='spouse-photo' aria-describedby="fileHelp">                                                        
                                            </div>
                                            <div class="col-sm-6">
                                                <label for="w2-email" class="control-label">Occupation</label>
                                                <input type="text" class="form-control"  name="spouse-occupation" @if($file && !is_null($file->person)) @if($file && !is_null($file->person->spouse)) value="{{$file->person->spouse->occupation}}" @endif @endif>
                                            </div> 
                                            <div class="col-sm-6">
                                                <img @if($file && !is_null($file->person)) @if($file && !is_null($file->person->spouse))  src="{{asset('/'.$file->person->spouse->image)}}" @endif @endif width="128">
                                            </div>
                                            <div class="col-sm-6">
                                                <label  class="control-label">Income activity</label>

                                                @foreach($income_types as $income_type)
                                                <div class="radio">
                                                    <label>
                                                        <input type="radio" name="spouse-income-type" value="{{$income_type->id}}" @if($file && !is_null($file->person)) @if($file && !is_null($file->person->spouse)) @if(!is_null($file->person->spouse->income)) @if($income_type->id==$file->person->spouse->income->income_type_id) checked @endif @endif @endif @endif> {{$income_type->name}}
                                                    </label>
                                                </div>
                                                @endforeach
                                            </div>
                                        </div>                                        

                                        <div class="row form-group" id="spouse-employment-info" style="display:none">
                                            <h2>Spouse employment Details</h2>
                                            <div class="col-sm-6">
                                                <label for="w2-email" class="control-label">Name of office employed</label>
                                                <input type="text" class="form-control"  name="spouse-office-employed" @if($file && !is_null($file->person)) @if($file && !is_null($file->person->spouse)) @if(!is_null($file->person->spouse->income)) value="{{$file->person->spouse->income->office_employed}}" @endif  @endif @endif>
                                            </div>
                                            <div class="col-sm-6">
                                                <label for="w2-email" class="control-label">Name of employer</label>
                                                <input type="text" class="form-control"  name="spouse-employers-name" @if($file && !is_null($file->person)) @if($file && !is_null($file->person->spouse)) @if(!is_null($file->person->spouse->income)) value="{{$file->person->spouse->income->employers_name}}" @endif @endif @endif>
                                            </div>
                                            <div class="col-sm-6">
                                                <label for="w2-email" class="control-label">Employed as(position)</label>
                                                <input type="text" class="form-control"  name="spouse-position-employed" @if($file && !is_null($file->person)) @if($file && !is_null($file->person->spouse)) @if(!is_null($file->person->spouse->income)) value="{{$file->person->spouse->income->position_employed}}"@endif @endif @endif>
                                            </div>
                                            <div class="col-sm-6">
                                                <label for="w2-email" class="control-label">Salary per month<span id="registration-spouse-salary-per-month"></span></label>
                                                <input type="text" class="form-control"onkeyup="onInputChange(this,'registration-spouse-salary-per-month');"  name="spouse-salary-per-month" @if($file && !is_null($file->person)) @if($file && !is_null($file->person->spouse)) @if(!is_null($file->person->spouse->income)) value="{{$file->person->spouse->income->salary_per_month}}"@endif @endif @endif>
                                            </div>
                                        </div>
                                        <div class="row form-group" id="children-info"> 
                                            
                                            <h2>Childrens details</h2>
                                            <?php $gotoC = false;
                                            	if($file && !is_null($file->person))
                                            	{
                                                    if($file && $file->person->children)
                                                    {
                                                        if($file->person->children->count()>0)
                                                            $gotoC = true;
                                                    }
                                                } 
                                                $childIndex = 0;
                                            ?>

					
                                            @if($gotoC)
                                                @foreach($file->person->children as $key => $child) 
                                                <?php $childIndex = $key; ?>
                                                <input type="hidden" name="child_ids[]" value="{{$child->id}}">
                                                <div class="child row form-group">
                                                    <div class="col-sm-6">
                                                        <label  class="control-label">Name</label>
                                                        <input type="text" class="form-control"  name="child-name[]" value="{{$child->full_name}}">
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <label  class="control-label">Gender</label>
                                                            <div class="radio">
                                                                <label>
                                                                    <input type="radio" name="child-gender-{{$key}}" value="F" @if($child->gender == 'F')checked @endif > Female
                                                                </label>
                                                            </div>
                                                            <div class="radio">
                                                                <label>
                                                                    <input type="radio" name="child-gender-{{$key}}" value="M" @if($child->gender == 'M')checked @endif> Male
                                                                </label>
                                                            </div>                                                    
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <input type="checkbox" class=""  name="child-married[{{$key}}]" value="married" @if($child->married == 'married')checked @endif>
                                                        <label  class="control-label">Married</label>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <label  class="control-label">Date of birth</label>
                                                        <div class="input-group">
                                                            <span class="input-group-addon x-primary"><i class="fa fa-calendar"></i></span>
                                                            <input type="text" class="form-control datepicker"  name="child-date-of-birth[]" value="{{$child->date_of_birth->format('d/m/Y')}}">
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="col-sm-6">
                                                        <label  class="control-label">Occupation</label>
                                                        <input type="text" class="form-control"  name="child-occupation[]" value="{{$child->occupation}}">
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <label  class="control-label">Any other information</label>
                                                        <input type="text" class="form-control"  name="child-other-info[]" value="{{$child->other_info}}">
                                                    </div>

                                                    <div class="col-sm-3">
                                                        <label for="exampleInputFile">Attach photograph</label>
                                                        <input type="file" class="form-control-file" name='child-photo[{{$key}}]' aria-describedby="fileHelp">                                                        
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <label for="exampleInputFile">School/college</label>
                                                        <input type="text" class="form-control" name='child-school-college[]' value="{{$child->school_college}}">                                                        
                                                    </div>

                                                    <div class="col-sm-3">
                                                        <img src="{{asset('/'.$child->image)}}" width="128">
                                                    </div>
                                                </div>
                                                <hr/>
                                                @endforeach
                                            @else

                                            <div class="child row form-group">
                                                <div class="col-sm-6">
                                                    <label  class="control-label">Name</label>
                                                    <input type="text" class="form-control"  name="child-name[]">
                                                </div>
                                                <div class="col-sm-6">
                                                    <label  class="control-label">Gender</label>
                                                        <div class="radio">
                                                            <label>
                                                                <input type="radio" name="child-gender-{{$childIndex}}" value="F" > Female
                                                            </label>
                                                        </div>
                                                        <div class="radio">
                                                            <label>
                                                                <input type="radio" name="child-gender-{{$childIndex}}" value="M" > Male
                                                            </label>
                                                        </div>                                                    
                                                </div>
                                                <div class="col-sm-6">
                                                    <input type="checkbox" class=""  name="child-married[{{$childIndex}}]" value="married">
                                                    <label  class="control-label">Married</label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <label  class="control-label">Date of birth</label>
                                                    <div class="input-group">
                                                        <span class="input-group-addon x-primary"><i class="fa fa-calendar"></i></span>
                                                        <input type="text" class="form-control datepicker"  name="child-date-of-birth[]">
                                                    </div>
                                                </div>
                                                
                                                <div class="col-sm-6">
                                                    <label  class="control-label">Occupation</label>
                                                    <input type="text" class="form-control"  name="child-occupation[]">
                                                </div>
                                                <div class="col-sm-6">
                                                    <label  class="control-label">Any other information</label>
                                                    <input type="text" class="form-control"  name="child-other-info[]">
                                                </div>

                                                <div class="col-sm-6">
                                                    <label for="exampleInputFile">Attach photograph</label>
                                                    <input type="file" class="form-control-file" name='child-photo[{{$childIndex}}]' aria-describedby="fileHelp">
                                                </div>
                                                <div class="col-sm-3">
                                                    <label for="exampleInputFile">School/college</label>
                                                    <input type="text" class="form-control" name='child-school-college[]'>                                                        
                                                </div>
                                            </div> 
                                            @endif                                           
                                        </div>
                                        <div class="row" style="margin-top:15px;">
                                            <a class="btn btn-primary" id="add-child"> + </a>
                                        </div>
                                        
                                        <div class="row form-group" id="other-info"> 
                                            
                                            <h2>Other family relative details</h2>
<?php $gotoC = false;
                                            	if($file && !is_null($file->person))
                                            	{
                                            	if($file && $file->person->other)
                                            	{
                                            		$gotoC = true;
                                            	}
                                            	}
                                            ?>
					
                                            @if($gotoC)
                                                @foreach($file->person->other as $key => $other)
                                                <div class="child row form-group">
                                                    <div class="col-sm-6">
                                                        <label  class="control-label">Name</label>
                                                        <input type="text" class="form-control"  name="other-name[]" value="{{$other->full_name}}">
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <label  class="control-label">Gender</label>
                                                            <div class="radio">
                                                                <label>
                                                                    <input type="radio" name="other-gender-{{$key}}" value="F" @if($other->gender == 'F')checked @endif > Female
                                                                </label>
                                                            </div>
                                                            <div class="radio">
                                                                <label>
                                                                    <input type="radio" name="other-gender-{{$key}}" value="M" @if($other->gender == 'M')checked @endif> Male
                                                                </label>
                                                            </div>                                                    
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <label  class="control-label">Date of birth</label>
                                                        <div class="input-group">
                                                            <span class="input-group-addon x-primary"><i class="fa fa-calendar"></i></span>
                                                            <input type="text" class="form-control datepicker"  name="other-date-of-birth[]" value="{{$other->date_of_birth->format('d/m/Y')}}">
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="col-sm-6">
                                                        <label  class="control-label">Occupation</label>
                                                        <input type="text" class="form-control"  name="other-occupation[]" value="{{$other->occupation}}">
                                                    </div>
                                                    
													<div class="col-sm-6">
														<label  class="control-label">Relationship</label> <?php
														$index = 1;
														?>
														@foreach($relationships as $relationship)
																<div class="radio">
																   <label>
																	  <input type="radio" name="other-relationship-{{$index}}" value="{{$relationship->id}}"@if($other->relationship_id == $relationship->id) checked  @endif> {{$relationship->name}}
																   </label>
																</div>
																<?php
														$index = $index + 1;
														?>
														@endforeach
													</div>
													
													<div class="col-sm-6">
														<label for="w2-username" class="control-label">Phone#</label>
														<input type="text" class="form-control"  name="other-phone[]" value="{{$other->phone}}">
													</div>
													<div class="col-sm-6">
														<label for="w2-username" class="control-label">Email</label>
														<input type="email" class="form-control"  name="other-email[]" value="{{$other->email}}">
													</div>
													<div class="col-sm-6">
														<label for="w2-username" class="control-label">Address</label>
														<input type="text" class="form-control"  name="other-address[]" value="{{$other->address}}">
													</div>	
													<div class="col-sm-6">
                                                        <label for="w2-email" class="control-label">Information of any assistance to you & your family</label>
														<input type="text" class="form-control"  name="other-family-assistance[]"  value="{{$other->family_assistance}}">
													</div> 	
													<div class="col-sm-6">
                                                        <label for="w2-email" class="control-label">Comments</label>
														<textarea class="form-control"  name="other-comments[]">{{$other->comments}}</textarea>
													</div>  		
                                                    
                                                </div>
                                                <hr/>
                                                @endforeach
                                            @else

												<div class="other row form-group">
													<div class="col-sm-6">
														<label for="w2-username" class="control-label">Name</label>
														<input type="text" class="form-control"  name="other-name[]">
													</div>
													<div class="col-sm-6">
														<label for="w2-username" class="control-label">Gender</label>
															<div class="radio">
																<label>
																	<input type="radio" name="other-gender-0" value="F" > Female
																</label>
															</div>
															<div class="radio">
																<label>
																	<input type="radio" name="other-gender-0" value="M" > Male
																</label>
															</div>                                                    
													</div>
                                                    <div class="col-sm-6">
                                                        <label  class="control-label">Date of birth</label>
                                                        <div class="input-group">
                                                            <span class="input-group-addon x-primary"><i class="fa fa-calendar"></i></span>
                                                            <input type="text" class="form-control datepicker" name="other-date-of-birth[]">
                                                        </div>
                                                    </div>
													 <div class="col-sm-6">
														<label for="w2-username" class="control-label">Occupation</label>
														<input type="text" class="form-control"  name="other-occupation[]">
													</div>
													<div class="col-sm-6">
													<label  class="control-label">Relationship</label>
													@foreach($relationships as $relationship)
															<div class="radio">
															   <label>
																  <input class="other" type="radio" data="other-relation-name-0" name="other-relationship-0" value="{{$relationship->id}}"> {{$relationship->name}}
															   </label>
															</div>
													@endforeach
													</div>
													<div id="other-relation-name-0" class="col-sm-6" style="display:none;">
        <label for="w2-username" class="control-label">Relation Name</label>
        <input type="text" class="form-control"  name="other-relation-name-0">
    </div>
													<div class="col-sm-6">
														<label for="w2-username" class="control-label">Phone#</label>
														<input type="text" class="form-control"  name="other-phone[]">
													</div>
													<div class="col-sm-6">
														<label for="w2-username" class="control-label">Email</label>
														<input type="email" class="form-control"  name="other-email[]">
													</div>
													<div class="col-sm-6">
														<label for="w2-username" class="control-label">Address</label>
														<input type="text" class="form-control"  name="other-address[]">
													</div> 		
													<div class="col-sm-6">
                                                        <label for="w2-email" class="control-label">Information of any assistance to you & your family</label>
														<input type="text" class="form-control"  name="other-family-assistance[]">
													</div> 	 
													<div class="col-sm-6">
														<label for="w2-email" class="control-label">Comments</label>
														<textarea class="form-control"  name="other-comments[]"></textarea>
													</div>   

												   
												</div>
                                            @endif                                           
                                        </div>
										
										<div class="row" style="margin-top:15px;">
                                            <a class="btn btn-primary" id="add-other"> + </a>
                                        </div>
                                        
                                    </div>
                                    <div class="tab-pane" id="w2-tab5">
                                    <?php $gotoC = false;
                                            	if($file && !is_null($file->person))
                                            	{
                                            	if($file && $file->person->assistance)
                                            	{
                                            		$gotoC = true;
                                            	}
                                            	}
                                            ?>
					
                                            @if($gotoC)
                                       
                                        <div class="row form-group">
                                            <h2>Assistance received from Jamaat</h2>
                                            <p>(If yes write the amount, otherwise write NO)</p>
                                            <div class="col-sm-6">
                                                <label for="w2-email" class="control-label">Rent<span id="registration-assistance-rent"></span></label>
                                                <input type="text" class="form-control"onkeyup="onInputChange(this,'registration-assistance-rent');"  name="rent" @if($file) value="{{$file->person->assistance->rent}}"@endif >
                                            </div>
                                            <div class="col-sm-6">
                                                <label for="w2-email" class="control-label">School Fees<span id="registration-assistance-fees"></span></label>
                                                <input type="text" class="form-control"onkeyup="onInputChange(this,'registration-assistance-fees');"  name="school-fees" @if($file)value="{{$file->person->assistance->school_fees}}"@endif >
                                            </div>
                                            <div class="col-sm-6">
                                                <label for="w2-email" class="control-label">Medical<span id="registration-assistance-medical"></span></label>
                                                <input type="text" class="form-control"onkeyup="onInputChange(this,'registration-assistance-medical');"  name="medical" @if($file)value="{{$file->person->assistance->medical}}"@endif >
                                            </div>
                                            <div class="col-sm-6">
                                                <label for="w2-email" class="control-label">Electricity<span id="registration-assistance-electricity"></span></label>
                                                <input type="text" class="form-control"onkeyup="onInputChange(this,'registration-assistance-electricity');"  name="electricity" @if($file)value="{{$file->person->assistance->electricity}}"@endif >
                                            </div>
                                            <div class="col-sm-6">
                                                <label for="w2-email" class="control-label">Water<span id="registration-assistance-water"></span></label>
                                                <input type="text" class="form-control"onkeyup="onInputChange(this,'registration-assistance-water');"  name="water" @if($file)value="{{$file->person->assistance->water}}"@endif >
                                            </div>
                                            <div class="col-sm-6">
                                                <label for="w2-email" class="control-label">Any other<span id="registration-assistance-any-other"></span></label>
                                                <input type="text" class="form-control"onkeyup="onInputChange(this,'registration-assistance-any-other');"  name="others" @if($file)value="{{$file->person->assistance->others}}"@endif >
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                        	
                                            <div class="col-sm-12">
                                                <label  class="control-label col-sm-2">Medical Payment Type</label>
						
                                                @foreach($medical_payment_types as $medical_payment_type)
                                                <div class="radio  col-md-6">
                                                    <label>
                                                        <input type="radio" name="medical-payment-type" @if($medical_payment_type->id == $file->person->assistance->medical_payment_type_id) checked @endif  value="{{$medical_payment_type->id}}"> {{$medical_payment_type->name}}
                                                    </label>
                                                </div>
                                                
                                                @endforeach
                                            </div>
                                          
                                        	
                                            <div id='medical-type' class="col-sm-12" >
                                                
						                        <div class="col-md-6">
						                        <label for="w2-email" class="control-label">Enter Amount<span id="registration-medical-type-amount"></span></label>
                                                <input type="text" class="form-control"onkeyup="onInputChange(this,'registration-medical-type-amount');"  name="medical-payment-type-cash" @if($file)value="{{$file->person->assistance->medical_cash_amount}}"@endif >
                                            </div>
                                           
                                        	</div>
                                            <div class="col-sm-12">
                                                <label for="left-addon-icon" class="col-sm-2 control-label">Are you a receiver of Ration?</label>
                                                <div class="col-sm-6">
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox" value="yes" name="ramadhan" @if($file && $file->person->assistance->ramadhan=='ramadhan') checked @endif> Ramadhan
                                                        </label>
                                                    </div>
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox" value="yes" name="monthly-allocate" @if($file && $file->person->assistance->monthly_allocate=='yes') checked @endif> Monthly Allocate
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                                <?php
                                                if($file && $file->person->assistance->monthly_allocate=='yes')
                                                    $has_assistance_amount = true;
                                                else
                                                    $has_assistance_amount = false;
                                                ?>
						<div id="monthly-allocated-amount" class="col-sm-12" style="{{$has_assistance_amount?'':'display:none'}};">
						<div class="col-md-6">
						<label for="w2-email" class="control-label">Enter Amount<span id="registration-allocated-amount-amount"></span></label>
                                                <input type="text" class="form-control"onkeyup="onInputChange(this,'registration-allocated-amount-amount');" value="{{$has_assistance_amount?$file->person->assistance->monthly_allocated_amount:''}}" name="monthly-allocated-amount"  >
                                            </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <label for="w2-email" class="control-label">Please comment</label>
                                                <input type="text" class="form-control"  name="fitro-comment" @if($file && $file->person->assistance )value="{{$file->person->assistance->fitro_comment}}"@endif>
                                            </div>
                                            <div class="col-sm-6">
                                                <label for="w2-email" class="control-label">Are you a receiver of Fitro (yes/no) please comment</label>
                                                <input type="text" class="form-control"  name="fitro"  name="fitro" @if($file && $file->person->assistance )value="{{$file->person->assistance->fitro}}"@endif>
                                            </div>
                                        </div>
                                        @else
                                        <div class="row form-group">
                                            <h2>Assistance received from Jamaat</h2>
                                            <p>(If yes write the amount, otherwise write NO)</p>
                                            <div class="col-sm-6">
                                                <label for="w2-email" class="control-label">Rent<span id="registration-jamaat-assistance-rent"></span></label>
                                                <input type="text" class="form-control"onkeyup="onInputChange(this,'registration-jamaat-assistance-rent');"  name="rent" >
                                            </div>
                                            <div class="col-sm-6">
                                                <label for="w2-email" class="control-label">School Fees<span id="registration-jamaat-assistance-rent-school"></span></label>
                                                <input type="text" class="form-control"onkeyup="onInputChange(this,'registration-jamaat-assistance-rent-school');"  name="school-fees" >
                                            </div>
                                            <div class="col-sm-6">
                                                <label for="w2-email" class="control-label">Medical<span id="registration-jamaat-assistance-rent-medical"></span></label>
                                                <input type="text" class="form-control"onkeyup="onInputChange(this,'registration-jamaat-assistance-rent-medical');"  name="medical" >
                                            </div>
                                            <div class="col-sm-6">
                                                <label for="w2-email" class="control-label">Electricity<span id="registration-jamaat-assistance-rent-electricity"></span></label>
                                                <input type="text" class="form-control"onkeyup="onInputChange(this,'registration-jamaat-assistance-rent-electricity');"  name="electricity" >
                                            </div>
                                            <div class="col-sm-6">
                                                <label for="w2-email" class="control-label">Water<span id="registration-jamaat-assistance-rent-water"></span></label>
                                                <input type="text" class="form-control"onkeyup="onInputChange(this,'registration-jamaat-assistance-rent-water');"  name="water" >
                                            </div>
                                            <div class="col-sm-6">
                                                <label for="w2-email" class="control-label">Any other<span id="registration-jamaat-assistance-rent-other"></span></label>
                                                <input type="text" class="form-control"onkeyup="onInputChange(this,'registration-jamaat-assistance-rent-other');"  name="others" >
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                        <div class="col-sm-12">
                                                <label  class="control-label">Medical Payment Type</label>
						
                                                @foreach($medical_payment_types as $medical_payment_type)
                                                <div class="radio  col-sm-6">
                                                    <label>
                                                        <input type="radio" name="medical-payment-type" @if($medical_payment_type->id == 1) checked @endif value="{{$medical_payment_type->id}}"> {{$medical_payment_type->name}}
                                                    </label>
                                                </div>
                                                
                                                @endforeach
                                            </div>
                                            <div id="medical-type" class="col-sm-12" >
                                                
						<div class="col-md-6">
						<label for="w2-email" class="control-label">Enter Amount<span id="registration-jamaat-ass-amount"></span></label>
                                                <input type="text" class="form-control"onkeyup="onInputChange(this,'registration-jamaat-ass-amount');"  name="medical-payment-type-cash"  >
                                            </div>
                                            </div>
                                            <div class="col-sm-12">
                                                <label for="left-addon-icon" class="col-sm-2 control-label">Are you a receiver of Ration?</label>
                                                <div class="col-sm-6">
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox" value="yes" name="ramadhan"> Ramadhan
                                                        </label>
                                                    </div>
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox" value="yes" name="monthly-allocate" > Monthly Allocate
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
					<div id="monthly-allocated-amount" class="col-sm-12" style="display:none;">
                                                
						<div class="col-md-6">
						<label for="w2-email" class="control-label">Enter Amount<span id="reg-ass-amount"></span></label>
                                                <input type="text" class="form-control"onkeyup="onInputChange(this,'reg-ass-amount');"  name="monthly-allocated-amount"  >
                                            </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <label for="w2-email" class="control-label">Please comment</label>
                                                <input type="text" class="form-control"  name="fitro-comment">
                                            </div>
                                            <div class="col-sm-6">
                                                <label for="w2-email" class="control-label">Are you a receiver of Fitro (yes/no) please comment</label>
                                                <input type="text" class="form-control"  name="fitro"  name="fitro">
                                            </div>
                                        </div>
                                        @endif
                                        

                                        <div class="row form-group">
                                            <div class="col-sm-12">
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox" value="1" name="agree-terms" @if($file && $file->terms_agreed='1') checked @endif> I certifiy that whatever I have stated above is true
                                                        </label>
                                                    </div>
                                            </div>

                                        </div>
                                    </div>

                                    <div class="tab-pane" id="w2-tab6">
                                        <div class="row form-group ">
                                            <h2>For official use only</h2>
                                            <div class="col-sm-12">
                                                <label for="">Remarks by Member</label>
                                                <textarea class="form-control" rows="3"  placeholder="Write a comment" name="remarks-member" >@if($file && $file->remarks){{$file->remarks->by_member}}@endif</textarea>
                                            </div>
                                            <div class="col-sm-12">
                                                <label for="">Remarks by Head of Department</label>
                                                <textarea class="form-control" rows="3"  placeholder="Write a comment" name="remarks-head-of-department" >@if($file && $file->remarks){{$file->remarks->by_head_of_department}}@endif</textarea>
                                            </div>
                                            <div class="col-sm-12">
                                                <label for="">Remarks by the chairman of welfare</label>
                                                <textarea class="form-control" rows="3"  placeholder="Write a comment" name="remarks-chairman-of-welfare" >@if($file && $file->remarks){{$file->remarks->by_chairman_of_welfare}}@endif</textarea>
                                            </div>
                                            <div class="col-sm-12">
                                                <label for="">Notes</label>
                                                <textarea class="form-control" rows="3"  placeholder="Write notes" name="remarks-notes" >@if($file && $file->notes){{$file->remarks->notes}}@endif</textarea>
                                            </div>
                                            <div class="col-sm-12">
                                                <label for="" class="control-label">Approve File</label>
                                                
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" value="1" name="approved" @if($file && $file->approved=="1") checked @endif> Approve
                                                    </label>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="row form-group" style="visibility:hidden">
                                            <div class="col-sm-12">
                                                <label for="left-addon-icon" class="col-sm-2 control-label">Concluded?</label>
                                                <div class="radio">
                                                    <label>
                                                        <input type="radio" value="yes" name="concluded" @if($file && $file->concluded=='yes') checked @endif> Yes
                                                    </label>
                                                    <label>
                                                        <input type="radio" value="yes" name="concluded" @if($file && $file->concluded=='no') checked @endif> No
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div class="tab-buttons row">
                                    <a class="btn btn-primary previous">Previous</a>
                                    <button class="btn btn-primary finish hidden" type="submit">Submit</button>
                                    <a class="btn btn-primary next">Next</a>
                                </div>
                                <div class="">
                                    <progress id="progressBar" value="0" max="100" style="width:300px;"></progress>
                                    <h3 id="status"></h3>
                                    <p id="loaded_n_total"></p>
                                </div>                                
                            </form>

                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>

</div>

@endsection

@section('page-scripts')

<script src="{{asset('/vendor/twitter-bootstrap-wizard/jquery.bootstrap.wizard.js')}}"></script>
 <script src="{{asset('/vendor/jquery-validation/jquery.validate.min.js')}}"></script> 
<script src="{{asset('/javascripts/examples/forms/wizard.js')}}"></script>

<script type="text/javascript">
    jQuery('.datepicker').datepicker({format: "d/m/yyyy",});
    
    $('[name=type-of-residence]').change(function(event) {
        var radio = $(this);
        var value = radio.val();
        if(radio.prop('checked')==true){

            if(value=='1'){
                $('#rented-residence-info').slideDown();               
                $('#trustee-residence-info').slideUp();               
            }else if(value == '5'){
                $('#rented-residence-info').slideUp();               
                $('#trustee-residence-info').slideDown();               
            } else{
                $('#rented-residence-info').slideUp();               
                $('#trustee-residence-info').slideUp();               
            }                                                        

        }
    });

    $('[name=residence-owner]').change(function(event) {
        var radio = $(this);
        var value = radio.val();
        if(radio.prop('checked')==true){

            if(value=='2'){
                $('[name=private-owner-name]').closest('div').fadeIn();               
            }else{
                $('[name=private-owner-name]').closest('div').fadeOut();               
            }                                                        

        }
    });
    
    $('[name=maritual-status]').change(function(event) {
        var radio = $(this);
        var value = radio.val();
        if(radio.prop('checked')==true){

            if(value=='Other'){
                $('.maritual-status').slideDown();               
            }else{
                $('.maritual-status').slideUp();              
            }                                                        

        }
    });
	
    $('[name=income-type]').change(function(event) {
        var radio = $(this);
        var value = radio.val();

        if(radio.prop('checked')==true){
            if(value=='1'){
                $('#business-info').slideDown();
                $('#employment-info').slideUp();
                $('#unemployment-info').slideUp();
            }else if(value=='2'){
                $('#business-info').slideUp();
                $('#employment-info').slideDown();
                $('#unemployment-info').slideUp();
            }else if(value== '3'){
                $('#business-info').slideUp();
                $('#employment-info').slideUp();
                $('#unemployment-info').slideDown();
            }else{
                $('#business-info').slideUp();
                $('#employment-info').slideUp();
                $('#unemployment-info').slideUp();
            }
        }
    }); 

    $('[name=medical-payment-type]').change(function(event) {
        var radio = $(this);
        var value = radio.val();

        if(radio.prop('checked')==true){
            if(value=='1'){
                $('#medical-type').slideDown();
               
            }else if(value=='2'){
                $('#medical-type').slideUp();
               
            }else{
                $('#medical-type').slideUp();
               
            }
        }
    }); 
    
    $('[name=monthly-allocate]').change(function(event) {
        var check = $(this);
        var value = check.val();

        if(check.prop('checked')==true){
            
                $('#monthly-allocated-amount').slideDown();
              }
        else
        {
        	 $('#monthly-allocated-amount').slideUp();
        }
    }); 
    
    $('.other').change(function(event) {
        var check = $(this);
        var value = check.val();

        if(value == 8){
            
                $('#'+ check.attr("data")).slideDown();
              }
        else
        {
        	 $('#'+ check.attr("data")).slideUp();
        }
    }); 
    
    $('[name=user-type-assigned]').change(function(event) {
        var combo= $(this);
        var value = combo.val();

        if(value == 5)
        {
        	$('#outreach-type').slideDown();
         }
        else
        {
        	 $('#outreach-type').slideUp();
        }
    });       
	
    $('[name=spouse-income-type]').change(function(event) {
        var radio = $(this);
        var value = radio.val();

        if(radio.prop('checked')==true){
            if(value=='2'){
                $('#spouse-employment-info').slideDown();
            }else{
                $('#spouse-employment-info').slideUp();
            }
        }
    });

    $('[name=type-of-residence],[name=residence-owner],[name=income-type],[name=spouse-income-type],[name=maritual-status]').trigger('change');

    var childIndex = {{$childIndex + 1}};
    $('#add-child').click(function(event) {
        event.preventDefault();
        $.ajax({
            url: '{{url('/registration/child-sub-form')}}',
            type: 'GET',
            dataType: 'html',
            data: {child_index: childIndex},
        })
        .done(function(feedback) {
            $('#children-info').append(feedback);
            jQuery('.datepicker').datepicker({format: "d/m/yyyy",});
            childIndex = childIndex + 1;
        })
        .fail(function() {
            console.log("error");
        });
        
    });
    
    var otherIndex = 1;
    $('#add-other').click(function(event) {
        event.preventDefault();
        $.ajax({
            url: '{{url('/registration/other-sub-form')}}',
            type: 'GET',
            dataType: 'html',
            data: {other_index: otherIndex},
        })
        .done(function(feedback) {
            $('#other-info').append(feedback);
            $('.other').change(function(event) {
		        var check = $(this);
		        var value = check.val();
		
		        if(value == 8){
		            
		                $('#'+ check.attr("data")).slideDown();
		              }
		        else
		        {
		        	 $('#'+ check.attr("data")).slideUp();
		        }
		    }); 
            otherIndex = otherIndex + 1;
        })
        .fail(function() {
            console.log("error");
        });
        
    });



    // Submit registration form
    var w2_form = $('#registration-form')
    //Submit form button
    var w2_submit = $('#registration-form .tab-buttons .finish');
    //Validate
    var w2_validate = buildValidation(w2_form);
    w2_form.bootstrapWizard({
        tabClass: 'steps',
        nextSelector: '.tab-buttons .next',
        previousSelector: '.tab-buttons .previous',
        onInit: function(activeTab, navigation){
            this.totalTabs =  navigation.children('li').length;
        },
        onNext: function() {
            //return validTabInfo(w2_form,w2_validate);
            return true;
        },
        onTabClick: function( activeTab, navigation, actualIndex, clickedIndex ) {
            return onTabClick(clickedIndex, actualIndex, this);
        },
        onTabChange: function( activeTab, navigation, previousIndex, actualIndex ) {
            inLastTab(actualIndex, this.totalTabs, w2_submit);
        },
         onTabShow: function( activeTab, navigation, actualIndex ) {
             updateValidateTabs(activeTab);
         }
    });
    
    //On Submit
    w2_form.on('submit', function( event ) {
        event.preventDefault();
        var form = $(this);
        var formData = new FormData(this);
        formData.append('id', form.data('id'));
        console.log(formData.get('id'));

        $.ajax({
            xhr: function() {
                var xhr = new window.XMLHttpRequest();
                xhr.upload.addEventListener("progress", progressHandler, false);
                xhr.addEventListener("load", completeHandler, false);
                xhr.addEventListener("error", errorHandler, false);
                xhr.addEventListener("abort", abortHandler, false);

                return xhr;
            },     
            url: '{{url('/registration/submit')}}',
            type: 'POST',
            dataType: 'json',
            data: formData,
            cache: false,
            contentType: false,
            processData: false
        })
        .done(function(re) {
            console.log("success");
            if(re.status == 1)
            {
            toastr.success('file created!');
            }
            else if(re.status == 2)
            {
            toastr.success('file updated!');
            }            
            successNotification();
            w2_form[0].reset();
            w2_form.bootstrapWizard('first');
            window.location = "{{url('/')}}";
        })
        .fail(function() {
            console.log("error");
        });


        function _(el) {
            return document.getElementById(el);
        }

        function progressHandler(event) {
            _("loaded_n_total").innerHTML = "Uploaded " + event.loaded + " bytes of " + event.total;
            var percent = (event.loaded / event.total) * 100;
            _("progressBar").value = Math.round(percent);
            _("status").innerHTML = Math.round(percent) + "% uploaded... please wait";
        }

        function completeHandler(event) {
            _("status").innerHTML = event.target.responseText;
            _("progressBar").value = 0;
        }

        function errorHandler(event) {
            _("status").innerHTML = "Upload Failed";
        }

        function abortHandler(event) {
            _("status").innerHTML = "Upload Aborted";
        }


        //if ( w2_form.valid() ) {
            // successNotification();
            // w2_form[0].reset();
            // w2_form.bootstrapWizard('first');
        //}
    });
</script>


@endsection