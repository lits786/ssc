@extends('layouts.main')



@section('content')

<div class="content-header">

  <div class="leftside-content-header">

    <ul class="breadcrumbs">

      <li><i class="fa fa-home" aria-hidden="true"></i><a href="#">Registration Form</a></li>

    </ul>

  </div>

</div>

<div class="row animated fadeInUp">

  <div class="col-sm-12 col-lg-12">

    <div class="row">
      <div class="col-sm-12 col-md-12">
        {{-- <h4 class="section-subtitle">Interview</h4> --}}
        <div class="panel">
          <div class="panel-content">
            <form id="school-assistance-form" data-id="0">
              <div class="row form-group">
                <div class="col-sm-6">
                  <label for="w2-username" class="control-label">Name of Parent/Guardian<span class="required">*</span></label>
                  <input type="text" class="form-control" name="name-of-parent" @if($assistance)value="{{$assistance->name_of_parent}}"@endif>
                </div>
                <div class="col-sm-6">
                  <label for="w2-username" class="control-label">Name of student<span class="required">*</span></label>
                  <input type="text" class="form-control" name="name-of-student" @if($assistance)value="{{$assistance->name_of_student}}"@endif>
                </div>
                <div class="col-sm-6">
                  <label for="w2-username" class="control-label">Grade/Form<span class="required">*</span></label>
                  <input type="text" class="form-control" name="grade" @if($assistance)value="{{$assistance->grade}}"@endif>
                </div>
                <div class="col-sm-6">
                  <label for="w2-username" class="control-label">No. of students studying at Al Muntazir<span class="required">*</span></label>
                  <input type="text" class="form-control" name="students-at-al-muntazir" @if($assistance)value="{{$assistance->students_at_al_muntazir}}"@endif>
                </div>
                <div class="col-sm-6">
                  <label for="w2-username" class="control-label">No. of students at other Schools<span class="required">*</span></label>
                  <input type="text" class="form-control" name="students-at-other-schools" @if($assistance)value="{{$assistance->students_at_other_schools}}"@endif>
                </div>
                <div class="col-sm-6">
                  <label for="w2-username" class="control-label">Specify the name of school<span class="required">*</span></label>
                  <input type="text" class="form-control" name="other-schools" @if($assistance)value="{{$assistance->other_schools}}"@endif>
                </div>
              </div>
              <div class="row form-group">
                <div class="col-sm-6">
                  <label for="w2-username" class="control-label">Residence Telephone<span class="required">*</span></label>
                  <input type="text" class="form-control" name="residence-telephone" @if($assistance)value="{{$assistance->residence_telephone}}"@endif>
                </div>
                <div class="col-sm-6">
                  <label for="w2-username" class="control-label">Office Telephone<span class="required">*</span></label>
                  <input type="text" class="form-control" name="office-telephone" @if($assistance)value="{{$assistance->office_telephone}}"@endif>
                </div>
                <div class="col-sm-6">
                  <label for="w2-username" class="control-label">mobile<span class="required">*</span></label>
                  <input type="text" class="form-control" name="mobile" @if($assistance)value="{{$assistance->mobile}}"@endif>
                </div>
              </div>
              <div class="row form-group">
                <h2>Father's information</h2>
                <div class="col-sm-6">
                  <label for="w2-username" class="control-label">Occupation<span class="required">*</span></label>
                  <input type="text" class="form-control" name="father-occupation" @if($assistance)value="{{$assistance->father->occupation}}"@endif>
                </div>
                <div class="col-sm-6">
                  <label for="w2-username" class="control-label">Name of Company/Employer<span class="required">*</span></label>
                  <input type="text" class="form-control" name="father-employer" @if($assistance)value="{{$assistance->father->employer}}"@endif>
                </div>
                <div class="col-sm-6">
                  <label for="w2-username" class="control-label">Montly Salary/Income<span id="school-assistance-monthly-salary4"><span class="required">*</span></label>
                  <input type="text" class="form-control" onkeyup="onInputChange(this,'school-assistance-monthly-salary4');"  name="father-salary" @if($assistance)value="{{$assistance->father->salary}}"@endif>
                </div>
                <div class="col-sm-6">
                  <label for="w2-username" class="control-label">Contact number of employer<span class="required">*</span></label>
                  <input type="text" class="form-control" name="father-contact-employer" @if($assistance)value="{{$assistance->father->contact_employer}}"@endif>
                </div>
                <div class="col-sm-6">
                  <label for="w2-username" class="control-label">If any other source of income please specify (Type) and amount<span class="required">*</span></label>
                  <input type="text" class="form-control" name="father-other-income-source" @if($assistance)value="{{$assistance->father->other_income_source}}"@endif>
                </div>
              </div>

              <div class="row form-group">
                <h2>Mother's information</h2>
                <div class="col-sm-6">
                  <label for="w2-username" class="control-label">Occupation<span class="required">*</span></label>
                  <input type="text" class="form-control" name="mother-occupation" @if($assistance)value="{{$assistance->mother->occupation}}"@endif>
                </div>
                <div class="col-sm-6">
                  <label for="w2-username" class="control-label">Name of Company/Employer<span class="required">*</span></label>
                  <input type="text" class="form-control" name="mother-employer" @if($assistance)value="{{$assistance->mother->employer}}"@endif>
                </div>
                <div class="col-sm-6">
                  <label for="w2-username" class="control-label">Montly Salary/Income<span id="school-assistance-monthly-salary0"><span class="required">*</span></label>
                  <input type="text" class="form-control" onkeyup="onInputChange(this,'school-assistance-monthly-salary0');"  name="mother-salary" @if($assistance)value="{{$assistance->mother->salary}}"@endif>
                </div>
                <div class="col-sm-6">
                  <label for="w2-username" class="control-label">Contact number of employer<span class="required">*</span></label>
                  <input type="text" class="form-control" name="mother-contact-employer" @if($assistance)value="{{$assistance->mother->contact_employer}}"@endif>
                </div>
                <div class="col-sm-6">
                  <label for="w2-username" class="control-label">If any other source of income please specify (Type) and amount<span class="required">*</span></label>
                  <input type="text" class="form-control" name="mother-other-income-source" @if($assistance)value="{{$assistance->mother->other_income_source}}"@endif>
                </div>
              </div>

              <div class="row form-group">
                <h2>Brother's/Sister's information</h2>
                <div class="col-sm-6">
                  <label for="w2-username" class="control-label">Occupation<span class="required">*</span></label>
                  <input type="text" class="form-control" name="sibling-occupation" @if($assistance)value="{{$assistance->sibling->occupation}}"@endif>
                </div>
                <div class="col-sm-6">
                  <label for="w2-username" class="control-label">Name of Company/Employer<span class="required">*</span></label>
                  <input type="text" class="form-control" name="sibling-employer" @if($assistance)value="{{$assistance->sibling->employer}}"@endif>
                </div>
                <div class="col-sm-6">
                  <label for="w2-username" class="control-label">Montly Salary/Income<span id="school-assistance-monthly-salary"><span class="required">*</span></label>
                  <input type="text" class="form-control" onkeyup="onInputChange(this,'school-assistance-monthly-salary');" name="sibling-salary" @if($assistance)value="{{$assistance->sibling->salary}}"@endif>
                </div>
                <div class="col-sm-6">
                  <label for="w2-username" class="control-label">Contact number of employer<span class="required">*</span></label>
                  <input type="text" class="form-control" name="sibling-contact-employer" @if($assistance)value="{{$assistance->sibling->contact_employer}}"@endif>
                </div>
                <div class="col-sm-6">
                  <label for="w2-username" class="control-label">If any other source of income please specify (Type) and amount<span class="required">*</span></label>
                  <input type="text" class="form-control" name="sibling-other-income-source">
                </div>
              </div>

              <div class="row form-group">
                <h2>Any other</h2>
                <div class="col-sm-6">
                  <label for="w2-username" class="control-label">Occupation<span class="required">*</span></label>
                  <input type="text" class="form-control" name="others-occupation" @if($assistance)value="{{$assistance->other->occupation}}"@endif>
                </div>
                <div class="col-sm-6">
                  <label for="w2-username" class="control-label">Name of Company/Employer<span class="required">*</span></label>
                  <input type="text" class="form-control" name="others-employer" @if($assistance)value="{{$assistance->other->employer}}"@endif>
                </div>
                <div class="col-sm-6">
                  <label for="w2-username" class="control-label">Montly Salary/Income<span class="required">*<span id="school-assistance-monthly-salary2"></span></label>
                  <input type="text" class="form-control" onkeyup="onInputChange(this,'school-assistance-monthly-salary2');"  name="others-salary" @if($assistance)value="{{$assistance->other->salary}}"@endif>
                </div>
                <div class="col-sm-6">
                  <label for="w2-username" class="control-label">Contact number of employer<span class="required">*</span></label>
                  <input type="text" class="form-control" name="others-contact-employer" @if($assistance)value="{{$assistance->other->contact_employer}}"@endif>
                </div>
                <div class="col-sm-6">
                  <label for="w2-username" class="control-label">If any other source of income please specify (Type) and amount<span class="required">*</span></label>
                  <input type="text" class="form-control" name="others-other-income-source" @if($assistance)value="{{$assistance->other->other_income_source}}"@endif>
                </div>
              </div>

              <div class="row form-group">
                <div class="col-sm-6">
                  <label for="w2-username" class="control-label">Have you received an assistance from Jamaat in the past<span class="required">*</span></label>
                  <div class="radio">
                    <label>
                      <input type="radio" name="previous-assistance" value="yes" @if($assistance && $assistance->previous_assistance=='yes') checked @endif> Yes
                    </label>
                  </div>
                  <div class="radio">
                    <label>
                      <input type="radio" name="previous-assistance" value="no" @if($assistance && $assistance->previous_assistance=='no') checked @endif> No
                    </label>
                  </div>
                </div>
              </div>

              <div class="row form-group">
                <div class="col-sm-6">
                  <label for="w2-email" class="control-label">Assistance for<span class="required">*</span></label>
                  <input type="text" class="form-control" name="assistance-for" @if($assistance)value="{{$assistance->assistance_for}}"@endif>
                </div>
                <div class="col-sm-6">
                  <label for="w2-email" class="control-label">When was the assitance given<span class="required">*</span></label>
                  <input type="text" class="form-control" name="assistance-when" @if($assistance)value="{{$assistance->assistance_when}}"@endif>
                </div>
                <div class="col-sm-6">
                  <label for="w2-email" class="control-label">Amount<span id="school-assistance-amount"><span class="required">*</span></label>
                  <input type="text" class="form-control" onkeyup="onInputChange(this,'school-assistance-amount');"  name="assistance-amount" @if($assistance)value="{{$assistance->assistance_amount}}"@endif>
                </div>
                <div class="col-sm-6">
                  <label for="w2-email" class="control-label">For how long<span class="required">*</span></label>
                  <input type="text" class="form-control" name="assistance-duration" @if($assistance)value="{{$assistance->assistance_duration}}"@endif>
                </div>
              </div>

              <div class="row form-group">
                <div class="col-sm-12">
                  <p>Note: It is the OBLIGATION of the student to work hard in order to obtain better RESULTS. PARENTS should monitor their CHILDREN'S performance and regularly send us their results. Students who keep on failing year after year can bepenalized by SSC either by SUSPENSION of fee assistance or reduction in the assistance</p>
                </div>
                <div class="col-sm-12">  
                  <label>
                    <input type="checkbox" name="agree" value="1" @if($assistance && $assistance->agree=='1') checked @endif>
                    I certify the above information is true and I allow the Jamaat to verify in which ever way it beems it.
                  </label>
                </div>
              </div>
              <div class="row form-group ">
                    <div class="col-sm-12">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </div>
                <div class="progress">
                  <progress id="progressBar" value="0" max="100" style="width:300px;"></progress>
                  <h3 id="status"></h3>
                  <p id="loaded_n_total"></p>
              </div>

            </form>
          </div>
        </div>
      </div>

    </div>

  </div>

</div>

@endsection

@section('page-scripts')

<script src="{{asset('/vendor/twitter-bootstrap-wizard/jquery.bootstrap.wizard.js')}}"></script>
<script src="{{asset('/vendor/jquery-validation/jquery.validate.min.js')}}"></script>
<script src="{{asset('/javascripts/examples/forms/wizard.js')}}"></script>


<script type="text/javascript">
    $('#school-assistance-form').submit(function(event) {
        event.preventDefault();
        var form = $(this);
        var formData = new FormData(this);
        formData.append('id', form.data('id'));

        $.ajax({
          xhr: function() {
              var xhr = new window.XMLHttpRequest();
              xhr.upload.addEventListener("progress", progressHandler, false);
              xhr.addEventListener("load", completeHandler, false);
              xhr.addEventListener("error", errorHandler, false);
              xhr.addEventListener("abort", abortHandler, false);

              return xhr;
          },
            url: '{{url('/school-assistance/submit')}}',
            type: 'POST',
            dataType: 'json',
            data: formData,
            cache: false,
            processData: false,
            contentType: false
        })
        .done(function() {
            console.log("success");
        })
        .fail(function() {
            console.log("error");
        });

        function _(el) {
            return document.getElementById(el);
        }

        function progressHandler(event) {
            _("loaded_n_total").innerHTML = "Uploaded " + event.loaded + " bytes of " + event.total;
            var percent = (event.loaded / event.total) * 100;
            _("progressBar").value = Math.round(percent);
            _("status").innerHTML = Math.round(percent) + "% uploaded... please wait";
        }

        function completeHandler(event) {
            _("status").innerHTML = event.target.responseText;
            _("progressBar").value = 0;
        }

        function errorHandler(event) {
            _("status").innerHTML = "Upload Failed";
        }

        function abortHandler(event) {
            _("status").innerHTML = "Upload Aborted";
        }
        

    });
</script>
@endsection