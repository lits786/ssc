@extends('layouts.main')



@section('content')

<div class="content-header">

    <div class="leftside-content-header">

        <ul class="breadcrumbs">

            <li><i class="fa fa-home" aria-hidden="true"></i><a href="#">Voucher</a></li>

        </ul>

    </div>

</div>

<div class="row animated fadeInUp">

    <div class="col-sm-12 col-lg-12">

        <div class="row">
            <div class="col-sm-12 col-md-12">
                {{-- <h4 class="section-subtitle">Interview</h4> --}}
                <div class="panel">
                    <div class="panel-content">
                        <div class="row">
                            <div class="col-md-12">
                                <form id="voucher-form" data-id="{{$voucher_id}}">                                    
                                    <div class="row form-group ">
                                        <div class="col-sm-6">
                                                <label for="w2-username" class="control-label">Date<span class="required">*</span></label>
                                                <div class="input-group">
                                                    <span class="input-group-addon x-primary"><i class="fa fa-calendar"></i></span>
                                                <input type="text" class="form-control datepicker" name="date" @if($voucher)value="{{$voucher->date->format('d/m/Y')}}"@else value="{{Carbon\Carbon::now()->format('d/m/Y')}}" @endif>
                                                </div>
                                            </div>
                                        <div class="col-sm-6">
                                            <label for="">File No.</label>
                                            <input type="text" class="form-control" name="file_no" value="{{Auth::user()->active_file}}" disabled>
                                        </div>
                                    </div>
                                    <div class="row form-group "> 
                                        <div class="col-sm-6">
                                            <label for="">Amount<span id="add-voucher"></label>
                                            <input type="number" class="form-control" onkeyup="onInputChange(this,'add-voucher');"  name="amount" @if($voucher)value="{{$voucher->amount}}"@endif>
                                        </div>
                                        <div class="col-sm-6">
                                            <label for="">Payable to</label>
                                            <input type="text" class="form-control" name="payable_to" @if($voucher)value="{{$voucher->payable_to}}"@endif>
                                        </div>
                                    </div>
                                    <div class="row form-group ">
                                        <div class="col-sm-6">
                                            <label for="">In respect of</label>
                                            <input type="text" class="form-control" name="in_respect_of" @if($voucher)value="{{$voucher->in_respect_of}}"@endif>
                                        </div>
                                        <div class="col-sm-6">
                                            <label for="">Amount in words</label>
                                            <input type="text" class="form-control" name="amount_in_words" @if($voucher)value="{{$voucher->amount_in_words}}"@endif>
                                        </div>
                                    </div> 
                                    <div class="row form-group ">
                                        <div class="col-sm-6">
                                            <label for="">Prepared by</label>
                                            <input type="text" class="form-control" name="prepared_by" @if($voucher)value="{{$voucher->prepared_by}}"@endif>
                                        </div> 
                                        <div class="col-sm-6">
                                            <label for="">Authorised</label>
                                            <input type="text" class="form-control" name="authorised" @if($voucher)value="{{$voucher->authorised}}"@endif>
                                        </div>
                                    </div> 
                                    <div class="row form-group ">
                                        <div class="col-sm-6">
                                            <label for="">Received</label>
                                            <input type="text" class="form-control" name="received" @if($voucher)value="{{$voucher->received}}"@endif>
                                        </div>
                                        <div class="col-sm-6">
                                            <label for="">Reference</label>
                                            <input type="text" class="form-control" name="reference" @if($voucher)value="{{$voucher->reference}}"@endif>
                                        </div>
                                    </div>     
                                    <div class="row form-group ">
                                        <div class="col-sm-12">
                                            <button type="submit" class="btn btn-primary">Submit</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>

</div>

@endsection


@section('page-scripts')
<script type="text/javascript">
    jQuery('.datepicker').datepicker({format: "d/m/yyyy",});
</script>

<script type="text/javascript">
    $('#voucher-form').submit(function(event) { 

        event.preventDefault();
        var form = $(this);
        var formData = new FormData(this);
        formData.append('id', form.data('id'));

        $.ajax({
            url: '{{url('/voucher/submit')}}',
            type: 'POST',
            dataType: 'json',
            data: formData,
            cache: false,
            contentType: false,
            processData: false
        })
        .done(function() {
            console.log("success");
            window.location.href= '{{url('/vouchers/all')}}';
        })
        .fail(function(error){
            console.log(error);
        });
        
    });
</script>
@endsection