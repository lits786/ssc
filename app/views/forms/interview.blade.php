@extends('layouts.main')



@section('content')

<div class="content-header">

    <div class="leftside-content-header">

        <ul class="breadcrumbs">

            <li><i class="fa fa-home" aria-hidden="true"></i><a href="#">Interview</a></li>

        </ul>

    </div>

</div>

<div class="row animated fadeInUp">

    <div class="col-sm-12 col-lg-12">

        <div class="row">
            <div class="col-sm-12 col-md-12">
                {{-- <h4 class="section-subtitle">Interview</h4> --}}
                <div class="panel">
                    <div class="panel-content">
                        <div class="row">
                            <div class="col-md-12">
                                <form id="interview-form" data-id="{{$interview_id}}">                                    
                                    <div class="row form-group ">
                                        <div class="col-sm-6">
                                                <label for="w2-username" class="control-label">Date<span class="required">*</span></label>
                                                <div class="input-group">
                                                    <span class="input-group-addon x-primary"><i class="fa fa-calendar"></i></span>
                                                    <input type="text" class="form-control datepicker" name="date" @if($interview)value="{{$interview->date->format('d/m/Y')}}"@endif>
                                                </div>
                                            </div>
                                        <div class="col-sm-6">
                                            <label for="">Department</label>
                                            <input type="text" class="form-control" name="department" @if($interview)value="{{$interview->department}}"@endif>
                                        </div>
                                    </div>
                                    <div class="row form-group ">
                                        {{-- <div class="col-sm-6">
                                            <label for="">File No.</label>
                                            <input type="text" class="form-control" name="file-no" @if($interview)value="{{$interview->file_no}}"@endif>
                                        </div> --}}
                                        <div class="col-sm-6">
                                            <label for="">Interview with</label>
                                            <input type="text" class="form-control" name="interview-with" @if($interview)value="{{$interview->interview_with}}"@endif>
                                        </div>
                                        <div class="col-sm-6">
                                            <label for="">Tel</label>
                                            <input type="text" class="form-control" name="telephone" @if($interview)value="{{$interview->telephone}}"@endif>
                                        </div>
                                    </div>
                                    <div class="row form-group ">
                                        <div class="col-sm-6">
                                            <label for="">Family</label>
                                            <input type="text" class="form-control" name="family" @if($interview)value="{{$interview->family}}"@endif>
                                        </div>
                                        <div class="col-sm-6">
                                            <label for="">Employer</label>
                                            <input type="text" class="form-control" name="employer" @if($interview)value="{{$interview->employer}}"@endif>
                                        </div>
                                    </div> 
                                    <div class="row form-group ">
                                        <div class="col-sm-6">
                                            <label for="">Residence</label>
                                            <input type="text" class="form-control" name="residence" @if($interview)value="{{$interview->residence}}"@endif>
                                        </div> 
                                        <div class="col-sm-6">
                                            <label for="">Interviewer</label>
                                            <input type="text" class="form-control" name="interviewer" @if($interview)value="{{$interview->interviewer}}"@endif>
                                        </div>
                                    </div> 
                                    <div class="row form-group ">
                                        <div class="col-sm-6">
                                            <label for="">Capacity</label>
                                            <input type="text" class="form-control" name="capacity" @if($interview)value="{{$interview->capacity}}"@endif>
                                        </div>
                                        <div class="col-sm-6">
                                            <label for="">Employed Since</label>
                                            <input type="text" class="form-control" name="employed-since" @if($interview)value="{{$interview->employed_since}}"@endif>
                                        </div>
                                    </div>
                                    <div class="row form-group ">
                                        <div class="col-sm-6">
                                            <label for="">Gross Salary<span id="interview-new-interview"></label>
                                            <input type="text" class="form-control" onkeyup="onInputChange(this,'interview-new-interview');"  name="gross-salary" @if($interview)value="{{$interview->gross_salary}}"@endif>
                                        </div>
                                        <div class="col-sm-6">
                                            <label for="">Signature</label>
                                            <input type="text" class="form-control" name="signature" @if($interview)value="{{$interview->signature}}"@endif>
                                        </div>
                                    </div>
                                    <div class="row form-group ">
                                        <div class="col-sm-12">
                                            <label for="">Assistance currently receiving</label>
                                            <textarea class="form-control" rows="3"  name="assistance-currently-receiving">@if($interview){{$interview->assistance_currently_receiving}}@endif</textarea>
                                        </div>
                                        <div class="col-sm-12">
                                            <label for="">Issues discussed</label>
                                            <textarea class="form-control" rows="3" name="issues-discussed">@if($interview){{$interview->issues_discussed}}@endif</textarea>
                                        </div>
                                        <div class="col-sm-12">
                                            <label for="">conclusion</label>
                                            <textarea class="form-control" rows="3" name="conclusion">@if($interview){{$interview->conclusion}}@endif</textarea>
                                        </div>

                                    </div>                                    

                                    <div class="row form-group ">
                                        <div class="col-sm-6">
                                            <label for="">Task allocated to</label>
                                            <input type="text" class="form-control" name="task_allocation" @if($interview)value="{{$interview->task_allocation}}"@endif>
                                        </div>
                                        <div class="col-sm-6">
                                            <label for="" class="control-label">Next interview Date</label>
                                            <div class="input-group">
                                                <span class="input-group-addon x-primary"><i class="fa fa-calendar"></i></span>
                                                <input type="text" class="form-control datepicker" name="next_interview" @if($interview)value="{{$interview->next_interview->format('d/m/Y')}}"@endif>
                                            </div>
                                        </div>
                                    </div>

                                    <h2>Outstanding</h2>
                                    <div class="row form-group ">
                                        <div class="col-sm-6">
                                            <label for="">Lawajam</label>
                                            <input type="text" class="form-control" name="lawajam" @if($interview)value="{{$interview->lawajam}}"@endif>
                                        </div>
                                        <div class="col-sm-6">
                                            <label for="">Jamaat Rent<span id="interview-jamaat-rent"></label>
                                            <input type="text" class="form-control"  onkeyup="onInputChange(this,'interview-jamaat-rent');"  name="jamaat_rent" @if($interview)value="{{$interview->jamaat_rent}}"@endif>
                                        </div>
                                        <div class="col-sm-6">
                                            <label for="">School Fees<span id="interview-school-fees"></label>
                                            <input type="text" class="form-control"  onkeyup="onInputChange(this,'interview-school-fees');"  name="school_fees" @if($interview)value="{{$interview->school_fees}}"@endif>
                                        </div>
                                    </div>
                                    <div class="row form-group ">
                                        <div class="col-sm-12">
                                            <label for="">Remarks</label>
                                            <textarea class="form-control" rows="3"  name="remarks">@if($interview){{$interview->remarks}}@endif</textarea>
                                        </div>
                                    </div>

                                    <div class="row form-group" style="visibility:hidden">
                                        <div class="col-sm-12">
                                            <label for="left-addon-icon" class="col-sm-2 control-label">Concluded?</label>
                                            <div class="radio">
                                                <label>
                                                    <input type="radio" value="yes" name="concluded" @if($interview && $interview->concluded=='yes') checked @endif> Yes
                                                </label>
                                                <label>
                                                    <input type="radio" value="no" name="concluded" @if($interview && $interview->concluded=='no') checked @endif> No
                                                </label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row form-group ">
                                        <div class="col-sm-12">
                                            <button type="submit" class="btn btn-primary">Submit</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>

</div>

@endsection


@section('page-scripts')
<script type="text/javascript">
    jQuery('.datepicker').datepicker({format: "d/m/yyyy",});
</script>

<script type="text/javascript">
    $('#interview-form').submit(function(event) {

        event.preventDefault();
        var form = $(this);
        var formData = new FormData(this);
        formData.append('id', form.data('id'));

        $.ajax({
            url: '{{url('/interview/submit')}}',
            type: 'POST',
            dataType: 'json',
            data: formData,
            cache: false,
            contentType: false,
            processData: false
        })
        .done(function() {
            console.log("success");
            window.location.href= '{{url('/interview/all')}}';
        })
        .fail(function() {
            console.log("error");
        });
        
    });
</script>
@endsection