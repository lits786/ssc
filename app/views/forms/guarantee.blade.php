@extends('layouts.main')



@section('content')


<div class="content-header">

    <div class="leftside-content-header">

        <ul class="breadcrumbs">

            <li><i class="fa fa-home" aria-hidden="true"></i><a href="#">Guarantee</a></li>

        </ul>

    </div>

</div>

<div class="row animated fadeInUp">

    <div class="col-sm-12 col-lg-12">

        <div class="row">
            <div class="col-sm-12 col-md-12">
                {{-- <h4 class="section-subtitle">Interview</h4> --}}
                <div class="panel">
                    <div class="panel-content">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="tabs">
                                    <ul class="nav nav-tabs">
                                        <li class="active"><a href="#guarantee" data-toggle="tab">new Guarantee</a></li>
                                        <li><a href="#all-guarantees" data-toggle="tab">All Guarantees</a></li>
                                    </ul>
                                    <div class="tab-content">
                                        <div class="tab-pane fade in active" id="guarantee">
                        <form id="guarantee-form" data-id="{{$id}}">
                            <div class="row">
                                <div class="col-sm-6">
                                    <label class="control-label">Loan Code<span class="required">*</span></label>
                                    <select class="form-control"  name="loan-code" required>
                                            <option value="">--select loan code--</option>
                                        @foreach($loan_codes as $code)
                                            <option value="{{$code}}" @if($id && $guarantee->loan_id == $code) selected @endif>{{$code}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-sm-6">
                                    <label for="w2-username" class="control-label">Full Name<span class="required">*</span></label>
                                    <input type="text" class="form-control"  name="full-name" @if($id)value="{{$guarantee->full_name}}"@endif>
                                </div>
                                <div class="col-sm-6">
                                    <label for="w2-username" class="control-label">Postal Address<span class="required">*</span></label>
                                    <input type="text" class="form-control"  name="postal-address" @if($id)value="{{$guarantee->postal_address}}"@endif>
                                </div>
                                <div class="col-sm-6">
                                    <label for="w2-username" class="control-label">Telephone<span class="required">*</span></label>
                                    <input type="text" class="form-control"  name="telephone" @if($id)value="{{$guarantee->telephone}}"@endif>
                                </div>
                                <div class="col-sm-6">
                                    <label for="w2-username" class="control-label">Email<span class="required">*</span></label>
                                    <input type="text" class="form-control"  name="email" @if($id)value="{{$guarantee->email}}"@endif>
                                </div>
                                <div class="col-sm-6">
                                    <label for="w2-username" class="control-label">Loan Purpose<span class="required">*</span></label>
                                    <input type="text" class="form-control"  name="loan-purpose" @if($id)value="{{$guarantee->loan_purpose}}"@endif>
                                </div>
                                <div class="col-sm-6">
                                    <label for="w2-username" class="control-label">Borrower's Full Name<span class="required">*</span></label>
                                    <input type="text" class="form-control"  name="borrowers-full-name" @if($id)value="{{$guarantee->borrowers_full_name}}"@endif>
                                </div>
                                <div class="col-sm-6">
                                    <label for="w2-username" class="control-label">Borrower's Postal Address<span class="required">*</span></label>
                                    <input type="text" class="form-control"  name="borrowers-postal-address" @if($id)value="{{$guarantee->borrowers_postal_address}}"@endif>
                                </div>
                                <div class="col-sm-6">
                                    <label for="w2-username" class="control-label">Loan Amount<span class="required">*</span></label>
                                    <input type="text" class="form-control"  name="loan-amount" @if($id)value="{{$guarantee->loan_amount}}"@endif>
                                </div>
                                <div class="col-sm-6">
                                    <label for="" class="control-label">Date of Loan Agreement<span class="required">*</span></label>
                                    <div class="input-group">
                                        <span class="input-group-addon x-primary"><i class="fa fa-calendar"></i></span>
                                        <input type="text" class="form-control datepicker"  name="date-of-loan-agreement" @if($id)value="{{$guarantee->date_of_loan_agreement->format('m/d/Y')}}"@endif>
                                    </div>
                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col-sm-12">
                                    <p>I, the undersigned hereby guarantee to the Lender the due and punctual performance and the observance by the Borrower of the terms of the Loan Agreement specified made between the Lender and the Borrower and confirm acknowledge and undertake that in case the borrower fails to pay the said loan to the Lender in full on and in accordance withthe provisions of the Loan Agreement aforesaid, I as a primary debtor will settle the outstanding balance in full.<br/>
                                    I hereby for hte same consideration and as primary obligor and not merely as surely hereby covenant and agrees to idemnify (on a full and unqualified indemnity basis) the Lender, forthwith on demand being made in writing by the Lender upon me against the amount of all costs charges liabilities taxs and expenses now or hereafter incurred by the Lender (and irrespective of whether the same may be recoverable from the Borrower) in enforcing or attempting to enforce the payment or discharge othewise that under this Guarantee of all or any of the amounts due from the Borrower or of enforcing or atttempting to enforce any other guarantee in respect thereof or any security therefore.</p>
                                </div>
                                <div class="col-sm-12">    
                                    <label>
                                        <input type="checkbox" name="agree" value="1" @if($id) checked @endif>
                                        I've read and understood all the terms and agree to adhere
                                    </label>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-6">
                                    <label for="w2-username" class="control-label">Witnessed by (Full Name)<span class="required">*</span></label>
                                    <input type="text" class="form-control"  name="witness-full-name" @if($id)value="{{$guarantee->witness_full_name}}"@endif>
                                </div>
                                <div class="col-sm-6">
                                    <label for="w2-username" class="control-label">Address<span class="required">*</span></label>
                                    <input type="text" class="form-control"  name="witness-address" @if($id)value="{{$guarantee->witness_address}}"@endif>
                                </div>
                            </div>

                            <div class="row submit-div">
                                <div class="col-sm-12">
                                    <a class="btn btn-primary ml" style="float:right;" href="{{url('/forms/guarantee')}}">Cancel</a>
                                    <button class="btn btn-primary" type="submit">Submit</button>

                                </div>
                            </div>

                        </form>
                                        </div>

                                        <div class="tab-pane fade" id="all-guarantees">
                                            <table class="table table-hover table-striped" id="all_guarantees_table">
                                                <thead>
                                                    <tr>
                                                        <th>LOAN CODE</th>
                                                        <th>ACTIONS</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>

        </div>

    </div>

</div>

@endsection

@section('page-scripts')

<script src="{{asset('/vendor/twitter-bootstrap-wizard/jquery.bootstrap.wizard.js')}}"></script>
<script src="{{asset('/vendor/jquery-validation/jquery.validate.min.js')}}"></script>
<script src="{{asset('/javascripts/examples/forms/wizard.js')}}"></script>

<script type="text/javascript">
    jQuery('.datepicker').datepicker({format: "d/m/yyyy",});
</script>

<script type="text/javascript">
    var guarantee_form = $('#guarantee-form');
   
    guarantee_form.on('submit', function( event ) {
        event.preventDefault();
        var form = $(this);
        var formData = new FormData(this);
        formData.append('id', form.data('id'));

        $.ajax({
            url: '{{url('/loan-guarantee/save')}}',
            type: 'POST',
            dataType: 'json',
            data: formData,
            cache: false,
            contentType: false,
            processData: false
        })
        .done(function() {
            console.log("success");
            window.location.href="{{url('/forms/guarantee')}}";
            successNotification();
        })
        .fail(function() {
            console.log("error");
        });
    });
</script>

<script type="text/javascript">
    var all_guarantees_table =  $('#all_guarantees_table');
    all_guarantees_table = all_guarantees_table.DataTable({ 
        "scrollX": true,               
        "processing": true,
        "serverSide": true,
        "ajax": "{{url('/loan-guarantee/all-guarantees-table')}}",
        "pageLength":25,
        "ordering":false,
        "bAutoWidth": false,
        "oSearch": {"sSearch": ''},
        "columns": [
            {data: 'loan_id', name: 'loan_id'},
            // {data: 'currency', name: 'currency'},
            // {data: 'amount', name: 'amount'},
            // {data: 'remarks', name: 'remarks'},            
            {data: 'actions', name: 'actions',orderable: false, searchable: false}
        ]
    });


    all_guarantees_table.on('draw', function(event) {
        all_guarantees_table.rows().every( function ( rowIdx, tableLoop, rowLoop ) {
            $(this.node()).find('.delete').on('click', function(event) {
                event.preventDefault(); 
                var link = $(this);                  
                var id = link.data('id');

                $.ajax({
                    url: '{{url('/dawasco/destroy')}}',
                    type: 'POST',
                    dataType: 'json',
                    data: {id: id},
                })
                .done(function() {
                    all_guarantees_table.draw(false);
                })
                .fail(function() {
                    console.log("error");
                });
                
            });


        });
        // all_guarantees_table.row.add({
        //         'date': 1,
        //         'amount': 2,
        //         'currency': 3,
        //         'remarks':4
        //     }).draw( false );
    });
</script>

@endsection 