@extends('layouts.main')



@section('content')

<div class="content-header">

  <div class="leftside-content-header">

    <ul class="breadcrumbs">

      <li><i class="fa fa-home" aria-hidden="true"></i><a href="#">Maid Support Form {{Session::get('file-number', '')}}</a></li>

    </ul>

  </div>

</div>

<div class="row animated fadeInUp">

  <div class="col-sm-12 col-lg-12">

    <div class="row">
      <div class="col-sm-12 col-md-12">
        {{-- <h4 class="section-subtitle">Interview</h4> --}}
        <div class="panel">
            <div class="panel-content">
                <div class="tabs">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#add-maid-support" data-toggle="tab">Add Maid Support</a></li>
                        <li><a href="#upload-maid-support-file" data-toggle="tab">Upload maid support file</a></li>
                        <li><a href="#all-maid-support" data-toggle="tab">All Maid Support</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane fade in active" id="add-maid-support">
                            <form id="maid-support-form" data-id="{{$maid_support_id}}" enctype="multipart/form-data">
                	            <div class="row form-group">
                	                <div class="col-sm-6">
                	                  <label class="control-label">Date<span class="required">*</span></label>
                	                  <div class="input-group">
                                            <span class="input-group-addon x-primary"><i class="fa fa-calendar"></i></span>
                                            <input type="text" class="form-control datepicker"  name="date" @if($maid_support)value="{{$maid_support->date->format('d/m/Y')}}"@endif>
                                        </div>
                	                </div>
                	                <div class="col-sm-6">
                	                  <label class="control-label">Attach File<span class="required">*</span></label>
                	                  <input type="file" class="form-control-file" name='files[]' aria-describedby="fileHelp" multiple>
                	                </div>
                                </div>
                                <div class="row form-group">
                	                <div class="col-sm-6">
                	                    <label class="control-label">Currency<span class="required">*</span></label>
                	                    <select class="form-control" name="currency" required>
                                            <option value="">--select currency--</option>
                                            @foreach($currencies as $currency)
                                                <option value="{{$currency->id}}" @if($maid_support && $maid_support->currency_id == $currency->id) selected @endif>{{$currency->name}}</option>
                                            @endforeach                         
                                        </select>
                	                </div>
                	                <div class="col-sm-6">
                	                    <label class="control-label">Total amount<span class="required">*<span id="maid-support-total-amount"></span></label>
                	                    <input type="text" class="form-control" onkeyup="onInputChange(this,'maid-support-total-amount');"  name="total-amount" @if($maid_support)value="{{$maid_support->total_amount}}"@endif required>
                	                </div>
                                  <div class="col-sm-6">
                                      <label class="control-label">Self Contribution<span class="required">*<span id="maid-support-self-contribution"></span></label>
                                      <input type="text" class="form-control" onkeyup="onInputChange(this,'maid-support-self-contribution');"  name="self-contribution" @if($maid_support)value="{{$maid_support->self_contribution}}"@endif required>
                                  </div>
                                  <div class="col-sm-6">
                                      <label class="control-label">SSC Contribution<span class="required">*<span id="maid-support-ssc-contribution"></span></label>
                                      <input type="text" class="form-control" onkeyup="onInputChange(this,'maid-support-ssc-contribution');"  name="ssc-contribution" @if($maid_support)value="{{$maid_support->ssc_contribution}}"@endif required>
                                  </div>
                	                <div class="col-sm-6">
                	                    <label class="control-label">Remarks<span class="required">*</span></label>
                	                    <input type="text" class="form-control" name="remarks" @if($maid_support)value="{{$maid_support->remarks}}"@endif>
                	                </div>
                	            </div>

                                <div class="row form-group" style="visibility:hidden">
                                    <div class="col-sm-12">
                                        <label for="left-addon-icon" class="col-sm-2 control-label">Concluded?</label>
                                        <div class="radio">
                                            <label>
                                                <input type="radio" value="yes" name="concluded" @if($maid_support && $maid_support->concluded=='yes') checked @endif> Yes
                                            </label>
                                            <label>
                                                <input type="radio" value="yes" name="concluded" @if($maid_support && $maid_support->concluded=='no') checked @endif> No
                                            </label>
                                        </div>
                                    </div>
                                </div>
                              
                                <div class="row form-group ">
                                    <div class="col-sm-12">
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                    </div>
                                </div>
                                <div class="progress">
                                    <progress id="progressBar" value="0" max="100" style="width:300px;"></progress>
                                    <h3 id="status"></h3>
                                    <p id="loaded_n_total"></p>
                                </div> 
                            </form>
                        </div>
                        <div class="tab-pane fade" id="upload-maid-support-file">
                            <div class="row">
                                <div class="col-sm-12">
                                    <form id="upload-file-form" data-id="0" enctype="multipart/form-data">                                
                                      <div class="row form-group">
                                          <div class="col-sm-6">
                                              <label>Title</label>
                                              <input type="text" class="form-control" name="title" required>
                                          </div>
                                          <div class="col-sm-6">
                                            <label class="control-label">Date<span class="required">*</span></label>
                                            <div class="input-group">
                                              <span class="input-group-addon x-primary"><i class="fa fa-calendar"></i></span>
                                              <input type="text" class="form-control datepicker" name="date" required>
                                            </div>
                                          </div>
                                          <div class="col-sm-6">
                                              <label for="exampleInputFile">Attach File</label>
                                              <input type="file" class="form-control-file" name="file" aria-describedby="fileHelp" required>
                                          </div>
                                      </div>

                                      <div class="row form-group ">
                                          <div class="col-sm-12">
                                              <button type="submit" class="btn btn-primary">Submit</button>
                                          </div>
                                      </div>
                                      <div class="progress">
                                        <progress id="progressBar" value="0" max="100" style="width:300px;"></progress>
                                        <h3 id="status"></h3>
                                        <p id="loaded_n_total"></p>
                                    </div> 
                                    </form>
                                </div>
                            </div>

                            <div class="row" style="margin-top:100px;">
                              <div class="col-sm-12">
                                <table class="table table-hover table-striped" id="form-files-table">
                                  <thead>
                                    <tr>
                                      <th>FILE</th>
                                      <th>DATE</th>
                                      <th>ACTIONS</th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                    
                                  </tbody>
                                </table>
                              </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="all-maid-support">
                            <table class="table table-hover table-striped" id="all-maid-support-table">
                                <thead>
                                    <tr>
                                        <th>DATE</th>
                                        <th>CURRENCY</th>
                                        <th>TOTAL</th>
                                        <th>SELF CONTRIBUTION</th>
                                        <th>SSC CONTRIBUTION</th>
                                        <th>REMARKS</th>
                                        <th>ACTIONS</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    
                                </tbody>
                            </table>
                            <div class="total-statement" style="margin-top:25px;">{{$file->totalMaidSupportStatement()}}</div>
                        </div>
                    </div>
                </div>
          </div>
        </div>
      </div>

    </div>

  </div>

</div>

@endsection

@section('page-scripts')

<script src="{{asset('/vendor/twitter-bootstrap-wizard/jquery.bootstrap.wizard.js')}}"></script>
<script src="{{asset('/vendor/jquery-validation/jquery.validate.min.js')}}"></script>
<script src="{{asset('/javascripts/examples/forms/wizard.js')}}"></script>

<script type="text/javascript">
    jQuery('.datepicker').datepicker({format: "d/m/yyyy",});
</script>


<script type="text/javascript">
    $('#maid-support-form').submit(function(event) {
        event.preventDefault();
        var form = $(this);
        var formData = new FormData(this);
        formData.append('id', form.data('id'));

        $.ajax({
            xhr: function() {
                var xhr = new window.XMLHttpRequest();
                xhr.upload.addEventListener("progress", progressHandler, false);
                xhr.addEventListener("load", completeHandler, false);
                xhr.addEventListener("error", errorHandler, false);
                xhr.addEventListener("abort", abortHandler, false);

                return xhr;
            }, 
            url: '{{url('/maid-support/save')}}',
            type: 'POST',
            dataType: 'json',
            data: formData,
            cache: false,
            processData: false,
            contentType: false
        })
        .done(function(feedback) {
            resetMaidSupportForm();
            toastr.success('success', 'STATUS');
            window.location.href='{{url('/forms/maid-support')}}';
            console.log("success");
        })
        .fail(function() {
            console.log("error");
        });
        function _(el) {
            return document.getElementById(el);
        }

        function progressHandler(event) {
            _("loaded_n_total").innerHTML = "Uploaded " + event.loaded + " bytes of " + event.total;
            var percent = (event.loaded / event.total) * 100;
            _("progressBar").value = Math.round(percent);
            _("status").innerHTML = Math.round(percent) + "% uploaded... please wait";
        }

        function completeHandler(event) {
            _("status").innerHTML = event.target.responseText;
            _("progressBar").value = 0;
        }

        function errorHandler(event) {
            _("status").innerHTML = "Upload Failed";
        }

        function abortHandler(event) {
            _("status").innerHTML = "Upload Aborted";
        }
        

    });

    function resetMaidSupportForm(){
        var form = $('#maid-support-form');
        form.trigger('reset');
        form.find('input').val('');
        form.data('id',0);
    }
</script>

<script type="text/javascript">
    var all_maid_support_table =  $('#all-maid-support-table');
    all_maid_support_table = all_maid_support_table.DataTable({ 
        "scrollX": true,               
        "processing": true,
        "serverSide": true,
        "ajax": "{{url('/maid-support/all-maid-support-data-table')}}",
        "pageLength":25,
        "ordering":false,
        "bAutoWidth":false,
        "oSearch": {"sSearch": ''},
        "columns": [
            {data: 'date', name: 'date'},
            {data: 'currency', name: 'currencies.name'},
            {data: 'total_amount', name: 'total_amount'},
            {data: 'self_contribution', name: 'self_contribution'},
            {data: 'ssc_contribution', name: 'ssc_contribution'},
            {data: 'remarks', name: 'remarks'},            
            {data: 'actions', name: 'actions',orderable: false, searchable: false}
            // {data: 'search_slug', name: "search_slug", visible: false, searchable:true}
        ]
    });


    all_maid_support_table.on('draw', function(event) {
        all_maid_support_table.rows().every( function ( rowIdx, tableLoop, rowLoop ) {
            $(this.node()).find('.delete').on('click', function(event) {
                event.preventDefault(); 
                var link = $(this);                  
                var id = link.data('id');

                $.ajax({
                    url: '{{url('/maid-support/destroy')}}',
                    type: 'POST',
                    dataType: 'json',
                    data: {id: id},
                })
                .done(function() {
                    all_maid_support_table.draw(false);
                })
                .fail(function() {
                    console.log("error");
                });
                
            });

            $(this.node()).find('.history').on('click', function(event) {
              event.preventDefault(); 
              var link = $(this);
              showHistory(link);                            
            });


        });
        // all_maid_support_table.row.add({
        //         'date': 1,
        //         'amount': 2,
        //         'currency': 3,
        //         'remarks':4
        //     }).draw( false );
    });
</script>

<script type="text/javascript">
  var file_form = $('#upload-file-form');

  file_form.submit(function(event) {
    event.preventDefault();
    var formData = new FormData(this);
    $.ajax({
        xhr: function() {
                var xhr = new window.XMLHttpRequest();
                xhr.upload.addEventListener("progress", progressHandler, false);
                xhr.addEventListener("load", completeHandler, false);
                xhr.addEventListener("error", errorHandler, false);
                xhr.addEventListener("abort", abortHandler, false);

                return xhr;
            }, 
      url: '{{url('/maid-support/upload-file')}}',
      type: 'POST',
      dataType: 'json',
      data: formData,
      cache: false,
      contentType: false,
      processData: false
    })
    .done(function(feedback) {
        file_form.trigger('reset');
        files_table.draw(false);
      console.log("success");
    })
    .fail(function() {
      console.log("error");
    });
    function _(el) {
        return document.getElementById(el);
    }

    function progressHandler(event) {
        _("loaded_n_total").innerHTML = "Uploaded " + event.loaded + " bytes of " + event.total;
        var percent = (event.loaded / event.total) * 100;
        _("progressBar").value = Math.round(percent);
        _("status").innerHTML = Math.round(percent) + "% uploaded... please wait";
    }

    function completeHandler(event) {
        _("status").innerHTML = event.target.responseText;
        _("progressBar").value = 0;
    }

    function errorHandler(event) {
        _("status").innerHTML = "Upload Failed";
    }

    function abortHandler(event) {
        _("status").innerHTML = "Upload Aborted";
    }
    
  });
</script>

<script type="text/javascript">
  var files_table = $('#form-files-table');
 files_table = files_table.DataTable({        
    "processing": true,
    "serverSide": true,
    "ajax": "{{url('/maid-support/all-form-files-table')}}",
    "pageLength":25,
    "ordering":false,
    "bAutoWidth": false,
    "oSearch": {"sSearch": ''},
    "columns": [
      {data: 'title', name: 'title'},
      {data: 'date', name: 'date'},
      {data: 'actions', name: 'actions',orderable: false, searchable: false}
    ]
  });
  files_table.on('draw', function(event) {
    files_table.rows().every( function ( rowIdx, tableLoop, rowLoop ) {
      $(this.node()).find('.delete').on('click', function(event) {
        event.preventDefault(); 
        var link = $(this);         
        var href = link.attr('href');
        $.ajax({
          url: href,
          type: 'POST',
          dataType: 'json',
          data:{},
        })
        .done(function() {
          toastr.success('success', 'STATUS');          
          files_table.draw(false);
        })
        .fail(function() {
          console.log("error");
        });
        
      });
    });
  });
</script>
@endsection