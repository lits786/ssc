@extends('layouts.main')



@section('content')

<div class="content-header">

    <div class="leftside-content-header">

        <ul class="breadcrumbs">

            <li><i class="fa fa-home" aria-hidden="true"></i><a href="#">Business Plan</a></li>

        </ul>

    </div>

</div>

<div class="row animated fadeInUp">

    <div class="col-sm-12 col-lg-12">

        <div class="row">
            <div class="col-sm-12 col-md-12">
                {{-- <h4 class="section-subtitle">Interview</h4> --}}
                <div class="panel">
                    <div class="panel-content">
                        <div class="row">
                                <div class="col-md-12">
                                    <div class="tabs">
                                        <ul class="nav nav-tabs">
                                            <li class="active"><a href="#new-plan" data-toggle="tab">New Business Plan</a></li>
                                            <li><a href="#all-plans" data-toggle="tab">All Business Plans</a></li>
                                        </ul>
                                        <div class="tab-content">
                                            <div class="tab-pane fade in active" id="new-plan">
                        <div class="form-wizard wizard-basic wizard-block wizard-list-tabs">
                            <form id="business-plan-form" data-id="{{$id}}">
                                <div class="tab-steps">
                                    <ul>
                                        <li class="active"><a href="#w2-tab1" data-toggle="tab">You and Business</a></li>
                                        <li><a href="#w2-tab2" data-toggle="tab">The Business</a></li>
                                        <li><a href="#w2-tab3" data-toggle="tab">About You</a></li>
                                        <li><a href="#w2-tab4" data-toggle="tab">Products & Services</a></li>
                                        <li><a href="#w2-tab5" data-toggle="tab">The market</a></li>
                                        <li><a href="#w2-tab6" data-toggle="tab">Cost of operation</a></li>
                                        <li><a href="#w2-tab7" data-toggle="tab">The competition</a></li>
                                        <li><a href="#w2-tab8" data-toggle="tab">SWOT</a></li>
                                        <li><a href="#w2-tab9" data-toggle="tab">Marketing plan</a></li>

                                    </ul>
                                </div>
                                <div class="tab-content">
                                    <div class="tab-pane active" id="w2-tab1">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <label class="control-label">Loan Code<span class="required">*</span></label>
                                                <input type="text" class="form-control"  name="loan-code" @if($id)value="{{$plan->loan_id}}"@endif required>
                                            </div>
                                        </div>
                                        {{-- <div class="">
                                            <p>Lets get started with some basic information about your business and you.</p>
                                        </div> --}}
                                        <div class="row form-group">
                                            <div class="col-sm-6">
                                                <label for="" class="control-label">Business Name<span class="required">*</span></label>
                                                <input type="text" class="form-control"  name="business-name" @if($id)value="{{$plan->business_name}}"@endif>
                                            </div>
                                            <div class="col-sm-6">
                                                <label for="" class="control-label">Business Location<span class="required">*</span></label>
                                                <input type="text" class="form-control"  name="business-location" @if($id)value="{{$plan->business_location}}"@endif>
                                            </div>
                                        </div>
                                        <div class="row form-group">                
                                            <div class="col-sm-6">
                                                <label for="" class="control-label">Business Start Date<span class="required">*</span></label>
                                                <div class="input-group">
                                                    <span class="input-group-addon x-primary"><i class="fa fa-calendar"></i></span>
                                                    <input type="text" class="form-control datepicker"  name="start-date" @if($id)value="{{$plan->start_date->format('d/m/Y')}}"@endif>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <label for="" class="control-label">Business Website<span class="required">*</span></label>
                                                <input type="text" class="form-control"  name="business-website" @if($id)value="{{$plan->business_website}}"@endif>
                                            </div>
                                            <div class="col-sm-6">
                                                <label for="" class="control-label">Other social media links<span class="required">*</span></label>
                                                <input type="text" class="form-control"  name="social-media-links" @if($id)value="{{$plan->social_media_links}}"@endif>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col-sm-12">
                                                <label for="">Academic qualifications</label>
                                                <textarea class="form-control" rows="3"  placeholder="Write a comment" name="academic-qualifications">@if($id){{$plan->academic_qualifications}}@endif</textarea>
                                            </div>                                      
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="w2-tab2">
                                        <p>Describe what the business is all about. How will it operate, what products or services will be sold, what is the vision for the company's future</p>
                                        
                                        <div class="row form-group">
                                            <div class="col-sm-12">
                                                <label for="">Give and overview of what you are going to do and how are you going to do it</label>
                                                <textarea class="form-control" name="business-activities" rows="3"  placeholder="Write a comment">@if($id){{$plan->business_activities}}@endif</textarea>
                                            </div>
                                            <div class="col-sm-12">
                                                <label for="">Describe the product(s) or service(s) the business is going to sell</label>
                                                <textarea class="form-control" name="products-services" rows="3"  placeholder="Write a comment" name="">@if($id){{$plan->products_services}}@endif</textarea>
                                            </div>
                                            <div class="col-sm-12">
                                                <label for="">Describe how the product(s) or service(s) will be sold</label>
                                                <textarea class="form-control" name="how-sell" rows="3"  placeholder="Write a comment">@if($id){{$plan->how_sell}}@endif</textarea>
                                            </div>
                                            <div class="col-sm-12">
                                                <label for="">What is the business mission (the main purpose fo the business)</label>
                                                <textarea class="form-control" name="business-mission" rows="3"  placeholder="Write a comment">@if($id){{$plan->business_mission}}@endif</textarea>
                                            </div>
                                            <div class="col-sm-12">
                                                <label for="">What is the business vision (where will the business be in one, three, and five years)</label>
                                                <p>Example 1: Year 1 - 100 customers; Year 3 - 350 customers; Year 5 - 600 customers<br/>
                                                Example 1: Year 1 - 200M in one sales; Year 3 - 40M in annual sales; Year 5 - 60M in annual sales
                                                </p>
                                                <textarea class="form-control" name="business-vision" rows="3"  placeholder="Write a comment">@if($id){{$plan->business_vision}}@endif</textarea>
                                            </div>
                                            <div class="col-sm-12">
                                                <label for="">What sets this business apart from the competition</label>
                                                <p>Is the business' product/service cheaper? Is the business prouct/service better quality? Is the business filling a hole in the market?</p>
                                                <textarea class="form-control" name="business-uniqueness" rows="3"  placeholder="Write a comment">@if($id){{$plan->business_uniqueness}}@endif</textarea>
                                            </div>
                                            <div class="col-sm-12">
                                                <label for="">How will this business be staffed</label>
                                                <textarea class="form-control" name="business-staff" rows="3"  placeholder="Write a comment">@if($id){{$plan->business_staff}}@endif</textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="w2-tab3"> 
                                        <p>Describe you background and why this business is right for you. give a short account of your personal and business background detailing the areas that are relevant to this business</p>     
                                        <div class="row form-group">                
                                            <div class="col-sm-12">
                                                <label for="">Why do you want to run you own business</label>
                                                <textarea class="form-control" name="why-run-business" rows="3"  placeholder="Write a comment">@if($id){{$plan->why_run_business}}@endif</textarea>
                                            </div>
                                            <div class="col-sm-12">
                                                <label for="">Previous work experience</label>
                                                <textarea class="form-control" name="previous-experience" rows="3"  placeholder="Write a comment">@if($id){{$plan->previous_experience}}@endif</textarea>
                                            </div>
                                            <div class="col-sm-12">
                                                <label for="">Education and Qualifications</label>
                                                <textarea class="form-control"  name="education" rows="3"  placeholder="Write a comment">@if($id){{$plan->education}}@endif</textarea>
                                            </div>
                                            <div class="col-sm-12">
                                                <label for="exampleInputFile">Providing copies of any certificates earned will strengthen you application for financing</label>
                                                <input type="file" class="form-control-file" name="" aria-describedby="fileHelp" multiple>
                                            </div>
                                            <div class="col-sm-12">
                                                <label for="">Hobbies and interests</label>
                                                <textarea class="form-control" name="hobbies" rows="3"  placeholder="Write a comment">@if($id){{$plan->hobbies}}@endif</textarea>
                                            </div>
                                            <div class="col-sm-12">
                                                <label for="">Describe your knowledge and experience within this business industry</label>
                                                <p>Link your industry knowledge back to your experience.</p>
                                                <textarea class="form-control" name="business-experience" rows="3"  placeholder="Write a comment">@if($id){{$plan->business_experience}}@endif</textarea>
                                            </div>
                                            <div class="col-sm-12">
                                                <label for="">Please list any other personal information relevant to this business</label>
                                                <p>This may include family history in this industry, existing connection with suppliers or clients and established mentor relationships etc.</p>
                                                <textarea class="form-control" name="other-personal-info" rows="3"  placeholder="Write a comment">@if($id){{$plan->other_personal_info}}@endif</textarea>
                                            </div>
                                        </div>
                                        
                                    </div>
                                    <div class="tab-pane" id="w2-tab4">    
                                        <div class="row form-group">
                                            <div class="col-sm-12">
                                                <p>Detail the products and servies this business will sell. Give an overview of the day to day business operations.</p>
                                                <label for="">Give an overview of the product or service this business will sell</label>
                                                <p>Will this business be selling a product, a service or both? Desribe the basic product or service that will be sold. Explain what different types of products and services will be sold. Detail if you foresee additional products of services bieng offered in the future.</p>
                                                <textarea class="form-control" name="product-overview" rows="3"  placeholder="Write a comment">@if($id){{$plan->product_overview}}@endif</textarea>
                                            </div>
                                            <div class="col-sm-12">
                                                <label for="">How will the product be produced or how will the servie be carried out</label>
                                                <p>Include what equipment, tools, intellectual property or other assets that will be necessary to produce or deliver you product/service</p>
                                                <textarea class="form-control" name="how-to-produce" rows="3"  placeholder="Write a comment">@if($id){{$plan->how_to_produce}}@endif</textarea>
                                            </div>
                                            <div class="col-sm-12">
                                                <label for="">How much does the product(s) or service(s) cost to produce/deliver</label>
                                                <textarea class="form-control" name="production-cost" rows="3"  placeholder="Write a comment">@if($id){{$plan->production_cost}}@endif</textarea>
                                            </div>
                                            <div class="col-sm-12">
                                                <label for="">How much does the product(s) or service(s) be sold to customers for</label>
                                                <textarea class="form-control" name="selling-price" rows="3"  placeholder="Write a comment">@if($id){{$plan->selling_price}}@endif</textarea>
                                            </div>
                                            <div class="col-sm-12">
                                                <label for="">How will the product(s) or service(s) be delivered to the customers</label>
                                                <p>Explain if you will have a shop to sell goods, visit customers yourself, sell online etc.</p>
                                                <textarea class="form-control" name="how-to-deliver" rows="3"  placeholder="Write a comment">@if($id){{$plan->how_to_deliver}}@endif</textarea>
                                            </div>
                                            <div class="col-sm-12">
                                                <label for="">What is the growth potential for the product(s) or services</label>
                                                <textarea class="form-control" name="growth-potential" rows="3"  placeholder="Write a comment">@if($id){{$plan->growth_potential}}@endif</textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="w2-tab5">
                                        <div class="row form-group">                                            
                                            <div class="col-sm-12">
                                                <p>Details this business's customers, their demand for the product/service and how you will reach them</p>
                                                <label for="">Describe the business' typical customer adn where they are based</label>
                                                <p>Will customers be individuals, businesses or both? Describe the profile and your understanding of your expected customers (age, gender, what they like, how they socialize, where they shop, etc.)</p>
                                                <textarea class="form-control" name="customers" rows="3"  placeholder="Write a comment">@if($id){{$plan->customers}}@endif</textarea>
                                            </div>
                                            <div class="col-sm-12">
                                                <label for="">How many of these customers will this business hae the potential opportunity to reach:</label>
                                                <p>Outline the size of you market and the potential share of this market this business can sell to</p>
                                                <textarea class="form-control" name="customers-to-reach" rows="3"  placeholder="Write a comment">@if($id){{$plan->customers_to_reach}}@endif</textarea>
                                            </div>
                                            <div class="col-sm-12">
                                                <label for="">Have you sold any products/services to customers already</label>
                                                <p>If yes, please describe these sales and be prepared to show evidence of these sales. If customers have expressed interest in butying your products or services already you can detail that information here</p>
                                                <textarea class="form-control" name="have-sold-already" rows="3"  placeholder="Write a comment">@if($id){{$plan->have_sold_already}}@endif</textarea>
                                            </div>

                                            <div class="col-sm-12">
                                                <label for="">Why will customers buy this business' products or services instead of your competitors</label>
                                                <textarea class="form-control" name="advantages-over-competitors" rows="3"  placeholder="Write a comment">@if($id){{$plan->advantages_over_competitors}}@endif</textarea>
                                            </div>
                                            <div class="col-sm-12">
                                                <label for="">What can be learned about the business' market from field research</label>
                                                <p>Field research is market testing using your prospective customers. This may include customer questionnaires, focus group feedback, product testing, selling your product a limited basis (test trading)</p>
                                                <textarea class="form-control" name="market-research" rows="3"  placeholder="Write a comment">@if($id){{$plan->market_research}}@endif</textarea>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="tab-pane" id="w2-tab6">
                                        <div class="row form-group">
                                            <div class="col-sm-12">
                                                <p>This section should outline all the purchases and expenses necessary to get your business started. All or part of these expenses will be purchased using the SSC Loan funds. All of therse costs will alsoe be includedinyou Cash Flow Statemen. It is beneficial to be frugal when beginning a new business as cash will always be tight. Considering second-hand, or cheaper alternatives will give your businessa better chance at success.</p>
                                            </div>
                                            <div class="col-sm-12">
                                                <label for="">What non-asset start up cost must be considered</label>
                                                <p>Startup costs will go beyond the physical items that are necessary to start this business. These may include renting a premises, electric/water bills at that premises, costs of producing your product/service, marketing/advertising, transporting your product to your customers, insurance for your business, staff, salaries, etc. All these costs should be considered in you Cash Flow Statement</p>
                                                <textarea class="form-control" name="non-asset-cost" rows="3"  placeholder="Write a comment">@if($id){{$plan->non_asset_cost}}@endif</textarea>
                                            </div>
                                            <div class="col-sm-12">
                                                <label for="exampleInputFile">Attach table of asset/equipment purchases</label>
                                                <input type="file" class="form-control-file" id="exampleInputFile" aria-describedby="fileHelp" multiple>
                                            </div>                                    
                                        </div>
                                    </div>

                                    <div class="tab-pane" id="w2-tab7">
                                        <div class="row form-group ">               
                                            <div class="col-sm-12">
                                                <p>
                                                    Understanding who your competitor companies are, where they are located and how they compare to your company is essential in planning any business and sales strategy. Search around the geography of your business and search the internet for your business competition. Please consider the following types of competitors:
                                                </p>
                                                <ul>
                                                    <li><p>Direct Competitors - Those selling the same or similar products or services. (If your business is a coffee shop, direct competitors are other coffee shops)</p></li>
                                                    <li><p>Indirect Competitors - Those selling substitute of alternative products or services. (If your business is a coffee shop, indirect competitors are other business selling food and drink with place to sit such as tea shops, fast food or any other places satisfying a customers need for food or drink</p></li>
                                                </ul>
                                            </div>
                                            <div class="col-sm-12">
                                                <label for="exampleInputFile">Attach table of competitors</label>
                                                <input type="file" class="form-control-file" id="exampleInputFile" aria-describedby="fileHelp" multiple>
                                            </div>  
                                        </div>
                                    </div>

                                    <div class="tab-pane" id="w2-tab8">
                                        <div class="row form-group">
                                            <div class="col-sm-12">
                                                <p>In business jargon this is called a SWOT analysis and is always considered by entrepreneurs starting a new business</p>
                                            </div>
                                            <div class="col-sm-12">
                                                <label for="">What are your business strengths</label>
                                                <p>What gives your business an advantage over the competition?</p>
                                                <textarea class="form-control" name="business-strength" rows="3"  placeholder="Write a comment">@if($id){{$plan->business_strength}}@endif</textarea>
                                            </div>
                                            <div class="col-sm-12">
                                                <label for="">What are your business weaknesses</label>
                                                <p>What places you business at a disadvantage compared to the competition</p>
                                                <textarea class="form-control" name="business-weaknesses" rows="3"  placeholder="Write a comment">@if($id){{$plan->business_weaknesses}}@endif</textarea>
                                            </div>
                                            <div class="col-sm-12">
                                                <label for="">What are the main opportunities available to your business</label>
                                                <p>What people, elements, assets or connections can your business use to its advantage</p>
                                                <textarea class="form-control" name="main-opportunities" rows="3"  placeholder="Write a comment">@if($id){{$plan->main_opportunities}}@endif</textarea>
                                            </div>
                                            <div class="col-sm-12">
                                                <label for="">What are the main threats to your business</label>
                                                <p>What elements of your environment or competition can cause your business trouble?</p>
                                                <textarea class="form-control" name="main-threats" rows="3"  placeholder="Write a comment">@if($id){{$plan->main_threats}}@endif</textarea>
                                            </div>                                   
                                        </div>
                                    </div>

                                    <div class="tab-pane" id="w2-tab9">
                                        <div class="row form-group">
                                            <div class="col-sm-12">
                                                <label for="">What do you plan to do if your business fails</label>
                                                <textarea class="form-control" name="plan-if-fails" rows="3"  placeholder="Write a comment">@if($id){{$plan->plan_if_fails}}@endif</textarea>
                                            </div>
                                            <div class="col-sm-12">
                                                <label for="">In order to assuer loan repayments, please list at least two guarantor names who can pledge to repay your loan on you behalf. The guarantors cannot be part of any Jamaat committee or your family members</label>
                                                <textarea class="form-control" name="guarantors" rows="3"  placeholder="Write a comment">@if($id){{$plan->guarantors}}@endif</textarea>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div class="tab-buttons row">
                                    <a class="btn btn-primary previous">Previous</a>
                                    <button class="btn btn-primary finish hidden" type="submit">Submit</button>

                                    <a class="btn btn-primary next">Next</a>
                                    <a class="btn btn-primary mr" href="{{url('/forms/business-plan')}}" style="float:right;">Cancel</a>
                                </div>
                                <div class="progress">
                                    <progress id="progressBar" value="0" max="100" style="width:300px;"></progress>
                                    <h3 id="status"></h3>
                                    <p id="loaded_n_total"></p>
                                </div> 
                            </form>

                        </div>
                                            </div>

                                            <div class="tab-pane fade" id="all-plans">
                                                <table class="table table-hover table-striped" id="all_plans_table">
                                                    <thead>
                                                        <tr>
                                                            <th>Ref. No</th>
                                                            <th>ACTIONS</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        
                    </div>
                </div>
            </div>

        </div>

    </div>

</div>

@endsection

@section('page-scripts')

<script src="{{asset('/vendor/twitter-bootstrap-wizard/jquery.bootstrap.wizard.js')}}"></script>
<script src="{{asset('/vendor/jquery-validation/jquery.validate.min.js')}}"></script>
<script src="{{asset('/javascripts/examples/forms/wizard.js')}}"></script>

<script type="text/javascript">
    jQuery('.datepicker').datepicker({format: "d/m/yyyy",});
</script>

<script type="text/javascript">
    // Submit registration form
    var business_form = $('#business-plan-form');
    //Submit form button
    var business_form_submit = $('#business-plan-form .tab-buttons .finish');
    //Validate
    var business_form_validate = buildValidation(business_form);
    business_form.bootstrapWizard({
        tabClass: 'steps',
        nextSelector: '.tab-buttons .next',
        previousSelector: '.tab-buttons .previous',
        onInit: function(activeTab, navigation){
            this.totalTabs =  navigation.children('li').length;
        },
        onNext: function() {
            //return validTabInfo(business_form,business_form_validate);
            return true;
        },
        onTabClick: function( activeTab, navigation, actualIndex, clickedIndex ) {
            return onTabClick(clickedIndex, actualIndex, this);
        },
        onTabChange: function( activeTab, navigation, previousIndex, actualIndex ) {
            inLastTab(actualIndex, this.totalTabs, business_form_submit);
        },
         onTabShow: function( activeTab, navigation, actualIndex ) {
             updateValidateTabs(activeTab);
         }
    });
    //On Submit
    business_form.on('submit', function( event ) {
        event.preventDefault();
        var form = $(this);
        var formData = new FormData(this);
        formData.append('id', form.data('id'));
        console.log(formData.get('id'));

        $.ajax({
            xhr: function() {
                var xhr = new window.XMLHttpRequest();
                xhr.upload.addEventListener("progress", progressHandler, false);
                xhr.addEventListener("load", completeHandler, false);
                xhr.addEventListener("error", errorHandler, false);
                xhr.addEventListener("abort", abortHandler, false);

                return xhr;
            },
            url: '{{url('/business-plan/save')}}',
            type: 'POST',
            dataType: 'json',
            data: formData,
            cache: false,
            contentType: false,
            processData: false
        })
        .done(function() {
            console.log("success");
            successNotification();
            // business_form[0].reset();
            business_form.bootstrapWizard('first');
        })
        .fail(function() {
            console.log("error");
        });

        function _(el) {
            return document.getElementById(el);
        }

        function progressHandler(event) {
            _("loaded_n_total").innerHTML = "Uploaded " + event.loaded + " bytes of " + event.total;
            var percent = (event.loaded / event.total) * 100;
            _("progressBar").value = Math.round(percent);
            _("status").innerHTML = Math.round(percent) + "% uploaded... please wait";
        }

        function completeHandler(event) {
            _("status").innerHTML = event.target.responseText;
            _("progressBar").value = 0;
        }

        function errorHandler(event) {
            _("status").innerHTML = "Upload Failed";
        }

        function abortHandler(event) {
            _("status").innerHTML = "Upload Aborted";
        }


        //if ( business_form.valid() ) {
            // successNotification();
            // business_form[0].reset();
            // business_form.bootstrapWizard('first');
        //}
    });
</script>

<script type="text/javascript">
    var all_plans_table =  $('#all_plans_table');
    all_plans_table = all_plans_table.DataTable({  
        "scrollX": true,              
        "processing": true,
        "serverSide": true,
        "ajax": "{{url('/business-plan/all-plans-table')}}",
        "pageLength":25,
        "ordering":false,
        "bAutoWidth": false,
        "oSearch": {"sSearch": ''},
        "columns": [
            {data: 'loan_id', name: 'loan_id'},
            // {data: 'currency', name: 'currency'},
            // {data: 'amount', name: 'amount'},
            // {data: 'remarks', name: 'remarks'},            
            {data: 'actions', name: 'actions',orderable: false, searchable: false}
        ]
    });


    all_plans_table.on('draw', function(event) {
        all_plans_table.rows().every( function ( rowIdx, tableLoop, rowLoop ) {
            $(this.node()).find('.delete').on('click', function(event) {
                event.preventDefault(); 
                var link = $(this);                  
                var id = link.data('id');

                $.ajax({
                    url: '{{url('/dawasco/destroy')}}',
                    type: 'POST',
                    dataType: 'json',
                    data: {id: id},
                })
                .done(function() {
                    all_plans_table.draw(false);
                })
                .fail(function() {
                    console.log("error");
                });
                
            });


        });
        // all_plans_table.row.add({
        //         'date': 1,
        //         'amount': 2,
        //         'currency': 3,
        //         'remarks':4
        //     }).draw( false );
    });
</script>
@endsection