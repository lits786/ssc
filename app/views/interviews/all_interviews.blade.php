@extends('layouts.main')



@section('content')
<div class="content-header">
    <div class="leftside-content-header">
        <ul class="breadcrumbs">
            <li>
                <i class="fa fa-home" aria-hidden="true"></i>
                <a href="#">All interviews
                </a>
            </li>
        </ul>
    </div>
</div>

<div class="row animated fadeInUp">
    <div class="col-sm-12 col-lg-12">
        <div class="row">
            <div class="col-sm-12 col-md-12">
                {{-- <h4 class="section-subtitle">Interview</h4> --}}
                <div class="panel">
                    <div class="panel-content">
                        <table class="table table-hover table-striped" id="all-interviews-table">
                        	<thead>
                        		<tr>
                        			<th>FILE NO.</th>
                        			<th>NAME OF APPLICANT</th>
                        			<th>ACTIONS</th>
                        		</tr>
                        	</thead>
                        	<tbody>
                        		
                        	</tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection




@section('page-scripts')
<script type="text/javascript">
	var all_interviews_table =  $('#all-interviews-table');
    all_interviews_table = all_interviews_table.DataTable({   
        "scrollX": false,             
        "processing": true,
        "serverSide": true,
        "ajax": "{{url('/interviews/all-interviews-data-table')}}",
        "pageLength":25,
        "ordering":false,
        "oSearch": {"sSearch": ''},
        "columns": [
            {data: 'file_no', name: 'files.file_no'},
            {data: 'department', name: 'department'},
            
            {data: 'actions', name: 'actions',orderable: false, searchable: false},
            // {data: 'search_slug', name: "search_slug", visible: false, searchable:true}
        ]
    });

    all_interviews_table.on('draw', function(event) {
        all_interviews_table.rows().every( function ( rowIdx, tableLoop, rowLoop ) {
            $(this.node()).find('.delete').on('click', function(event) {
                event.preventDefault(); 
                var link = $(this);         
                var id = link.data('id');
                $.ajax({
                  url: '{{url('/interview/delete/')}}'+id,
                  type: 'POST',
                  dataType: 'json',
                  data:{},
                })
                .done(function() {
                  toastr.success('success', 'STATUS');          
                  all_interviews_table.draw(false);
                })
                .fail(function() {
                  console.log("error");
                });
            
            });

            $(this.node()).find('.history').on('click', function(event) {
                event.preventDefault(); 
                var link = $(this);
                showHistory(link);                            
            });
        });
      });
</script>
@endsection