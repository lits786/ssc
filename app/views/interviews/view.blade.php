@extends('layouts.main')



@section('content')

<div class="row animated fadeInUp">
    <div class="col-sm-12 col-lg-12">
        <div class="row">
            <div class="col-sm-12 col-md-12">
                {{-- <h4 class="section-subtitle">Interview</h4> --}}
                <div class="panel">
                    <div class="panel-content">
                        <table class="table table-hover table-striped view" id="all-interviews-table">
                        	<thead>
                        		{{-- <tr>
                        			<th>FILE NO.</th>
                        			<th>NAME OF APPLICANT</th>
                        		</tr> --}}
                        	</thead>

                        	<tbody>
                                    <tr>
                                          <td>Department</td>
                                          <td>{{$interview->department}}</td>
                                    </tr>
                                    <tr>
                                          <td>Interview with</td>
                                          <td>{{$interview->interview_with}}</td>
                                    </tr>
                                    <tr>
                                          <td>Telephone</td>
                                          <td>{{$interview->telephone}}</td>
                                    </tr>
                                    <tr>
                                          <td>Employer</td>
                                          <td>{{$interview->employer}}</td>
                                    </tr>
                                    <tr>
                                          <td>Capacity</td>
                                          <td>{{$interview->capacity}}</td>
                                    </tr>
                                    <tr>
                                          <td>Employed since</td>
                                          <td>{{$interview->employed_since}}</td>
                                    </tr>
                                    <tr>
                                          <td>Gross Salary</td>
                                          <td>{{number_format($interview->gross_salary)}}</td>
                                    </tr>
                                    <tr>
                                          <td>Interviewer</td>
                                          <td>{{$interview->interviewer}}</td>
                                    </tr>
                                    <tr>
                                          <td>Assistance currently received</td>
                                          <td>{{$interview->assistance_currently_receiving}}</td>
                                    </tr>
                                    <tr>
                                          <td>Issues discussed</td>
                                          <td>{{$interview->issues_discussed}}</td>
                                    </tr>
                                    <tr>
                                          <td>Conclusion</td>
                                          <td>{{$interview->conclusion}}</td>
                                    </tr>
                                    <tr>
                                          <td>Date</td>
                                          <td>{{$interview->date->format('d M Y')}}</td>
                                    </tr>

                        	</tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection