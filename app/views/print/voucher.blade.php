<link rel="stylesheet" href="{{ asset('vendor/bootstrap/css/bootstrap.css')}}">
<style>
#parent{
    background-color: white;
}
#title-container{
    width: 100%;
}

#title{
    text-align: center;
    font-size: 24px; 
    margin-bottom: -12px;
}
#sub-title{
    font-size:14px;
    text-align: center;
    font-style: italic;
}
.text{
    font-size:14px;
}
#table-container{ 
    width: 100%;
    height: auto; 
}
table{
    width:100%;
    border-collapse: collapse;
}
td{
    border: 1px solid black;
    padding-left: 10px;
    padding-top: 4px;
    padding-bottom: 4px;

}
#first-column{
    width: 65%;
} 
.details-lines{
    min-height: 30px;
}
#voucher-header{
    width: 100%;
}
</style>
<?php
    $file = SSCFile::where('id',$voucher->file_id)->first();
?>
<div id="parent">

<div class="container">
    <img src="{{asset('images/header.jpg')}}" id="voucher-header" alt="">
</div><br><br>
<div class="container" id="table-container">
    
    <table id="table">
        <tr>
            <td colspan="4">
                <div class="row">
                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">FILE NUMBER: <strong>{{$file->file_no}}</strong></div>  
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">DATE: <strong>{{$voucher->date->format('d/m/Y')}}</strong></div>
                </div>
            </td>
        </tr>
        <tr>
            <td colspan="3" id="first-column" class="text-center"><strong>DETAILS</strong></td>
            <td colspan="1" class="text-center"><strong>AMOUNT</strong></td>
        </tr>
        <tr>
            <td colspan="3">
                <div class="row details-lines">
                    <div class="col-lg-4 col-md-4 col-sm-4 col-lg-4 col-xs-4">
                        Payable: <br>
                    </div>
                    <div class="col-lg-8 col-md-8 col-lg-8 col-sm-8 col-xs-8">
                        {{$voucher->payable_to}}<br>
                    </div>
                </div>
                <div class="row details-lines">
                    <div class="col-lg-4 col-md-4 col-sm-4 col-lg-4 col-xs-4">
                        In respect of: <br>
                    </div>
                    <div class="col-lg-8 col-md-8 col-lg-8 col-sm-8 col-xs-8">
                        {{$voucher->in_respect_of}}<br>
                    </div>
                </div>
                <div class="row details-lines">
                    <div class="col-lg-4 col-md-4 col-sm-4 col-lg-4 col-xs-4">
                        Amount in words: <br> 
                    </div>
                    <div class="col-lg-8 col-md-8 col-lg-8 col-sm-8 col-xs-8">
                        {{$voucher->amount_in_words}}<br> 
                    </div>
                </div> 
            </td>
            <td colspan="1">
                {{number_format($voucher->amount)}}
            </td>
        </tr>
        <tr>
            <td colspan="1" class="text-center"><strong>PREPARED BY</strong></td>
            <td colspan="1" class="text-center"><strong>AUTHORISED</strong></td>
            <td colspan="1" class="text-center"><strong>RECEIVED</strong></td>
            <td colspan="1" class="text-center"><strong>REFERENCE</strong></td>
        </tr>
        <tr>
            <td colspan="1">{{$voucher->prepared_by}}</td>
            <td colspan="1">{{$voucher->authorised}}</td>
            <td colspan="1">{{$voucher->received}}</td>
            <td colspan="1">{{$voucher->reference}}</td>
        </tr>
    </table>
</div>
</div>