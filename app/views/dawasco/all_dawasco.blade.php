@extends('layouts.main')



@section('content')
<div class="content-header">
    <div class="leftside-content-header">
        <ul class="breadcrumbs">
            <li>
                <i class="fa fa-home" aria-hidden="true"></i>
                <a href="#">All Dawasco {{Session::get('file-number', '')}}
                </a>
            </li>
        </ul>
    </div>
</div>

<div class="row animated fadeInUp">
    <div class="col-sm-12 col-lg-12">
        <div class="row">
            <div class="col-sm-12 col-md-12">
                {{-- <h4 class="section-subtitle">Interview</h4> --}}
                <div class="panel">
                    <div class="panel-content">
                        <table class="table table-hover table-striped" id="all-dawasco-table">
                            <thead>
                                <tr>
                                    <th>DATE</th>
                                    <th>CURRENCY</th>
                                    <th>AMOUNT</th>
                                    <th>SELF CONTRIBUTION</th>
                                    <th>REMARKS</th>
                                    <th>ACTIONS</th>
                                </tr>
                            </thead>
                            <tbody>
                                
                            </tbody>
                        </table>
                        <label class="total-statement">TOTAL: {{$file->totalDawascoStatement()}}</label>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection




@section('page-scripts')
<script type="text/javascript">
    var all_dawasco_table =  $('#all-dawasco-table');
    all_dawasco_table = all_dawasco_table.DataTable({  
        "scrollX": true,              
        "processing": true,
        "serverSide": true,
        "ajax": "{{url('/dawasco/all-dawasco-data-table')}}",
        "pageLength":25,
        "ordering":false,
        "oSearch": {"sSearch": ''},
        "columns": [
            {data: 'date', name: 'date'},
            {data: 'currency', name: 'currency'},
            {data: 'amount', name: 'amount'},
            {data: 'self_contribution', name: 'self_contribution'},
            {data: 'remarks', name: 'remarks'},            
            {data: 'actions', name: 'actions',orderable: false, searchable: false}
            
            // {data: 'search_slug', name: "search_slug", visible: false, searchable:true}
        ]
    });


    all_dawasco_table.on('draw', function(event) {
        all_dawasco_table.rows().every( function ( rowIdx, tableLoop, rowLoop ) {
            $(this.node()).find('.delete').on('click', function(event) {
                event.preventDefault(); 
                var link = $(this);                  
                var id = link.data('id');

                $.ajax({
                    url: '{{url('/dawasco/destroy')}}',
                    type: 'POST',
                    dataType: 'json',
                    data: {id: id},
                })
                .done(function() {
                    all_dawasco_table.draw(false);
                })
                .fail(function() {
                    console.log("error");
                });
                
            });


        });
        // all_dawasco_table.row.add({
        //         'date': 1,
        //         'amount': 2,
        //         'currency': 3,
        //         'remarks':4
        //     }).draw( false );
    });
</script>
@endsection