@extends('layouts.main')
@section('content')
<div class="content-header">
    <div class="leftside-content-header">
        <ul class="breadcrumbs">
            <li><i class="fa fa-home" aria-hidden="true"></i><a href="#">Users</a></li>
        </ul>
    </div>
</div>
<div class="row animated fadeInUp">
    <div class="col-sm-12 col-lg-12">
        <div class="row">
            <div class="col-sm-12 col-md-12">
                {{-- <h4 class="section-subtitle">Interview</h4> --}}
                <div class="panel">
                    <div class="panel-content">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="tabs">
                                    <ul class="nav nav-tabs">
                                        <li class="active"><a href="#add-user" data-toggle="tab">Add User</a></li>
                                        <li><a href="#all-users" data-toggle="tab">All Users</a></li>
                                    </ul>
                                    <div class="tab-content">
                                        <div class="tab-pane fade in active" id="add-user">
                                            <form id="user-form" data-id="0" enctype="multipart/form-data">
                                                
                                                <div class="row form-group">
                                                    <div class="col-sm-6">
                                                        <label class="control-label">Full name<span class="required">*</span></label>
                                                        <input type="text" class="form-control" name="full-name" required>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <label class="control-label">Username<span class="required">*</span></label>
                                                        <input type="text" class="form-control" name="username" required>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <label for="w2-username" class="control-label">User type<span class="required">*</span></label>
                                                        <select class="form-control" name="user-type" required>
                                                            <option value="">--select user type--</option>
                                                            @foreach($user_types as $type)
                                                                <option value="{{$type->id}}" >{{$type->name}}</option>
                                                            @endforeach                         
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <label for="w2-username" class="control-label">Password<span class="required">*</span></label>
                                                        <input type="password" class="form-control" name="password" required>
                                                    </div>
                                                </div>                                              
                                                <div class="row form-group ">
                                                    <div class="col-sm-12">
                                                        <button type="submit" class="btn btn-primary">Submit</button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                        
                                        <div class="tab-pane fade" id="all-users">
                                            <table class="table table-hover table-striped" id="all-users-table">
                                                <thead>
                                                    <tr>
                                                        <th>NAME</th>
                                                        <th>USERNAME</th>
                                                        <th>PRIVILEDGE</th>
                                                        <th>ACTION</Sth>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> 
</div>
@endsection
@section('page-scripts')
<script src="{{asset('/vendor/twitter-bootstrap-wizard/jquery.bootstrap.wizard.js')}}"></script>
<script src="{{asset('/vendor/jquery-validation/jquery.validate.min.js')}}"></script>
<script src="{{asset('/javascripts/examples/forms/wizard.js')}}"></script>
<script type="text/javascript">
    jQuery('.datepicker').datepicker({format: "d/m/yyyy",});
</script>
<script type="text/javascript">
    var user_form = $('#user-form');
   
    user_form.on('submit', function( event ) {
        event.preventDefault();
        var form = $(this);
        var formData = new FormData(this);
        formData.append('id', form.data('id'));
        $.ajax({
            url: '{{url('/user/save')}}',
            type: 'POST',
            dataType: 'json',
            data: formData,
            cache: false,
            contentType: false,
            processData: false
        })
        .done(function() {
            console.log("success");
            successNotification();
            clearUserForm();
            all_users_table.draw(false);
        })
        .fail(function() {
            console.log("error");
        });
    });
    function clearUserForm(){
        user_form.trigger('reset');
        user_form.data('id', 0);
    }
</script>
<script type="text/javascript">
    var all_users_table =  $('#all-users-table');
    all_users_table = all_users_table.DataTable({                
        "processing": true,
        "serverSide": true,
        "ajax": "{{url('/users/all-users-table')}}",
        "pageLength":25,
        "ordering":false,
        "bAutoWidth": false,
        "oSearch": {"sSearch": ''},
        "columns": [
            {data: 'full_name', name: 'full_name'},
            {data: 'username', name: 'username'},
            {data: 'user_type_name', name: 'user_types.name'},
            {data: 'actions', name: 'actions',orderable: false, searchable: false}
        ]
    });
    all_users_table.on('draw', function(event) {
        all_users_table.rows().every( function ( rowIdx, tableLoop, rowLoop ) {
            $(this.node()).find('.delete').on('click', function(event) {
                event.preventDefault(); 
                var link = $(this);                  
                var id = link.data('id');
                $.ajax({
                    url: '{{url('/user/delete/')}}'+id,
                    type: 'POST',
                    dataType: 'json',
                    data: {},
                })
                .done(function() {
                    all_users_table.draw(false);
                })
                .fail(function() {
                    console.log("error");
                });
                
            });

            $(this.node()).find('.edit').on('click', function(event) {
                event.preventDefault(); 
                var link = $(this);                  
                var id = link.data('id');
                var full_name = link.data('name');
                var username = link.data('username');
                var userType = link.data('usertype');

                var form = $('#user-form');
                form.data('id', id);
                form.find('[name="full-name"]').val(full_name);
                form.find('[name="username"]').val(username);
                form.find('[name="user-type"]').val(userType);


                switchTabs('#add-user', '#all-users');                
            });
        });
        // all_users_table.row.add({
        //         'date': 1,
        //         'amount': 2,
        //         'currency': 3,
        //         'remarks':4
        //     }).draw( false );
    });

    
</script>
@endsection