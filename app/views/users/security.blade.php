@extends('layouts.main')
@section('content')
<div class="content-header">
    <div class="leftside-content-header">
        <ul class="breadcrumbs">
            <li><i class="fa fa-home" aria-hidden="true"></i><a href="#">User Permissions</a></li>
        </ul>
    </div>
</div>
<div class="row animated fadeInUp">
    <div class="col-sm-12 col-lg-12">
        <div class="row">
            <div class="col-sm-12 col-md-12">
                {{-- <h4 class="section-subtitle">Interview</h4> --}}
                <div class="panel">
                    <div class="panel-content">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="tabs">
                                    <ul class="nav nav-tabs">
                                        <li class="active"><a href="#add-permissions" data-toggle="tab">Select permissions</a></li>
                                        <li><a href="#all-permissions" data-toggle="tab">View permissions</a></li>
                                    </ul>
                                    <div class="tab-content">
                                        <div class="tab-pane fade in active" id="add-permissions">
                                            <form id="permissions-form" data-id="0" enctype="multipart/form-data">
                                                
                                                <div class="row form-group">                       
                                                    <div class="col-sm-6">
                                                        <label for="w2-username" class="control-label">User type<span class="required">*</span></label>
                                                        <select class="form-control" name="user-type" required>
                                                            <option value="">--select user type--</option>
                                                            @foreach($user_types as $type)
                                                                <option value="{{$type->id}}" >{{$type->name}}</option>
                                                            @endforeach                         
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    @foreach($permissions as $permission)
                                                    <div class="col-sm-4">
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" value="{{$permission->id}}" name="permissions[]"> {{$permission->name}} 
                                                            </label>
                                                        </div>
                                                    </div>
                                                    @endforeach
                                                </div>
                                              
                                                <div class="row form-group ">
                                                    <div class="col-sm-12">
                                                        <button type="submit" class="btn btn-primary">Submit</button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                        
                                        <div class="tab-pane fade" id="all-permissions">
                                            <table class="table table-hover table-striped" id="all-permissions-table">
                                                <thead>
                                                    <tr>
                                                        <th>NAME</th>
                                                        <th>PERMISSIONS</th>
                                                        <th>ACTION</Sth>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('page-scripts')
<script src="{{asset('/vendor/twitter-bootstrap-wizard/jquery.bootstrap.wizard.js')}}"></script>
<script src="{{asset('/vendor/jquery-validation/jquery.validate.min.js')}}"></script>
<script src="{{asset('/javascripts/examples/forms/wizard.js')}}"></script>
<script type="text/javascript">
    jQuery('.datepicker').datepicker({format: "d/m/yyyy",});
</script>
<script type="text/javascript">
    var permissions_form = $('#permissions-form');
   
    permissions_form.on('submit', function( event ) {
        event.preventDefault();
        var form = $(this);
        var formData = new FormData(this);
        formData.append('id', form.data('id'));
        $.ajax({
            url: '{{url('/user/permissions/save')}}',
            type: 'POST',
            dataType: 'json',
            data: formData,
            cache: false,
            contentType: false,
            processData: false
        })
        .done(function() {
            console.log("success");
            successNotification();
            clearUserForm();
            all_permissions_table.draw(false);
        })
        .fail(function() {
            console.log("error");
        });
    });
    function clearUserForm(){
        permissions_form.trigger('reset');
        permissions_form.data('id', 0);
        window.location.href= '{{url('/security')}}';
    }
    var permissions_form = $('#loan-payment-form');
    permissions_form.find('[name=loan-id]').change(function(event) {
        var select = $(this);
        var loan_id = select.val();
        var form = permissions_form;
        if(loan_id){
            $.ajax({
                url: '{{url('/loan/status')}}',
                type: 'POST',
                dataType: 'json',
                data: {'loan-id': loan_id},
            })
            .done(function(feedback) {
                form.find('[name=loan-amount]').val(feedback.currency_amount);
                form.find('[name=amount-remaining]').val(feedback.amount_remaining);
            })
            .fail(function() {
                console.log("error");
            });
        }else{
            form.find('[name=loan-amount]').val('');
            form.find('[name=amount-remaining]').val('');
        }
        
    });
   
    
</script>
<script type="text/javascript">
    var all_permissions_table =  $('#all-permissions-table');
    all_permissions_table = all_permissions_table.DataTable({                
        "processing": true,
        "serverSide": true,
        "ajax": "{{url('/user-type/all-permissions')}}",
        "pageLength":25,
        "ordering":false,
        "bAutoWidth": false,
        "oSearch": {"sSearch": ''},
        "columns": [
            {data: 'name', name: 'name'},
            {data: 'permissions', name: 'permissions', orderable: false, searchable: false},
            {data: 'actions', name: 'actions',orderable: false, searchable: false}
        ]
    });
    all_permissions_table.on('draw', function(event) {
        all_permissions_table.rows().every( function ( rowIdx, tableLoop, rowLoop ) {
            $(this.node()).find('.reset').on('click', function(event) {
                event.preventDefault(); 
                var link = $(this);                  
                var id = link.data('id');
                $.ajax({
                    url: '{{url('/user/reset-permissions/')}}'+id,
                    type: 'POST',
                    dataType: 'json',
                    data: {},
                })
                .done(function() {
                    all_permissions_table.draw(false);
                })
                .fail(function() {
                    console.log("error");
                });
                
            });
            $(this.node()).find('.edit').on('click', function(event) {
                event.preventDefault(); 
                $('input:checkbox').prop('checked',false);
               
                var link = $(this);                  
                var id = link.data('id');
                var data = '';
                data += link.data('ids');
                
        	
                if(data.indexOf(',') > -1)
                {
               
                	var permissionIds = link.data('ids').split(',');
                	
                	$.each(permissionIds, function(index, value) {
                    $('[name="permissions[]"][value="'+value+'"]').prop('checked', true);
                });
                }
                else
                {
                
                	
                	$('[name="permissions[]"][value="'+id+'"]').prop('checked', true);
                }
                
                permissions_form.data('id', id);

                
                $('[name="user-type"]').val(id);
                

                switchTabs('#add-permissions', '#all-permissions');

                                
            });
        });
    });
</script>
@endsection