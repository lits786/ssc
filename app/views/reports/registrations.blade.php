@extends('layouts.main')



@section('content')
<div class="content-header">
    <div class="leftside-content-header">
        <ul class="breadcrumbs">
            <li>
                <i class="fa fa-home" aria-hidden="true"></i>
                <a href="#">Registrations
                </a>
            </li>
        </ul>
    </div>
</div>

<div class="row animated fadeInUp">
    <div class="col-sm-12 col-lg-12">
        <div class="row">
            <div class="col-sm-12 col-md-12">
                {{-- <h4 class="section-subtitle">Interview</h4> --}}
                <div class="panel">
                    <div class="panel-content">
                        <div class="row col-md-12" style="padding-bottom:20px;">
                            <div class="col-md-5">
                                <label>From</label>
                                <input type="text" id="fromDate" class="form-control datepicker" name="fromDate" placeholder="From">
                            </div>
                            <div class="col-md-5">
                                <label>To</label>
                                <input type="text" id="toDate" class="form-control datepicker" name="toDate" placeholder="To">
                            </div>
                            <!-- <div class="col-md-2">
                                <a href="#" class="btn btn-primary btn-xs filter" style="margin-bottom:0;">Filter</a>
                            </div> -->
                        </div>
                        <table class="table table-hover table-striped" id="all-interviews-table">   
                            
                        	<thead>
                        		<tr>
                                    
                        			<th>FILE NO.</th>
                        			<th>FULL NAME</th>
                                    <th>DATE OF BIRTH</th>
                                    <th>MOBILE NO</th>
                        			<th>ACTIONS</th>
                        		</tr>
                        	</thead>
                        	<tbody>
                        		
                        	</tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection




@section('page-scripts')
<script type="text/javascript">
    
	var table =  $('#all-interviews-table');
    table = table.DataTable({                
        "bProcessing": true,
        "bServerSide": true,
        "pageLength":25,
        "ordering":false,
        "bAutoWidth": false,
        "oSearch": {"sSearch": ''},
        "sAjaxSource": "{{url('/reports/registrations-table')}}",
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
            aoData.push({ "name": "fromDate", "value": $('#fromDate').val() });
            aoData.push({ "name": "toDate", "value": $('#toDate').val() });
            oSettings.jqXHR = $.ajax( {
               "dataType": 'json',
               "type": "GET",
               "url": sSource,
               "data": aoData,
               "success": fnCallback,
               "error": function (e) {
                   console.log(e.message);
               }
            });
        },

        "columns": [
            {data: 'file_no', name: 'files.file_no'},
            {data: 'person', name: 'people.full_name'},
            {data: 'date_of_birth', name: 'people.date_of_birth'},
            {data: 'mobile_phone_number', name: 'addresses.mobile_phone_number'},            
            // {data: 'actions', name: 'actions',orderable: false, searchable: false}
        ]
    });

    table.on('draw', function(event) {
        table.rows().every( function ( rowIdx, tableLoop, rowLoop ) {
            $(this.node()).find('.delete').on('click', function(event) {
                event.preventDefault(); 
                var link = $(this);         
                var id = link.data('id');
                $.ajax({
                  url: 'url('/interview/delete/')}}''+id,
                  type: 'POST',
                  dataType: 'json',
                  data:{},
                })
                .done(function() {
                  toastr.success('success', 'STATUS');          
                  table.draw(false);
                })
                .fail(function() {
                  console.log("error");
                });
            
            });

            $(this.node()).find('.history').on('click', function(event) {
                event.preventDefault(); 
                var link = $(this);
                showHistory(link);                            
            });
        });
    });

    $('.filter').on('click', function(event) {
        event.preventDefault;
        table.draw();
    });

    $('#fromDate').datepicker({
        format: "dd/mm/yyyy",
        autoclose: true
    }).on('change', function(){
        table.draw();
    });

    $('#toDate').datepicker({
        format: "dd/mm/yyyy",
        autoclose: true
    }).on('change', function(){
        table.draw();
    });
</script>
@endsection