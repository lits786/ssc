<table class="table table-hover table-striped" >
    <thead>
        <tr>
            <th>USER</th>
            <th>ACTIVITY</th>
            <th>DATE</th>
        </tr>
    </thead>

    <tbody>
    @foreach($activities as $activity)
    <tr>
          <td>{{$activity->full_name}}</td>
          <td>{{$activity->pivot->comment}}</td>                                  
          <td>{{$activity->pivot->created_at->format('d M Y h:i A')}}</td>
    </tr>
    @endforeach
    </tbody>
</table>