@extends('layouts.main')



@section('content')

<div class="content-header">

  <div class="leftside-content-header">

    <ul class="breadcrumbs">

      <li><i class="fa fa-home" aria-hidden="true"></i><a href="#">File Upload</a></li>

    </ul>

  </div>

</div>

<div class="row animated fadeInUp">

  <div class="col-sm-12 col-lg-12">

    <div class="row">
        <div class="col-sm-12 col-md-12">
            {{-- <h4 class="section-subtitle">Interview</h4> --}}
            <div class="panel">
                <div class="panel-content">               
                    
                    <div class="row">
                        <div class="col-sm-12">
                            <form id="upload-file-form" data-id="0" enctype="multipart/form-data">                                
                                <div class="row form-group">
                                    <div class="col-sm-4">
                                        <label>Title<span class="required">*</span></label>
                                        <input type="text" class="form-control" name="title" required>
                                    </div>
                                    <div class="col-sm-4">
                                        <label class="control-label">Date<span class="required">*</span></label>
                                        <div class="input-group">
                                            <span class="input-group-addon x-primary"><i class="fa fa-calendar"></i></span>
                                            <input type="text" class="form-control datepicker" name="date" required>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <label>Description<span class="required">*</span></label>
                                        <input type="text" class="form-control" name="description" required>
                                    </div>
                                    <div class="col-sm-6">
                                        <label for="exampleInputFile">Attach File</label>
                                        <input type="file" class="form-control-file" name="file" aria-describedby="fileHelp" required>
                                    </div>
                                </div>

                                <div class="row form-group ">
                                    <div class="col-sm-12">
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                    </div>
                                </div>
                                <div class="progress">
                                    <progress id="progressBar" value="0" max="100" style="width:300px;"></progress>
                                    <h3 id="status"></h3>
                                    <p id="loaded_n_total"></p>
                                </div>  
                            </form>
                        </div>
                    </div>
                    <div class="row" style="margin-top:100px;">
                        <div class="col-sm-12">
                        <table class="table table-hover table-striped" id="files-table">
                            <thead>
                            <tr>
                                <th>TITLE</th>
                                <th>DATE</th>
                                <th>DESCRIPTION</th>
                                <th>ACTIONS</th>
                            </tr>
                            </thead>
                            <tbody>
                            
                            </tbody>
                        </table>
                        </div>
                    </div>                    
                
                </div>
            </div>
        </div>
    </div>

  </div>

</div>

@endsection

@section('page-scripts')

<script src="/vendor/twitter-bootstrap-wizard/jquery.bootstrap.wizard.js"></script>
<script src="/vendor/jquery-validation/jquery.validate.min.js"></script>
<script src="/javascripts/examples/forms/wizard.js"></script>

<script type="text/javascript">
    jQuery('.datepicker').datepicker({format: "d/m/yyyy",});
</script>

<script type="text/javascript">
  var file_form = $('#upload-file-form');

  file_form.submit(function(event) {
    event.preventDefault();
    var formData = new FormData(this);
    $.ajax({
        xhr: function() {
            var xhr = new window.XMLHttpRequest();
            xhr.upload.addEventListener("progress", progressHandler, false);
            xhr.addEventListener("load", completeHandler, false);
            xhr.addEventListener("error", errorHandler, false);
            xhr.addEventListener("abort", abortHandler, false);

            return xhr;
        }, 
        url: '{{url('upload/upload-file')}}',
        type: 'POST',
        dataType: 'json',
        data: formData,
        cache: false,
        contentType: false,
        processData: false
    })
    .done(function(feedback) {
        file_form.trigger('reset');
        files_table.draw(false);
      console.log("success");
    })
    .fail(function() {
      console.log("error");
    });

    function _(el) {
        return document.getElementById(el);
    }

    function progressHandler(event) {
        _("loaded_n_total").innerHTML = "Uploaded " + event.loaded + " bytes of " + event.total;
        var percent = (event.loaded / event.total) * 100;
        _("progressBar").value = Math.round(percent);
        _("status").innerHTML = Math.round(percent) + "% uploaded... please wait";
    }

    function completeHandler(event) {
        _("status").innerHTML = event.target.responseText;
        _("progressBar").value = 0;
    }

    function errorHandler(event) {
        _("status").innerHTML = "Upload Failed";
    }

    function abortHandler(event) {
        _("status").innerHTML = "Upload Aborted";
    }
    
  });
</script>

<script type="text/javascript">
  var files_table = $('#files-table');
  files_table = files_table.DataTable({ 
    "scrollX": true,       
    "processing": true,
    "serverSide": true,
    "ajax": '{{url('upload/get-files')}}',
    "pageLength":25,
    "ordering":false,
    "bAutoWidth": false,
    "oSearch": {"sSearch": ''},
    "columns": [
      {data: 'title', name: 'title'},
      {data: 'date', name: 'date'},
      {data: 'description', name: 'description'},
      {data: 'actions', name: 'actions',orderable: false, searchable: false}
    ]
  });

  files_table.on('draw', function(event) {
    files_table.rows().every( function ( rowIdx, tableLoop, rowLoop ) {
      $(this.node()).find('.delete').on('click', function(event) {
        event.preventDefault(); 
        var link = $(this);         
        var href = link.attr('href');
        $.ajax({
          url: href,
          type: 'POST',
          dataType: 'json',
          data:{},
        })
        .done(function() {
          toastr.success('success', 'STATUS');          
          files_table.draw(false);
        })
        .fail(function() {
          console.log("error");
        });
        
      });
    });
  });
</script>

@endsection