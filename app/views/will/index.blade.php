@extends('layouts.main')
@section('content')
<div class="content-header">
    <div class="leftside-content-header">
        <ul class="breadcrumbs">
            <li><i class="fa fa-home" aria-hidden="true"></i><a href="#">Will attachment</a></li>
        </ul>
    </div>
</div>
<div class="row animated fadeInUp">
    <div class="col-sm-12 col-lg-12">
        <div class="row">
            <div class="col-sm-12 col-md-12">
                {{-- <h4 class="section-subtitle">Interview</h4> --}}
                <div class="panel">
                    <div class="panel-content">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="tabs">
                                    <ul class="nav nav-tabs">
                                        <li class="active"><a href="#add-will" data-toggle="tab">Add</a></li>
                                        <li><a href="#all-wills" data-toggle="tab">All</a></li>
                                    </ul>
                                    <div class="tab-content">
                                        <div class="tab-pane fade in active" id="add-will">
                                            <form id="will-attachment-form" data-id="{{$will_id}}" enctype="multipart/form-data">
                                            	                                                
                                                <div class="row form-group">                       
                                                    <div class="col-sm-6">
                                                        <label for="">Title</label>
			                                            <input type="text" class="form-control" name="title" @if($will)value="{{$will->title}}"@endif>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <label for="w2-username" class="control-label">Attach File<span class="required">*</span></label>
                                                        <input type="file" class="form-control-file" name='file' aria-describedby="fileHelp" multiple>
                                                    </div>                                                    
                                                </div>

                                                <div class="row form-group">
                                                	<div class="col-sm-12">
                                                		<label for="">Description</label>
			                                            <textarea class="form-control" rows="3"  name="description">@if($will){{$will->description}}@endif</textarea>
                                                	</div>
                                                </div>
                                                                                              
                                                <div class="row form-group ">
                                                    <div class="col-sm-12">
                                                        <button type="submit" class="btn btn-primary">Submit</button>
                                                    </div>
                                                </div>
                                                <div class="progress">
                                                    <progress id="progressBar" value="0" max="100" style="width:300px;"></progress>
                                                    <h3 id="status"></h3>
                                                    <p id="loaded_n_total"></p>
                                                </div> 
                                            </form>
                                        </div>
                                        
                                        <div class="tab-pane fade" id="all-wills">
                                            <table class="table table-hover table-striped" id="all-wills-table">
                                                <thead>
                                                    <tr>
                                                        <th>NAME</th>
                                                        <th>Description</th>
                                                        <th>ACTION</Sth>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('page-scripts')
<script src="{{asset('/vendor/twitter-bootstrap-wizard/jquery.bootstrap.wizard.js')}}"></script>
<script src="{{asset('/vendor/jquery-validation/jquery.validate.min.js')}}"></script>
<script src="{{asset('/javascripts/examples/forms/wizard.js')}}"></script>
<script type="text/javascript">
    jQuery('.datepicker').datepicker({format: "d/m/yyyy",});
</script>
<script type="text/javascript">
	

    var will_attachment = $('#will-attachment-form');
   
    will_attachment.on('submit', function( event ) {
        event.preventDefault();
        var form = $(this);
        var formData = new FormData(this);
        formData.append('id', form.data('id'));
        $.ajax({
            xhr: function() {
                var xhr = new window.XMLHttpRequest();
                xhr.upload.addEventListener("progress", progressHandler, false);
                xhr.addEventListener("load", completeHandler, false);
                xhr.addEventListener("error", errorHandler, false);
                xhr.addEventListener("abort", abortHandler, false);

                return xhr;
            },
            url: '{{url('/will-attachment/save')}}',
            type: 'POST',
            dataType: 'json',
            data: formData,
            cache: false,
            contentType: false,
            processData: false
        })
        .done(function(feedback) {
            successNotification();
            clearwillattachmentFrom();
            all_wills_table.draw(false);
        })
        .fail(function() {
            console.log("error");
        });
        function _(el) {
            return document.getElementById(el);
        }

        function progressHandler(event) {
            _("loaded_n_total").innerHTML = "Uploaded " + event.loaded + " bytes of " + event.total;
            var percent = (event.loaded / event.total) * 100;
            _("progressBar").value = Math.round(percent);
            _("status").innerHTML = Math.round(percent) + "% uploaded... please wait";
        }

        function completeHandler(event) {
            _("status").innerHTML = event.target.responseText;
            _("progressBar").value = 0;
        }

        function errorHandler(event) {
            _("status").innerHTML = "Upload Failed";
        }

        function abortHandler(event) {
            _("status").innerHTML = "Upload Aborted";
        }
    });
    function clearwillattachmentFrom(){
        will_attachment.trigger('reset');
        will_attachment.data('id', 0);
        window.location.href = "{{url('/will-attachment/form')}}";
    }
</script>
<script type="text/javascript">
    var all_wills_table =  $('#all-wills-table');
    all_wills_table = all_wills_table.DataTable({                
        "processing": true,
        "serverSide": true,
        "ajax": "{{url('/will-attachment/all-wills-table')}}",
        "pageLength":25,
        "ordering":false,
        "bAutoWidth": false,
        "oSearch": {"sSearch": ''},
        "columns": [
            {data: 'title', name: 'title'},
            {data: 'description', name: 'description'},
            {data: 'actions' , name:'actions', orderable: false, searchable: false}
        ]
    });
    all_wills_table.on('draw', function(event) {
        all_wills_table.rows().every( function ( rowIdx, tableLoop, rowLoop ) {
            $(this.node()).find('.delete').on('click', function(event) {
                event.preventDefault(); 
                var link = $(this);                  
                var id = link.data('id');
                $.ajax({
                    url: '{{url('/will/delete/')}}/'+id,
                    type: 'POST',
                    dataType: 'json',
                    data: {},
                })
                .done(function() {
                    successNotification();
                    all_wills_table.draw(false);
                })
                .fail(function() {
                    console.log("error");
                });
                
            });

            $(this.node()).find('.history').on('click', function(event) {
              event.preventDefault(); 
              var link = $(this);
              showHistory(link);                            
            });
        });
        // all_wills_table.row.add({
        //         'date': 1,
        //         'amount': 2,
        //         'currency': 3,
        //         'remarks':4
        //     }).draw( false );
    });
</script>
@endsection