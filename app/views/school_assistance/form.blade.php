@extends('layouts.main')
@section('content')
<div class="content-header">
 <div class="leftside-content-header">
  <ul class="breadcrumbs">
   <li><i class="fa fa-home" aria-hidden="true"></i><a href="#">Education Form {{Session::get('file-id')}}</a></li>
  </ul>
 </div>
</div>
<div class="row animated fadeInUp">
 <div class="col-sm-12 col-lg-12">
  <div class="row">
   <div class="col-sm-12 col-md-12">
    {{-- <h4 class="section-subtitle">Interview</h4> --}}
    <div class="panel">
     <div class="panel-content">
      <div class="row">
       <div class="col-sm-12">
        <div class="tabs">
         <ul class="nav nav-tabs">
          <li class="active"><a href="#form" data-toggle="tab">Form</a></li>
          <li><a href="#upload-file" data-toggle="tab">Upload file</a></li>
          <li><a href="#all" data-toggle="tab">All</a></li>
         </ul>
         <div class="tab-content">
          <div class="tab-pane fade in active" id="form">
           <form id="school-assistance-form" data-id="{{$assistance_id}}">
            <div class="row form-group">        
             <div class="col-sm-6">
              <label class="control-label">Name of student<span class="required">*</span></label>
              <input type="text" class="form-control" name="name-of-student" @if($assistance)value="{{$assistance->name_of_student}}"@endif required>
             </div>
             <div class="col-sm-6"> 
              <br/><br/> 
              <label style="margin-right:20px;">
               <input type="radio" name="gender" value="Female" @if($assistance && $assistance->gender == 'Female') checked @endif required>Female
              </label>
              <label>
               <input type="radio" name="gender" value="Male" @if($assistance && $assistance->gender == 'Male') checked @endif required>Male
              </label>
             </div>
             <div class="col-sm-6">
              <label class="control-label">Date of Birth<span class="required">*</span></label>
              <div class="input-group">
                <span class="input-group-addon x-primary"><i class="fa fa-calendar"></i></span>
                <input type="text" class="form-control datepicker" name="date-of-birth" @if($assistance)value="{{$assistance->date_of_birth->format('d/m/Y')}}"@endif required>
              </div>
            </div>
            </div>
            <div class="row form-group">
             <div class="col-sm-6">
              <label class="control-label">Education institution<span class="required">*</span></label>
              <select class="form-control" name="school-id" required>
                <option value="">--select school--</option>
                @foreach($schools as $school)
                  <option value="{{$school->id}}" @if($assistance && $assistance->school_id == $school->id) selected @endif>{{$school->name}}</option>
                @endforeach 
                <option value="0">Other school</option>          
               </select>
             </div>
             <div class="col-sm-6">
              <label class="control-label">School Name<span class="required">*</span></label>
              <input type="text" class="form-control" name="school-name" @if($assistance)value="{{$assistance->school_name}}"@endif>
             </div> 
             <div class="col-sm-6">
              <label class="control-label">Grade/Form<span class="required">*</span></label>
              <input type="text" class="form-control" name="grade" @if($assistance)value="{{$assistance->grade}}"@endif>
             </div>
             <div class="col-sm-6">
              <label class="control-label">Course<span class="required">*</span></label>
              <input type="text" class="form-control" name="course" @if($assistance)value="{{$assistance->course}}"@endif>
             </div>
            </div>
            <div class="row form-group">
              <div class="col-sm-6">
                <label class="control-label">Currency<span class="required">*</span></label>
                <select class="form-control" name="currency-id" required>
                 <option value="">--select currency--</option>
                 @foreach($currencies as $currency)
                   <option value="{{$currency->id}}" @if($assistance && $assistance->currency_id == $currency->id) selected @endif>{{$currency->name}}</option>
                 @endforeach    
                </select>
              </div>
              <div class="col-sm-6">
                <label class="control-label">Total Amount<span class="required">*</span></label>
                <input type="text" class="form-control" name="total-amount" @if($assistance)value="{{$assistance->total_amount}}"@endif required>
              </div>
              <div class="col-sm-6">
                <label class="control-label">Self Contribution<span class="required">*</span></label>
                <input type="text" class="form-control" name="self-contribution" @if($assistance)value="{{$assistance->self_contribution}}"@endif required>
              </div>
              <div class="col-sm-6">
                <label class="control-label">SSC Contribution<span class="required">*</span></label>
                <input type="text" class="form-control" name="ssc-contribution" @if($assistance)value="{{$assistance->ssc_contribution}}"@endif required>
              </div>
              
            </div> 
            <div class="row form-group">
             <div class="col-sm-12">
              <label >Attachment</label>
              <input type="file" class="form-control-file" name="file" accept="application/pdf" aria-describedby="fileHelp">
             </div>
           </div>      
            
            <div class="row form-group">
             <div class="col-sm-12">
              <p>Note: It is the OBLIGATION of the student to work hard in order to obtain better RESULTS. PARENTS should monitor their CHILDREN'S performance and regularly send us their results. Students who keep on failing year after year can bepenalized by SSC either by SUSPENSION of fee assistance or reduction in the assistance</p>
             </div>
             <div class="col-sm-12"> 
              <label>
               <input type="checkbox" name="agree" value="1" @if($assistance && $assistance->agree=='1') checked @endif>
               I certify the above information is true and I allow the Jamaat to verify in which ever way it beems it.
              </label>
             </div>
            </div> 
            <div class="row form-group ">
               <div class="col-sm-12">                 
                 <button type="reset" class="btn btn-primary">Cancel</button>
                 <button type="submit" class="btn btn-primary">Submit</button>
               </div>
             </div>
             <div class="progress">
              <progress id="progressBar" value="0" max="100" style="width:300px;"></progress>
              <h3 id="status"></h3>
              <p id="loaded_n_total"></p>
            </div>  
           </form>
          </div>
          <div class="tab-pane fade" id="all">
            <table class="table table-hover table-striped" id="assistances-table">
              <thead>
                <tr>
                  <th>STUDENT</th>
                  <th>INSTITUTION</th>
                  <th>GENDER</th>
                  <th>CURRENCY</th>
                  <th>TOTAL</th>
                  <th>SELF</th>
                  <th>SSC</th>
                  <th>ACTIONS</th>
                </tr>
              </thead>
              <tbody>
                
              </tbody>
            </table>
          
            <div class="total-statement">{{$file->totalSchoolAssistanceStatement()}}</div>
          </div>
          <div class="tab-pane fade" id="upload-file">
            <div class="row">
              <div class="col-sm-12">
                <form id="upload-file-form" data-id="0" enctype="multipart/form-data">                                
                  <div class="row form-group">
                      <div class="col-sm-6">
                          <label>Title</label>
                          <input type="text" class="form-control" name="title" required>
                      </div>
                      <div class="col-sm-6">
                        <label class="control-label">Date<span class="required">*</span></label>
                        <div class="input-group">
                          <span class="input-group-addon x-primary"><i class="fa fa-calendar"></i></span>
                          <input type="text" class="form-control datepicker" name="date" required>
                        </div>
                      </div>
                      <div class="col-sm-6">
                          <label for="exampleInputFile">Attach File</label>
                          <input type="file" class="form-control-file" name="file" aria-describedby="fileHelp" required>
                      </div>
                  </div>

                  <div class="row form-group ">
                      <div class="col-sm-12">
                          <button type="submit" class="btn btn-primary">Submit</button>
                      </div>
                  </div>
                  <div class="progress">
                    <progress id="progressBar" value="0" max="100" style="width:300px;"></progress>
                    <h3 id="status"></h3>
                    <p id="loaded_n_total"></p>
                </div>  
                </form>
              </div>
            </div>
            <div class="row" style="margin-top:100px;">
              <div class="col-sm-12">
                <table class="table table-hover table-striped" id="form-files-table">
                  <thead>
                    <tr>
                      <th>FILE</th>
                      <th>DATE</th>
                      <th>ACTIONS</th>
                    </tr>
                  </thead>
                  <tbody>
                    
                  </tbody>
                </table>
              </div>
            </div>
          </div>
         </div>
        </div>
       </div>
      </div>
      
     </div>
    </div>
   </div>
  </div>
 </div>
</div>
@endsection
@section('page-scripts')
<script src="{{asset('/vendor/twitter-bootstrap-wizard/jquery.bootstrap.wizard.js')}}"></script>
<script src="{{asset('/vendor/jquery-validation/jquery.validate.min.js')}}"></script>
<script src="{{asset('/javascripts/examples/forms/wizard.js')}}"></script>
<script type="text/javascript">
 jQuery('.datepicker').datepicker({format: "d/m/yyyy",});
</script>
<script type="text/javascript">
  var school_assistance_form = $('#school-assistance-form');
  school_assistance_form.submit(function(event) {
    event.preventDefault();
    var form = $(this);
    var formData = new FormData(this);
    formData.append('id', form.data('id'));
    $.ajax({
      xhr: function() {
            var xhr = new window.XMLHttpRequest();
            xhr.upload.addEventListener("progress", progressHandler, false);
            xhr.addEventListener("load", completeHandler, false);
            xhr.addEventListener("error", errorHandler, false);
            xhr.addEventListener("abort", abortHandler, false);

            return xhr;
        },
      url: '{{url('/school-assistance/submit')}}',
      type: 'POST',
      dataType: 'json',
      data: formData,
      cache: false,
      processData: false,
      contentType: false
    })
    .done(function() {
      resetSchoolResistanceForm();
      toastr.success('success', 'STATUS');
      window.location.href = '{{url('/forms/school-assistance')}}';
    })
    .fail(function() {
      console.log("error");
    });

    function _(el) {
        return document.getElementById(el);
    }

    function progressHandler(event) {
        _("loaded_n_total").innerHTML = "Uploaded " + event.loaded + " bytes of " + event.total;
        var percent = (event.loaded / event.total) * 100;
        _("progressBar").value = Math.round(percent);
        _("status").innerHTML = Math.round(percent) + "% uploaded... please wait";
    }

    function completeHandler(event) {
        _("status").innerHTML = event.target.responseText;
        _("progressBar").value = 0;
    }

    function errorHandler(event) {
        _("status").innerHTML = "Upload Failed";
    }

    function abortHandler(event) {
        _("status").innerHTML = "Upload Aborted";
    }
    
  });
  school_assistance_form.find('[name=school-id]').change(function(event) {
    var select = $(this);
    var value = select.val();
    var school_name = school_assistance_form.find('[name=school-name]').closest('div');
    var course = school_assistance_form.find('[name=course]').closest('div');
    var grade = school_assistance_form.find('[name=grade]').closest('div');
    if(value == 1){
      grade.show();
    }else{
      grade.hide();
    }
    if(value == 0){
      school_name.show();
      course.show();
    }else{
      school_name.hide();
    }
   
  });
  school_assistance_form.find('[type=reset]').click(function(event) {
   window.location.href='{{url('/forms/school-assistance')}}';
  });
  function resetSchoolResistanceForm(){
   school_assistance_form.data('id', 0);
   school_assistance_form.trigger('reset');
  }
</script>
<script type="text/javascript">
  var assistances_table = $('#assistances-table');
  assistances_table = assistances_table.DataTable({        
    "processing": true,
    "serverSide": true,
    "ajax": "{{url('/school-assistance/all-assistances-table')}}",
    "pageLength":25,
    "ordering":false,
    "bAutoWidth": false,
    "oSearch": {"sSearch": ''},
    "columns": [
      {data: 'name_of_student', name: 'name_of_student'},
      {data: 'school', name: 'schools.name'},
      {data: 'gender', name: 'gender'},
      {data: 'currency', name: 'currencies.name'},
      {data: 'total_amount', name: 'total_amount'},
      {data: 'self_contribution', name: 'self_contribution'},
      {data: 'ssc_contribution', name: 'ssc_contribution'},
      {data: 'actions', name: 'actions',orderable: false, searchable: false}
    ]
  });
  assistances_table.on('draw', function(event) {
    assistances_table.rows().every( function ( rowIdx, tableLoop, rowLoop ) {
      $(this.node()).find('.delete').on('click', function(event) {
        event.preventDefault(); 
        var link = $(this);         
        var id = link.data('id');
        $.ajax({
          url: '{{url('/school-assistance/delete/')}}'+id,
          type: 'POST',
          dataType: 'json',
          data:{},
        })
        .done(function() {
          toastr.success('success', 'STATUS');          
          assistances_table.draw(false);
        })
        .fail(function() {
          console.log("error");
        });
        
      });

      $(this.node()).find('.history').on('click', function(event) {
        event.preventDefault(); 
        var link = $(this);
        showHistory(link);                            
      });
    });
   // updateTotal();
  });
  function updateTotal(){
   $.ajax({
    url: '{{url('/school-assistance/update-total')}}',
    type: 'POST',
    dataType: 'html',
    data: {'file-id': '{{$file->id}}'},
   })
   .done(function(feedback) {
    $('.total-statement').html(feedback);
   })
   .fail(function() {
    console.log("error");
   });
   
  }
  function updateTotal(){
   $.ajax({
    url: '{{url('/school-assistance/update-total')}}',
    type: 'POST',
    dataType: 'html',
    data: {},
   })
   .done(function(feedback) {
    console.log("success");
   })
   .fail(function() {
    console.log("error");
   });
   
  }
</script>

<script type="text/javascript">
  var file_form = $('#upload-file-form');

  file_form.submit(function(event) {
    event.preventDefault();
    var formData = new FormData(this);
    $.ajax({
      xhr: function() {
            var xhr = new window.XMLHttpRequest();
            xhr.upload.addEventListener("progress", progressHandler, false);
            xhr.addEventListener("load", completeHandler, false);
            xhr.addEventListener("error", errorHandler, false);
            xhr.addEventListener("abort", abortHandler, false);

            return xhr;
        },
      url: '{{url('/school-assistance/upload-file')}}',
      type: 'POST',
      dataType: 'json',
      data: formData,
      cache: false,
      contentType: false,
      processData: false
    })
    .done(function(feedback) {
      file_form.trigger('reset');
      files_table.draw(false);
      console.log("success");
    })
    .fail(function() {
      console.log("error");
    });

    function _(el) {
        return document.getElementById(el);
    }

    function progressHandler(event) {
        _("loaded_n_total").innerHTML = "Uploaded " + event.loaded + " bytes of " + event.total;
        var percent = (event.loaded / event.total) * 100;
        _("progressBar").value = Math.round(percent);
        _("status").innerHTML = Math.round(percent) + "% uploaded... please wait";
    }

    function completeHandler(event) {
        _("status").innerHTML = event.target.responseText;
        _("progressBar").value = 0;
    }

    function errorHandler(event) {
        _("status").innerHTML = "Upload Failed";
    }

    function abortHandler(event) {
        _("status").innerHTML = "Upload Aborted";
    }
    
  });
</script>

<script type="text/javascript">
  var files_table = $('#form-files-table');
 files_table = files_table.DataTable({        
    "processing": true,
    "serverSide": true,
    "ajax": "{{url('/school-assistance/all-form-files-table')}}",
    "pageLength":25,
    "ordering":false,
    "bAutoWidth": false,
    "oSearch": {"sSearch": ''},
    "columns": [
      {data: 'title', name: 'title'},
      {data: 'date', name: 'date'},
      {data: 'actions', name: 'actions',orderable: false, searchable: false}
    ]
  });
  files_table.on('draw', function(event) {
    files_table.rows().every( function ( rowIdx, tableLoop, rowLoop ) {
      $(this.node()).find('.delete').on('click', function(event) {
        event.preventDefault(); 
        var link = $(this);         
        var id = link.data('id');
        $.ajax({
          url: '{{url('/school-assistance/delete/')}}'+id,
          type: 'POST',
          dataType: 'json',
          data:{},
        })
        .done(function() {
          toastr.success('success', 'STATUS');          
          assistances_table.draw(false);
        })
        .fail(function() {
          console.log("error");
        });
        
      });
    });
  });
</script>
@endsection


