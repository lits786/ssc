@extends('layouts.main')



@section('content')

<div class="row animated fadeInUp">
    <div class="col-sm-12 col-lg-12">
        <div class="row">
            <div class="col-sm-12 col-md-12">
                {{-- <h4 class="section-subtitle">Interview</h4> --}}
                <div class="panel">
                    <div class="panel-content">
                        <table class="table table-hover table-striped view" id="all-interviews-table">
                        	<thead>
                        		{{-- <tr>
                        			<th>FILE NO.</th>
                        			<th>NAME OF APPLICANT</th>
                        		</tr> --}}
                        	</thead>

                        	<tbody>
                            <tr>
                              <td>Name of student</td>
                              <td>{{$assistance->name_of_student}}</td>
                            </tr>
                            <tr>
                              <td>Gender</td>
                              <td>{{$assistance->gender}}</td>
                            </tr>
                            <tr>
                              <td>Date of birth</td>
                              <td>{{$assistance->date_of_birth->format('d M Y')}}</td>
                            </tr>
                            @if($assistance->school)
                            <tr>
                              <td>School name</td>
                              <td>{{$assistance->school->name}}</td>
                            </tr>
                            @endif
                            @if($assistance->school_name)
                            <tr>
                              <td>School name</td>
                              <td>{{$assistance->school_name}}</td>
                            </tr>
                            @endif                            
                            <tr>
                              <td>Grade</td>
                              <td>{{$assistance->grade}}</td>
                            </tr>
                            <tr>
                              <td>Course</td>
                              <td>{{$assistance->course}}</td>
                            </tr>
                            <tr>
                              <td>Amount</td>
                              <td>{{$assistance->totalAmount()}}</td>
                            </tr>
                            <tr>
                              <td>Self Contribution</td>
                              <td>{{$assistance->selfContribution()}}</td>
                            </tr>
                        	</tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection