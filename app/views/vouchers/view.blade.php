@extends('layouts.main')



@section('content')

<div class="row animated fadeInUp">
    <div class="col-sm-12 col-lg-12">
        <div class="row">
            <div class="col-sm-12 col-md-12">
                {{-- <h4 class="section-subtitle">voucher</h4> --}}
                <div class="panel">
                    <div class="panel-content">
                        <table class="table table-hover table-striped view" id="all-vouchers-table">
                        	<thead>
                        		{{-- <tr>
                        			<th>FILE NO.</th>
                        			<th>NAME OF APPLICANT</th>
                        		</tr> --}}
                        	</thead> 
                        	<tbody>
                                    <tr>
                                          <td>Date</td>
                                          <td>{{$voucher->date}}</td>
                                    </tr> 
                                    <tr>
                                          <td>Amount</td>
                                          <td>{{number_format($voucher->amount)}}</td>
                                    </tr>
                                    <tr>
                                          <td>Payable to</td>
                                          <td>{{$voucher->payable_to}}</td>
                                    </tr>
                                    <tr>
                                          <td>In respect of</td>
                                          <td>{{$voucher->in_respect_of}}</td>
                                    </tr>
                                    <tr>
                                          <td>Amount in words</td>
                                          <td>{{$voucher->amount_in_words}}</td>
                                    </tr>
                                    <tr>
                                          <td>Prepared by</td>
                                          <td>{{$voucher->prepared_by}}</td>
                                    </tr>
                                    <tr>
                                          <td>Authorised</td>
                                          <td>{{$voucher->authorised}}</td>
                                    </tr>
                                    <tr>
                                          <td>Received</td>
                                          <td>{{$voucher->received}}</td>
                                    </tr>
                                    <tr>
                                          <td>Reference</td>
                                          <td>{{$voucher->reference}}</td>
                                    </tr> 
                        	</tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection