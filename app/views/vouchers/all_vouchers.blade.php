@extends('layouts.main')



@section('content')
<div class="content-header">
    <div class="leftside-content-header">
        <ul class="breadcrumbs">
            <li>
                <i class="fa fa-home" aria-hidden="true"></i>
                <a href="#">All vouchers
                </a>
            </li>
        </ul>
    </div>
</div>

<div class="row animated fadeInUp">
    <div class="col-sm-12 col-lg-12">
        <div class="row">
            {{-- <a href="{{url('print')}}" class="btn btn-xs btn-primary print-button">Print</a> --}}
            <div class="col-sm-12 col-md-12">
                {{-- <h4 class="section-subtitle">Voucher</h4> --}}
                <div class="panel">
                    <div class="panel-content">
                        <table class="table table-hover table-striped" id="all-vouchers-table">
                        	<thead>
                        		<tr>
                        			<th>FILE NO.</th>
                        			<th>PAYABLE TO</th>
                        			<th>ACTIONS</th>
                        		</tr>
                        	</thead>
                        	<tbody>
                        		
                        	</tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection




@section('page-scripts')
<script type="text/javascript"> 
	var all_vouchers_table =  $('#all-vouchers-table'); 

    all_vouchers_table = all_vouchers_table.DataTable({   
        "scrollX": false,             
        "processing": true,
        "serverSide": true,
        "ajax": "{{url('/vouchers/all-vouchers-data-table')}}",
        "pageLength":25,
        "ordering":false,
        "oSearch": {"sSearch": ''},
        "columns": [
            {data: 'file_no', name: 'files.file_no'},
            {data: 'payable_to', name: 'payable_to'},
            {data: 'actions', name: 'actions',orderable: false, searchable: false},
            // {data: 'search_slug', name: "search_slug", visible: false, searchable:true}
        ]
    }); 

    all_vouchers_table.on('draw', function(event) {
        $('.print-button').printPage(); 
        all_vouchers_table.rows().every( function ( rowIdx, tableLoop, rowLoop ) {
            $(this.node()).find('.delete').on('click', function(event) {
                event.preventDefault(); 
                var link = $(this);         
                var id = link.data('id');
                $.ajax({
                  url: '{{url('/voucher/delete/')}}'+id,
                  type: 'POST',
                  dataType: 'json',
                  data:{},
                })
                .done(function() {
                  toastr.success('success', 'STATUS');          
                  all_vouchers_table.draw(false);
                })
                .fail(function() {
                  console.log("error");
                });
            
            });

            $(this.node()).find('.history').on('click', function(event) {
                event.preventDefault(); 
                var link = $(this);
                showHistory(link);                            
            });
        });
      });
</script>
@endsection