@extends('layouts.main')
@section('content')
<div class="content-header">
    <div class="leftside-content-header">
        <ul class="breadcrumbs">
            <li><i class="fa fa-home" aria-hidden="true"></i><a href="#">Loan</a></li>
        </ul>
    </div>
</div>
<div class="row animated fadeInUp">
    <div class="col-sm-12 col-lg-12">
        <div class="row">
            <div class="col-sm-12 col-md-12">
                {{-- <h4 class="section-subtitle">Loans</h4> --}}
                <div class="panel">
                    <div class="panel-content">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="tabs">
                                    <ul class="nav nav-tabs">
                                        {{-- <li class="active"><a href="#add-loan" data-toggle="tab">Add Loan</a></li> --}}
                                        <li class="active"><a href="#make-payment" data-toggle="tab">Make Payment</a></li>
                                        <li><a href="#all-payments" data-toggle="tab">All Payments</a></li>
                                    </ul>
                                    <div class="tab-content">
                                        <div class="tab-pane fade" id="add-loan">
                                            <form id="loan-form" data-id="0" enctype="multipart/form-data">
                                                
                                                <div class="row form-group">
                                                    <div class="col-sm-12">
                                                        <label for="w2-username" class="control-label">Loan Code<span class="required">*</span></label>
                                                        <input type="text" class="form-control" name="code">
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <label for="w2-username" class="control-label">Currency<span class="required">*</span></label>
                                                        <select class="form-control" name="currency-id">
                                                            <option value="">--select currency--</option>
                                                            @foreach($currencies as $currency)
                                                                <option value="{{$currency->id}}" >{{$currency->name}}</option>
                                                            @endforeach                         
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <label for="w2-username" class="control-label">Amount<span class="required">*</span></label>
                                                        <input type="number" class="form-control" name="amount">
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <label for="w2-username" class="control-label">Deadline<span class="required">*</span></label>
                                                        <input type="text" class="form-control datepicker" name="deadline">
                                                    </div>
                                                </div>
                                              
                                                <div class="row form-group ">
                                                    <div class="col-sm-12">
                                                        <button type="submit" class="btn btn-primary">Submit</button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                        <div class="tab-pane fade in active" id="make-payment">
                                            <form id="loan-payment-form" data-id="0" enctype="multipart/form-data">
                                                
                                                <div class="row form-group">
                                                    <div class="col-sm-12">
                                                        <label for="w2-username" class="control-label">Loan<span class="required">*</span></label>
                                                        <select class="form-control" name="loan-id">
                                                            <option value="">--Loan--</option>
                                                            @foreach($loans as $loan)
                                                                <option value="{{$loan->id}}">{{$loan->code}}</option>
                                                            @endforeach          
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <label for="w2-username" class="control-label">Loan Taken<span class="required">*</span></label>
                                                        <input type="text" class="form-control" name="loan-amount" value="" disabled>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <label for="w2-username" class="control-label">Amount remaining<span class="required">*</span></label>
                                                        <input type="text" class="form-control" name="amount-remaining" value="" disabled>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <label for="w2-username" class="control-label">Payment<span class="required">*</span></label>
                                                        <input type="number" class="form-control" name="amount" >
                                                    </div>
                                                </div>
                                              
                                                <div class="row form-group ">
                                                    <div class="col-sm-12">
                                                        <button type="submit" class="btn btn-primary">Submit</button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                        <div class="tab-pane fade" id="all-payments">
                                            <table class="table table-hover table-striped" id="all-loans-table">
                                                <thead>
                                                    <tr>
                                                        <th>LOAN</th>
                                                        <th>CURRENCY</th>
                                                        <th>AMOUNT</th>
                                                        <th>PAID</th>
                                                        <th>REMAINING</th>
                                                        <th>DAYS LEFT</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('page-scripts')
<script src="{{asset('/vendor/twitter-bootstrap-wizard/jquery.bootstrap.wizard.js')}}"></script>
<script src="{{asset('/vendor/jquery-validation/jquery.validate.min.js')}}"></script>
<script src="{{asset('/javascripts/examples/forms/wizard.js')}}"></script>
<script type="text/javascript">
    jQuery('.datepicker').datepicker({format: "d/m/yyyy",});
</script>
<script type="text/javascript">
    var loan_form = $('#loan-form');
   
    loan_form.on('submit', function( event ) {
        event.preventDefault();
        var form = $(this);
        var formData = new FormData(this);
        formData.append('id', form.data('id'));
        $.ajax({
            url: '{{url('/loan/save')}}',
            type: 'POST',
            dataType: 'json',
            data: formData,
            cache: false,
            contentType: false,
            processData: false
        })
        .done(function() {
            console.log("success");
            successNotification();
            clearLoanForm();
            all_loans_table.draw(false);
        })
        .fail(function() {
            console.log("error");
        });
    });
    function clearLoanForm(){
        loan_form.trigger('reset');
        loan_form.data('id', 0);
    }
    var loan_payment_form = $('#loan-payment-form');
    loan_payment_form.find('[name=loan-id]').change(function(event) {
        var select = $(this);
        var loan_id = select.val();
        var form = loan_payment_form;
        if(loan_id){
            $.ajax({
                url: '{{url('/loan/status')}}',
                type: 'POST',
                dataType: 'json',
                data: {'loan-id': loan_id},
            })
            .done(function(feedback) {
                form.find('[name=loan-amount]').val(feedback.currency_amount);
                form.find('[name=amount-remaining]').val(feedback.amount_remaining);
                form.find('[name=amount]').attr('max', feedback.amount_remaining);
            })
            .fail(function() {
                console.log("error");
            });
        }else{
            form.find('[name=loan-amount]').val('');
            form.find('[name=amount-remaining]').val('');
        }
        
    });
   
    loan_payment_form.on('submit', function( event ) {
        event.preventDefault();
        var form = $(this);
        var formData = new FormData(this);
        formData.append('id', form.data('id'));
        $.ajax({
            url: '{{url('/loan/payment/save')}}',
            type: 'POST',
            dataType: 'json',
            data: formData,
            cache: false,
            contentType: false,
            processData: false
        })
        .done(function() {
            console.log("success");
            successNotification();
            clearLoanPaymentForm();
            all_loans_table.draw(false);
        })
        .fail(function() {
            console.log("error");
        });
    });
    function clearLoanPaymentForm(){
        loan_payment_form.trigger('reset');
        loan_payment_form.data('id',0);
    }
</script>
<script type="text/javascript">
    var all_loans_table =  $('#all-loans-table');
    all_loans_table = all_loans_table.DataTable({
        "scrollX": true,                
        "processing": true,
        "serverSide": true,
        "ajax": "{{url('/loan/all-loans-table')}}",
        "pageLength":25,
        "ordering":false,
        "bAutoWidth": false,
        "oSearch": {"sSearch": ''},
        "columns": [
            {data: 'code', name: 'code'},
            {data: 'currency', name: 'currencies.name'},
            {data: 'amount', name: 'amount'},
            {data: 'paid', name: 'paid',orderable: false, searchable: false},
            {data: 'remaining', name: 'remaining',orderable: false, searchable: false},            
            {data: 'days_left', name: 'days_left',orderable: false, searchable: false},
            // {data: 'actions', name: 'actions',orderable: false, searchable: false}
        ]
    });
    all_loans_table.on('draw', function(event) {
        all_loans_table.rows().every( function ( rowIdx, tableLoop, rowLoop ) {
            $(this.node()).find('.delete').on('click', function(event) {
                event.preventDefault(); 
                var link = $(this);                  
                var id = link.data('id');
                $.ajax({
                    url: '{{url('/dawasco/destroy')}}',
                    type: 'POST',
                    dataType: 'json',
                    data: {id: id},
                })
                .done(function() {
                    all_loans_table.draw(false);
                })
                .fail(function() {
                    console.log("error");
                });
                
            });
        });
        // all_loans_table.row.add({
        //         'date': 1,
        //         'amount': 2,
        //         'currency': 3,
        //         'remarks':4
        //     }).draw( false );
    });
</script>
@endsection