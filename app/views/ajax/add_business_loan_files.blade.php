<div class="row">
    <div class="col-sm-6">
        <label class="control-label">Description<span class="required">*</span></label>
        <input type="text" class="form-control" name="description[]">
    </div>
    <div class="col-sm-6">
        <label class="control-label">Attach<span class="required">*</span></label>
        <input type="file" class="form-control attach-multiple" multiple>
    </div>
    <div class="col-sm-12">
        <hr>
    </div>
</div>