<div class="other row form-group">
    <div class="col-sm-6">
        <label for="w2-username" class="control-label">Name</label>
        <input type="text" class="form-control"  name="other-name[]">
    </div>
    <div class="col-sm-6">
        <label for="w2-username" class="control-label">Gender</label>
            <div class="radio">
                <label>
                    <input type="radio" name="other-gender-{{$index}}" value="F" > Female
                </label>
            </div>
            <div class="radio">
                <label>
                    <input type="radio" name="other-gender-{{$index}}" value="M" > Male
                </label>
            </div>                                                    
    </div>
    <div class="col-sm-6">
        <label  class="control-label">Date of birth</label>
        <div class="input-group">
            <span class="input-group-addon x-primary"><i class="fa fa-calendar"></i></span>
            <input type="date" class="form-control datepicker"  name="other-date-of-birth[]">
        </div>
    </div>
     <div class="col-sm-6">
        <label for="w2-username" class="control-label">Occupation</label>
        <input type="text" class="form-control"  name="other-occupation[]">
    </div>
    <div class="col-sm-6">
    <label  class="control-label">Relationship</label>
    @foreach($relationships as $relationship)
            <div class="radio">
               <label>
                  <input class="other" data="other-relation-name-{{$index}}" type="radio" name="other-relationship-{{$index}}" value="{{$relationship->id}}"> {{$relationship->name}}
               </label>
            </div>
    @endforeach
    </div>
    <div id="other-relation-name-{{$index}}" class="col-sm-6" style="display:none;">
        <label for="w2-username" class="control-label">Relation Name</label>
        <input type="text" class="form-control"  name="other-relation-name-{{$index}}">
    </div>
    <div class="col-sm-6">
        <label for="w2-username" class="control-label">Phone#</label>
        <input type="text" class="form-control"  name="other-phone[]">
    </div>
    <div class="col-sm-6">
        <label for="w2-username" class="control-label">Email</label>
        <input type="email" class="form-control"  name="other-email[]">
    </div>
    <div class="col-sm-6">
        <label for="w2-username" class="control-label">Address</label>
        <input type="text" class="form-control"  name="other-address[]">
    </div>
    <div class="col-sm-6">
        <label for="w2-username" class="control-label">Information of any assistance to you & your family</label>
        <input type="text" class="form-control"  name="other-family-assistance[]">
    </div> 	
    <div class="col-sm-6">
        <label for="w2-email" class="control-label">Comments</label>
        <textarea class="form-control"  name="other-comments[]"></textarea>
    </div>  	 
</div>
<hr/>