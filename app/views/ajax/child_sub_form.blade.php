<div class="child row form-group">
    <div class="col-sm-6">
        <label for="w2-username" class="control-label">Name</label>
        <input type="text" class="form-control"  name="child-name[]">
    </div>
    <div class="col-sm-6">
        <label for="w2-username" class="control-label">Gender</label>
            <div class="radio">
                <label>
                    <input type="radio" name="child-gender-{{$index}}" value="F" > Female
                </label>
            </div>
            <div class="radio">
                <label>
                    <input type="radio" name="child-gender-{{$index}}" value="M" > Male
                </label>
            </div>                                                    
    </div>
    <div class="col-sm-6">
        <input type="checkbox" class=""  name="child-married[{{$index}}]" value="married">
        <label  class="control-label">Married</label>
    </div>
    <div class="col-sm-6">
        <label  class="control-label">Date of birth</label>
        <div class="input-group">
            <span class="input-group-addon x-primary"><i class="fa fa-calendar"></i></span>
            <input type="text" class="form-control datepicker" name="child-date-of-birth[]">
        </div>
    </div>
    
    <div class="col-sm-6">
        <label for="w2-username" class="control-label">Occupation</label>
        <input type="text" class="form-control"  name="child-occupation[]">
    </div>
    <div class="col-sm-6">
        <label for="w2-username" class="control-label">Any other information</label>
        <input type="text" class="form-control"  name="child-other-info[]">
    </div>

    <div class="col-sm-6">
        <label for="exampleInputFile">Attach photograph</label>
        <input type="file" class="form-control-file" name='child-photo[{{$index}}]' aria-describedby="fileHelp">
    </div>
    <div class="col-sm-3">
        <label for="exampleInputFile">School/college</label>
        <input type="text" class="form-control" name='child-school-college[]'>                                                        
    </div>
</div>
<hr/>