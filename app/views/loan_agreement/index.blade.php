@extends('layouts.main')
@section('content')
<div class="content-header">
  <div class="leftside-content-header">
    <ul class="breadcrumbs">
      <li><i class="fa fa-home" aria-hidden="true"></i><a href="#">Loan Agreement {{Session::get('file-number', '')}}</a></li>
    </ul>
  </div>
</div>
<div class="row animated fadeInUp">
  <div class="col-sm-12 col-lg-12">
    <div class="row">
      <div class="col-sm-12 col-md-12">
        {{-- <h4 class="section-subtitle">Interview</h4> --}}
        <div class="panel">
            <div class="panel-content">
                <div class="tabs">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#add-agreement" data-toggle="tab">Add Agreement</a></li>
                        <li><a href="#all-agreements" data-toggle="tab">All Agreements</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane fade in active" id="add-agreement">
                            <form id="loan-agreement-form" data-id="0" enctype="multipart/form-data">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <label class="control-label">Loan Code<span class="required">*</span></label>
                                        <select class="form-control"  name="loan-code" required>
                                                <option value="">--select loan code--</option>
                                            @foreach($loan_codes as $code)
                                                <option value="{{$code}}">{{$code}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                
                                <div class="row form-group">
                                    <div class="col-sm-6">
                                        <label>Title</label>
                                        <input type="text" class="form-control" name="title" >
                                    </div>
                                    <div class="col-sm-6">
                                        <label for="exampleInputFile">Attach File</label>
                                        <input type="file" class="form-control-file" name="file" aria-describedby="fileHelp">
                                    </div>
                                </div>

                                <div class="row form-group ">
                                    <div class="col-sm-12">
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                    </div>
                                </div>
                                <div class="progress row form-group">
                                    <progress id="progressBar" value="0" max="100" style="width:300px;"></progress>
                                    <h3 id="status"></h3>
                                    <p id="loaded_n_total"></p>
                                </div>
                            </form>
                        </div>
                        <div class="tab-pane fade" id="all-agreements">
                            <table class="table table-hover table-striped" id="all-agreements-table">
                                <thead>
                                    <tr>
                                        <th>FILE</th>
                                        <th>LOAN CODE</th>
                                        <th>ACTIONS</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>                            
            </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('page-scripts')
<script src="{{asset('/vendor/twitter-bootstrap-wizard/jquery.bootstrap.wizard.js')}}"></script>
<script src="{{asset('/vendor/jquery-validation/jquery.validate.min.js')}}"></script>
<script src="{{asset('/javascripts/examples/forms/wizard.js')}}"></script>
<script type="text/javascript">
    jQuery('.datepicker').datepicker({format: "d/m/yyyy",});
</script>
<script type="text/javascript">
	var loan_agreement_form =  $('#loan-agreement-form');
	loan_agreement_form.submit(function(event) {
		console.log("submit");
        event.preventDefault();
        var form = $(this);
        var formData = new FormData(this);
        formData.append('id', form.data('id'));
        $.ajax({
            xhr: function() {
                var xhr = new window.XMLHttpRequest();
                xhr.upload.addEventListener("progress", progressHandler, false);
                xhr.addEventListener("load", completeHandler, false);
                xhr.addEventListener("error", errorHandler, false);
                xhr.addEventListener("abort", abortHandler, false);

                return xhr;
            },  
            url: '{{url('/loan/agreement/save')}}',
            type: 'POST',
            dataType: 'json',
            data: formData,
            cache: false,
            processData: false,
            contentType: false
        })
        .done(function(feedback) {
            resetLoanAgreementForm();
            toastr.success('success', 'STATUS');
            all_agreements_table.draw(false);
        })
        .fail(function() {
            console.log("error");
        });
        function _(el) {
            return document.getElementById(el);
        }

        function progressHandler(event) {
            _("loaded_n_total").innerHTML = "Uploaded " + event.loaded + " bytes of " + event.total;
            var percent = (event.loaded / event.total) * 100;
            _("progressBar").value = Math.round(percent);
            _("status").innerHTML = Math.round(percent) + "% uploaded... please wait";
        }

        function completeHandler(event) {
            _("status").innerHTML = event.target.responseText;
            _("progressBar").value = 0;
        }

        function errorHandler(event) {
            _("status").innerHTML = "Upload Failed";
        }

        function abortHandler(event) {
            _("status").innerHTML = "Upload Aborted";
        }
        
    });
    function resetLoanAgreementForm(){
        var form = $('#loan-agreement-form');
        form.trigger('reset');
        form.data('id',0);
    }
</script>

<script type="text/javascript">
    var all_agreements_table =  $('#all-agreements-table');
    all_agreements_table = all_agreements_table.DataTable({
        "scrollX": true,                
        "processing": true,
        "serverSide": true,
        "ajax": "{{url('/loan-agreements/all-agreements-data-table')}}",
        "pageLength":25,
        "ordering":false,
        "bAutoWidth": false,
        "oSearch": {"sSearch": ''},
        "columns": [
            {data: 'title', name: 'title'},            
            {data: 'loan_id', name: 'loan_id'},
            {data: 'actions', name: 'actions',orderable: false, searchable: false}
        ]
    });


    all_agreements_table.on('draw', function(event) {
        all_agreements_table.rows().every( function ( rowIdx, tableLoop, rowLoop ) {
            $(this.node()).find('.delete').on('click', function(event) {
                event.preventDefault(); 
                var link = $(this);                  
                var id = link.data('id');

                $.ajax({
                    url: '/agreements/destroy',
                    type: 'POST',
                    dataType: 'json',
                    data: {id: id},
                })
                .done(function() {
                    all_agreements_table.draw(false);
                })
                .fail(function() {
                    console.log("error");
                });
                
            });


        });
    });
</script>
@endsection