@extends('layouts.main')
@section('content')
<div class="content-header">
    <div class="leftside-content-header">
        <ul class="breadcrumbs">
            <li><i class="fa fa-home" aria-hidden="true"></i><a href="#">Loan</a></li>
        </ul>
    </div>
</div>
<div class="row animated fadeInUp">
    <div class="col-sm-12 col-lg-12">
        <div class="row">
            <div class="col-sm-12 col-md-12">
                {{-- <h4 class="section-subtitle">Interview</h4> --}}
                <div class="panel">
                    <div class="panel-content">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="tabs">
                                    <ul class="nav nav-tabs">
                                        <li class="active"><a href="#make-payment" data-toggle="tab">Make Payment</a></li>
                                        <li><a href="#all-payments" data-toggle="tab">All Payments</a></li>
                                    </ul>
                                    <div class="tab-content">
                                        <div class="tab-pane fade in active" id="make-payment">
                                            <form id="loan-payment-form" data-id="0" enctype="multipart/form-data">
                                                
                                                <div class="row form-group">
                                                    <div class="col-sm-12">
                                                        <label class="control-label">Loan<span class="required">*</span></label>
                                                        <select class="form-control" name="loan-code">
                                                            <option value="">--Loan--</option>
                                                            @foreach($loans as $loan)
                                                            <option value="{{$loan->loan_code}}" data-amount="{{$loan->getAmount()}}" data-amount_remaining="{{$loan->amountRemaining()}}">{{$loan->loan_code}}</option>
                                                            @endforeach
                                                                      
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <label class="control-label">Loan Taken<span class="required">*</span></label>
                                                        <input type="text" class="form-control"  name="loan-amount" value="" disabled>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <label class="control-label">Amount remaining<span class="required">*</span></label>
                                                        <input type="text" class="form-control"  name="amount-remaining" value="" disabled>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <label class="control-label">Payment<span class="required">*<span id="simple-loan-payment"></span></label>
                                                        <input type="number" max="100" class="form-control" onkeyup="onInputChange(this,'simple-loan-payment');"   name="amount" >
                                                    </div>
                                                </div>
                                              
                                                <div class="row form-group ">
                                                    <div class="col-sm-12">
                                                        <button type="submit" class="btn btn-primary">Submit</button>
                                                    </div>
                                                </div>
                                                <div class="progress">
                                                    <progress id="progressBar" value="0" max="100" style="width:300px;"></progress>
                                                    <h3 id="status"></h3>
                                                    <p id="loaded_n_total"></p>
                                                </div>  
                                            </form>
                                        </div>
                                        <div class="tab-pane fade" id="all-payments">
                                            <table class="table table-hover table-striped" id="all-loans-table">
                                                <thead>
                                                    <tr>
                                                        <th>LOAN</th>
                                                        <th>CURRENCY</th>
                                                        <th>AMOUNT</th>
                                                        <th>PAID</th>
                                                        <th>REMAINING</th>
                                                        <th>DAYS LEFT</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('page-scripts')
<script src="{{asset('/vendor/twitter-bootstrap-wizard/jquery.bootstrap.wizard.js')}}"></script>
<script src="{{asset('/vendor/jquery-validation/jquery.validate.min.js')}}"></script>
<script src="{{asset('/javascripts/examples/forms/wizard.js')}}"></script>
<script type="text/javascript">
    jQuery('.datepicker').datepicker({format: "d/m/yyyy",});
</script>
<script type="text/javascript">    
    var loan_payment_form = $('#loan-payment-form');
    loan_payment_form.find('[name=loan-code]').change(function(event) {
        var select = $(this);
        var loan_code = select.val();
        var form = loan_payment_form;
        
        if(loan_code){
            var amount = select.find('option[value='+loan_code+']').data('amount');
            var amount_remaining = select.find('option[value='+loan_code+']').data('amount_remaining');
            form.find('[name=loan-amount]').val(amount);
            form.find('[name=amount-remaining]').val(amount_remaining);
            form.find('[name=amount]').attr('max', amount_remaining);
        }else{
            form.find('[name=loan-amount]').val('');
            form.find('[name=amount-remaining]').val('');
        }
        
    });
   
    loan_payment_form.on('submit', function( event ) {
        event.preventDefault();
        var form = $(this);
        var formData = new FormData(this);
        formData.append('id', form.data('id'));
        $.ajax({
            xhr: function() {
                var xhr = new window.XMLHttpRequest();
                xhr.upload.addEventListener("progress", progressHandler, false);
                xhr.addEventListener("load", completeHandler, false);
                xhr.addEventListener("error", errorHandler, false);
                xhr.addEventListener("abort", abortHandler, false);

                return xhr;
            },
            url: '{{url('/simple-loan/payment/save')}}',
            type: 'POST',
            dataType: 'json',
            data: formData,
            cache: false,
            contentType: false,
            processData: false
        })
        .done(function() {
            console.log("success");
            successNotification();
            clearLoanPaymentForm();
            all_loans_table.draw(false);
        })
        .fail(function() {
            console.log("error");
        });

        function _(el) {
            return document.getElementById(el);
        }

        function progressHandler(event) {
            _("loaded_n_total").innerHTML = "Uploaded " + event.loaded + " bytes of " + event.total;
            var percent = (event.loaded / event.total) * 100;
            _("progressBar").value = Math.round(percent);
            _("status").innerHTML = Math.round(percent) + "% uploaded... please wait";
        }

        function completeHandler(event) {
            _("status").innerHTML = event.target.responseText;
            _("progressBar").value = 0;
        }

        function errorHandler(event) {
            _("status").innerHTML = "Upload Failed";
        }

        function abortHandler(event) {
            _("status").innerHTML = "Upload Aborted";
        }
    });
    function clearLoanPaymentForm(){
        loan_payment_form.trigger('reset');
        loan_payment_form.data('id',0);
        window.location.href = '{{url('/simple-loan/loan-payments')}}';
    }
</script>
<script type="text/javascript">
    var all_loans_table =  $('#all-loans-table');
    all_loans_table = all_loans_table.DataTable({                
        "processing": true,
        "serverSide": true,
        "ajax": "{{url('/simple-loan/all-payments-table')}}",
        "pageLength":25,
        "ordering":false,
        "bAutoWidth": false,
        "oSearch": {"sSearch": ''},
        "columns": [
            {data: 'simple_loan_code', name: 'simple_loan_code'},
            {data: 'currency', name: 'currencies.name'},
            {data: 'amount', name: 'simple_loan_agreements.amount'},
            {data: 'paid', name: 'simple_loan_payments.amount'},
            {data: 'remaining', name: 'remaining'},            
            {data: 'days_left', name: 'days_left',orderable: false, searchable: false},
            {data: 'actions', name: 'actions',orderable: false, searchable: false}
        ]
    });
    all_loans_table.on('draw', function(event) {
        all_loans_table.rows().every( function ( rowIdx, tableLoop, rowLoop ) {
            $(this.node()).find('.delete').on('click', function(event) {
                event.preventDefault(); 
                var link = $(this);                  
                var id = link.data('id');
                $.ajax({
                    url: '{{url('/simple-loan/payment/delete/')}}'+id,
                    type: 'POST',
                    dataType: 'json',
                    data: {id: id},
                })
                .done(function() {
                    all_loans_table.draw(false);
                })
                .fail(function() {
                    console.log("error");
                });
                
            });
        });
    });
</script>
@endsection