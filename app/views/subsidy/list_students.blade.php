@extends('layouts.main')
@section('content')
<div class="content-header">

    <div class="leftside-content-header">
  
      <ul class="breadcrumbs">
  
        <li><i class="fa fa-home" aria-hidden="true"></i><a href="#">Students List</a></li>
  
      </ul>
  
    </div>
  
  </div>
  
  <div class="row animated fadeInUp">
  
    <div class="col-sm-12 col-lg-12">
  
      <div class="row">
          <div class="col-sm-12 col-md-12">
              {{-- <h4 class="section-subtitle">Interview</h4> --}}
              <div class="panel">
                  <div class="panel-content">
                      <div class="row" style="margin-top:100px;">
                          <div class="col-sm-12">
                          <table class="table table-hover table-striped" id="students-table">
                              <thead>
                              <tr>
                                  <th>STUDENT NAME</th>
                                  <th>STUDENT#</th>
                                  {{-- <th>GRADE</th> --}}
                                  <th>PARENT NAME</th>
                                  <th>PHONE#</th>
                                  <th>ACTION</th>
                              </tr>
                              </thead>
                              <tbody>
                              
                              </tbody>
                          </table>
                          </div>
                      </div>                    
                  
                  </div>
              </div>
          </div>
      </div>
  
    </div>
  
  </div>
  
  @endsection
  
  @section('page-scripts')
  
  <script src="{{asset('/vendor/twitter-bootstrap-wizard/jquery.bootstrap.wizard.js')}}"></script>
  <script src="{{asset('/vendor/jquery-validation/jquery.validate.min.js')}}"></script>
  <script src="{{asset('/javascripts/examples/forms/wizard.js')}}"></script>  
  
  <script type="text/javascript">
    var students_table = $('#students-table');
    students_table = students_table.DataTable({ 
      "scrollX": true,       
      "processing": true,
      "serverSide": true,
      "ajax": "{{url('/subsidy/students/table')}}",
      "pageLength":25,
      "ordering":false,
      "bAutoWidth": false,
      "oSearch": {"sSearch": ''},
      "columns": [
        {data: 'student_name', name: 'student_name'},
        {data: 'student_no', name: 'student_no'},
        // {data: 'grade', name: 'grade'},
        {data: 'parent_name', name: 'parent_name'},
        {data: 'phone_no', name: 'phone_no'},
        {data: 'actions', name: 'actions',orderable: false, searchable: false}
      ]
    });
  
    students_table.on('draw', function(event) {
      students_table.rows().every( function ( rowIdx, tableLoop, rowLoop ) {
        $(this.node()).find('.delete').on('click', function(event) {
          event.preventDefault(); 
          var link = $(this);         
          var href = link.attr('href');
          var id = link.data('id');
          console.log(id)
          $.ajax({
            url: href,
            type: 'POST',
            dataType: 'json',
            data:{id:id},
          })
          .done(function() {
            toastr.success('success', 'STATUS');          
            students_table.draw(false);
          })
          .fail(function() {
            console.log("error");
          });
          
        });
      });
    });
  </script>
@stop