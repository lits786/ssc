@extends('layouts.main')
@section('content')
<div class="content-header">

    <div class="leftside-content-header">
  
      <ul class="breadcrumbs">
  
        <li><i class="fa fa-home" aria-hidden="true"></i><a href="#">Add Students</a></li>
  
      </ul>
  
    </div>
  
  </div>
  
  <div class="row animated fadeInUp">
  
    <div class="col-sm-12 col-lg-12">
  
      <div class="row">
          <div class="col-sm-12 col-md-12">
              {{-- <h4 class="section-subtitle">Interview</h4> --}}
              <div class="panel">
                  <div class="panel-content">               
                      
                      <div class="row">
                          <div class="col-sm-12">
                              <form id="student-form" @if($student)value="{{$student->id}}"@endif enctype="multipart/form-data">       <div class="row form-group">
                                    <div class="col-sm-6">
                                        <label>School Level</label>
                                    <input type="text" class="form-control" name="level" @if($student) value="{{$student->level}}"@else value="PRIMARY FEES SUBSIDY" @endif>
                                    </div>
                                  </div>                        
                                  <div class="row form-group">
                                    <div class="col-sm-4">
                                        <label>Student Name<span class="required">*</span></label>
                                        <input type="text" class="form-control" name="student_name" @if($student)value="{{$student->student_name}}"@endif required>
                                    </div>
                                    <div class="col-sm-4">
                                        <label>Student #<span class="required">*</span></label>
                                        <input type="text" class="form-control" name="student_no" @if($student)value="{{$student->student_no}}"@endif required>
                                    </div>
                                    <div class="col-sm-4">
                                        <label>S.No<span class="required">*</span></label>
                                        <input type="text" class="form-control" name="serial_no" @if($student)value="{{$student->serial_no}}"@endif required>
                                    </div>
                                  </div>

                                  <div class="row form-group">
                                    <div class="col-sm-4">
                                        <label>Parent Name<span class="required">*</span></label>
                                        <input type="text" class="form-control" name="parent_name" @if($student)value="{{$student->parent_name}}"@endif required>
                                    </div>
                                    <div class="col-sm-4">
                                        <label>Parent Email<span class="required">*</span></label>
                                        <input type="email" class="form-control" name="email" @if($student)value="{{$student->email}}"@endif required>
                                    </div>
                                    <div class="col-sm-4">
                                        <label>Phone #<span class="required">*</span></label>
                                        <input type="text" class="form-control" name="phone_no" @if($student)value="{{$student->phone_no}}"@endif required>
                                    </div>
                                  </div>

                                  <div class="row form-group">
                                    <div class="col-sm-6">
                                        <label>Lawajam Details</label>
                                        <input type="text" class="form-control" name="lawajam" @if($student)value="{{$student->lawajam}}"@endif>
                                    </div>
                                    <div class="col-sm-6">
                                        <label  class="control-label">Date</label>
                                        <div class="input-group">
                                            <span class="input-group-addon x-primary"><i class="fa fa-calendar"></i></span>
                                            <input type="text" class="form-control datepicker"  name="date" @if($installment)value="{{$installment->date}}"@endif >
                                        </div>
                                    </div>
                                  </div>
                                  <hr>
                                  <div class="row form-group">
                                    <div class="col-sm-6" id="select">
                	                    <label class="control-label">Grade</label>
                	                    <select class="form-control" id="grade-id" name="grade_id">
                                            <option value="">--select grade--</option>
                                            @foreach($grades as $grade)
                                                <option value="{{$grade->id}}" @if($student && $student->grade_id == $grade->id) selected @endif>{{$grade->name}}</option>
                                            @endforeach                         
                                        </select>
                	                </div>
                                  </div>

                                  <div class="row form-group">
                                    <div class="col-sm-4">
                                        <label>Actual Fees</label>
                                        <input readonly type="text" class="form-control" id="actual_fees" name="actual_fees" @if($student)value="{{$student->actual_fees}}"@endif>
                                    </div>
                                    <div class="col-sm-4">
                                        <label>25%</label>
                                        <input readonly type="text" class="form-control" id="quarter" name="quarter" @if($student)value="{{$student->quarter}}"@endif>
                                    </div>
                                    <div class="col-sm-4">
                                        <label>Total Fees</label>
                                        <input readonly type="text" class="form-control" id="total_fees" name="total_fees" @if($student)value="{{$student->total_fees}}"@endif>
                                    </div>
                                  </div>

                                <div class="row form-group">
                                    <div class="col-sm-12">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" value="yes" name="new_student" @if($student && $student->new_student=='yes') checked @endif> New Student
                                            </label>
                                            <label>
                                                <input type="checkbox" value="yes" name="exam_fees" @if($student && $student->exam_fees=='yes') checked @endif> Examination Fees
                                            </label>
                                        </div>
                                    </div>
                                </div>

                                  <div class="row form-group">
                                    <div class="col-sm-6">
                	                    <label class="control-label">Installment</label>
                	                    <select class="form-control" name="installment">
                                            <option value="">--select installment--</option>
                                            <option value="1st Installment" @if($installment && $installment->name == "1st Installment") selected @endif>1st Installment</option>
                                            <option value="2nd Installment" @if($installment && $installment->name == "2nd Installment") selected @endif>2nd Installment</option>
                                            <option value="3rd Installment" @if($installment && $installment->name == "3rd Installment") selected @endif>3rd Installment</option>
                                            <option value="4th Installment" @if($installment && $installment->name == "4th Installment") selected @endif>4th Installment</option>
                                            <option value="5th Installment" @if($installment && $installment->name == "5th Installment") selected @endif>5th Installment</option>
                                            <option value="6th Installment" @if($installment && $installment->name == "6th Installment") selected @endif>6th Installment</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-6">
                                        <label>Installment Amount</label>
                                        <input type="text" readonly class="form-control" id="amount" name="amount" @if($installment) value="{{$installment->amount}}"@endif>
                                    </div>
                                  </div>
  
                                  <div class="row form-group ">
                                      <div class="col-sm-12">
                                          <button type="submit" class="btn btn-primary">Submit</button>
                                      </div>
                                  </div>
                              </form>
                          </div>
                      </div>     
                  </div>
              </div>
          </div>
      </div>
  
    </div>
  
  </div>
  
  @endsection
  
  @section('page-scripts')
  
  <script src="{{asset('/vendor/twitter-bootstrap-wizard/jquery.bootstrap.wizard.js')}}"></script>
  <script src="{{asset('/vendor/jquery-validation/jquery.validate.min.js')}}"></script>
  <script src="{{asset('/javascripts/examples/forms/wizard.js')}}"></script>

  <script type="text/javascript">
    jQuery('.datepicker').datepicker({format: "d/m/yyyy",});
  </script>

  <script type="text/javascript">
    var student_form = $('#student-form');
  
    student_form.submit(function(event) {
      event.preventDefault();
      var formData = new FormData(this);
      formData.append('id', student_form.data('id'));
      $.ajax({
        url: '{{url('/subsidy/students/save')}}',
        type: 'POST',
        dataType: 'json',
        data: formData,
        cache: false,
        contentType: false,
        processData: false
      })
      .done(function(re) {
        student_form.trigger('reset');
        console.log("success");
        if(re.status == 1)
        {
            toastr.success('Student details saved successfully!');
            window.location.reload();
        }   
      })
      .fail(function() {
        console.log("error");
        toastr.warning('An error occured!');
      });
      
    });
  </script>
  <script type="text/javascript">  
    $('#select').on('change','#grade-id',function() {
      var id = $(this).val();

      //alert(id);
      $.ajax({
        url: '{{url('/subsidy/students/fees')}}',
        type: 'GET',
        dataType: 'html',
        data: 'id='+id,
        cache: false,
        contentType: false,
        processData: false
      })
      .done(function(feedback) {
          var dt = $.parseJSON(feedback);     
          console.log(dt);     
          $('#actual_fees').val(dt.actual_fees);
          $('#quarter').val(dt.qt);
          $('#total_fees').val(dt.total_fees);
          $('#amount').val(dt.installment);
      })
      .fail(function() {
        console.log("error");
      });
      
    });
  </script>
@stop