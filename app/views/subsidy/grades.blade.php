@extends('layouts.main')
@section('content')
<div class="content-header">

    <div class="leftside-content-header">
  
      <ul class="breadcrumbs">
  
        <li><i class="fa fa-home" aria-hidden="true"></i><a href="#">Grades</a></li>
  
      </ul>
  
    </div>
  
  </div>
  
  <div class="row animated fadeInUp">
  
    <div class="col-sm-12 col-lg-12">
  
      <div class="row">
          <div class="col-sm-12 col-md-12">
              {{-- <h4 class="section-subtitle">Interview</h4> --}}
              <div class="panel">
                  <div class="panel-content">               
                      
                      <div class="row">
                          <div class="col-sm-12">
                              <form id="grades-form"  @if($grade)data-id="{{$grade->id}}"@endif enctype="multipart/form-data">                                
                                  <div class="row form-group">
                                      <div class="col-sm-6">
                                          <label>Name<span class="required">*</span></label>
                                          <input type="text" class="form-control" name="name" @if($grade)value="{{$grade->name}}"@endif required>
                                      </div>
                                      <div class="col-sm-6">
                                        <label>Fees<span class="required">*</span><span id="subsidy-grades-fees"></span></label>
                                        <input type="text" class="form-control" onkeyup="onInputChange(this,'subsidy-grades-fees');" name="fees" @if($grade)value="{{$grade->fees}}"@endif required>
                                    </div>
                                  </div>
  
                                  <div class="row form-group ">
                                      <div class="col-sm-12">
                                          <button type="submit" class="btn btn-primary">Submit</button>
                                      </div>
                                  </div>
                              </form>
                          </div>
                      </div>
                      <div class="row" style="margin-top:100px;">
                          <div class="col-sm-12">
                          <table class="table table-hover table-striped" id="grades-table">
                              <thead>
                              <tr>
                                  <th>NAME</th>
                                  <th>FEES</th>
                                  <th>ACTIONS</th>
                              </tr>
                              </thead>
                              <tbody>
                              
                              </tbody>
                          </table>
                          </div>
                      </div>                    
                  
                  </div>
              </div>
          </div>
      </div>
  
    </div>
  
  </div>
  
  @endsection
  
  @section('page-scripts')
  
  <script src="{{asset('/vendor/twitter-bootstrap-wizard/jquery.bootstrap.wizard.js')}}"></script>
  <script src="{{asset('/vendor/jquery-validation/jquery.validate.min.js')}}"></script>
  <script src="{{asset('/javascripts/examples/forms/wizard.js')}}"></script>
  
  <script type="text/javascript">
    var grade_form = $('#grades-form');
  
    grade_form.submit(function(event) {
      event.preventDefault();
      var formData = new FormData(this);
      formData.append('id', grade_form.data('id'));
      $.ajax({
        url: '{{url('/subsidy/grades/save')}}',
        type: 'POST',
        dataType: 'json',
        data: formData,
        cache: false,
        contentType: false,
        processData: false
      })
      .done(function(feedback) {
        grade_form.trigger('reset');
        grades_table.draw(false);
        console.log("success");
      })
      .fail(function() {
        console.log("error");
      });
      
    });
  </script>
  
  <script type="text/javascript">
    var grades_table = $('#grades-table');
    grades_table = grades_table.DataTable({ 
      "scrollX": true,       
      "processing": true,
      "serverSide": true,
      "ajax": "{{url('/subsidy/grades/get-grades')}}",
      "pageLength":25,
      "ordering":false,
      "bAutoWidth": false,
      "oSearch": {"sSearch": ''},
      "columns": [
        {data: 'name', name: 'name'},
        {data: 'fees', name: 'fees'},
        {data: 'actions', name: 'actions',orderable: false, searchable: false}
      ]
    });
  
    grades_table.on('draw', function(event) {
      grades_table.rows().every( function ( rowIdx, tableLoop, rowLoop ) {
        $(this.node()).find('.delete').on('click', function(event) {
          event.preventDefault(); 
          var link = $(this);         
          var href = link.attr('href');
          var id = link.data('id');
          $.ajax({
            url: href,
            type: 'POST',
            dataType: 'json',
            data:{id:id},
          })
          .done(function() {
            toastr.success('success', 'STATUS');          
            grades_table.draw(false);
          })
          .fail(function() {
            console.log("error");
          });
          
        });
      });
    });
  </script>
@stop