@extends('layouts.main')
@section('content')
<div class="content-header">
  <div class="leftside-content-header">
    <ul class="breadcrumbs">
      <li><i class="fa fa-home" aria-hidden="true"></i><a href="#">Files</a></li>
    </ul>
  </div>
</div>
<div class="row animated fadeInUp">
  <div class="col-sm-12 col-lg-12">
    <div class="row">
      <div class="col-sm-12 col-md-12">
        {{-- <h4 class="section-subtitle">Interview</h4> --}}
        <div class="panel">
            <div class="panel-content">
                <table class="table table-hover table-striped" id="files-table">
                    <thead>
                        <tr>
                            <th>FILE</th>
                            <th>MEMBER</th>
                            <th>NEXT INTERVIEW</th>
                            <th>ACTIONS</th>
                        </tr>
                    </thead>
                    <tbody>
                        
                    </tbody>
                </table>                          
            </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('page-scripts')
<script src="/vendor/twitter-bootstrap-wizard/jquery.bootstrap.wizard.js"></script>
<script src="/vendor/jquery-validation/jquery.validate.min.js"></script>
<script src="/javascripts/examples/forms/wizard.js"></script>
<script type="text/javascript">
    jQuery('.datepicker').datepicker({format: "d/m/yyyy",});
</script>

<script type="text/javascript">
    var files_table =  $('#files-table');
    files_table = files_table.DataTable({     
        "scrollX": true,           
        "processing": true,
        "serverSide": true,
        "ajax": "/user/files-datatable/{{$userTypeId}}",
        "pageLength":25,
        "ordering":false,
        "bAutoWidth": false,
        "oSearch": {"sSearch": ''},
        "columns": [
            {data: 'file_no', name: 'file_no'},  
            {data: 'person', name: 'people.file_name'}, 
            {data: 'next_date', name:'interviews.next_interview'},
            {data: 'actions', name: 'actions',orderable: false, searchable: false}
        ]
    });


    files_table.on('draw', function(event) {
        files_table.rows().every( function ( rowIdx, tableLoop, rowLoop ) {
            $(this.node()).find('.delete').on('click', function(event) {
                event.preventDefault(); 
                var link = $(this);                  
                var id = link.data('id');

                $.ajax({
                    url: '/agreements/destroy',
                    type: 'POST',
                    dataType: 'json',
                    data: {id: id},
                })
                .done(function() {
                    files_table.draw(false);
                })
                .fail(function() {
                    console.log("error");
                });
                
            });

            $(this.node()).find('.close-file').on('click', function(event) {
                event.preventDefault(); 
                var link = $(this);                  
                var url = link.attr('href');

                $.ajax({
                    url: url,
                    type: 'POST',
                    dataType: 'json',
                    data: {},
                })
                .done(function() {
                    files_table.draw(false);
                    successNotification();
                })
                .fail(function() {
                    console.log("error");
                });
                
            });


        });
    });
</script>
@endsection