<?php
use Carbon\Carbon;

class LoanController extends BaseController{
	public function getIndex(){
		$file_id = Auth::user()->active_file;
		$currencies = Currency::all();
		$loans = Loan::where('file_id', $file_id)->get();
		$data = compact('currencies', 'loans');
		return View::make('loan.index')->with($data);
	}
	public function save(){
		$feedback = [];
		$loan = new Loan();
		$loan->file_id = Auth::user()->active_file;
		$loan->code = Input::get('code');
		$loan->currency_id = Input::get('currency-id');
		$loan->amount = Input::get('amount');
		$loan->deadline = Carbon::createFromFormat('d/m/Y', Input::get('deadline'));
		$loan->save();
		return Response::json($feedback);
	}
	public function savePayment(){
		$feedback = [];
		$payment = new LoanPayment;
		$payment->loan_id = Input::get('loan-id');
		$payment->amount = Input::get('amount');
		$payment->save();
		return Response::json($feedback);
	}
	public function loanStatus(){
		$id = Input::get('loan-id');
		$loan = Loan::find($id);
		
		$loan->amount_remaining = $loan->amountRemaining();
		$loan->currency_amount = $loan->currency->name.$loan->amount;
		return Response::json($loan);
	}
	public function allLoansTable(){
		$file_id = Auth::user()->active_file;
		$loans = Loan::leftJoin('currencies', 'currencies.id', '=', 'loans.currency_id')
							->where('file_id', $file_id)
							->select('loans.*',
										'currencies.name as currency');
		$table =  Datatables::of($loans)
					->addColumn('days_left', function($data){
						return $data->deadline->diffInDays(Carbon::now());
					})
					->editColumn('paid', function($data){
						return $data->payments()->sum('amount');;
					})
					->editColumn('remaining', function($data){
						return $data->amountRemaining();
					})
					// ->addColumn('actions', function($data){ 
	    // 				$output = '<a href="/forms/medical/?id='.$data->id.'"" class="btn  btn-primary btn-xs">Edit</a>';
	    // 				$output .= '<a class="btn btn-xs btn-danger delete" data-id="'.$data->id.'">Delete</a>';
	    // 				return $output;
	    // 			})			
    				->make(true);
    	return $table;
	}
}