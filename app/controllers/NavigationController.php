<?php
class NavigationController extends BaseController{
	
	public function getVisitForm()
	{
		
	}
	
	public function getRegistrationForm($id=0){
		$residence_types = ResidenceType::all();
		$relationships = Relationship::all();
		$residence_ownership_types = ResidenceOwnershipType::all();
		$income_types = IncomeType::all();
		$medical_payment_types = MedicalPaymentType::all();
		$outreach_types = OutreachType::all();
		$user = Auth::user();
		$active_file = $user->active_file;


		//$user_types = UserType::where('id', '!=' ,1)->orderBy('name','ASC')->get();
		$user_types = UserType::orderBy('name','ASC')->get();

		if($id){
			$file = SSCFile::with('person', 
									'person.residence', 
									'person.address', 
									'person.income', 
									'person.spouse',
									'remarks')
							->find($id);
			$file_id = $file->id;
		}else{
			$file = null;
			$file_id = 0;
			$user->active_file = null;
			$user->save();
		}

		$data = compact('outreach_types','medical_payment_types','relationships','residence_types', 
							'residence_ownership_types', 
							'income_types', 
							'user_types',
							'file',
							'user', 
							'file_id');


		return View::make('forms.registration')->with($data);
	}

	public function getLoanApplicationForm($id = 0)
	{
		$application = ($id == 0) ? new LoanApplication : LoanApplication::find($id);
		$file_id = Auth::user()->active_file;
		$file = SSCFile::find($file_id);
		$loan_codes = $file->businessPlans()->lists('loan_id');

		$data = compact('id', 'application', 'loan_codes');

		return View::make('forms.loan_application')->with($data);
	}

	

	public function getGuaranteeForm($id = 0){
		$guarantee = ($id == 0) ? new LoanGuarantee : LoanGuarantee::find($id);

		$file_id = Auth::user()->active_file;
		$file = SSCFile::find($file_id);
		$loan_codes = $file->businessPlans()->lists('loan_id');

		$data = compact('id','guarantee', 'loan_codes');
		return View::make('forms.guarantee')->with($data);
	}

	public function getBusinessPlanForm($id = 0){
		$plan = ($id == 0) ? new BusinessPlan : BusinessPlan::find($id);
		$data = compact('id', 'plan');

		return View::make('forms.business_plan')->with($data);
	}
}
