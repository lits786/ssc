<?php
class SchoolAssistanceController extends BaseController{
	
	public function getSchoolAssistanceForm(){
		$id = Input::has('id')?Input::get('id'):0;
		$active_file = Auth::user()->active_file;

		$file = SSCFile::find($active_file);
		$currencies = Currency::all();
		$schools = School::orderBy('name','ASC')->get();


		if($id != 0){
			$assistance = SchoolAssistance::find($id);
			$assistance_id = $assistance->id;
		}else{
			$assistance = null;
			$assistance_id = 0;
		}

		$data = compact('assistance', 
						'assistance_id',
						'file', 
						'currencies',
						'schools');

		return View::make('school_assistance.form')->with($data);
	}

	public function submit(){
		$id = Input::get('id'); 

		$assistance = ($id == 0) ? new SchoolAssistance : SchoolAssistance::find($id);
		$assistance->file_id = Auth::user()->active_file;
		$assistance->created_by = Auth::id();
		$assistance->name_of_student = Input::get('name-of-student');
		$assistance->date_of_birth = Input::get('date-of-birth');
		$assistance->gender = Input::get('gender');
		$assistance->school_id = Input::get('school-id');
		$assistance->school_name = Input::get('school-name');
		$assistance->grade = Input::get('grade');
		$assistance->course = Input::get('course');
		$assistance->currency_id = Input::get('currency-id');
		$assistance->total_amount = Input::get('total-amount');
		$assistance->self_contribution = Input::get('self-contribution');
		$assistance->ssc_contribution = Input::get('ssc-contribution');
		$assistance->agree = Input::get('agree');
		$assistance->concluded = "no";//Input::get('concluded');

		if(Input::hasFile('file')){
			$file = Input::file('file');
			$path = 'files/';
			$name = Str::random(3).time().'.'.$file->getClientOriginalExtension();
			$file->move($path, $name);
			$assistance->attachment = $path.$name;
		}
		

		$assistance->save();

		return Response::json([]);
	}

	public function allSchoolAssistancesTable(){

		$user = Auth::user();
		$file_id = $user->active_file;

		if($file_id){
			$assistances = SchoolAssistance::leftJoin('currencies', 'currencies.id', '=', 'school_assistances.currency_id')
								->leftJoin('schools', 'schools.id', '=', 'school_assistances.school_id')
								->where('file_id', $file_id)
								->where('file','=','')
								->select('school_assistances.*',
											'currencies.name as currency',
											'schools.name as school');
		}else{
			$assistances = SchoolAssistance::leftJoin('currencies', 'currencies.id', '=', 'school_assistances.currency_id')
								->leftJoin('schools', 'schools.id', '=', 'school_assistances.school_id')
								->select('school_assistances.*',
											'currencies.name as currency',
											'schools.name as school');
		}

		$table =  Datatables::of($assistances)
					->editColumn('ssc_contribution', function($data){
						return $data->ssc_contribution.' ('.$data->percentage().')';
					})
					->editColumn('self_contribution', function($data){
						return $data->self_contribution.' ('.$data->selfPercentage().')';
					})
					->addColumn('actions', function($data) use ($user){ 
						$output = '<a href="'.url('/school-assistance/view/'.$data->id).'" class="btn  btn-primary btn-xs">View</a>';
	    				$output .= '<a href="'.url('/forms/school-assistance').'?id='.$data->id.'" class="btn  btn-primary btn-xs">Edit</a>';

	    				if($user->user_type == 1){
	    					$output .= '<a class="btn btn-xs btn-primary history" href="'.url('/file-history/education/'.$data->id).'" data-id="'.$data->id.'">History</a>';
		    				$output .= '<a class="btn btn-xs btn-danger delete" data-id="'.$data->id.'">Delete</a>';
		    			}


	    				return $output;
	    			})			
    				->make(true);

    	return $table;
	}

	public function delete($id){
		SchoolAssistance::destroy($id);
		return Response::json([]);
	}

	public function updateTotal(){
		$id = Input::get('file-id');
		$file = SSCFile::find($id);

		return 'TOTAL: '.$file->totalSchoolAssistanceStatement();
	}


	public function view($id){
		$assistance = SchoolAssistance::find($id);

		$data = compact('assistance');

		return View::make('school_assistance.view')->with($data);
	}


	public function uploadFile(){
		$feedback = [];

		$assistance = new SchoolAssistance;
		$assistance->file_id = Auth::user()->active_file;
		$assistance->title = Input::get('title');
		$assistance->date = Input::get('date');

		if(Input::hasFile('file')){
			$file = Input::file('file');
			$path = 'files/';
			$name = Str::random(3).time().'.'.$file->getClientOriginalExtension();
			$file->move($path, $name);
			$assistance->file = $path.$name;
		}
		$assistance->save();

		return Response::json($feedback);
	}

	public function allFormFilesTable(){

		$file_id = Auth::user()->active_file;
		$assistances = SchoolAssistance::whereNotNull('date')
							->where('file_id', $file_id)
							->select('*');
		

		$table =  Datatables::of($assistances)
					->addColumn('actions', function($data){ 
					    $output = '<a href="'.url('/school-assistance/view',$data->id).'" class="btn  btn-primary btn-xs">View</a>';
	    	// 			$output .= '<a href="/forms/school-assistance/'.$data->id.'" class="btn  btn-primary btn-xs">Edit</a>';
	    				$output .= '<a class="btn btn-xs btn-danger delete" data-id="'.$data->id.'">Delete</a>';

	    				return '';
	    			})	
	    			->editColumn('title', function($data){
	    				return '<a href="'.url('/'.$data->file).'" target="_blank">'.$data->title.'</a>';
	    			})
    				->make(true);

    	return $table;
	}
}