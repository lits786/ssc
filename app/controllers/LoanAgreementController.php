<?php
class LoanAgreementController extends BaseController{
	public function index(){
		$file_id = Auth::user()->active_file;
		$file = SSCFile::find($file_id);
		$loan_codes = $file->businessPlans()->lists('loan_id');

		$data = compact('loan_codes');

		return View::make('loan_agreement.index')->with($data);
	}
	public function save(){
		$user = Auth::user();
		$feedback = [];
		$agreement = new LoanAgreement;
		$agreement->file_id = $user->active_file;
		$agreement->loan_id = Input::get('loan-code');
		$agreement->title = Input::get('title');
		
		if(Input::hasFile('file')){
			$file = Input::file('file');
			$path = 'files/';
			$name = Str::random(3).time().'.'.$file->getClientOriginalExtension();
			$file->move($path, $name);
			$agreement->file = $path.$name;
		}
		$agreement->save();
		
		return Response::json($feedback);
	}

	public function allAgreementsDatatable(){
		$file_id = Auth::user()->active_file;
		$agreement = LoanAgreement::select('*');

		$table =  Datatables::of($agreement)
					->editColumn('title', function($data){
						$output = '<a href="'.url('/'.$data->file).'">'.$data->title.'</a>';
						return $output;
					})
					->addColumn('actions', function($data){ 
						$output = '<a href="'.url('/luku/view/'.$data->id).'" class="btn  btn-primary btn-xs">View</a>'; 
	    				// $output .= '<a href="/forms/luku/?id='.$data->id.'"" class="btn  btn-primary btn-xs">Edit</a>';
	    				// $output .= '<a class="btn btn-xs btn-danger delete" data-id="'.$data->id.'">Delete</a>';

	    				return $output;
	    			})			
    				->make(true);

    	return $table;
	}
	
}