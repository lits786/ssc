<?php

class WillController extends BaseController{

	public function getIndex($id=0){
		if($id){
			$data = compact('');
		}else{
			$will = null;
			$will_id = 0;

			$data = compact('will','will_id');
		}

		return View::make('will.index')->with($data);

	}

	public function save(){
		$feedback = [];
		$id = Input::get('id');

		$will = ($id==0) ? new Will : Will::find($id);
		$will->created_by = Auth::id();
		$will->file_id = Auth::user()->active_file;
		$will->title = Input::get('title');
		$will->description = Input::get('description');

		$file = Input::file('file');
		$path = 'files/wills/';
		$name = Str::random(3).time().'.'.$file->getClientOriginalExtension();
		$file->move($path, $name);

		$will->path = $path.$name;
		$will->save();

		return Response::json($feedback);
	}

	public function delete($id){
		Will::destroy($id);

		return Response::json([]);
	}



	public function allWillsTable(){
		$user = Auth::user();
		$file_id = $user->active_file;
		$wills = Will::where('file_id', $file_id)
							->select('*');
		

		$table =  Datatables::of($wills)
					->editColumn('title', function($data){
						$output = '<a href="'.url('/'.$data->path).'" target="_blank">'.$data->title.'</a>';
						return $output;
					})
					->addColumn('actions', function($data) use ($user){
						$output = '';
						if(true){//$user->user_type == 1){
							$output .= '<a class="btn btn-xs btn-primary history" href="'.url('/file-history/will/'.$data->id).'" data-id="'.$data->id.'">History</a>';
							$output .= '<a class="btn btn-xs btn-danger delete" data-id="'.$data->id.'">Delete</a>';
						}
						return $output;
					})
    				->make(true);

    	return $table;
	}
}