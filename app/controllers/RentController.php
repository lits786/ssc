<?php

class RentController extends BaseController{
	public function getRentForm(){
		$file_id = Auth::user()->active_file;
		$file = SSCFile::find($file_id);

		$id = Input::get('id',0);
		if($id){
			$rent = Rent::find($id);
			$rent_id = $id;
		}else{
			$rent = null;
			$rent_id = 0;
		}
		$currencies = Currency::orderBy('name','ASC')->get();
		$data = compact('currencies', 'rent', 'rent_id', 'file');

		return View::make('forms.rent')->with($data);
	}

	public function save(){
		$id = Input::get('id');
		$rent = ($id == 0) ? new Rent : Rent::find($id);
		$rent->created_by = Auth::id();
		$rent->file_id = Auth::user()->active_file;
		$rent->date = date_create_from_format('m/d/Y', Input::get('date'));
		$rent->self_contribution = Input::get('self-contribution');
		$rent->currency_id = Input::get('currency');
		$rent->total_amount = Input::get('total-amount');
		$rent->ssc_contribution = Input::get('ssc-contribution');
		$rent->concluded = Input::get('concluded','no');
		$rent->remarks = Input::get('remarks');

		
		$rent->save();

		if(Input::hasFile('files')){
			$files = Input::file('files');

			foreach ($files as $file) {
				$path = 'files/';
				$name = Str::random(3).time().'.'.$file->getClientOriginalExtension();
				$file->move($path, $name);
				$full_path = $path.$name;

				$rent_file = new RentFile;
				$rent_file->path = $full_path;
				$rent->files()->save($rent_file);
			}
		}

		return Response::json([]);
	}

	public function allRent(){
		$file_id = Auth::user()->active_file;
		$file = SSCFile::find($file_id);
		$data = compact('file');

		return View::make('rent.all_rent')->with($data);
	}

	public function allRentDataTable(){
		$user = Auth::user();
		$file_id = $user->active_file;

		$rent = Rent::leftJoin('currencies', 'currencies.id', '=', 'rent.currency_id')
						->where('file_id', $file_id)
						->select('rent.*',
									'currencies.name as currency');

		$table =  Datatables::of($rent)
					->editColumn('date', function($data){
						return $data->date->format('d-m-Y');
					})
					->editColumn('ssc_contribution', function($data){
						return $data->ssc_contribution.' ('.$data->percentage().')';
					})
					->editColumn('self_contribution', function($data){
						return $data->self_contribution.' ('.$data->selfPercentage().')';
					})
					->addColumn('percentage', function($data){
						return $data->percentage();
					})
					->addColumn('actions', function($data) use ($user){ 
						$output = '<a href="'.url('/rent/view/'.$data->id).'" class="btn  btn-primary btn-xs">View</a>'; 
	    				$output .= '<a href="'.url('forms/rent?id='.$data->id).'"" class="btn  btn-primary btn-xs">Edit</a>';
	    				if($user->user_type == 1){
	    					$output .= '<a href="'.url('forms/delete?id='.$data->id).'"" class="btn btn-xs btn-primary history" href="'.url('/file-history/rent/'.$data->id).'" data-id="'.$data->id.'">History</a>';
		    			}
		    			$output .= '<a class="btn btn-xs btn-danger delete" data-id="'.$data->id.'">Delete</a>';

	    				return $output;
	    			})			
    				->make(true);

    	return $table;
	}

	public function destroy(){
		$id = Input::get('id');
		Rent::destroy($id);

		return Response::json([]);
	}

	public function view($id){
		$rent = Rent::find($id);
		$files = $rent->files;

		$data = compact('rent', 'files');
		return View::make('rent.view')->with($data);
	}

	public function uploadFile(){
		$feedback = [];

		$rent = new Rent;
		$rent->file_id = Auth::user()->active_file;
		$rent->title = Input::get('title');
		$rent->date = date_create_from_format('m/d/Y', Input::get('date'));

		if(Input::hasFile('file')){
			$file = Input::file('file');
			$path = 'files/';
			$name = Str::random(3).time().'.'.$file->getClientOriginalExtension();
			$file->move($path, $name);
			$rent->file = $path.$name;
		}
		$rent->save();

		return Response::json($feedback);
	}

	public function allFormFilesTable(){

		$file_id = Auth::user()->active_file;
		$rents = Rent::whereNotNull('date')
							->where('file','!=','')
							->where('file_id', $file_id)
							->select('*');
		

		$table =  Datatables::of($rents)
					->addColumn('actions', function($data){ 
					//	$output = '<a href="'.url('rent/view',$data->id).'" class="btn  btn-primary btn-xs delete">View</a>';
	    	 		//	$output .= '<a href="'.url('/forms/rent/').'?id='.$data->id.'" class="btn  btn-primary btn-xs">Edit</a>';
	    	 			$output = '<a href="'.url('/rent/destroy').'?id='.$data->id.'" class="btn btn-xs btn-danger delete" data-id="'.$data->id.'">Delete</a>';

	    				return $output;
	    			})	
	    			->editColumn('title', function($data){
	    				return '<a href="'.url('/'.$data->file).'" target="_blank">'.$data->title.'</a>';
	    			})
	    			->editColumn('date', function($data){
	    				return $data->date->format('d-m-Y');
	    			})
    				->make(true);

    	return $table;
	}

	public function delete($id){
		Rent::destroy($id);
		return Response::json([]);
	}
}