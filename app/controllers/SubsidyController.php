<?php 

use Barryvdh\DomPDF\Facade as PDF;

class SubsidyController extends BaseController{
    public function student()
    {
        $id = Input::get('id',0);
        
		if($id){
            $ins_id = Installment::where('student_id',$id)->orderBy('updated_at','DESC')->first()->id;
            $student = Student::find($id);
            $installment = Installment::find($ins_id);
			$student_id = $id;
		}else{
            $student = null;
            $installment = null;
			$student_id = 0;
		}
		$grades = Grade::orderBy('name','ASC')->get();
		$data = compact('grades', 'student', 'student_id','installment');

		return View::make('subsidy.student')->with($data);
    }

    public function studentSave()
    { 
        $id = Input::get('id');

		$student = ($id == 0) ? new Student : Student::find($id);
        $student->created_by = Auth::id();
        $student->student_name = Input::get('student_name');
        $student->student_no = Input::get('student_no');
        $student->serial_no = Input::get('serial_no');
        $student->lawajam = Input::get('lawajam');
        $student->parent_name = Input::get('parent_name');
        $student->phone_no = Input::get('phone_no');
        $student->grade_id = Input::get('grade_id');
        $student->actual_fees = Input::get('actual_fees');
        $student->quarter = Input::get('quarter');
        $student->total_fees = Input::get('total_fees');
        $student->new_student = Input::get('new_student');
        $student->exam_fees = Input::get('exam_fees');        
        $student->email = Input::get('email');
        $student->level = Input::get('level');
		
        $student->save();

        //installments
        $st_id = Student::orderBy('updated_at','DESC')->first();
        //$st_id = $s_id->id;
        $ins_id = Installment::where('student_id',$id)->first();
        //$ins_id = $in_id->id;
        $installment = ($id == 0) ? new Installment : Installment::find($ins_id->id);
        $installment->student_id = ($id == 0) ? $st_id->id : $id;
        $installment->amount = Input::get('amount');
        $installment->date = Input::get('date');
        $installment->name = Input::get('installment');

        $installment->save();

        $e_id = $installment->student_id;
        //$this->sendEmail($e_id);
        $status = 1;

        $feedback = compact('status');
        return Response::json($feedback);
	}

    public function studentTable()
    {
        $student = Student::select('*');
		

		$table =  Datatables::of($student)
					->addColumn('actions', function($data){ 
						$output = '<a href="'.url('subsidy/students').'?id='.$data->id.'" class="btn btn-xs btn-primary">Edit</a>';
	    	 			$output .= '<a href="'.url('subsidy/students/destroy').'" class="btn btn-xs btn-danger delete" data-id="'.$data->id.'">Delete</a>';

	    				return $output;
	    			})
    				->make(true);

    	return $table;
    }

    public function studentDestroy()
    {
        $id = Input::get('id');
		Student::destroy($id);

		return Response::json([]);
    }
    
    //grades
    public function grades()
    {
        $id = Input::get('id',0);

		if($id){
			$grade = Grade::find($id);
			$grade_id = $id;
		}else{
			$grade = null;
			$grade_id = 0;
        }
        
		$data = compact('grade', 'grade_id');
		return View::make('subsidy.grades')->with($data);
    }

    public function gradesSave()
    {
        $feedback = [];

        $id = Input::get('id');

		$grade = ($id == 0) ? new Grade : Grade::find($id);
        $grade->name = Input::get('name');
        $grade->fees = Input::get('fees');

        $grade->save();

        return Response::json($feedback);
    }

    public function gradesDestroy()
    {
        $id = Input::get('id');
		Grade::destroy($id);

		return Response::json([]);
    }

    public function getGrades()
    {
		$grade = Grade::select('*');
		

		$table =  Datatables::of($grade)
					->addColumn('actions', function($data){ 
						$output = '<a href="'.url('subsidy/grades').'?id='.$data->id.'" class="btn btn-xs btn-primary">Edit</a>';
	    	 			$output .= '<a href="'.url('subsidy/grades/destroy').'" class="btn btn-xs btn-danger delete" data-id="'.$data->id.'">Delete</a>';

	    				return $output;
	    			})
    				->make(true);

    	return $table;
    }

    public function calculateFees()
    {
        $id = Input::get('id');
        $fee = Grade::where('id',$id)->first();
        $qt = floatval($fee->fees) * 25 / 100;
        $total_fees = floatval($fee->fees) - $qt;
        $installment = $total_fees/6;

        $feedback = array(
            'actual_fees' => $fee->fees,
            'qt' => $qt,
            'total_fees' => $total_fees,
            'installment' => $installment
        );

        return Response::json($feedback);
    }

    function sendEmail($id) 
    {
        $student = Student::where('id',$id)->first();
        $file = $this->create_pdf($id);
        
        Mail::send('mail-form',
            array(
                'student_name' => $student->student_name,
                'student_no' => $student->student_no
            ), 

            function($message) use($file)
            {
                $message->from('sarfaraz@legendaryits.com','SSC')                 
					->to('sarfaraz@legendaryits.com','SSC')
					//->cc('sarfaraz@legendaryits.com', '')
                    ->subject('Student Information')
                    ->attach('student/'.$file,
                        [
                            'as' => 'fee_payments.pdf',
                            'mime' => 'application/pdf',
                        ]);
            }
        );

    }

    function create_pdf($id)
    {
        $student = Student::where('id',$id)->first();
        $installment = Installment::where('student_id', $id)->get();

        $data = array(
            'student' => $student,
            'installments' => $installment
        );

        $file_name = $student->student_no;
        
        $pdf = PDF::loadView('data_form', $data);
        $pdf->save('student/'.$file_name);

        return $file_name;
    }
}