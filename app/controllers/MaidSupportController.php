<?php

class MaidSupportController extends BaseController{
	public function getMaidSupportForm(){
		$id = Input::get('id',0);

		$file_id = Auth::user()->active_file;
		$file = SSCFile::find($file_id);
		if($id){
			$maid_support = MaidSupport::find($id);
			$maid_support_id = $id;
		}else{
			$maid_support = null;
			$maid_support_id = 0;
		}
		$currencies = Currency::orderBy('name','ASC')->get();
		$data = compact('currencies', 'maid_support', 'maid_support_id', 'file');

		return View::make('forms.maid_support')->with($data);
	}

	public function save(){
		$id = Input::get('id');
		$maid_support = ($id == 0) ? new MaidSupport : MaidSupport::find($id);
		$maid_support->created_by = Auth::id();
		
		$maid_support->file_id = Auth::user()->active_file;
		$maid_support->date = date_create_from_format('m/d/Y', Input::get('date'));
		$maid_support->currency_id = Input::get('currency');
		$maid_support->self_contribution = Input::get('self-contribution');
		$maid_support->ssc_contribution = Input::get('ssc-contribution');
		$maid_support->total_amount = Input::get('total-amount');
		$maid_support->concluded = Input::get('concluded', 'no');
		$maid_support->remarks = Input::get('remarks');

		
		$maid_support->save();

		if(Input::hasFile('files')){
			$files = Input::file('files');

			foreach ($files as $file) {
				$path = 'files/';
				$name = Str::random(3).time().'.'.$file->getClientOriginalExtension();
				$file->move($path, $name);
				$full_path = $path.$name;

				$maid_support_file = new MaidSupportFile;
				$maid_support_file->path = $full_path;
				$maid_support->files()->save($maid_support_file);
			}
		}

		return Response::json(compact('files'));
	}

	public function allMaidSupport(){
		$file_id = Auth::user()->active_file;
		$file = SSCFile::find($file_id);
		$data = compact('file');

		return View::make('maid_support.all_maid_support')->with($data);
	}

	public function allMaidSupportDataTable(){
		$user = Auth::user();

		$file_id = $user->active_file;
		$maid_support = MaidSupport::leftJoin('currencies', 'currencies.id', '=', 'maid_support.currency_id')
							->where('file_id', $file_id)
							->select('maid_support.*',
										'currencies.name as currency');

		$table =  Datatables::of($maid_support)
					->editColumn('date', function($data){
						return $data->date->format('d-m-Y');
					})
					->editColumn('ssc_contribution', function($data){
						return $data->ssc_contribution.' ('.$data->percentage().')';
					})
					->editColumn('self_contribution', function($data){
						return $data->self_contribution.' ('.$data->selfPercentage().')';
					})
					->addColumn('percentage', function($data){
						return $data->percentage();
					})
					->addColumn('actions', function($data) use ($user){ 
						$output = '<a href="'.url('/maid-support/view/'.$data->id).'" class="btn  btn-primary btn-xs">View</a>'; 
	    				$output .= '<a href="'.url('/forms/maid-support/?id='.$data->id).'"" class="btn  btn-primary btn-xs">Edit</a>';
	    				if($user->user_type == 1){
	    					$output .= '<a class="btn btn-xs btn-primary history" href="'.url('/file-history/maid-support/',$data->id).'" data-id="'.$data->id.'">History</a>';
		    				$output .= '<a class="btn btn-xs btn-danger delete" data-id="'.$data->id.'">Delete</a>';
		    			}

	    				return $output;
	    			})			
    				->make(true);

    	return $table;
	}

	public function destroy(){
		$id = Input::get('id');
		MaidSupport::destroy($id);

		return Response::json([]);
	}

	public function view($id){
		$maid_support = MaidSupport::find($id);
		$files = $maid_support->files;

		$data = compact('maid_support', 'files');
		return View::make('maid_support.view')->with($data);
	}

	public function uploadFile(){
		$feedback = [];

		$maid_support = new MaidSupport;
		$maid_support->file_id = Auth::user()->active_file;
		$maid_support->title = Input::get('title');
		$maid_support->date = date_create_from_format('m/d/Y', Input::get('date'));

		if(Input::hasFile('file')){
			$file = Input::file('file');
			$path = 'files/';
			$name = Str::random(3).time().'.'.$file->getClientOriginalExtension();
			$file->move($path, $name);
			$maid_support->file = $path.$name;
		}
		$maid_support->save();

		return Response::json($feedback);
	}

	public function allFormFilesTable(){

		$file_id = Auth::user()->active_file;
		$maid_supports = MaidSupport::whereNotNull('date')
							->where('file','!=','')
							->where('file_id', $file_id)
							->select('*');
		

		$table =  Datatables::of($maid_supports)
					->addColumn('actions', function($data){ 
						$output = '<a href="'.url('/maid-support/delete/'.$data->id).'" class="btn  btn-primary btn-xs delete">Delete</a>';
	    	// 			$output .= '<a href="/forms/school-assistance/'.$data->id.'" class="btn  btn-primary btn-xs">Edit</a>';
	    	// 			$output .= '<a class="btn btn-xs btn-danger delete" data-id="'.$data->id.'">Delete</a>';

	    				return $output;
	    			})	
	    			->editColumn('title', function($data){
	    				return '<a href="'.url('/'.$data->file).'" target="_blank">'.$data->title.'</a>';
	    			})
	    			->editColumn('date', function($data){
	    				return $data->date->format('d-m-Y');
	    			})
    				->make(true);

    	return $table;
	}

	public function delete($id){
		MaidSupport::destroy($id);
		return Response::json([]);
	}
}