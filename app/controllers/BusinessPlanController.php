<?php
class BusinessPlanController extends BaseController{
	public function save(){
		$feedback = [];
		$id = Input::get('id');
		$plan = ($id == 0) ? new BusinessPlan : BusinessPlan::find($id);
		
		$plan->loan_id = Input::get('loan-code');
		$plan->business_name = Input::get('business-name');
		$plan->business_location = Input::get('business-location');
		$plan->start_date = date_create_from_format('d/m/Y', Input::get('start-date'));
		$plan->business_website = Input::get('business-website');
		$plan->social_media_links = Input::get('social-media-links');
		$plan->academic_qualifications = Input::get('academic-qualifications');
		$plan->business_activities = Input::get('business-activities');
		$plan->products_services = Input::get('products-services');
		$plan->how_sell = Input::get('how-sell');
		$plan->business_mission = Input::get('business-mission');
		$plan->business_vision = Input::get('business-vision');
		$plan->business_uniqueness = Input::get('business-uniqueness');
		$plan->business_staff = Input::get('business-staff');
		$plan->why_run_business = Input::get('why-run-business');
		$plan->previous_experience = Input::get('previous-experience');
		$plan->education = Input::get('education');
		$plan->hobbies = Input::get('hobbies');
		$plan->business_experience = Input::get('business-experience');
		$plan->other_personal_info = Input::get('other-personal-info');
		$plan->product_overview = Input::get('product-overview');
		$plan->how_to_produce = Input::get('how-to-produce');
		$plan->production_cost = Input::get('production-cost');
		$plan->selling_price = Input::get('selling-price');
		$plan->how_to_deliver = Input::get('how-to-deliver');
		$plan->growth_potential = Input::get('growth-potential');
		$plan->customers = Input::get('customers');
		$plan->customers_to_reach = Input::get('customers-to-reach');
		$plan->have_sold_already = Input::get('have-sold-already');
		$plan->advantages_over_competitors = Input::get('advantages-over-competitors');
		$plan->market_research = Input::get('market-research');
		$plan->non_asset_cost = Input::get('non-asset-cost');
		$plan->business_strength = Input::get('business-strength');
		$plan->business_weaknesses = Input::get('business-weaknesses');
		$plan->main_opportunities = Input::get('main-opportunities');
		$plan->main_threats = Input::get('main-threats');
		$plan->plan_if_fails = Input::get('plan-if-fails');
		$plan->guarantors = Input::get('guarantors');
		$plan->save();
		return Response::json($feedback);
	}
	public function allPlansTable(){
		$file_id = Auth::user()->active_file;
		$plans = BusinessPlan::where('file_id', $file_id)
							->select('*');
		$table =  Datatables::of($plans)
					->addColumn('actions', function($data){ 
	    				$output = '<a href="'.url('/forms/business-plan/'.$data->id).'" class="btn  btn-primary btn-xs">Edit</a>';
	    				$output .= '<a class="btn btn-xs btn-danger delete" data-id="'.$data->id.'">Delete</a>';
	    				
	    				return $output;	    				
	    			})			
    				->make(true);
    	return $table;
	}
}
