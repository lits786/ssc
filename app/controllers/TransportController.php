<?php

class TransportController extends BaseController{
	public function getTransportForm(){
		$id = Input::get('id',0);

		$file_id = Auth::user()->active_file;
		$file = SSCFile::find($file_id);
		if($id){
			$transport = Transport::find($id);
			$transport_id = $id;
		}else{
			$transport = null;
			$transport_id = 0;
		}
		$currencies = Currency::orderBy('name','ASC')->get();
		$data = compact('currencies', 'transport', 'transport_id', 'file');

		return View::make('forms.transport')->with($data);
	}

	public function save(){
		$id = Input::get('id');
		$transport = ($id == 0) ? new Transport : Transport::find($id);
		$transport->created_by = Auth::id();
		
		$transport->file_id = Auth::user()->active_file;
		$transport->date = date_create_from_format('m/d/Y', Input::get('date'));
		$transport->currency_id = Input::get('currency');
		$transport->self_contribution = Input::get('self-contribution');
		$transport->ssc_contribution = Input::get('ssc-contribution');
		$transport->total_amount = Input::get('total-amount');
		$transport->concluded = Input::get('concluded', 'no');
		$transport->remarks = Input::get('remarks');

		
		$transport->save();

		if(Input::hasFile('files')){
			$files = Input::file('files');

			foreach ($files as $file) {
				$path = 'files/';
				$name = Str::random(3).time().'.'.$file->getClientOriginalExtension();
				$file->move($path, $name);
				$full_path = $path.$name;

				$transport_file = new TransportFile;
				$transport_file->path = $full_path;
				$transport->files()->save($transport_file);
			}
		}

		return Response::json(compact('files'));
	}

	public function allTransport(){
		$file_id = Auth::user()->active_file;
		$file = SSCFile::find($file_id);
		$data = compact('file');

		return View::make('transport.all_transport')->with($data);
	}

	public function allTrasnportDataTable(){
		$user = Auth::user();

		$file_id = $user->active_file;
		$transport = Transport::leftJoin('currencies', 'currencies.id', '=', 'transports.currency_id')
							->where('file_id', $file_id)
							->select('transports.*',
										'currencies.name as currency');

		$table =  Datatables::of($transport)
					->editColumn('date', function($data){
						return $data->date->format('d-m-Y');
					})
					->editColumn('ssc_contribution', function($data){
						return $data->ssc_contribution.' ('.$data->percentage().')';
					})
					->editColumn('self_contribution', function($data){
						return $data->self_contribution.' ('.$data->selfPercentage().')';
					})
					->addColumn('percentage', function($data){
						return $data->percentage();
					})
					->addColumn('actions', function($data) use ($user){ 
						$output = '<a href="'.url('/transport/view/'.$data->id).'" class="btn  btn-primary btn-xs">View</a>'; 
	    				$output .= '<a href="'.url('/forms/transport/?id='.$data->id).'"" class="btn  btn-primary btn-xs">Edit</a>';
	    				if($user->user_type == 10){
	    					$output .= '<a class="btn btn-xs btn-primary history" href="'.url('/file-history/transport/',$data->id).'" data-id="'.$data->id.'">History</a>';
		    				$output .= '<a class="btn btn-xs btn-danger delete" data-id="'.$data->id.'">Delete</a>';
		    			}

	    				return $output;
	    			})			
    				->make(true);

    	return $table;
	}

	public function destroy(){
		$id = Input::get('id');
		Transport::destroy($id);

		return Response::json([]);
	}

	public function view($id){
		$transport = Transport::find($id);
		$files = $transport->files;

		$data = compact('transport', 'files');
		return View::make('transport.view')->with($data);
	}

	public function uploadFile(){
		$feedback = [];

		$transport = new Transport;
		$transport->file_id = Auth::user()->active_file;
		$transport->title = Input::get('title');
		$transport->date = date_create_from_format('m/d/Y', Input::get('date'));

		if(Input::hasFile('file')){
			$file = Input::file('file');
			$path = 'files/';
			$name = Str::random(3).time().'.'.$file->getClientOriginalExtension();
			$file->move($path, $name);
			$transport->file = $path.$name;
		}
		$transport->save();

		return Response::json($feedback);
	}

	public function allFormFilesTable(){

		$file_id = Auth::user()->active_file;
		$transports = Transport::whereNotNull('date')
							->where('file','!=','')
							->where('file_id', $file_id)
							->select('*');
		

		$table =  Datatables::of($transports)
					->addColumn('actions', function($data){ 
						$output = '<a href="'.url('/transport/delete/'.$data->id).'" class="btn  btn-primary btn-xs delete">Delete</a>';
	    	// 			$output .= '<a href="/forms/school-assistance/'.$data->id.'" class="btn  btn-primary btn-xs">Edit</a>';
	    	// 			$output .= '<a class="btn btn-xs btn-danger delete" data-id="'.$data->id.'">Delete</a>';

	    				return $output;
	    			})	
	    			->editColumn('title', function($data){
	    				return '<a href="'.url('/'.$data->file).'" target="_blank">'.$data->title.'</a>';
	    			})
	    			->editColumn('date', function($data){
	    				return $data->date->format('d-m-Y');
	    			})
    				->make(true);

    	return $table;
	}

	public function delete($id){
		Transport::destroy($id);
		return Response::json([]);
	}
}