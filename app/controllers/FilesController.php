<?php

use Carbon\Carbon;
class FilesController extends BaseController{

	public function viewUserFiles($id=10){
		$user = Auth::user();
		$userTypeId = $id;
		$userTypes = UserType::all();
		$admin_files = SSCFile::count();

		//$data = compact('user','userTypeId');
		$data = compact('userTypes', 
						'admin_files','user', 'userTypeId');
		//return Redirect::to('/')->with(['userTypeId'=>$userTypeId]);
		return View::make('dashboard')->with($data);
		return View::make('user_assigned_files')->with($data);

	}

	public function filesDatatable($id){
		$user = Auth::user();		
		
		if($user->user_type == 1 || $user->user_type == 10)
		{
			//this was before
			if($id == 1 || $id == 10){
			$files = SSCFile::leftJoin('people','people.file_id','=','files.id')
								->leftJoin('addresses', 'people.id', '=', 'addresses.person_id')
								->select('files.*',
											'people.full_name as person',
											'people.date_of_birth',
											'addresses.mobile_phone_number');
			}else{
				
				
				$files = SSCFile::leftJoin('people','people.file_id','=','files.id')
									->leftJoin('addresses', 'people.id', '=', 'addresses.person_id')
									->where('user_type_assigned', $id)
									->select('files.*',
												'people.full_name as person',
												'people.date_of_birth',
												'addresses.mobile_phone_number');
			}
			//end of before
		}
		else
		{
			$files = SSCFile::leftJoin('people','people.file_id','=','files.id')
									->leftJoin('addresses', 'people.id', '=', 'addresses.person_id')
									->where('user_type_assigned', $user->user_type)
									->select('files.*',
												'people.full_name as person',
												'people.date_of_birth',
												'addresses.mobile_phone_number')->where('files.closed','no');
		}
		
		
		
		$table =  Datatables::of($files)
					->editColumn('next_date', function($data){
						if($data->interviews){
							$output = $data->nextInterviewDate();
						}else{
							$output = '-';
						}
						return $output;
					})
					->addColumn('concludedStatus', function($data){
						return $data->concludedStatus();
					})
					->addColumn('file_type', function($data){
						return $data->userType->name;
					})
					->addColumn('actions', function($data){ 
						$output = (Auth::user()->user_type == 1 || Auth::user()->user_type == 10) ? '<a href="" data-user_type="'.$data->user_type_assigned.'" data-file_id="'.$data->id.'" class="btn  btn-primary btn-xs move">Move</a>' : ''; 
						$output .= '<a href="'.url('/ssc-file/manage',$data->id).'" class="btn  btn-primary btn-xs">Manage</a>';
						//$output .= '<a class="btn btn-xs btn-primary" href="/card?id='.$data->file_no.'|'.$data->user_type_file_no.'" data-id="'.$data->id.'" target="_blank">Membership</a>';
						if(Auth::user()->user_type == 1 || Auth::user()->user_type == 10)
						{
							if($data->closed == 'yes')
							{
								$output .= '<a href="'.url('/ssc-file/open',$data->id).'" class="btn  btn-primary btn-xs open-file">Open</a>'; 
							}
							else
							{
								$output .= '<a href="'.url('/ssc-file/close',$data->id).'" class="btn  btn-danger btn-xs close-file">Close</a>'; 
							}
						}
						

	    				// $output .= '<a href="/forms/luku/?id='.$data->id.'"" class="btn  btn-primary btn-xs">Edit</a>';
	    				// $output .= '<a class="btn btn-xs btn-danger delete" data-id="'.$data->id.'">Delete</a>';

	    				return $output;
	    			})
	    			->addColumn('assigned_to', function($data){
	    				return $data->userType->name;
	    			})			
    				->make(true);

    	return $table;
	}

	public function manageFile($id){
		$user = Auth::user();
		$user->active_file = $id;
		$user->save();

		return Redirect::to('/forms/registration/'.$id);
	}

	public function closeFile($id){
		$file = SSCFile::find($id);
		$file->closed = 'yes';
		$file->save();

		return Response::json([]);
	}
	
	public function openFile($id){
		$file = SSCFile::find($id);
		$file->closed = 'no';
		$file->save();

		return Response::json([]);
	}
	public function move(){
		$user_type_assigned = Input::get('user-type-assigned');
		$file_id = Input::get('file-id');

		$file = SSCFile::find($file_id);
		$file->user_type_assigned = $user_type_assigned;
		$file->save();

		return Response::json([]);
	}

	public function getHistory($type, $id){
		switch ($type) {
			case 'interview':
				$file = Interview::find($id);
				break;

			case 'education':
				$file = SchoolAssistance::find($id);
				break;

			case 'medical':
				$file = Medical::find($id);
				break;

			case 'rent':
				$file = Rent::find($id);
				break;

			case 'dawasco':
				$file = Dawasco::find($id);
				break;

			case 'luku':
				$file = Luku::find($id);
				break;

			case 'capacity-building':
				$file = CapacityBuilding::find($id);
				break;

			case 'will':
				$file = Will::find($id);
				break;

			case 'files':
				$file = SSCFile::find($id);
				break;
			
			default:
				# code...
				break;
		}

		$activities =  $file->activities;
		$data = compact('activities');
		return View::make('reports.history')->with($data);
	}
	
}