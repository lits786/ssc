<?php
use \Carbon\Carbon;

class ReportsController extends BaseController{

	public function getRegistrationsReports(){
		return View::make('reports.registrations');
	}

	public function getRegistrationsTable(){
		$user = Auth::user();		
		$files = SSCFile::leftJoin('people','people.file_id','=','files.id')
								->leftJoin('addresses', 'people.id', '=', 'addresses.person_id')
								->select('files.file_no',
											'people.full_name as person',
											'people.date_of_birth',
											'addresses.mobile_phone_number');
		$fromDate = Input::get('fromDate');
		$toDate = Input::get('toDate');

		if($fromDate){
			$fromDate = Carbon::createFromFormat('d/m/Y H', $fromDate.' 0');
			$files = $files->whereDate('files.created_at','>=',$fromDate);
		}

		if($toDate){
			$toDate = Carbon::createFromFormat('d/m/Y H', $toDate.' 0');
			$files = $files->whereDate('files.created_at','<=',$toDate);
		}
		

		$table =  Datatables::of($files)
					->editColumn('next_date', function($data){
						if($data->interviews){
							$output = $data->nextInterviewDate();
						}else{
							$output = '-';
						}
						return $output;
					})
					->addColumn('concludedStatus', function($data){
						return $data->concludedStatus();
					})
					->addColumn('actions', function($data){ 
						$output = (Auth::user()->user_type == 1) ? '<a href="" data-user_type="'.$data->user_type_assigned.'" data-file_id="'.$data->id.'" class="btn  btn-primary btn-xs move">Move</a>' : ''; 
						$output .= '<a href="'.url('/ssc-file/manage/'.$data->id).'" class="btn  btn-primary btn-xs">Manage</a>'; 
						$output .= '<a href="'.url('/ssc-file/close/'.$data->id).'" class="btn  btn-primary btn-xs close-file">Close</a>'; 

	    				// $output .= '<a href="/forms/luku/?id='.$data->id.'"" class="btn  btn-primary btn-xs">Edit</a>';
	    				// $output .= '<a class="btn btn-xs btn-danger delete" data-id="'.$data->id.'">Delete</a>';

	    				return $output;
	    			})			
    				->make(true);

    	return $table;
	}

	public function getInterviewsReports(){
		return View::make('reports.interviews');
	}

	public function getInterviewsTable(){
		$user = Auth::user();
		$interviews = Interview::orderBy('file_id','DESC')
											->leftJoin('files', 'interviews.file_id', '=', 'files.id')
											->orderBy('files.file_no', 'ASC')
											->select('files.file_no as file_no', 
													'interviews.id',
													'interviews.department');

		$fromDate = Input::get('fromDate');
		$toDate = Input::get('toDate');

		if($fromDate){
			$fromDate = Carbon::createFromFormat('d/m/Y H', $fromDate.' 0');
			$interviews = $interviews->whereDate('interviews.created_at','>=',$fromDate);
		}

		if($toDate){
			$toDate = Carbon::createFromFormat('d/m/Y H', $toDate.' 0');
			$interviews = $interviews->whereDate('interviews.created_at','<=',$toDate);
		}

		$table =  Datatables::of($interviews)
					->editColumn('actions', function($data) use ($user){ 
						$output = '';
						// $output = '<a href="/forms/interview/'.$data->id.'"" class="btn btn-primary btn-xs">Edit</a>';
						$output .= '<a href="'.url('/interview/view/'.$data->id).'" class="btn btn-primary btn-xs">Preview</a>';

						if($user->user_type == 1){
							$output .= '<a class="btn btn-xs btn-primary history" href="'.url('/file-history/interview/'.$data->id).'" data-id="'.$data->id.'">History</a>';
		    				// $output .= '<a class="btn btn-xs btn-danger delete" data-id="'.$data->id.'">Delete</a>';
		    			}

						return $output;
	    			})			
    				->make(true);

    	return $table;
	}

	public function getEducationReports(){
		return View::make('reports.education');
	}

	public function getEducationTable(){
		$user = Auth::user();
		
		$assistances = SchoolAssistance::leftJoin('currencies', 'currencies.id', '=', 'school_assistances.currency_id')
								->leftJoin('schools', 'schools.id', '=', 'school_assistances.school_id')
								->leftJoin('files', 'school_assistances.file_id', '=', 'files.id')
								->where('file','=','')
								->select('name_of_student','gender', 
											'total_amount', 
											'self_contribution', 
											'ssc_contribution',
											'files.file_no as file_no',
											'currencies.name as currency',
											'schools.name as school');
		$fromDate = Input::get('fromDate');
		$toDate = Input::get('toDate');

		if($fromDate){
			$fromDate = Carbon::createFromFormat('d/m/Y H', $fromDate.' 0');
			$assistances = $assistances->whereDate('school_assistances.created_at','>=',$fromDate);
		}

		if($toDate){
			$toDate = Carbon::createFromFormat('d/m/Y H', $toDate.' 0');
			$assistances = $assistances->whereDate('school_assistances.created_at','<=',$toDate);
		}
		

		$table =  Datatables::of($assistances)
					->editColumn('ssc_contribution', function($data){
						return $data->ssc_contribution.' ('.$data->percentage().')';
					})
					->editColumn('self_contribution', function($data){
						return $data->self_contribution.' ('.$data->selfPercentage().')';
					})
					->addColumn('actions', function($data) use ($user){ 
						$output = '';
						$output = '<a href="'.url('/school-assistance/view/'.$data->id).'" class="btn  btn-primary btn-xs">View</a>';
	    				// $output .= '<a href="/forms/school-assistance/'.$data->id.'" class="btn  btn-primary btn-xs">Edit</a>';

	    				if($user->user_type == 1){
	    					$output .= '<a class="btn btn-xs btn-primary history" href="'.url('/file-history/education/'.$data->id).'" data-id="'.$data->id.'">History</a>';
		    				// $output .= '<a class="btn btn-xs btn-danger delete" data-id="'.$data->id.'">Delete</a>';
		    			}


	    				return $output;
	    			})			
    				->make(true);

    	return $table;
	}

	public function getMedicalsReports(){
		return View::make('reports.medicals');
	}

	public function getMedicalsTable(){
		$user = Auth::user();
		$medicals = Medical::leftJoin('currencies', 'currencies.id', '=', 'medicals.currency_id')
							->leftJoin('files', 'files.id', '=', 'medicals.file_id')
							->select('medicals.date', 
									'remarks', 
									'total_amount',
									'self_contribution', 
									'ssc_contribution',
									'currencies.name as currency',
									'file_no');
		$fromDate = Input::get('fromDate');
		$toDate = Input::get('toDate');

		if($fromDate){
			$fromDate = Carbon::createFromFormat('d/m/Y H', $fromDate.' 0');
			$medicals = $medicals->whereDate('medicals.created_at','>=',$fromDate);
		}

		if($toDate){
			$toDate = Carbon::createFromFormat('d/m/Y H', $toDate.' 0');
			$medicals = $medicals->whereDate('medicals.created_at','<=',$toDate);
		}

		$table =  Datatables::of($medicals)
					->editColumn('date', function($data){
						return $data->date->format('d-m-Y');
					})
					->editColumn('ssc_contribution', function($data){
						return $data->ssc_contribution.' ('.$data->percentage().')';
					})
					->editColumn('self_contribution', function($data){
						return $data->self_contribution.' ('.$data->selfPercentage().')';
					})
					->addColumn('actions', function($data) use ($user){
						$output = '';
						$output = '<a href="'.url('/medical/view/'.$data->id).'" class="btn  btn-primary btn-xs">View</a>'; 
	    				// $output .= '<a href="/forms/medical/?id='.$data->id.'" class="btn  btn-primary btn-xs">Edit</a>';

	    				if($user->user_type == 1){
	    					$output .= '<a class="btn btn-xs btn-primary history" href="'.url('/file-history/medical/'.$data->id).'" data-id="'.$data->id.'">History</a>';
		    				// $output .= '<a class="btn btn-xs btn-danger delete" data-id="'.$data->id.'">Delete</a>';
	    				}

	    				return $output;
	    			})			
    				->make(true);

    	return $table;
	}

	public function getRentsReports(){
		return View::make('reports.rents');
	}

	public function getRentsTable(){
		$user = Auth::user();

		$rent = Rent::leftJoin('currencies', 'currencies.id', '=', 'rent.currency_id')
						->leftJoin('files', 'files.id', '=', 'rent.file_id')
						->select('rent.date', 
									'total_amount', 
									'ssc_contribution', 
									'self_contribution',
									'remarks',
									'currencies.name as currency',
									'file_no');
		$fromDate = Input::get('fromDate');
		$toDate = Input::get('toDate');

		if($fromDate){
			$fromDate = Carbon::createFromFormat('d/m/Y H', $fromDate.' 0');
			$rent = $rent->whereDate('rent.created_at','>=',$fromDate);
		}

		if($toDate){
			$toDate = Carbon::createFromFormat('d/m/Y H', $toDate.' 0');
			$rent = $rent->whereDate('rent.created_at','<=',$toDate);
		}

		$table =  Datatables::of($rent)
					->editColumn('date', function($data){
						return $data->date->format('d-m-Y');
					})
					->editColumn('ssc_contribution', function($data){
						return $data->ssc_contribution.' ('.$data->percentage().')';
					})
					->editColumn('self_contribution', function($data){
						return $data->self_contribution.' ('.$data->selfPercentage().')';
					})
					->addColumn('percentage', function($data){
						return $data->percentage();
					})
					->addColumn('actions', function($data) use ($user){
						$output = '' ;
						$output = '<a href="'.url('/rent/view/'.$data->id).'" class="btn  btn-primary btn-xs">View</a>'; 
	    				// $output .= '<a href="/forms/rent/?id='.$data->id.'"" class="btn  btn-primary btn-xs">Edit</a>';
	    				if($user->user_type == 1){
	    					$output .= '<a class="btn btn-xs btn-primary history" href="'.url('/file-history/rent/'.$data->id).'" data-id="'.$data->id.'">History</a>';
		    				// $output .= '<a class="btn btn-xs btn-danger delete" data-id="'.$data->id.'">Delete</a>';
		    			}

	    				return $output;
	    			})			
    				->make(true);

    	return $table;
	}

	public function getDawascoReports(){
		return View::make('reports.dawasco');
	}

	public function getDawascoTable(){

		$user = Auth::user();

		$dawasco = Dawasco::leftJoin('currencies', 'currencies.id', '=', 'dawasco.currency_id')
						->leftJoin('files', 'files.id', '=', 'dawasco.file_id')
						->select('dawasco.date', 
									'total_amount', 
									'ssc_contribution', 
									'self_contribution',
									'remarks',
									'currencies.name as currency',
									'file_no');
		$fromDate = Input::get('fromDate');
		$toDate = Input::get('toDate');

		if($fromDate){
			$fromDate = Carbon::createFromFormat('d/m/Y H', $fromDate.' 0');
			$dawasco = $dawasco->whereDate('dawasco.created_at','>=',$fromDate);
		}

		if($toDate){
			$toDate = Carbon::createFromFormat('d/m/Y H', $toDate.' 0');
			$dawasco = $dawasco->whereDate('dawasco.created_at','<=',$toDate);
		}

		$table =  Datatables::of($dawasco)
					->editColumn('date', function($data){
						return $data->date->format('d-m-Y');
					})
					->editColumn('ssc_contribution', function($data){
						return $data->ssc_contribution.' ('.$data->percentage().')';
					})
					->editColumn('self_contribution', function($data){
						return $data->self_contribution.' ('.$data->selfPercentage().')';
					})
					->addColumn('percentage', function($data){
						return $data->percentage();
					})
					->addColumn('actions', function($data) use ($user){
						$output = '' ;
						$output = '<a href="'.url('/dawasco/view/'.$data->id).'" class="btn  btn-primary btn-xs">View</a>'; 
	    				// $output .= '<a href="/forms/dawasco/?id='.$data->id.'"" class="btn  btn-primary btn-xs">Edit</a>';
	    				if($user->user_type == 1){
	    					$output .= '<a class="btn btn-xs btn-primary history" href="'.url('/file-history/dawasco/'.$data->id).'" data-id="'.$data->id.'">History</a>';
		    				// $output .= '<a class="btn btn-xs btn-danger delete" data-id="'.$data->id.'">Delete</a>';
		    			}

	    				return $output;
	    			})			
    				->make(true);

    	return $table;
	}

	public function getLukuReports(){
		return View::make('reports.luku');
	}

	public function getLukuTable(){

		$user = Auth::user();

		$luku = Luku::leftJoin('currencies', 'currencies.id', '=', 'luku.currency_id')
						->leftJoin('files', 'files.id', '=', 'luku.file_id')
						->select('luku.date', 
									'total_amount', 
									'ssc_contribution', 
									'self_contribution',
									'remarks',
									'currencies.name as currency',
									'file_no');
		$fromDate = Input::get('fromDate');
		$toDate = Input::get('toDate');

		if($fromDate){
			$fromDate = Carbon::createFromFormat('d/m/Y H', $fromDate.' 0');
			$luku = $luku->whereDate('luku.created_at','>=',$fromDate);
		}

		if($toDate){
			$toDate = Carbon::createFromFormat('d/m/Y H', $toDate.' 0');
			$luku = $luku->whereDate('luku.created_at','<=',$toDate);
		}

		$table =  Datatables::of($luku)
					->editColumn('date', function($data){
						return $data->date->format('d-m-Y');
					})
					->editColumn('ssc_contribution', function($data){
						return $data->ssc_contribution.' ('.$data->percentage().')';
					})
					->editColumn('self_contribution', function($data){
						return $data->self_contribution.' ('.$data->selfPercentage().')';
					})
					->addColumn('percentage', function($data){
						return $data->percentage();
					})
					->addColumn('actions', function($data) use ($user){
						$output = '' ;
						$output = '<a href="'.url('/luku/view/'.$data->id).'" class="btn  btn-primary btn-xs">View</a>'; 
	    				// $output .= '<a href="/forms/luku/?id='.$data->id.'"" class="btn  btn-primary btn-xs">Edit</a>';
	    				if($user->user_type == 1){
	    					$output .= '<a class="btn btn-xs btn-primary history" href="'.url('/file-history/luku/'.$data->id).'" data-id="'.$data->id.'">History</a>';
		    				// $output .= '<a class="btn btn-xs btn-danger delete" data-id="'.$data->id.'">Delete</a>';
		    			}

	    				return $output;
	    			})			
    				->make(true);

    	return $table;
	}


	public function getSimpleLoansReports(){
		return View::make('reports.simpleLoans');
	}

	public function getSimpleLoansTable(){

		$user = Auth::user();
		$loans = SimpleLoanPayment::leftJoin('simple_loan_agreements', 'simple_loan_agreements.loan_code', '=', 'simple_loan_payments.simple_loan_code')
							->leftJoin('currencies', 'currencies.id', '=', 'simple_loan_agreements.currency_id')
							->leftJoin('files', 'files.id', '=', 'simple_loan_payments.file_id')
							->select('loan_code',
										'simple_loan_agreements.amount as loan_amount',
										'simple_loan_payments.amount as paid_amount',
										'deadline',
										'currencies.name as currency',
										'files.file_no as file_no');

		$fromDate = Input::get('fromDate');
		$toDate = Input::get('toDate');

		if($fromDate){
			$fromDate = Carbon::createFromFormat('d/m/Y H', $fromDate.' 0');
			$loans = $loans->whereDate('simple_loan_payments.created_at','>=',$fromDate);
		}

		if($toDate){
			$toDate = Carbon::createFromFormat('d/m/Y H', $toDate.' 0');
			$loans = $loans->whereDate('simple_loan_payments.created_at','<=',$toDate);
		}

		$table =  Datatables::of($loans)
					->addColumn('days_left', function($data){
						return $data->deadline->diffInDays(Carbon::now());
					})
					->addColumn('paid', function($data){
						// return $data->payments()->sum('amount');;
					})
					->addColumn('remaining', function($data){
						// return $data->amountRemaining();
					})			
    				->make(true);
    	return $table;
	}

	public function getLoansReports(){
		return View::make('reports.loans');
	}

	public function getLoansTable(){
		$file_id = Auth::user()->active_file;
		$loans = Loan::leftJoin('currencies', 'currencies.id', '=', 'loans.currency_id')
							->rightJoin('loan_payments', 'loans.id', '=', 'loan_payments.loan_id')
							->leftJoin('files', 'files.id', '=', 'loans.file_id')
							->where('file_id', $file_id)
							->select('code',
										'loans.amount as loan_amount',
										'loan_payments.amount as paid_amount',
										'deadline',
										'currencies.name as currency',
										'file_no');

		$fromDate = Input::get('fromDate');
		$toDate = Input::get('toDate');

		if($fromDate){
			$fromDate = Carbon::createFromFormat('d/m/Y H', $fromDate.' 0');
			$loans = $loans->whereDate('loan_payments.created_at','>=',$fromDate);
		}

		if($toDate){
			$toDate = Carbon::createFromFormat('d/m/Y H', $toDate.' 0');
			$loans = $loans->whereDate('loan_payments.created_at','<=',$toDate);
		}

		$table =  Datatables::of($loans)
					->addColumn('days_left', function($data){
						return $data->deadline->diffInDays(Carbon::now());
					})
					->addColumn('paid', function($data){
						return $data->payments()->sum('amount');;
					})
					->addColumn('remaining', function($data){
						return $data->amountRemaining();
					})			
    				->make(true);
    	return $table;
	}


}