<?php
use Carbon\Carbon;

class SimpleLoanController extends BaseController{

	public function getLoanAgreement(){
		$currencies = Currency::orderBy('name', 'ASC')->get();
		$rates = Rate::orderBy('name', 'ASC')->get();

		$data = compact('currencies', 'rates');

		return View::make('simple_loan.loan_agreement')->with($data);
	}

	public function getLoanPayments(){
		$loans = SimpleLoanAgreement::all();
		$data = compact('loans');

		return View::make('simple_loan.loan_payments')->with($data);
	}

	public function saveSimpleLoanAgreement(){
		$feedback = [];
		$id = Input::get('id');

		$agreement = ($id==0) ? new SimpleLoanAgreement : SimpleLoanAgreement::find($id);
		$agreement->file_id = Auth::user()->active_file;
		$agreement->loan_code = Input::get('loan-code');
		$agreement->currency_id = Input::get('currency-id');
		$agreement->amount = Input::get('amount');
		$agreement->deadline = Carbon::createFromFormat('d/m/Y', Input::get('deadline'));
		$agreement->rate_id = 0;//Input::get('rate-id');
		$agreement->date = Carbon::createFromFormat('d/m/Y', Input::get('date-of-agreement'));
		$agreement->comments = Input::get('comments');

		if(Input::hasFile('file')){
			$file = Input::file('file');
			$path = 'files/simple_loans/';
			$name = Str::random(3).time().'.'.$file->getClientOriginalExtension();
			$file->move($path, $name);
			$path .= $name;
			$agreement->file = $path;
		}

		$agreement->save();

		return Response::json($feedback);
	}


	public function allAgreementsDataTable(){
		$user = Auth::user();
		$file_id = $user->active_file;

		$agreements = SimpleLoanAgreement::where('file_id', $file_id)
											->select('*');
		$table = Datatables::of($agreements)
								->editColumn('file', function($data){
									$output = '<a href="'.url($data->file).'" target="_blank">Attachment</a>';
									return $output;
								})
								->addColumn('amount', function($data){
									$output = $data->getAmount();
									return $output;
								})
								->editColumn('deadline', function($data){
									return $data->deadline->format('d M Y');
								})
								->addColumn('actions', function($data) use ($user){
									$output = '';
									if($user->user_type == 10){
										$output .= '<a class="btn btn-xs btn-danger delete" data-id="'.$data->id.'">Delete</a>';
									}
									return $output;
								})
								->make(true);

		return $table;
	}

	public function savePayment(){
		$feedback = [];

		$payment = new SimpleLoanPayment;
		$payment->file_id = Auth::user()->active_file;
		$payment->simple_loan_code = Input::get('loan-code');
		$payment->amount = Input::get('amount');
		$payment->save();
		$payment->remaining = $payment->SimpleLoanAgreement->amountRemaining();
		$payment->save();

		return Response::json($feedback);
	}


	public function allPaymentsTable(){
		$user = Auth::user();
		$file_id = $user->active_file;

		$payments = SimpleLoanPayment::where('simple_loan_payments.file_id', $file_id)
						->leftJoin('simple_loan_agreements','simple_loan_agreements.loan_code', '=', 'simple_loan_payments.simple_loan_code')
						->leftJoin('currencies','currencies.id','=','simple_loan_agreements.currency_id')
						->select('simple_loan_payments.*',
							'simple_loan_payments.amount as paid', 
							'simple_loan_agreements.amount as amount',
							'simple_loan_agreements.deadline as deadline',
							'currencies.name as currency');

		$table = Datatables::of($payments)
								->addColumn('days_left', function($data){
									return $data->deadline->diffInDays(Carbon::now());
								})
								->addColumn('actions', function($data) use ($user){
									$output = '';
									if($user->user_type == 1){
										$output .= '<a class="btn btn-xs btn-danger delete" data-id="'.$data->id.'">Delete</a>';
									}
									return $output;
								})
								->make(true);
		return $table;
	}

	public function deleteAgreement($id){
		SimpleLoanAgreement::destroy($id);

		return Response::json([]);
	}

	public function deletePayment($id){
		SimpleLoanPayment::destroy($id);
		
		return Response::json([]);
	}
}