<?php

class RegistrationController extends BaseController{
	public function save(){
		//dd(Input::all());
		$user = Auth::user();
		$id = Input::get('id');
		// echo('id is '.$id);
		if($id==0){
			$address_id = 0;
			$person_id = 0;
			$residence_id = 0;
			$income_id = 0;
			$spouse_id = 0;
			$assistance_id = 0;
			$remarks_id = 0;
		}else{
			$file = SSCFile::find($id);
			$address_id = (!is_null($file->person)) ? (!is_null($file->person->address))? $file->person->address->id: null : null;
			$person_id =(!is_null($file->person)) ? $file->person->id : null;
			$residence_id =(!is_null($file->person)) ? (!is_null($file->person->residence))? $file->person->residence->id : null : null;
			$income_id = (!is_null($file->person)) ? (!is_null($file->person->income))? $file->person->income->id : null : null;
			$spouse_id =(!is_null($file->person)) ? (!is_null($file->person->spouse))? $file->person->spouse->id : null : null;
			$assistance_id = (!is_null($file->person)) ? (!is_null($file->person->assistance))? $file->person->assistance->id : null : null;
			$remarks_id = (!is_null($file->remarks)) ? $file->remarks->id : null;

		}

		$file = ($id == 0) ? new SSCFile : SSCFile::find($id);		
		$status = ($id == 0) ? 1 : 2;
		$file->created_by = Auth::id();
		$file->terms_agreed = Input::get('agree-terms',0);		
		
		$file->user_type_assigned = Input::get('user-type-assigned', $user->user_type);
		$file->outreach_type_id = Input::get('outreach-type');
		$file->lawajam_details = Input::get('lawajam-details');
		$file->lawajam_member_id = Input::get('lawajam-member-id');
		$file->lawajam_member_paid = Input::get('lawajam-member-date-paid');
		$file->lawajam_member_last_paid = Input::get('lawajam-member-date-paid-to');
		$file->date_paid = date_create_from_format('d/m/Y', Input::get('date-paid'));
		$file->date = date_create_from_format('d/m/Y', Input::get('date'));
		$file->approved = Input::get('approved',0);
		$file->concluded = Input::get('concluded', 'no');

		if($file->save()){
			Session::put('file-id', $file->id);

		$file->file_no = Input::get('general-file');//$file -> generalFileNo();//formatFileNumber(Input::get('user-type-assigned', $user->user_type),Input::get('outreach-type'));
		if($file->user_type_file_no==null && $file->user_type_file_no=="")
			$file->user_type_file_no =  $file-> midFileNumber(Input::get('user-type-assigned', $user->user_type));
		$c = Input::get('user-type-assigned', $user->user_type);
		if($c == 5)
		{
			if($id==0)
			$file->user_type_or_no = $file-> orTypeFileNumber(Input::get('outreach-type'));
		}else
		{
			$file->user_type_or_no = '-';
		}
		
		$file->save();		


		$person = ($person_id == 0 || is_null($person_id)) ? new Person : Person::find($person_id);
		$person->file_id = $file->id;
		$person->full_name = Input::get('full-name');
		$person->date_of_birth = date_create_from_format('d/m/Y', Input::get('date-of-birth'));
		$person->passport_number = Input::get('passport-number');
		$person->nationality = Input::get('nationality');
		$person->passport_expiry_date = date_create_from_format('d/m/Y', Input::get('date-of-birth'));
		$person->education_level = Input::get('education-level');
		$person->head_of_family = Input::get('head-of-family');
		$person->voter_id= Input::get('voter-id');
		$person->driver_licence = Input::get('license-number');
		$person->national_id= Input::get('national-id');
		$person->maritual_status= Input::get('maritual-status');
		$person->maritual_status_name= Input::get('maritual-status-name');
		$person->maritual_status_comment= Input::get('maritual-status-comment');
		$person->total_family= Input::get('total-family');
		$person->total_kids = Input::get('total-kids');
		$person->total_adults = Input::get('total-adults');

		if(Input::hasFile('passport-image')){
					$ufile = Input::file('passport-image');
			        $name = str_random(12).time().'.'.$ufile->getClientOriginalExtension();
			        $path = 'assets/images/passports/'.$name;
			        $db_path = 'images/passports/'.$name;
			        $upload_success = Image::make($ufile)->save($path);
			        $person->passport_image = $db_path;
		}
		
		if(Input::hasFile('voter-image')){
					$ufile = Input::file('voter-image');
			        $name = str_random(12).time().'.'.$ufile->getClientOriginalExtension();
			        $path = 'assets/images/voters/'.$name;
			        $db_path = 'images/voters/'.$name;
			        $upload_success = Image::make($ufile)->save($path);
			        $person->voter_image = $db_path;
		}
		
		if(Input::hasFile('driver-licence-image')){
					$ufile = Input::file('driver-licence-image');
			        $name = str_random(12).time().'.'.$ufile->getClientOriginalExtension();
			        $path = 'assets/images/drivers/'.$name;
			        $db_path = 'images/drivers/'.$name;
			        $upload_success = Image::make($ufile)->save($path);
			        $person->driver_image = $db_path;
		}
		
		if(Input::hasFile('national-image')){
					$ufile = Input::file('national-image');
			        $name = str_random(12).time().'.'.$ufile->getClientOriginalExtension();
			        $path = 'assets/images/national/'.$name;
			        $db_path = 'images/national/'.$name;
			        $upload_success = Image::make($ufile)->save($path);
			        $person->national_image = $db_path;
		}
		
		if(Input::hasFile('photo')){
			$ufile = Input::file('photo');
	        $name = str_random(12).time().'.'.$ufile->getClientOriginalExtension();
	        $path = 'assets/images/files/'.$name;
	        $db_path = 'images/files/'.$name;
	        $upload_success = Image::make($ufile)->save($path);
	        $person->image = $db_path;
		}
		$person->save();

			if($person->save()){
				$address = ($address_id == 0 || is_null($address_id)) ? new Address : Address::find($address_id);
				$address->person_id = $person->id;
				$address->residence_phone_number = Input::get('residence-phone');
				$address->office_phone_number = Input::get('office-phone');
				$address->mobile_phone_number = Input::get('mobile-phone');
				$address->postal_address = Input::get('postal-address');
				$address->physical_address = Input::get('physical-address');
				$address->email = Input::get('email');
				$address->plot_number = Input::get('plot-no');
				$address->block_number = Input::get('block-no');
				$address->floor_number = Input::get('floor-no');
				$address->street = Input::get('street');
				$address->nearest_landmark = Input::get('nearest-landmark');
				$address->save();



				$residence = ($residence_id == 0 || is_null($residence_id)) ? new Residence : Residence::find($residence_id);
				$residence->person_id = $person->id;
				$residence->residence_type_id = Input::get('type-of-residence',0);
				$residence->residence_ownership_type_id = Input::get('residence-owner',0);
				$residence->rent_per_month = Input::get('rent-per-month');
				$residence->trustee_owner_name = Input::get('trustee-owner-name');
				$residence->trustee_rent = Input::get('trustee-rent'); 
				$residence->save();

				$income = ($income_id == 0 || is_null($income_id)) ? new Income : Income::find($income_id);
				$income->person_id = $person->id;
				$income->income_type_id = Input::get('income-type',0);
				$income->business_name = Input::get('business-name');
				$income->location = Input::get('location');
				$income->business_type = Input::get('business-type');
				$income->income_per_month = Input::get('income-per-month');
				$income->other_incentives = Input::get('other-incentives');
				$income->other_incentives_employed = Input::get('other-incentives-employed');
				$income->office_employed = Input::get('office-employed');
				$income->employers_name = Input::get('employers-name');		
				$income->position_employed = Input::get('position-employed');
				$income->salary_per_month = Input::get('salary-per-month');
				$income->unemployment_name  = Input::get('unemployment-name');
				$income->unemployment_amount = Input::get('unemployment-amount');
				$income->unemployment_relation = Input::get('unemployment-relation');
				$income->unemployment_comments = Input::get('unemployment-comments');
				$income->save();

				$spouse = ($spouse_id == 0 || is_null($spouse_id)) ? new Spouse : Spouse::find($spouse_id);
				$spouse->person_id = $person->id;
				$spouse->full_name = Input::get('spouse-full-name');
				$spouse->date_of_birth = date_create_from_format('d/m/Y', Input::get('spouse-date-of-birth'));
				$spouse->occupation = Input::get('spouse-occupation','');


				if(!empty(Input::file('spouse-photo'))){ 
					if(Input::file('spouse-photo')){
						// echo 'file present';
						$file = Input::file('spouse-photo');
						$name = str_random(12).time().'.'.$file->getClientOriginalExtension();
						$path = 'assets/images/spouse/'.$name;
						$db_path = 'images/spouse/'.$name;
						$upload_success = Image::make($file)->save($path);
						$spouse->image = $db_path;
					}else{
						// echo 'file absent';
					}
				}

				$spouse->save();

				if($spouse->save()){
					$spouse_income = new Income;
					$spouse_income->spouse_id = $spouse->id;
					$spouse_income->income_type_id = Input::get('spouse-income-type',0);
					$spouse_income->office_employed = Input::get('spouse-office-employed');
					$spouse_income->employers_name = Input::get('spouse-employers-name');
					$spouse_income->position_employed = Input::get('spouse-position-employed');
					$spouse_income->salary_per_month = Input::get('spouse-salary-per-month');
					$spouse_income->save();
				}

				// $person->children()->delete();

				$child_names = Input::get('child-name');
				$child_gender = Input::get('child-gender');
				$child_date_of_birth = Input::get('child-date-of-birth');
				$child_occupation = Input::get('child-occupation');
				$child_other_info = Input::get('child-other-info');
				// $child_photo = Input::file('child-photo');
				$child_photo = Input::file('child-photo');
				$child_school_college = Input::get('child-school-college');
				$child_married = Input::get('child-married');
				$child_ids = Input::get('child_ids'); 
				// echo count($child_photo);
				// return Response::json(Input::file('child-photo'));
				//dd($child_date_of_birth[0]); 
				//dd($child_date_of_birth);
				//if($child_names[0] != "")
				//{ 
					foreach($child_names as $key => $name){ 
						
						if(!empty($child_ids[$key])){
							$child = $child_ids[$key]!=null?Child::find($child_ids[$key]):new Child;
						}
						else
							$child = new Child;

						if(!empty($child_names[$key])||$child_names[$key]!=""){
							$child->person_id = $person->id;
							$child->full_name = $name;
							$child->gender = Input::get('child-gender-'.$key); 
								$child->date_of_birth = date_create_from_format('d/m/Y', $child_date_of_birth[$key]);
							//$child->date_of_birth = $child_date_of_birth[$key];
							$child->occupation = $child_occupation[$key];
							$child->other_info = $child_other_info[$key];
							$child->school_college = $child_school_college[$key];
							
							if($child_married!=null)
							if(array_key_exists($key,$child_married))
								$child->married = $child_married[$key];
							else
								$child->married = null;
							
							//var_dump($child_photo);
							if(!empty($child_photo)){
								// dd("here");
								if(!empty($child_photo[$key]))
								if($child_photo[$key]){
									// echo 'file present';
									$file = $child_photo[$key];
									$name = str_random(12).time().'.'.$file->getClientOriginalExtension();
									$path = 'assets/images/children/'.$name;
									$db_path = 'images/children/'.$name;
									$upload_success = Image::make($file)->save($path);
									$child->image = $db_path;
								}else{
									// echo 'file absent';
								}
							}	
							if($child!=null)
								$child->save();
						}else{ 
							if($child!=null)
								$child->delete(); 
						}			
					}
				//}				
				
				$person->other()->delete();

				$other_names = Input::get('other-name');
				$other_date_of_birth = Input::get('other-date-of-birth');
				$other_occupation = Input::get('other-occupation');
				$other_phones = Input::get('other-phone');
				$other_emails = Input::get('other-email');
				$other_address = Input::get('other-address');
				$other_family_assistance = Input::get('other-family-assistance');
				$other_comments = Input::get('other-comments');
				
				if($other_names[0] != "")
				{
				foreach($other_names as $key => $name){
					$other = new Other;
					$other ->person_id = $person->id;
					$other ->full_name = $name;
					$other ->gender = Input::get('other-gender-'.$key);
					$other ->date_of_birth = date_create_from_format('d/m/Y', $other_date_of_birth[$key]);
					$other ->occupation = $other_occupation[$key];
					$other ->email = $other_emails[$key];
					$other ->relationship_id= Input::get('other-relationship-'.$key);
					if(Input::get('other-relationship-'.$key) == 8)
					{
						$other ->relationship = Input::get('other-relation-name-'.$key);
					}
					$other ->address= $other_address[$key];
					$other ->phone= $other_phones[$key];
					$other ->family_assistance = $other_family_assistance[$key];
					$other ->comments = $other_comments[$key];
					$other ->save();
				}
				}
				
			}

		}

		$assistance = ($assistance_id ==  0 || is_null($assistance_id)) ? new Assistance : Assistance::find($assistance_id);
		$assistance->person_id = $person->id;
		$assistance->rent = Input::get('rent');
		$assistance->school_fees = Input::get('school-fees');
		$assistance->medical = Input::get('medical');
		$assistance->electricity = Input::get('electricity');
		$assistance->water = Input::get('water');
		$assistance->others = Input::get('others');
		$assistance->ramadhan = Input::get('ramadhan','');
		$assistance->monthly_allocate = Input::get('monthly-allocate','');
		
		if( Input::get('monthly-allocate') == 'yes')
		{
			$assistance->monthly_allocated_amount = Input::get('monthly-allocated-amount');
		}
		
		$assistance->fitro = Input::get('fitro');
		$assistance->fitro_comment = Input::get('fitro-comment');
		
		$medical_payment_type = Input::get('medical-payment-type');
		if($medical_payment_type == 1)
		{
			$assistance->medical_payment_type_id = $medical_payment_type;
			$assistance->medical_cash_amount = Input::get('medical-payment-type-cash');
		}
		else
		{
			$assistance->medical_payment_type_id = $medical_payment_type;
		}
		$assistance->save();

		$f_id = SSCFile::orderBy('updated_at','DESC')->first();
		
		$remark = ($remarks_id == 0 || is_null($remarks_id)) ? new Remark : Remark::find($remarks_id);
		$file_id = ($id == 0) ? $f_id->id : $id;
		//var_dump($file_id);		
		$remark->file_id = $file_id;
		$remark->by_member = Input::get('remarks-member');
		$remark->by_head_of_department = Input::get('remarks-head-of-department');
		$remark->by_chairman_of_welfare = Input::get('remarks-chairman-of-welfare');
		$remark->notes = Input::get('remarks-notes');
		$remark->save();

		$feedback = compact('status');
		return Response::json($feedback);
	}


	public function getChildSubform(){
		$index = Input::get('child_index');
		$data = compact('index');

		return View::make('ajax.child_sub_form')->with($data);
	}
	
	public function getOtherSubform(){
		$index = Input::get('other_index');
		$relationships = Relationship::all();
		$data = compact('index','relationships');

		return View::make('ajax.other_sub_form')->with($data);
	}


	public function allRegistered(){
		return View::make('registration.all_registrations');
	}

	public function allRegisteredDataTable(){
		$registrations = SSCFile::orderBy('file_no','ASC')
										->leftJoin('people', 'people.file_id', '=', 'files.id')
										->select('files.*', 'people.full_name as full_name');

		$table =  Datatables::of($registrations)
					->editColumn('actions', function($data){ 
						$output = '<a href="'.url('/forms/registration/?file_no='.$data->file_no).'" class="btn btn-primary btn-xs">Edit</a>';
	    				$output .='<a href="'.url('/registration/view/'.$data->id).'" class="btn btn-primary btn-xs">Preview</a>';

	    				return $output;
	    			})			
    				->make(true);

    	return $table;	
	}

	public function view($id){
		$file = SSCFile::find($id);
		$person = $file->person;
		$address = is_null($person->address) ? new Person : $person->address;
		$residence = is_null($person->residence) ? new Residence : $person->residence;
		$income = is_null($person->income) ? new Income : $person->income;
		$spouse = is_null($person->spouse) ? new Spouse : $person->spouse;
		$children = $person->children;
		$assistance = is_null($person->assistance) ? new Assistance : $person->assistance;
		$remarks = is_null($file->remarks) ? new Remark : $file->remarks;

		$data = compact('file' ,
					'person' ,
					'address', 
					'residence', 
					'income', 
					'spouse',
					'children',
					'assistance',
					'remarks');
		
		return View::make('registration.view')->with($data);
	}
}