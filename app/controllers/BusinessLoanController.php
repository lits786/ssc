
<?php

use Carbon\Carbon; 

class BusinessLoanController extends BaseController{

	public function getLoanAgreement(){
		$currencies = Currency::orderBy('name', 'ASC')->get();
		$rates = Rate::orderBy('name', 'ASC')->get();
		$data = compact('currencies', 'rates');
		return View::make('business_loan.loan_agreement')->with($data);
	}

	public function getLoanPayments(){
		$loans = BusinessLoanAgreement::all();
		$data = compact('loans');
		return View::make('business_loan.loan_payments')->with($data);
	}

	public function saveLoanAgreement(){
		$feedback = [];
		$id = Input::get('id'); 

		if($id==0){
			$prev_loan = BusinessLoanAgreement::orderBy('id','desc')->whereFileId(Auth::user()->active_file)->first(); 
			if($prev_loan)
			if($prev_loan->amountRemaining()>0)
				return Response::json(["status"=>900,"result"=>"previous loan be must zero, it is ".$prev_loan->getAmountRemaining()]);
		}
		$agreement = ($id==0) ? new BusinessLoanAgreement : BusinessLoanAgreement::find($id);
		$agreement->file_id = Auth::user()->active_file;
		$agreement->loan_code = Input::get('loan-code');
		$agreement->currency_id = Input::get('currency-id');
		$agreement->amount = Input::get('amount');
		$agreement->deadline = Carbon::createFromFormat('d/m/Y', Input::get('deadline'));
		// $agreement->rate_id = Input::get('rate-id');
		$agreement->date = Carbon::createFromFormat('d/m/Y', Input::get('date-of-agreement'));

		$agreement->name = Input::get('name');
		$agreement->save();

		//dd(Input::all());
		$descriptions = Input::get('description');
		$attachments = Input::file('attachment'); 

		if($attachments)
		foreach ($descriptions as $key => $value) {
			$loan_attachment = new BusinessLoanAttachment;
			$loan_attachment->description = $descriptions[$key];
			$loan_attachment->business_loan_agreement_id = $agreement->id;
			$loan_attachment->save();
			foreach ($attachments[$key] as $key => $value) {				
				$file = $value;
				$path = 'files/business_loans_attachments/';
				// $name = Str::random(3).time().'.'.$file->getClientOriginalExtension();
				$name = $file->getClientOriginalName().'.'.$file->getClientOriginalExtension();
				$file->move($path, $name);
				$path .= $name;
				$file_attachment = new BusinessLoanAttachmentFile;
				$file_attachment->business_loan_attachment_id = $loan_attachment->id; 
				$file_attachment->file_url = $path;
				$file_attachment->save();
			}
		}
		return Response::json($feedback);
	}


	public function allAgreementsDataTable(){
		$user = Auth::user();
		$file_id = $user->active_file;

		$agreements = BusinessLoanAgreement::where('file_id', $file_id)
											->select('*');
		$table = Datatables::of($agreements)
								->editColumn('file', function($data){
									$output = '<a href="/'.$data->file.'" target="_blank">Attachment</a>';
									return $output;
								})
								->addColumn('amount', function($data){
									$output = $data->getAmount();
									return $output;
								})
								->editColumn('deadline', function($data){
									return $data->deadline->format('d M Y');
								})
								->addColumn('actions', function($data) use ($user){
									$output = '';
										$output .= '<a class="btn btn-xs btn-success viw" href="'.url('business-loan/agreement/view',$data->id).'" data-id="'.$data->id.'">View</a>';
										$output .= '<a class="btn btn-xs btn-danger delete" data-id="'.$data->id.'">Delete</a>';
									if($user->user_type == 1){
									}
									return $output;
								})
								->make(true); 

		return $table;
	}

	public function savePayment(){
		$feedback = [];

		$payment = new BusinessLoanPayment;
		$payment->file_id = Auth::user()->active_file;
		$payment->business_loan_code = Input::get('loan-code');
		$payment->amount = Input::get('amount');
		$payment->save();
		$payment->remaining = $payment->BusinessLoanAgreement->amountRemaining();
		$payment->save();

		return Response::json($feedback);
	}


	public function allPaymentsTable(){
		$user = Auth::user();
		$file_id = $user->active_file;

		$payments = BusinessLoanPayment::where('business_loan_payments.file_id', $file_id)
						->leftJoin('business_loan_agreements','business_loan_agreements.loan_code', '=', 'business_loan_payments.business_loan_code')
						->leftJoin('currencies','currencies.id','=','business_loan_agreements.currency_id')
						->select('business_loan_payments.*',
							'business_loan_payments.amount as paid', 
							'business_loan_agreements.amount as amount',
							'business_loan_agreements.deadline as deadline',
							'currencies.name as currency')
							->where('business_loan_payments.deleted_at',null);
						//->whereNull('business_loan_payments.deleted_at');

		$table = Datatables::of($payments)
								->addColumn('days_left', function($data){
									return $data->deadline->diffInDays(Carbon::now());
								})
								->addColumn('actions', function($data) use ($user){
									$output = '';
									if($user->user_type == 1){
										$output .= '<a class="btn btn-xs btn-danger delete" data-id="'.$data->id.'">Delete</a>';
									}
									return $output;
								})
								->make(true);
		return $table;
	}

	public function deleteAgreement($id){ 
		$agreement = BusinessLoanAgreement::find($id);
		$attachments = $agreement->attachments; 
		foreach ($attachments as $key => $value) {
			$files = $value->files;
			foreach ($files as $key => $value) {
				File::delete(public_path().$value->file_url);
				$value->delete();
			}
			$value->delete();
		}
		$agreement->delete(); 

		return Response::json([]);
	}

	public function viewAgreement($id){ 
		$agreement = BusinessLoanAgreement::find($id); 
		return View::make('business_loan.view', ['agreement'=>$agreement]);;
	}

	public function deletePayment($id){
		BusinessLoanPayment::destroy($id);
		
		return Response::json([]);
	}

	public function addAttachment()
	{
		return View::make('ajax.add_business_loan_files');
	}
}