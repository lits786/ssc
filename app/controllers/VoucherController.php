<?php

use Carbon\Carbon;

class VoucherController extends BaseController{
	public function getVoucherForm($id=0){
		if($id!=0){
			$voucher = Voucher::find($id);
			$voucher_id = $voucher->id;
		}else{
			$voucher = null;
			$voucher_id = 0;
		}

		$data = compact('voucher', 
							'voucher_id');
		return View::make('forms.voucher')->with($data);
	}


	public function save(){
		$id = Input::get('id'); 
		$voucher = ($id == 0) ? new Voucher : Voucher::find($id);
		$voucher->created_by = Auth::id();
		$voucher->date = date_create_from_format('d/m/Y', Input::get('date')); 
		$voucher->file_id = Auth::user()->active_file;
		$voucher->amount = Input::get('amount');
		$voucher->payable_to = Input::get('payable_to');
		$voucher->in_respect_of = Input::get('in_respect_of');
		$voucher->amount_in_words = Input::get('amount_in_words');
		$voucher->prepared_by = Input::get('prepared_by');
		$voucher->authorised = Input::get('authorised');
		$voucher->received = Input::get('received');
		$voucher->reference = Input::get('reference');
		$voucher->save();

		return Response::json([]);
	}

	public function printVoucher($id)
	{
		$voucher = Voucher::find($id); 
		return View::make('print.voucher',['voucher'=>$voucher]);
	}


	public function allVouchers(){
		$user = Auth::user();

		if($user->active_file){
			$vouchers = Voucher::orderBy('file_id','DESC')
											->leftJoin('files', 'vouchers.file_id', '=', 'files.id')
											->where('file_id', $user->active_file)
											->select('files.file_no as file_no', 'vouchers.*');
		}else{
			$vouchers = Voucher::orderBy('file_id','DESC')
											->leftJoin('files', 'vouchers.file_id', '=', 'files.id')
											->select('files.file_no as file_no', 'vouchers.*');
		}
		//dd($vouchers->count());
		return View::make('vouchers.all_vouchers',['vouchers'=>$vouchers]);
	} 

	public function allVouchersDataTable(){
		$user = Auth::user();

		if($user->active_file){
			$vouchers = Voucher::orderBy('file_id','DESC')
											->leftJoin('files', 'vouchers.file_id', '=', 'files.id')
											->where('file_id', $user->active_file)
											->select('files.file_no as file_no', 'vouchers.*');
		}else{
			$vouchers = Voucher::orderBy('file_id','DESC')
											->leftJoin('files', 'vouchers.file_id', '=', 'files.id')
											->select('files.file_no as file_no', 'vouchers.*');
		}

		$table =  Datatables::of($vouchers)
					->editColumn('actions', function($data) use ($user){ 
						$output = '<a href="'.url('/forms/voucher/'.$data->id).'"" class="btn btn-primary btn-xs">Edit</a>';
						$output .= '<a href="'.url('/voucher/view/'.$data->id).'" class="btn btn-primary btn-xs">Preview</a>';
						$output .= '<a href="'.url('voucher/print/'.$data->id).'" class="btn btn-xs btn-primary print-button" id="'.$data->id.'">Print</a>';

						if($user->user_type == 1){
							$output .= '<a class="btn btn-xs btn-primary history" href="'.url('/file-history/voucher/'.$data->id).'" data-id="'.$data->id.'">History</a>';
		    				$output .= '<a class="btn btn-xs btn-danger delete" data-id="'.$data->id.'">Delete</a>';
		    			}

						return $output;
	    			})			
    				->make(true);

    	return $table;
	}

	public function delete($id){
		Voucher::destroy($id);

		return Response::json([]);
	}


	public function view($id){
		$voucher = Voucher::find($id);

		$data = compact('voucher');
		return View::make('vouchers.view')->with($data);
	}
}