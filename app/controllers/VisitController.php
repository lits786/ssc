<?php

class VisitController extends BaseController{
	
	public function getVisitForm(){
		$visit = (Input::get('id')!=null&&Input::get('id')!="")?Visit::find(Input::get('id')):null;
		return View::make('forms.visit',['visit'=>$visit]);
	}

	public function save(){ 
		$id = Input::get('id');
		$visit = ($id == 0) ? new Visit : Visit::find($id);
		$visit->created_by = Auth::id();
		
		$visit->file_id = Auth::user()->active_file;
		
		
		$visit ->date = date_create_from_format('m/d/Y', Input::get('date'));
		$visit ->name = Input::get('full-name');
		$visit ->purpose= Input::get('purpose');
		$visit ->comment = Input::get('comment');
		$visit ->action= Input::get('action');
		
		
		$visit ->save();
		
		if(Input::hasFile('files')){
			$files = Input::file('files');

			foreach ($files as $file) {
				$path = 'files/';
				$name = Str::random(3).time().'.'.$file->getClientOriginalExtension();
				$file->move($path, $name);
				$full_path = $path.$name;

				$visit_file = new VisitFile;
				$visit_file->path = $full_path;
				$visit->files()->save($visit_file);
			}
		}


	return Response::json(compact('$visit'));
	}



	public function allVisitsDataTable(){
		$user = Auth::user();
		$file_id = $user->active_file;
		$visits = Visit::where('file_id', $file_id)->select('visits.*');
		
		$table =  Datatables::of($visits)
					->editColumn('date', function($data){
						$timestamp = strtotime($data->date);
						$newDate = date('d/m/Y', $timestamp);
						return $newDate;
					})
					->editColumn('name', function($data){
						return $data->name;
					})
					->editColumn('purpose', function($data){
						return $data->purpose;
					})
					->addColumn('comment', function($data){
						return $data->comment;
					})
					->addColumn('action', function($data) use ($user){ 
						$output = ''; 
	    				$output .= '<a href="'.url('/forms/visit?id='.$data->id).'"" class="btn  btn-primary btn-xs">Edit</a>';
	    				if($user->user_type == 1){
	    					$output .= '<a class="btn btn-xs btn-danger delete" data-id="'.$data->id.'">Delete</a>';
		    			}

	    				return $output;
	    			})			
    				->make(true);

    	return $table;
	}

	public function destroy(){
		$id = Input::get('id');
		Visit::destroy($id);

		return Response::json([]);
	}

	

	public function delete($id){
		Visit::destroy($id);
		return Response::json([]);
	}
}