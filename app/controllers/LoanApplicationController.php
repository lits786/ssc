<?php
class LoanApplicationController extends BaseController{
	
	public function save(){
		$id = Input::get('id');
		$application = ($id == 0) ? new LoanApplication : LoanApplication::find($id);
		$application->loan_id = Input::get('loan-code');
		$application->how_did_you_hear = Input::get('how-did-you-hear');
		$application->bank_creditor = Input::get('bank-creditor');
		$application->credit_limit = Input::get('credit-limit');
		$application->outstand_balance = Input::get('outstand-balance');
		$application->payment_frequency = Input::get('payment-frequency');
		$application->arreas = Input::get('arreas');
		$application->date_started = Input::get('date-started', '0/0/0000');
		$application->date_completed = Input::get('date-completed',  '0/0/0000');
		$application->referee_name = Input::get('referee-name');
		$application->referee_relationship = Input::get('referee-relationship');
		$application->referee_address = Input::get('referee-address');
		$application->loan_amount = Input::get('loan-amount');
		$application->loan_duration = Input::get('loan-duration');
		$application->loan_purpose = Input::get('loan-purpose');
		$application->preffered_rate = Input::get('preffered-rate');
		$application->affordable_repayment = Input::get('affordable-repayment');
		$application->project_cost = Input::get('project-cost');
		$application->business_name = Input::get('business-name');
		$application->business_address = Input::get('business-address');
		$application->business_telephone = Input::get('business-telephone');
		$application->business_fax = Input::get('business-fax');
		$application->business_email = Input::get('business-email');
		$application->business_ownership = Input::get('business-ownership');
		$application->type_of_business = Input::get('type-of-business');
		$application->how_long_trading = Input::get('how-long-trading');
		$application->business_registration = Input::get('business-registration');
		$application->registration_date = Input::get('registration-date', '0/0/0000');
		$application->vat_no = Input::get('vat-no');
		$application->state_type = Input::get('state-type');
		$application->business_place_ownership = Input::get('business-place-ownership');
		$application->landlord_name = Input::get('landlord-name');
		$application->landlord_address = Input::get('landlord-address');
		$application->landlord_telephone = Input::get('landlord-telephone');
		$application->landlord_email = Input::get('landlord-email');
		$application->shop_location = Input::get('shop-location');
		$application->other_sources_of_income = Input::get('other-sources-of-income');
		$application->strength = Input::get('strength');
		$application->weaknesses = Input::get('weaknesses');
		$application->opportunities = Input::get('opportunities');
		$application->threats = Input::get('threats');
		$application->family_income_expenditure = Input::get('family-income-expenditure');
		$application->bank_details = Input::get('bank-details');
		$application->additional_info = Input::get('additional-info');
		$application->declaration_full_name = Input::get('declaration-full-name');
		$application->declaration_postal_address = Input::get('declaration-postal-address');
		$application->declaration_street = Input::get('declaration-street');
		$application->declaration_date = Input::get('declration-date', '0/0/0000');
		$application->declaration_accept = Input::get('declaration-accept');
		$application->save();
		return Response::json([]);
	}
	public function allApplicationsTable(){
		$file_id = Auth::user()->active_file;
		$applications = LoanApplication::where('file_id', $file_id)
							->select('*');
		$table =  Datatables::of($applications)
					->addColumn('actions', function($data){ 
	    				$output = '<a href="'.url('/forms/loan-application/'.$data->id).'" class="btn  btn-primary btn-xs">Edit</a>';
	    				$output .= '<a class="btn btn-xs btn-danger delete" data-id="'.$data->id.'">Delete</a>';
	    				return $output;
	    			})			
    				->make(true);
    	return $table;
	}
}