<?php 
//use App\models\UploadFile;
class UploadFileController extends BaseController{

    public function index(){
        return View::make('file_upload');
	}	
	public function uploadFile(){
		$feedback = [];

		$upload_file = new UploadFile;
		$upload_file->file_id = Auth::user()->active_file;
        $upload_file->title = Input::get('title');
        $upload_file->description = Input::get('description');
		$upload_file->date = date_create_from_format('m/d/Y', Input::get('date'));

		if(Input::hasFile('file')){
			$file = Input::file('file');
			$path = 'files/';
			$name = Str::random(3).time().'.'.$file->getClientOriginalExtension();
			$file->move($path, $name);
			$upload_file->file = $path.$name;
		}
		$upload_file->save();

		return Response::json($feedback);
    }
    
    public function destroy(){ 
		$id = Input::get('id');
		UploadFile::destroy($id);

		return Response::json([]);
	}

	public function view($id){
		$upload_file = UploadFile::find($id);
		$files = $upload_file->files;

		$data = compact('upload_file', 'files');
		//return View::make('upload.view')->with($data);
	}

	public function allFilesTable(){
		
		$file_id = Auth::user()->active_file;
		$upload_file = UploadFile::whereNotNull('date')
							->where('file','!=','')
							->where('file_id', $file_id)
							->select('*');
		

		$table =  Datatables::of($upload_file)
					->addColumn('actions', function($data){ 
						$output = '<a href="'.url('/'.$data->file).'" target="_blank" class="btn btn-xs btn-primary" data-id="'.$data->id.'">View</a>';
	    	 	//		$output .= '<a href="/forms/school-assistance/'.$data->id.'" class="btn  btn-primary btn-xs">Edit</a>';
	    	 			$output .= '<a href="'.url('upload/destroy?id='.$data->id).'" class="btn btn-xs btn-danger delete" data-id="'.$data->id.'">Delete</a>';

	    				return $output;
	    			})	
	    			->editColumn('title', function($data){
	    				return '<a href="'.url('/'.$data->file).'" target="_blank">'.$data->title.'</a>';
                    })
                    ->editColumn('description', function($data){
	    				return '<a href="'.url('/'.$data->file).'" target="_blank">'.$data->description.'</a>';
	    			})
	    			->editColumn('date', function($data){
	    				return $data->date->format('d-m-Y');
	    			})
    				->make(true);

    	return $table;
	}
}