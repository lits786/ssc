<?php

class LukuController extends BaseController{
	public function getLukuForm(){
		$id = Input::get('id',0);

		$file_id = Auth::user()->active_file;
		$file = SSCFile::find($file_id);
		if($id){
			$luku = Luku::find($id);
			$luku_id = $id;
		}else{
			$luku = null;
			$luku_id = 0;
		}
		$currencies = Currency::orderBy('name','ASC')->get();
		$data = compact('currencies', 'luku', 'luku_id', 'file');

		return View::make('forms.luku')->with($data);
	}

	public function save(){
		$id = Input::get('id');
		$luku = ($id == 0) ? new Luku : Luku::find($id);
		$luku->created_by = Auth::id();
		
		$luku->file_id = Auth::user()->active_file;
		$luku->date = date_create_from_format('m/d/Y', Input::get('date'));
		$luku->currency_id = Input::get('currency');
		$luku->self_contribution = Input::get('self-contribution');
		$luku->ssc_contribution = Input::get('ssc-contribution');
		$luku->total_amount = Input::get('total-amount');
		$luku->concluded = Input::get('concluded', 'no');
		$luku->remarks = Input::get('remarks');

		
		$luku->save();

		if(Input::hasFile('files')){
			$files = Input::file('files');

			foreach ($files as $file) {
				$path = 'files/';
				$name = Str::random(3).time().'.'.$file->getClientOriginalExtension();
				$file->move($path, $name);
				$full_path = $path.$name;

				$luku_file = new LukuFile;
				$luku_file->path = $full_path;
				$luku->files()->save($luku_file);
			}
		}

		return Response::json(compact('files'));
	}

	public function allLuku(){
		$file_id = Auth::user()->active_file;
		$file = SSCFile::find($file_id);
		$data = compact('file');

		return View::make('luku.all_luku')->with($data);
	}

	public function allLukuDataTable(){
		$user = Auth::user();

		$file_id = $user->active_file;
		$luku = Luku::leftJoin('currencies', 'currencies.id', '=', 'luku.currency_id')
							->where('file_id', $file_id)
							->select('luku.*',
										'currencies.name as currency');

		$table =  Datatables::of($luku)
					->editColumn('date', function($data){
						return $data->date->format('d-m-Y');
					})
					->editColumn('ssc_contribution', function($data){
						return $data->ssc_contribution.' ('.$data->percentage().')';
					})
					->editColumn('self_contribution', function($data){
						return $data->self_contribution.' ('.$data->selfPercentage().')';
					})
					->addColumn('percentage', function($data){
						return $data->percentage();
					})
					->addColumn('actions', function($data) use ($user){ 
						$output = '<a href="'.url('/luku/view/'.$data->id).'" class="btn  btn-primary btn-xs">View</a>'; 
	    				$output .= '<a href="'.url('/forms/luku/?id='.$data->id).'"" class="btn  btn-primary btn-xs">Edit</a>';
	    				if($user->user_type == 1){
	    					$output .= '<a class="btn btn-xs btn-primary history" href="'.url('/file-history/luku/',$data->id).'" data-id="'.$data->id.'">History</a>';
		    				$output .= '<a class="btn btn-xs btn-danger delete" data-id="'.$data->id.'">Delete</a>';
		    			}

	    				return $output;
	    			})			
    				->make(true);

    	return $table;
	}

	public function destroy(){
		$id = Input::get('id');
		Luku::destroy($id);

		return Response::json([]);
	}

	public function view($id){
		$luku = Luku::find($id);
		$files = $luku->files;

		$data = compact('luku', 'files');
		return View::make('luku.view')->with($data);
	}

	public function uploadFile(){
		$feedback = [];

		$luku = new Luku;
		$luku->file_id = Auth::user()->active_file;
		$luku->title = Input::get('title');
		$luku->date = date_create_from_format('m/d/Y', Input::get('date'));

		if(Input::hasFile('file')){
			$file = Input::file('file');
			$path = 'files/';
			$name = Str::random(3).time().'.'.$file->getClientOriginalExtension();
			$file->move($path, $name);
			$luku->file = $path.$name;
		}
		$luku->save();

		return Response::json($feedback);
	}

	public function allFormFilesTable(){

		$file_id = Auth::user()->active_file;
		$lukus = Luku::whereNotNull('date')
							->where('file','!=','')
							->where('file_id', $file_id)
							->select('*');
		

		$table =  Datatables::of($lukus)
					->addColumn('actions', function($data){ 
						$output = '<a href="'.url('/luku/delete/'.$data->id).'" class="btn  btn-primary btn-xs delete">Delete</a>';
	    	// 			$output .= '<a href="/forms/school-assistance/'.$data->id.'" class="btn  btn-primary btn-xs">Edit</a>';
	    	// 			$output .= '<a class="btn btn-xs btn-danger delete" data-id="'.$data->id.'">Delete</a>';

	    				return $output;
	    			})	
	    			->editColumn('title', function($data){
	    				return '<a href="'.url('/'.$data->file).'" target="_blank">'.$data->title.'</a>';
	    			})
	    			->editColumn('date', function($data){
	    				return $data->date->format('d-m-Y');
	    			})
    				->make(true);

    	return $table;
	}

	public function delete($id){
		Luku::destroy($id);
		return Response::json([]);
	}
}