<?php

use Carbon\Carbon;

class InterviewController extends BaseController{
	public function getInterviewForm($id=0){
		if($id!=0){
			$interview = Interview::find($id);
			$interview_id = $interview->id;
		}else{
			$interview = null;
			$interview_id = 0;
		}

		$data = compact('interview', 
							'interview_id');
		return View::make('forms.interview')->with($data);
	}


	public function save(){
		$id = Input::get('id');

		$interview = ($id == 0) ? new Interview : Interview::find($id);
		$interview->created_by = Auth::id();
		$interview->date = date_create_from_format('d/m/Y', Input::get('date'));
		$interview->department = Input::get('department');
		$interview->file_id = Auth::user()->active_file;
		$interview->interview_with = Input::get('interview-with');
		$interview->telephone = Input::get('telephone');
		$interview->employer = Input::get('employer');
		$interview->capacity = Input::get('capacity');
		$interview->employed_since = Input::get('employed-since');
		$interview->gross_salary = Input::get('gross-salary');
		$interview->interviewer = Input::get('interviewer');
		$interview->assistance_currently_receiving = Input::get('assistance-currently-receiving');
		$interview->issues_discussed = Input::get('issues-discussed');
		$interview->conclusion = Input::get('conclusion');
		$interview->concluded = Input::get('concluded','no');

		$interview->family = Input::get('family');
		$interview->residence = Input::get('residence');
		$interview->signature = Input::get('signature');
		$interview->next_date = $interview->date->copy()->addMonths(3);

		$interview->lawajam = Input::get('lawajam');
		$interview->jamaat_rent = Input::get('jamaat_rent');
		$interview->school_fees = Input::get('school_fees');
		$interview->task_allocation = Input::get('task_allocation');
		$interview->next_interview = date_create_from_format('d/m/Y', Input::get('next_interview'));
		$interview->remarks = Input::get('remarks');

		$interview->save();

		return Response::json([]);
	}


	public function allInterviews(){
		return View::make('interviews.all_interviews');
	}

	public function allInterviewsDataTable(){
		$user = Auth::user();

		if($user->active_file){
			$interviews = Interview::orderBy('file_id','DESC')
											->leftJoin('files', 'interviews.file_id', '=', 'files.id')
											->where('file_id', $user->active_file)
											->select('files.file_no as file_no', 'interviews.*');
		}else{
			$interviews = Interview::orderBy('file_id','DESC')
											->leftJoin('files', 'interviews.file_id', '=', 'files.id')
											->select('files.file_no as file_no', 'interviews.*');
		}

		$table =  Datatables::of($interviews)
					->editColumn('actions', function($data) use ($user){ 
						$output = '<a href="'.url('/forms/interview/'.$data->id).'"" class="btn btn-primary btn-xs">Edit</a>';
						$output .= '<a href="'.url('/interview/view/'.$data->id).'" class="btn btn-primary btn-xs">Preview</a>';

						if($user->user_type == 1){
							$output .= '<a class="btn btn-xs btn-primary history" href="'.url('/file-history/interview/'.$data->id).'" data-id="'.$data->id.'">History</a>';
		    				$output .= '<a class="btn btn-xs btn-danger delete" data-id="'.$data->id.'">Delete</a>';
		    			}

						return $output;
	    			})			
    				->make(true);

    	return $table;
	}

	public function delete($id){
		Interview::destroy($id);

		return Response::json([]);
	}


	public function view($id){
		$interview = Interview::find($id);

		$data = compact('interview');
		return View::make('interviews.view')->with($data);
	}
}