<?php 

class MedicalController extends BaseController{

	public function getMedicalForm(){
		$id = Input::get('id',0);

		$file_id = Auth::user()->active_file;
		$file = SSCFile::find($file_id);

		if($id){
			$medical = Medical::find($id);
			$medical_id = $id;
		}else{
			$medical = null;
			$medical_id = 0;
		}
		$currencies = Currency::orderBy('name','ASC')->get();
		$data = compact('currencies', 'medical', 'medical_id', 'file');

		return View::make('forms.medical')->with($data);
	}

	public function save(){
		$id = Input::get('id');

		$medical = ($id == 0) ? new Medical : Medical::find($id);
		$medical->created_by = Auth::id();
		$medical->file_id = Auth::user()->active_file;
		$medical->date = date_create_from_format('m/d/Y', Input::get('date'));
		$medical->self_contribution = Input::get('self-contribution');
		$medical->currency_id = Input::get('currency');
		$medical->concluded = Input::get('concluded', 'no');
		$medical->total_amount = Input::get('total-amount');
		$medical->ssc_contribution = Input::get('ssc-contribution');
		$medical->remarks = Input::get('remarks');
		
		$medical->save();

		if(Input::hasFile('files')){
			$files = Input::file('files');

			foreach ($files as $file) {
				$path = 'files/';
				$name = Str::random(3).time().'.'.$file->getClientOriginalExtension();
				$file->move($path, $name);
				$full_path = $path.$name;

				$medical_file = new MedicalFile;
				$medical_file->path = $full_path;
				$medical->files()->save($medical_file);
			}
		}

		return Response::json(compact('files'));
	}

	public function allMedicals(){
		$file_id = Auth::user()->active_file;
		$file = SSCFile::find($file_id);
		$data = compact('file');

		return View::make('medicals.all_medicals')->with($data);
	}

	public function allMedicalsDataTable(){
		$user = Auth::user();
		$file_id = $user->active_file;
		$medicals = Medical::leftJoin('currencies', 'currencies.id', '=', 'medicals.currency_id')
							->where('file_id', $file_id)
							->select('medicals.*',
										'currencies.name as currency');

		$table =  Datatables::of($medicals)
					->editColumn('date', function($data){
						return $data->date->format('d-m-Y');
					})
					->editColumn('ssc_contribution', function($data){
						return $data->ssc_contribution.' ('.$data->percentage().')';
					})
					->editColumn('self_contribution', function($data){
						return $data->self_contribution.' ('.$data->selfPercentage().')';
					})
					->addColumn('actions', function($data) use ($user){
						$output = '<a href="'.url('/medical/view/'.$data->id).'" class="btn  btn-primary btn-xs">View</a>'; 
	    				$output .= '<a href="'.url('/forms/medical?id='.$data->id).'" class="btn  btn-primary btn-xs">Edit</a>';

	    				if($user->user_type == 1){
	    					$output .= '<a class="btn btn-xs btn-primary history" href="'.url('/file-history/medical/'.$data->id).'" data-id="'.$data->id.'">History</a>';
		    				$output .= '<a class="btn btn-xs btn-danger delete" data-id="'.$data->id.'">Delete</a>';
	    				}

	    				return $output;
	    			})			
    				->make(true);

    	return $table;
	}

	public function destroy(){
		$id = Input::get('id');
		Medical::destroy($id);

		return Response::json([]);
	}

	public function view($id){
		$medical = Medical::find($id);
		$files = $medical->files;

		$data = compact('medical', 'files');
		return View::make('medicals.view')->with($data);
	}

	public function uploadFile(){
		$feedback = [];

		$medical = new Medical;
		$medical->file_id = Auth::user()->active_file;
		$medical->title = Input::get('title');
		$medical->date = date_create_from_format('m/d/Y', Input::get('date'));

		if(Input::hasFile('file')){
			$file = Input::file('file');
			$path = 'files/';
			$name = Str::random(3).time().'.'.$file->getClientOriginalExtension();
			$file->move($path, $name);
			$medical->file = $path.$name;
		}
		$medical->save();

		return Response::json($feedback);
	}

	public function allFormFilesTable(){

		$file_id = Auth::user()->active_file;
		$medicals = Medical::whereNotNull('date')
							->where('file','!=','')
							->where('file_id', $file_id)
							->select('*');		

		$table =  Datatables::of($medicals)
					->addColumn('actions', function($data){ 
						// $output = '<a href="'.url('/medical/destroy?id='.$data->id).'" class="btn  btn-primary btn-xs delete">View</a>';
						$output = '<a href="'.url('/medical/destroy?id='.$data->id).'" class="delete"></a>';
	    				// $output = '<a href="'.url('/forms/medical?id='.$data->id).'" class="btn  btn-primary btn-xs">Edit</a>';
	    				$output .= '<a class="btn btn-xs btn-danger delete" data-id="'.$data->id.'">Delete</a>';

	    				return $output;
	    			})	
	    			->editColumn('title', function($data){
	    				return '<a href="'.url('/'.$data->file).'" target="_blank">'.$data->title.'</a>';
	    			})
	    			->editColumn('date', function($data){
	    				return $data->date->format('d-m-Y');
	    			})
    				->make(true);

    	return $table;
	}
}