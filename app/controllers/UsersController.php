<?php
/**
* 
*/
class UsersController extends BaseController
{
	
	function index(){
		$user_types = UserType::get();
		$permissions = Permission::orderBy('name','ASC')->get();

		$data = compact('user_types', 'permissions');
		return View::make('users.index')->with($data);

	}

	public function getSecurity(){
		$user_types = UserType::get();
		$permissions = Permission::orderBy('name','ASC')->get();

		$data = compact('user_types', 'permissions');
		return View::make('users.security')->with($data);
	}

	public function save(){
		$feedback = [];
		$id = Input::get('id');		
		$password = Input::get('password');

		$user = ($id==0) ? new User : User::find($id);

		$user->username = Input::get('username');

		if($password){
			$user->password = Hash::make($password);
		}

		$user->user_type = Input::get('user-type');
		$user->full_name = Input::get('full-name');

		$user->save();	

		return Response::json($feedback);


	}

	public function allUsersTable(){
		$users = User::leftJoin('user_types', 'users.user_type', '=', 'user_types.id')
							->select('users.*',
										'user_types.name as user_type_name');
		$table =  Datatables::of($users)
					->addColumn('actions', function($data){ 
	    				$output = '<a href="javascript:void(0)" data-id="'.$data->id.'" data-name="'.$data->full_name.'" data-username="'.$data->username.'" data-userType="'.$data->user_type.'" class="btn  btn-primary btn-xs edit">Edit</a>';
	    				$output .= '<a class="btn btn-xs btn-danger delete" data-id="'.$data->id.'">Delete</a>';
	    				return $output;
	    			})			
    				->make(true);
    	return $table;
	}

	public function savePermissions(){
		$user_type = UserType::find(Input::get('user-type'));
		$permissions = Input::get('permissions');
		$user_type->permissions()->sync($permissions);

		return Response::json([]);
	}

	public function allUserTypesPermissionsTable(){
		$users = UserType::orderBy('name', 'ASC')->select('*');
		$table =  Datatables::of($users)
					->addColumn('permissions', function($data){ 
	    				return implode(', ',$data->permissions()->lists('name'));
	    			})			
	    			->addColumn('actions', function($data){ 
	    				$permissionIds = implode(',',$data->permissions()->lists('id'));
	    				$output = '<a href="javascript:void(0)" data-id="'.$data->id.'" data-name="'.$data->full_name.'" data-username="'.$data->username.'" data-userType="'.$data->user_type.'" data-ids="'.$permissionIds.'" class="btn  btn-primary btn-xs edit">Edit</a>';
	    				$output .= '<a class="btn btn-xs btn-danger reset" data-id="'.$data->id.'">Delete</a>';
	    				return $output;
	    			})
    				->make(true);
    	return $table;
	}

	public function resetPermissions($id){
		$userType = UserType::find($id);
		$userType->permissions()->detach();

		return Response::json([]);
	}	


	public function delete($id){
		$feedback = [];
		User::destroy($id);

		return Response::json($feedback);
	}

}