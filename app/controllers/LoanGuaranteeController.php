<?php



class LoanGuaranteeController extends BaseController{

	public function save(){

		$feedback = [];



		$id = Input::get('id');



		$guarantee = ($id == 0) ? new LoanGuarantee : LoanGuarantee::find($id);

		$guarantee->loan_id = Input::get('loan-code');

		$guarantee->full_name = Input::get('full-name');

		$guarantee->postal_address = Input::get('postal-address');

		$guarantee->telephone = Input::get('telephone');

		$guarantee->email = Input::get('email');

		$guarantee->loan_purpose = Input::get('loan-purpose');

		$guarantee->borrowers_full_name = Input::get('borrowers-full-name');

		$guarantee->borrowers_postal_address = Input::get('borrowers-postal-address');

		$guarantee->loan_amount = Input::get('loan-amount');

		$guarantee->date_of_loan_agreement = date_create_from_format('d/m/Y', Input::get('date-of-loan-agreement'));

		$guarantee->witness_full_name = Input::get('witness-full-name');

		$guarantee->witness_address = Input::get('witness-address');

		$guarantee->agree = Input::get('agree',0);

		$guarantee->save();



		return Response::json([$feedback]);

	}





	public function allGuaranteesTable(){

		$file_id = Auth::user()->active_file;

		$guarantees = LoanGuarantee::where('file_id', $file_id)

							->select('*');



		$table =  Datatables::of($guarantees)

					->addColumn('actions', function($data){ 

	    				$output = '<a href="'.url('/forms/guarantee/'.$data->id).'" class="btn  btn-primary btn-xs">Edit</a>';

	    				$output .= '<a class="btn btn-xs btn-danger delete" data-id="'.$data->id.'">Delete</a>';



	    				return $output;

	    			})			

    				->make(true);



    	return $table;

	}

}