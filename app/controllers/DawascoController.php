<?php 
class DawascoController extends BaseController{
	public function getDawascoForm(){
		$id = Input::get('id',0);

		$file_id = Auth::user()->active_file;
		$file = SSCFile::find($file_id);

		if($id){
			$dawasco = Dawasco::find($id);
			$dawasco_id = $id;
		}else{
			$dawasco = null;
			$dawasco_id = 0;
		}
		$currencies = Currency::orderBy('name','ASC')->get();
		$data = compact('currencies', 'dawasco', 'dawasco_id', 'file'); 

		return View::make('forms.dawasco')->with($data);
	}

	public function save(){
		$id = Input::get('id');

		$dawasco = ($id == 0) ? new Dawasco : Dawasco::find($id);
		$dawasco->created_by = Auth::id();
		$dawasco->file_id = Auth::user()->active_file;
		$dawasco->date = date_create_from_format('m/d/Y', Input::get('date'));
		$dawasco->currency_id = Input::get('currency');
		$dawasco->self_contribution = Input::get('self-contribution');
		$dawasco->ssc_contribution = Input::get('ssc-contribution');
		$dawasco->total_amount = Input::get('total-amount');
		$dawasco->concluded = Input::get('concluded','no');
		$dawasco->remarks = Input::get('remarks');

		
		$dawasco->save();

		if(Input::hasFile('files')){
			$files = Input::file('files');

			foreach ($files as $file) {
				$path = 'files/';
				$name = Str::random(3).time().'.'.$file->getClientOriginalExtension();
				$file->move($path, $name);
				$full_path = $path.$name;

				$dawasco_file = new DawascoFile;
				$dawasco_file->path = $full_path;
				$dawasco->files()->save($dawasco_file);
			}
		}

		return Response::json(compact('files'));
	}

	public function allDawasco(){
		$file_id = Auth::user()->active_file;
		$file = SSCFile::find($file_id);
		$data = compact('file');

		return View::make('dawasco.all_dawasco')->with($data);
	}

	public function allDawascoDataTable(){
		$user = Auth::user();
		
		$file_id = $user->active_file;
		$dawasco = Dawasco::leftJoin('currencies', 'currencies.id', '=', 'dawasco.currency_id')
							->where('file_id', $file_id)
							->select('dawasco.*',
										'currencies.name as currency');
		$table =  Datatables::of($dawasco)
					->editColumn('date', function($data){
						return $data->date->format('d-m-Y');
					})
					->editColumn('ssc_contribution', function($data){
						return $data->ssc_contribution.' ('.$data->percentage().')';
					})
					->editColumn('self_contribution', function($data){
						return $data->self_contribution.' ('.$data->selfPercentage().')';
					})
					->addColumn('percentage', function($data){
						return $data->percentage();
					})
					->addColumn('actions', function($data) use ($user){
						$output = '<a href="'.url('/dawasco/view/'.$data->id).'" class="btn  btn-primary btn-xs">View</a>';  
	    				$output .= '<a href="'.url('forms/dawasco?id='.$data->id).'"" class="btn  btn-primary btn-xs">Edit</a>';
	    				if($user->user_type == 1){
	    					$output .= '<a class="btn btn-xs btn-primary history" href="'.url('/file-history/dawasco/'.$data->id).'" data-id="'.$data->id.'">History</a>';
		    				$output .= '<a class="btn btn-xs btn-danger delete" data-id="'.$data->id.'">Delete</a>';
	    				}

	    				return $output;
	    			})			
    				->make(true);

    	return $table;
	}

	public function destroy(){
		$id = Input::get('id');
		Dawasco::destroy($id);

		return Response::json([]);
	}

	public function view($id){
		$dawasco = Dawasco::find($id);
		$files = $dawasco->files;

		$data = compact('dawasco', 'files');
		return View::make('dawasco.view')->with($data);
	}

	public function uploadFile(){
		$feedback = [];

		$dawasco = new Dawasco;
		$dawasco->file_id = Auth::user()->active_file;
		$dawasco->title = Input::get('title');
		$dawasco->date = date_create_from_format('m/d/Y', Input::get('date'));

		if(Input::hasFile('file')){
			$file = Input::file('file');
			$path = 'files/';
			$name = Str::random(3).time().'.'.$file->getClientOriginalExtension();
			$file->move($path, $name);
			$dawasco->file = $path.$name;
		}
		$dawasco->save();

		return Response::json($feedback);
	}

	public function allFormFilesTable(){

		$file_id = Auth::user()->active_file;
		$dawascos = Dawasco::whereNotNull('date')
							->where('file','!=','')
							->where('file_id', $file_id)
							->select('*');
		

		$table =  Datatables::of($dawascos)
					->addColumn('actions', function($data){ 
						$output = '<a href="'.url('/dawasco/delete/'.$data->id).'" class="btn  btn-danger btn-xs delete">Delete</a>';
	    	// 			$output .= '<a href="/forms/school-assistance/'.$data->id.'" class="btn  btn-primary btn-xs">Edit</a>';
	    	// 			$output .= '<a class="btn btn-xs btn-danger delete" data-id="'.$data->id.'">Delete</a>';

	    				return $output;
	    			})	
	    			->editColumn('title', function($data){
	    				return '<a href="'.url('/'.$data->file).'" target="_blank">'.$data->title.'</a>';
	    			})
	    			->editColumn('date', function($data){
	    				return $data->date->format('d-m-Y');
	    			})
    				->make(true);

    	return $table;
	}

	public function delete($id){
		Dawasco::destroy($id);
		return Response::json([]);
	}
}