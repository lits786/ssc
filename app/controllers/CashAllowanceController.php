<?php

class CashAllowanceController extends BaseController{
	public function getCashAllowanceForm(){
		$id = Input::get('id',0);

		$file_id = Auth::user()->active_file;
		$file = SSCFile::find($file_id);
		if($id){
			$cash_allowance = CashAllowance::find($id);
			$cash_allowance_id = $id;
		}else{
			$cash_allowance = null;
			$cash_allowance_id = 0;
		}
		$currencies = Currency::orderBy('name','ASC')->get();
		$data = compact('currencies', 'cash_allowance', 'cash_allowance_id', 'file');

		return View::make('forms.cash_allowance')->with($data);
	}

	public function save(){
		$id = Input::get('id');
		$cash_allowance = ($id == 0) ? new CashAllowance: CashAllowance::find($id);
		$cash_allowance->created_by = Auth::id();
		
		$cash_allowance->file_id = Auth::user()->active_file;
		$cash_allowance->date = date_create_from_format('m/d/Y', Input::get('date'));
		$cash_allowance->currency_id = Input::get('currency');
		$cash_allowance->self_contribution = Input::get('self-contribution');
		$cash_allowance->ssc_contribution = Input::get('ssc-contribution');
		$cash_allowance->total_amount = Input::get('total-amount');
		$cash_allowance->concluded = Input::get('concluded', 'no');
		$cash_allowance->remarks = Input::get('remarks');

		
		$cash_allowance->save();

		if(Input::hasFile('files')){
			$files = Input::file('files');

			foreach ($files as $file) {
				$path = 'files/';
				$name = Str::random(3).time().'.'.$file->getClientOriginalExtension();
				$file->move($path, $name);
				$full_path = $path.$name;

				$cash_allowance_file = new CashAllowanceFile;
				$cash_allowance_file->path = $full_path;
				$cash_allowance->files()->save($cash_allowance_file);
			}
		}

		return Response::json(compact('files'));
	}

	public function allCashAllowance(){
		$file_id = Auth::user()->active_file;
		$file = SSCFile::find($file_id);
		$data = compact('file');

		return View::make('cash_allowance.all_cash_allowance')->with($data);
	}

	public function allCashAllowanceDataTable(){
		$user = Auth::user();

		$file_id = $user->active_file;
		$cash_allowance = CashAllowance::leftJoin('currencies', 'currencies.id', '=', 'cash_allowance.currency_id')
							->where('file_id', $file_id)
							->select('cash_allowance.*',
										'currencies.name as currency');

		$table =  Datatables::of($cash_allowance)
					->editColumn('date', function($data){
						return $data->date->format('d-m-Y');
					})
					->editColumn('ssc_contribution', function($data){
						return $data->ssc_contribution.' ('.$data->percentage().')';
					})
					->editColumn('self_contribution', function($data){
						return $data->self_contribution.' ('.$data->selfPercentage().')';
					})
					->addColumn('percentage', function($data){
						return $data->percentage();
					})
					->addColumn('actions', function($data) use ($user){ 
						$output = '<a href="'.url('/cash_allowance/view/'.$data->id).'" class="btn  btn-primary btn-xs">View</a>'; 
	    				$output .= '<a href="'.url('/forms/cash-allowance/?id='.$data->id).'"" class="btn  btn-primary btn-xs">Edit</a>';
	    				if($user->user_type == 1){
	    					$output .= '<a class="btn btn-xs btn-primary history" href="'.url('/file-history/cash-allowance/',$data->id).'" data-id="'.$data->id.'">History</a>';
		    				$output .= '<a class="btn btn-xs btn-danger delete" data-id="'.$data->id.'">Delete</a>';
		    			}

	    				return $output;
	    			})			
    				->make(true);

    	return $table;
	}

	public function destroy(){
		$id = Input::get('id');
		CashAllowance::destroy($id);

		return Response::json([]);
	}

	public function view($id){
		$cash_allowance = CashAllowance::find($id);
		$files = $cash_allowance->files;

		$data = compact('cash_allowance', 'files');
		return View::make('cash_allowance.view')->with($data);
	}

	public function uploadFile(){
		$feedback = [];

		$cash_allowance = new CashAllowance;
		$cash_allowance->file_id = Auth::user()->active_file;
		$cash_allowance->title = Input::get('title');
		$cash_allowance->date = date_create_from_format('m/d/Y', Input::get('date'));

		if(Input::hasFile('file')){
			$file = Input::file('file');
			$path = 'files/';
			$name = Str::random(3).time().'.'.$file->getClientOriginalExtension();
			$file->move($path, $name);
			$cash_allowance->file = $path.$name;
		}
		$cash_allowance->save();

		return Response::json($feedback);
	}

	public function allFormFilesTable(){

		$file_id = Auth::user()->active_file;
		$cash_allowances = CashAllowance::whereNotNull('date')
							->where('file','!=','')
							->where('file_id', $file_id)
							->select('*');
		

		$table =  Datatables::of($cash_allowances)
					->addColumn('actions', function($data){ 
						$output = '<a href="'.url('/cash-allowance/delete/'.$data->id).'" class="btn  btn-primary btn-xs delete">Delete</a>';
	    	// 			$output .= '<a href="/forms/school-assistance/'.$data->id.'" class="btn  btn-primary btn-xs">Edit</a>';
	    	// 			$output .= '<a class="btn btn-xs btn-danger delete" data-id="'.$data->id.'">Delete</a>';

	    				return $output;
	    			})	
	    			->editColumn('title', function($data){
	    				return '<a href="'.url('/'.$data->file).'" target="_blank">'.$data->title.'</a>';
	    			})
	    			->editColumn('date', function($data){
	    				return $data->date->format('d-m-Y');
	    			})
    				->make(true);

    	return $table;
	}

	public function delete($id){
		CashAllowance::destroy($id);
		return Response::json([]);
	}
}