<?php

class CapacityBuildingController extends BaseController{
	public function getIndex($id=0){
		if($id){
			$capacity = CapacityBuilding::find($id);
			$capacity_id = $capacity->id;
		}else{
			$capacity = null;
			$capacity_id = 0;			
		}

		$data = compact('capacity','capacity_id');
		return View::make('capacity_building.index')->with($data);

	}
 
	public function delete($id){
		CapacityBuilding::destroy($id);

		return Response::json([]);
	}

	public function view($id){
		$capacity = CapacityBuilding::find($id);
		$data = compact('capacity');

		return View::make('capacity_building.view')->with($data);
	}

	public function saveImage(){

		$image = Input::file('image');
		$path = 'assets/images/capacity-building/';
		$name = Str::random(3).time().'.'.$image->getClientOriginalExtension();
		$image->move($path, $name);
		$path .= $name;
		// $path .= $name;
  //       Image::make($image)
  //               ->fit(600,300)
  //               ->save($path);

		$image = new CapacityBuildingImage;
		$image->capacity_building_id = Input::get('capacity-id');
		$image->path = $path;
		$image->save();
		return Response::json([]);
	}

	public function deleteImage($id){
		CapacityBuildingImage::destroy($id);

		return Response::json([]);
	}


	public function save(){
		$feedback = [];
		$id = Input::get('id');

		$capacity = ($id==0) ? new CapacityBuilding : CapacityBuilding::find($id);
		$capacity->created_by = Auth::id();
		$capacity->file_id = Auth::user()->active_file;
		$capacity->title = Input::get('title');
		$capacity->description = Input::get('description');
		$capacity->date = date_create_from_format('d/m/Y', Input::get('date'));
		$capacity->save();

		$feedback['capacity_id'] = $capacity->id;

		return Response::json($feedback);
	}

	public function allCapacitiesTable(){
		$user = Auth::user();
		$file_id = $user->active_file;
		$capacities = CapacityBuilding::where('file_id', $file_id)
							->select('*');
		

		$table =  Datatables::of($capacities)
					->addColumn('actions', function($data) use ($user){
						$output = '<a href="'.url('/capacity-building/view/'.$data->id).'" class="btn  btn-primary btn-xs">View</a>';  
	    				$output .= '<a href="'.url('/capacity-building/form/'.$data->id).'"" class="btn  btn-primary btn-xs">Edit</a>';
	    				if($user){
	    					$output .= '<a class="btn btn-xs btn-primary history" href="'.url('/file-history/capacity-building/'.$data->id).'" data-id="'.$data->id.'">History</a>';
		    				$output .= '<a class="btn btn-xs btn-danger delete" data-id="'.$data->id.'">Delete</a>';
	    				}

	    				return $output;
					})
    				->make(true);

    	return $table;
	}
}