<?php
class AuthController extends BaseController{
	public function getLogin(){ 
		return View::make('login');
	}
	
	public function checkUserTypeId(){
		$userTypeId= Input::get('userTypeId');
		$linkToFiles = Input::get('linkToFiles');
		$status = 0;
		
		$user = Auth::user();
		if($userTypeId == 10){
			if($user->user_type == 10){
				$status = 1;
			} else{ $status = 0;}
		}
		else if($user->user_type == 1 || $user->user_type == 10 ){
			$status = 1;
		}		
		else
		{
			if($user->user_type== $userTypeId)
			{
				$status = 1;
			}
			
		}
		
		
		
		$feedback = compact('linkToFiles', 'status');
		return Response::json($feedback);
	}

	public function postLogin(){
		$username = Input::get('username');
		$password = Input::get('password');
		$linkToFiles = Input::get('linkToFiles');
		$status = 0;
		

		$credentials = compact('username', 'password');

		if(Auth::attempt($credentials, true)){
			$user = Auth::user();
			$status = 1;
			if($user->user_type==1 || $user->user_type==10 ){
				 $linkToFiles = url('user/files/');
				 $userTypeId = Session::pull('userTypeId',1 );
			}else{
				$linkToFiles = url('user/files/').'/'.$user->user_type;
				$userTypeId = Session::pull('userTypeId',$user->user_type );
			}

			if(Input::has('include-file')){
				$file_number = Input::get('file-number');
				$file = SSCFile::where('file_no',$file_number)->first();

				if($file){
					$user->active_file = $file->id;
					$user->save();
					return Redirect::to('/');
				}else{
					return Redirect::back();
				}
			}else{
				$user->active_file = null;
				$user->save();
			}
		}else{
			$status = 0;
		}

		$feedback = compact('linkToFiles', 'status');
		return Response::json($feedback);
	}

	public function mainLogin(){
		$username = Input::get('username');
		$password = Input::get('password');		

		$credentials = compact('username', 'password');

		if(Auth::attempt($credentials, true)){
			$user = Auth::user();
			$status = 1;
			if($user->user_type==1 || $user->user_type==10){
				 $linkToFiles = url('user/files/');
				 $userTypeId = Session::pull('userTypeId',1 );
			}
			else{
				$linkToFiles = url('user/files/').'/'.$user->user_type;
				$userTypeId = Session::pull('userTypeId',$user->user_type );
			}

			if(Input::has('include-file')){
				$file_number = Input::get('file-number');
				$file = SSCFile::where('file_no',$file_number)->first();

				if($file){
					$user->active_file = $file->id;
					$user->save();
					return Redirect::to('/');
				}else{
					return Redirect::back();
				}
			}else{
				$user->active_file = null;
				$user->save();
			}
		}else{
			$status = 0;
			return Redirect::to('/');
		}

		//return $feedback = compact('linkToFiles', 'status');
		return Redirect::to($linkToFiles);
	}

	public function logout(){
		// Session::flush();
		$user = Auth::user();
		$user->active_file = 0;
		$user->save();
		
		Auth::logout();
		return Redirect::to('/');
	}
}