<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/
Route::get('/', function()
{
		
		$userTypes = UserType::all();
		$admin_files = SSCFile::count();
		$userTypeId = null;
		if(Auth::check())
		{
			$id_t = Input::get('userTypeId');
			
			if(isset($id_t))
			{
				$userTypeId = $id_t;
				$user = Auth::user();
				
			}
			else
			{
				$user = Auth::user();
				//$userTypeId = Session::pull('userTypeId',1 );
				$userTypeId = $user->user_type;
				
			}

			$data = compact('userTypes', 
						'admin_files','user', 'userTypeId');

			//Auth::loginUsingId(1);
			return View::make('dashboard')->with($data);
			
		}
		
		return Redirect::to('login');

		//if(!Auth::check()){
			
		//}

	
});

Route::get('login', 'AuthController@getLogin');
Route::get('users/checkUserTypeId', 'AuthController@checkUserTypeId');
Route::post('login', 'AuthController@postLogin');
Route::post('loginmain', 'AuthController@mainLogin');
Route::get('logout', 'AuthController@logout');

Route::group(['before' => 'auth'], function(){
 
	Route::get('users', 'UsersController@index');
	Route::post('user/save', 'UsersController@save');
	Route::get('users/all-users-table', 'UsersController@allUsersTable');
	Route::post('user/delete/{id}', 'UsersController@delete');
	Route::get('security', 'UsersController@getSecurity');
	Route::post('user/permissions/save', 'UsersController@savePermissions');
	Route::post('user/reset-permissions/{id}', 'UsersController@resetPermissions');
	Route::get('user-type/all-permissions', 'UsersController@allUserTypesPermissionsTable');


	Route::get('user/files/{id?}', 'FilesController@viewUserFiles');
	Route::get('user/files-datatable/{id}', 'FilesController@filesDatatable');
	Route::get('ssc-file/manage/{id}', 'FilesController@manageFile');
	Route::post('ssc-file/close/{id}', 'FilesController@closeFile');
	Route::post('ssc-file/open/{id}', 'FilesController@openFile');
	Route::post('ssc-file/move', 'FilesController@move');
	Route::get('file-history/{type}/{id}', 'FilesController@getHistory');


	Route::get('capacity-building/form/{id?}', 'CapacityBuildingController@getIndex');
	Route::post('capacity-building/save', 'CapacityBuildingController@save');
	Route::post('capacity-building/image/save', 'CapacityBuildingController@saveImage');
	Route::post('capacity-building-image/delete/{id}', 'CapacityBuildingController@deleteImage');
	Route::get('capacity-building/view/{id}', 'CapacityBuildingController@view');
	Route::get('capacity-building/all-capacities-table', 'CapacityBuildingController@allCapacitiesTable');
	Route::post('capacity-building/delete/{id}', 'CapacityBuildingController@delete');


	Route::get('will-attachment/form/{id?}', 'WillController@getIndex');
	Route::post('will-attachment/save', 'WillController@save');
	Route::get('will-attachment/all-wills-table', 'WillController@allWillsTable');
	Route::post('will/delete/{id}', 'WillController@delete');


	Route::get('simple-loan/loan-agreement', 'SimpleLoanController@getLoanAgreement');
	Route::get('simple-loan/loan-payments', 'SimpleLoanController@getLoanPayments');
	Route::get('simple-loan-agreement/all-agreements-data-table', 'SimpleLoanController@allAgreementsDataTable');
	Route::post('simple-loan/payment/save', 'SimpleLoanController@savePayment');
	Route::get('simple-loan/all-payments-table', 'SimpleLoanController@allPaymentsTable');
	Route::post('simple-loan/payment/delete/{id}', 'SimpleLoanController@deletePayment');
	Route::post('simple-loan/agreement/delete/{id}', 'SimpleLoanController@deleteAgreement');

	Route::post('simple-loan-agreement/save', 'SimpleLoanController@saveSimpleLoanAgreement');

	Route::get('business-loan/loan-agreement', 'BusinessLoanController@getLoanAgreement');
	Route::get('business-loan/loan-payments', 'BusinessLoanController@getLoanPayments');
	Route::get('business-loan-agreement/all-agreements-data-table', 'BusinessLoanController@allAgreementsDataTable');
	Route::post('business-loan/payment/save', 'BusinessLoanController@savePayment');
	Route::get('business-loan/all-payments-table', 'BusinessLoanController@allPaymentsTable');
	Route::post('business-loan/payment/delete/{id}', 'BusinessLoanController@deletePayment');
	Route::post('business-loan/agreement/delete/{id}', 'BusinessLoanController@deleteAgreement');
	Route::get('business-loan/agreement/view/{id}', 'BusinessLoanController@viewAgreement');
	Route::post('business-loan-agreement/save', 'BusinessLoanController@saveLoanAgreement');
	Route::get('business-loan-agreement/add-attachment', 'BusinessLoanController@addAttachment');



	
	Route::get('forms/loan-application/{id?}', 'NavigationController@getLoanApplicationForm');

	Route::get('forms/guarantee/{id?}', 'NavigationController@getGuaranteeForm');

	Route::group(['before'=>'registrationPermission'], function(){
		Route::get('forms/registration/{id?}', 'NavigationController@getRegistrationForm');
		Route::post('registration/submit', 'RegistrationController@save');
		Route::get('registration/child-sub-form', 'RegistrationController@getChildSubform');
		Route::get('registration/other-sub-form', 'RegistrationController@getOtherSubform');
		Route::get('registration/all', 'RegistrationController@allRegistered');
		Route::get('registration/view/{id}','RegistrationController@view');
		Route::get('registration/all-registered-data-table', 'RegistrationController@allRegisteredDataTable');
	});

	Route::get('forms/interview/{id?}', 'InterviewController@getInterviewForm');
	Route::post('interview/submit', 'InterviewController@save');
	Route::get('interview/all', 'InterviewController@allInterviews');
	Route::get('interview/view/{id}', 'InterviewController@view');
	Route::get('interviews/all-interviews-data-table', 'InterviewController@allInterviewsDataTable');
	Route::post('interview/delete/{id}', 'InterviewController@delete');
	
	Route::get('forms/voucher/{id?}', 'VoucherController@getVoucherForm');
	Route::post('voucher/submit', 'VoucherController@save');
	Route::get('vouchers/all', 'VoucherController@allVouchers');
	Route::get('voucher/view/{id}', 'VoucherController@view');
	Route::get('vouchers/all-vouchers-data-table', 'VoucherController@allVouchersDataTable');
	Route::post('voucher/delete/{id}', 'VoucherController@delete');
	Route::get('voucher/print/{id}','VoucherController@printVoucher');

	Route::get('forms/school-assistance/{id?}', 'SchoolAssistanceController@getSchoolAssistanceForm');
	Route::post('school-assistance/submit', 'SchoolAssistanceController@submit');
	Route::get('school-assistance/all-assistances-table', 'SchoolAssistanceController@allSchoolAssistancesTable');
	Route::post('school-assistance/delete/{id}', 'SchoolAssistanceController@delete');
	Route::post('school-assistance/update-total', 'SchoolAssistanceController@updateTotal');
	Route::get('school-assistance/view/{id}', 'SchoolAssistanceController@view');
	Route::post('school-assistance/upload-file', 'SchoolAssistanceController@uploadFile');
	Route::get('school-assistance/all-form-files-table', 'SchoolAssistanceController@allFormFilesTable');


	Route::get('forms/medical', 'MedicalController@getMedicalForm');
	Route::post('medical/save', 'MedicalController@save');
	Route::get('medical/all', 'MedicalController@allMedicals');
	Route::get('medicals/all-medicals-data-table', 'MedicalController@allMedicalsDataTable');
	Route::post('medical/destroy', 'MedicalController@destroy');
	Route::get('medical/view/{id}', 'MedicalController@view');
	Route::post('medical/upload-file', 'MedicalController@uploadFile');
	Route::get('medical/all-form-files-table', 'MedicalController@allFormFilesTable');


	Route::get('forms/rent', 'RentController@getRentForm');
	Route::post('rent/save', 'RentController@save');
	Route::get('rent/all', 'RentController@allRent');
	Route::get('rent/all-rent-data-table', 'RentController@allRentDataTable');
	Route::post('rent/destroy', 'RentController@destroy');
	Route::get('rent/view/{id}', 'RentController@view');
	Route::post('rent/upload-file', 'RentController@uploadFile');
	Route::get('rent/all-form-files-table', 'RentController@allFormFilesTable');
	Route::post('rent/delete/{id}', 'RentController@delete');


	Route::get('forms/luku', 'LukuController@getLukuForm');
	Route::post('luku/save', 'LukuController@save');
	Route::get('luku/all', 'LukuController@allLuku');
	Route::get('luku/all-luku-data-table', 'LukuController@allLukuDataTable');
	Route::post('luku/destroy', 'LukuController@destroy');
	Route::get('luku/view/{id}', 'LukuController@view');
	Route::post('luku/upload-file', 'LukuController@uploadFile');
	Route::get('luku/all-form-files-table', 'LukuController@allFormFilesTable');
	Route::post('luku/delete/{id}', 'LukuController@delete');

	Route::get('forms/transport', 'TransportController@getTransportForm');
	Route::post('transport/save', 'TransportController@save');
	Route::get('transport/all', 'TransportController@allTransport');
	Route::get('transport/all-transport-data-table', 'TransportController@allTrasnportDataTable');
	Route::post('transport/destroy', 'TransportController@destroy');
	Route::get('transport/view/{id}', 'TransportController@view');
	Route::post('transport/upload-file', 'TransportController@uploadFile');
	Route::get('transport/all-form-files-table', 'TransportController@allFormFilesTable');
	Route::post('transport/delete/{id}', 'TransportController@delete');

	Route::get('forms/cash-allowance', 'CashAllowanceController@getCashAllowanceForm');
	Route::post('cash-allowance/save', 'CashAllowanceController@save');
	Route::get('cash-allowance/all', 'CashAllowanceController@allCashAllowance');
	Route::get('cash-allowance/all-cash-allowance-data-table', 'CashAllowanceController@allCashAllowanceDataTable');
	Route::post('cash-allowance/destroy', 'CashAllowanceController@destroy');
	Route::get('cash-allowance/view/{id}', 'CashAllowanceController@view');
	Route::post('cash-allowance/upload-file', 'CashAllowanceController@uploadFile');
	Route::get('cash-allowance/all-form-files-table', 'CashAllowanceController@allFormFilesTable');
	Route::post('cash-allowance/delete/{id}', 'CashAllowanceController@delete');

	Route::get('forms/maid-support', 'MaidSupportController@getMaidSupportForm');
	Route::post('maid-support/save', 'MaidSupportController@save');
	Route::get('maid-support/all', 'MaidSupportController@allMaidSupport');
	Route::get('maid-support/all-maid-support-data-table', 'MaidSupportController@allMaidSupportDataTable');
	Route::post('maid-support/destroy', 'MaidSupportController@destroy');
	Route::get('maid-support/view/{id}', 'MaidSupportController@view');
	Route::post('maid-support/upload-file', 'MaidSupportController@uploadFile');
	Route::get('maid-support/all-form-files-table', 'MaidSupportController@allFormFilesTable');
	Route::post('maid-support/delete/{id}', 'MaidSupportController@delete');

	Route::get('forms/visit', 'VisitController@getVisitForm');
	Route::post('visit/save', 'VisitController@save');
	Route::get('visit/all-visit-data-table', 'VisitController@allVisitsDataTable');
	Route::post('visit/destroy', 'VisitController@destroy');
	Route::post('visit/delete/{id}', 'VisitController@delete');
	
	Route::get('forms/dawasco', 'DawascoController@getDawascoForm');
	Route::post('dawasco/save', 'DawascoController@save');
	Route::get('dawasco/all', 'DawascoController@allDawasco');
	Route::get('dawasco/all-dawasco-data-table', 'DawascoController@allDawascoDataTable');
	Route::post('dawasco/destroy', 'DawascoController@destroy');
	Route::get('dawasco/view/{id}', 'DawascoController@view');
	Route::post('dawasco/upload-file', 'DawascoController@uploadFile');
	Route::get('dawasco/all-form-files-table', 'DawascoController@allFormFilesTable');
	Route::post('dawasco/delete/{id}', 'DawascoController@delete');


	Route::get('forms/business-plan/{id?}', 'NavigationController@getBusinessPlanForm');
	Route::post('business-plan/save', 'BusinessPlanController@save');
	Route::get('business-plan/all-plans-table', 'BusinessPlanController@allPlansTable');

	Route::post('loan-guarantee/save', 'LoanGuaranteeController@save');
	Route::get('loan-guarantee/all-guarantees-table', 'LoanGuaranteeController@allGuaranteesTable');


	Route::post('loan-application/save', 'LoanApplicationController@save');
	Route::get('loan-application/all-applications-table', 'LoanApplicationController@allApplicationsTable');
	Route::get('loan/payment', 'LoanController@getIndex');
	Route::post('loan/save', 'LoanController@save');
	Route::post('loan/payment/save', 'LoanController@savePayment');
	Route::post('loan/status', 'LoanController@loanStatus');
	Route::get('loan/all-loans-table', 'LoanController@allLoansTable');


	Route::get('loan/agreement', 'LoanAgreementController@index');
	Route::post('loan/agreement/save', 'LoanAgreementController@save');
	Route::get('loan-agreements/all-agreements-data-table', 'LoanAgreementController@allAgreementsDatatable');

	Route::get('reports/registrations', 'ReportsController@getRegistrationsReports');
	Route::get('reports/interviews', 'ReportsController@getInterviewsReports');	
	Route::get('reports/education', 'ReportsController@getEducationReports');
	Route::get('reports/medicals', 'ReportsController@getMedicalsReports');
	Route::get('reports/rents', 'ReportsController@getRentsReports');
	Route::get('reports/dawasco', 'ReportsController@getDawascoReports');
	Route::get('reports/luku', 'ReportsController@getLukuReports');
	Route::get('reports/loans', 'ReportsController@getLoansReports');
	Route::get('reports/simple-loans', 'ReportsController@getSimpleLoansReports');

	Route::get('reports/registrations-table', 'ReportsController@getRegistrationsTable');
	Route::get('reports/interviews-table', 'ReportsController@getInterviewsTable');
	Route::get('reports/education-table', 'ReportsController@getEducationTable');
	Route::get('reports/medicals-table', 'ReportsController@getMedicalsTable');
	Route::get('reports/rents-table', 'ReportsController@getRentsTable');
	Route::get('reports/dawasco-table', 'ReportsController@getDawascoTable');
	Route::get('reports/luku-table', 'ReportsController@getLukuTable');
	Route::get('reports/loans-table', 'ReportsController@getLoansTable');
	Route::get('reports/simple-loans-table', 'ReportsController@getSimpleLoansTable');

	Route::get('/upload', 'UploadFileController@index');
	Route::post('upload/destroy', 'UploadFileController@destroy');
	Route::get('upload/view/{id}', 'UploadFileController@view');
	Route::post('upload/upload-file', 'UploadFileController@uploadFile');
	Route::get('upload/get-files', 'UploadFileController@allFilesTable');

	Route::get('/new-password', function(){return View::make('password');});
	Route::post('/new-password/save', function(){
		$user = Auth::user();
		$old_pass = Input::get('old_password');
		$new_pass = Input::get('new_password');

		if (!(Hash::check($old_pass, $user->password))) {
            // The passwords matches
            $status = 1;        
			return Response::json($status);
        }
 
        if(strcmp($old_pass, $new_pass) == 0){
            //Current password and new password are same
            $status = 2;        
			return Response::json($status);
        }
 
        //$validatedData = Input::validate([
        //    'old_password' => 'required',
        //    'new_password' => 'required|string|confirmed',
        //]);
 
        //Change Password
        $user->password = Hash::make($new_pass);
		$user->save();
		
		$status = 3;        
		return Response::json($status);
	});

	Route::get('/card?id={id}',function($id){
		$member_id = $id;
		return View::make('card',compact('member_id'));
	});

	Route::get('/card',function(){
		return View::make('card');
	});

	Route::get('/subsidy/students', 'SubsidyController@student');
	Route::post('subsidy/students/save', 'SubsidyController@studentSave');
	Route::get('subsidy/students/table', 'SubsidyController@studentTable');
	Route::post('subsidy/students/destroy', 'SubsidyController@studentDestroy');
	Route::get('/subsidy/students/fees', 'SubsidyController@calculateFees');
	Route::get('subsidy/students/list',function(){
		return View::make('subsidy.list_students');
	});

	Route::get('subsidy/grades', 'SubsidyController@grades');
	Route::post('subsidy/grades/save', 'SubsidyController@gradesSave');
	Route::post('subsidy/grades/destroy', 'SubsidyController@gradesDestroy');
	Route::get('subsidy/grades/get-grades', 'SubsidyController@getGrades');

});

// Route::get('dawasco-file/{id}', function($id){

// 	Response::stream(callback, status, headers)
	
// 	$path = '/home/legelalo/public_html/ssc/'.DawascoFile::find($id)->path;

// 	return Response::make(file_get_contents($path), 200, [
// 	    'Content-Type' => 'application/pdf',
// 	    'Content-Disposition' => 'inline; filename="'.'File'.'"'
// 	]);
// });
			

Route::get('developer', function(){
	
	return Request::path();
});

Route::get('mail-test',function(){ 
	$data = ['nlu'];
	$subject = "hello world";
	Mail::send('emails.mail', $data, function($message) use ($subject){
		//	$message->to('songensunza@gmail.com');
		$message->to('mwakalingajohn@gmail.com');
		$message->from('ssc@ssc.co.tz','Tiast');
		 $message->subject($subject);
	 });
});

