-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 13, 2019 at 11:45 AM
-- Server version: 10.1.32-MariaDB
-- PHP Version: 5.6.36

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ssc_databasebkup`
--

-- --------------------------------------------------------

--
-- Table structure for table `activities_track`
--

CREATE TABLE `activities_track` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `trackable_id` int(10) UNSIGNED NOT NULL,
  `trackable_type` varchar(255) NOT NULL,
  `comment` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `activities_track`
--

INSERT INTO `activities_track` (`id`, `user_id`, `trackable_id`, `trackable_type`, `comment`, `created_at`, `updated_at`) VALUES
(1, 32, 1, 'SSCFile', 'Created', '2019-04-01 08:46:40', '2019-04-01 08:46:40'),
(2, 32, 1, 'SSCFile', 'Edited', '2019-04-01 08:46:40', '2019-04-01 08:46:40'),
(3, 32, 1, 'SSCFile', 'Edited', '2019-04-01 08:50:02', '2019-04-01 08:50:02'),
(4, 32, 1, 'SSCFile', 'Edited', '2019-04-01 08:50:02', '2019-04-01 08:50:02'),
(5, 24, 1, 'Medical', 'Created', '2019-05-10 11:32:38', '2019-05-10 11:32:38');

-- --------------------------------------------------------

--
-- Table structure for table `addresses`
--

CREATE TABLE `addresses` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `person_id` mediumint(9) NOT NULL DEFAULT '0',
  `residence_phone_number` varchar(255) NOT NULL,
  `office_phone_number` varchar(255) NOT NULL,
  `mobile_phone_number` varchar(255) NOT NULL,
  `postal_address` varchar(255) NOT NULL,
  `physical_address` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `plot_number` varchar(255) NOT NULL,
  `block_number` varchar(255) NOT NULL,
  `floor_number` varchar(255) NOT NULL,
  `street` varchar(255) NOT NULL,
  `nearest_landmark` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `addresses`
--

INSERT INTO `addresses` (`id`, `person_id`, `residence_phone_number`, `office_phone_number`, `mobile_phone_number`, `postal_address`, `physical_address`, `email`, `plot_number`, `block_number`, `floor_number`, `street`, `nearest_landmark`, `created_at`, `updated_at`) VALUES
(1, 1, '', '', '0713602441', '16616 DSM', 'KKO', '', 'ILA-2-796', '149', '1st', 'Uhuru', 'Eco Bank', '2019-04-01 08:46:40', '2019-04-01 08:46:40');

-- --------------------------------------------------------

--
-- Table structure for table `assistances`
--

CREATE TABLE `assistances` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `person_id` mediumint(8) UNSIGNED NOT NULL DEFAULT '0',
  `rent` varchar(255) NOT NULL,
  `school_fees` varchar(255) NOT NULL,
  `medical` varchar(255) NOT NULL,
  `electricity` varchar(255) NOT NULL,
  `water` varchar(255) NOT NULL,
  `others` varchar(255) NOT NULL,
  `ramadhan` varchar(255) NOT NULL,
  `monthly_allocate` varchar(255) NOT NULL,
  `fitro` varchar(255) NOT NULL,
  `fitro_comment` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `medical_payment_type_id` int(11) DEFAULT NULL,
  `medical_cash_amount` double DEFAULT NULL,
  `monthly_allocated_amount` double DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `assistances`
--

INSERT INTO `assistances` (`id`, `person_id`, `rent`, `school_fees`, `medical`, `electricity`, `water`, `others`, `ramadhan`, `monthly_allocate`, `fitro`, `fitro_comment`, `created_at`, `updated_at`, `medical_payment_type_id`, `medical_cash_amount`, `monthly_allocated_amount`) VALUES
(1, 1, 'NO', 'NO', 'NO', 'NO', 'NO', 'NO', 'yes', 'yes', 'YES', '1 UNIT', '2019-04-01 08:46:40', '2019-04-01 08:50:02', 2, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `business_plans`
--

CREATE TABLE `business_plans` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `file_id` mediumint(9) NOT NULL,
  `loan_id` varchar(255) NOT NULL DEFAULT '',
  `business_name` varchar(255) NOT NULL DEFAULT '',
  `business_location` varchar(255) NOT NULL DEFAULT '',
  `start_date` date DEFAULT NULL,
  `business_website` varchar(255) NOT NULL DEFAULT '',
  `social_media_links` varchar(255) NOT NULL DEFAULT '',
  `academic_qualifications` text NOT NULL,
  `business_activities` text NOT NULL,
  `products_services` text NOT NULL,
  `how_sell` text NOT NULL,
  `business_mission` text NOT NULL,
  `business_vision` text NOT NULL,
  `business_uniqueness` text NOT NULL,
  `business_staff` text NOT NULL,
  `why_run_business` text NOT NULL,
  `previous_experience` text NOT NULL,
  `education` text NOT NULL,
  `hobbies` text NOT NULL,
  `business_experience` text NOT NULL,
  `other_personal_info` text NOT NULL,
  `product_overview` text NOT NULL,
  `how_to_produce` text NOT NULL,
  `production_cost` text NOT NULL,
  `selling_price` text NOT NULL,
  `how_to_deliver` text NOT NULL,
  `growth_potential` text NOT NULL,
  `customers` text NOT NULL,
  `customers_to_reach` text NOT NULL,
  `have_sold_already` text NOT NULL,
  `advantages_over_competitors` text NOT NULL,
  `market_research` text NOT NULL,
  `non_asset_cost` text NOT NULL,
  `business_strength` text NOT NULL,
  `business_weaknesses` text NOT NULL,
  `main_opportunities` text NOT NULL,
  `main_threats` text NOT NULL,
  `plan_if_fails` text NOT NULL,
  `guarantors` text NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `capacity_building`
--

CREATE TABLE `capacity_building` (
  `id` smallint(5) UNSIGNED NOT NULL,
  `file_id` smallint(5) UNSIGNED NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `date` date NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `created_by` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `capacity_building_images`
--

CREATE TABLE `capacity_building_images` (
  `id` smallint(5) UNSIGNED NOT NULL,
  `capacity_building_id` smallint(5) UNSIGNED NOT NULL,
  `path` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `children`
--

CREATE TABLE `children` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `person_id` mediumint(8) UNSIGNED NOT NULL,
  `full_name` varchar(255) DEFAULT NULL,
  `gender` varchar(255) DEFAULT NULL,
  `date_of_birth` date DEFAULT NULL,
  `occupation` varchar(255) DEFAULT NULL,
  `other_info` text,
  `school_college` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `children`
--

INSERT INTO `children` (`id`, `person_id`, `full_name`, `gender`, `date_of_birth`, `occupation`, `other_info`, `school_college`, `image`, `created_at`, `updated_at`) VALUES
(4, 1, 'Assad Mohamed', 'M', '0000-00-00', '', '', 'Union Nursery School', NULL, '2019-04-01 08:50:02', '2019-04-01 08:50:02'),
(3, 1, 'Salah Mohamed', 'M', '1999-07-11', '', '', 'O\'Levels Adult Collage', NULL, '2019-04-01 08:50:02', '2019-04-01 08:50:02');

-- --------------------------------------------------------

--
-- Table structure for table `currencies`
--

CREATE TABLE `currencies` (
  `id` tinyint(3) UNSIGNED NOT NULL,
  `name` varchar(50) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `currencies`
--

INSERT INTO `currencies` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'USD', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'TSHS', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 'Euro', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `dawasco`
--

CREATE TABLE `dawasco` (
  `id` mediumint(9) NOT NULL,
  `file_id` mediumint(9) NOT NULL,
  `title` varchar(255) NOT NULL DEFAULT '',
  `file` varchar(255) NOT NULL DEFAULT '',
  `date` date NOT NULL,
  `currency_id` tinyint(255) UNSIGNED NOT NULL,
  `total_amount` decimal(10,2) NOT NULL,
  `self_contribution` decimal(10,2) UNSIGNED NOT NULL,
  `ssc_contribution` decimal(10,2) NOT NULL,
  `remarks` text NOT NULL,
  `concluded` varchar(10) NOT NULL DEFAULT 'no',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `created_by` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `dawasco_files`
--

CREATE TABLE `dawasco_files` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `dawasco_id` mediumint(8) UNSIGNED NOT NULL,
  `path` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `files`
--

CREATE TABLE `files` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `file_no` varchar(255) NOT NULL,
  `user_type_assigned` tinyint(3) UNSIGNED NOT NULL DEFAULT '1',
  `date` date DEFAULT NULL,
  `lawajam_details` varchar(255) DEFAULT NULL,
  `date_paid` date DEFAULT NULL,
  `terms_agreed` tinyint(1) NOT NULL DEFAULT '0',
  `approved` tinyint(1) NOT NULL DEFAULT '0',
  `concluded` varchar(10) NOT NULL DEFAULT 'no',
  `closed` varchar(50) NOT NULL DEFAULT 'no',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `outreach_type_id` int(11) DEFAULT NULL,
  `user_type_file_no` varchar(1000) DEFAULT NULL,
  `user_type_or_no` varchar(1000) DEFAULT NULL,
  `lawajam_member_id` varchar(200) DEFAULT NULL,
  `lawajam_member_paid` date DEFAULT NULL,
  `lawajam_member_last_paid` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `files`
--

INSERT INTO `files` (`id`, `file_no`, `user_type_assigned`, `date`, `lawajam_details`, `date_paid`, `terms_agreed`, `approved`, `concluded`, `closed`, `created_at`, `updated_at`, `deleted_at`, `created_by`, `outreach_type_id`, `user_type_file_no`, `user_type_or_no`, `lawajam_member_id`, `lawajam_member_paid`, `lawajam_member_last_paid`) VALUES
(1, 'SSC/OR/38', 5, '2019-04-01', 'Marhum Mohamed Bachoo', '2017-03-30', 1, 1, 'yes', 'no', '2019-04-01 08:46:40', '2019-04-01 08:50:02', NULL, 32, 1, 'SSC/OR/0001', 'SSC/OR/US/0001', 'B/02', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `grades`
--

CREATE TABLE `grades` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `fees` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `grades`
--

INSERT INTO `grades` (`id`, `name`, `fees`, `created_at`, `updated_at`) VALUES
(1, 'FORM 4', '3350000', '2018-06-26 12:02:26', '2018-06-26 12:02:26');

-- --------------------------------------------------------

--
-- Table structure for table `incomes`
--

CREATE TABLE `incomes` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `person_id` mediumint(8) UNSIGNED NOT NULL DEFAULT '0',
  `spouse_id` mediumint(8) UNSIGNED NOT NULL DEFAULT '0',
  `income_type_id` tinyint(3) UNSIGNED NOT NULL,
  `business_name` varchar(255) NOT NULL,
  `location` varchar(255) NOT NULL,
  `business_type` varchar(255) NOT NULL,
  `income_per_month` varchar(255) NOT NULL,
  `other_incentives` varchar(255) DEFAULT NULL,
  `other_incentives_employed` int(100) DEFAULT NULL,
  `employers_name` varchar(255) NOT NULL,
  `office_employed` varchar(255) NOT NULL,
  `position_employed` varchar(255) NOT NULL,
  `salary_per_month` varchar(255) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `incomes`
--

INSERT INTO `incomes` (`id`, `person_id`, `spouse_id`, `income_type_id`, `business_name`, `location`, `business_type`, `income_per_month`, `other_incentives`, `other_incentives_employed`, `employers_name`, `office_employed`, `position_employed`, `salary_per_month`, `created_at`, `updated_at`) VALUES
(1, 1, 0, 3, '', '', '', '', '', 0, '', '', '', '', '2019-04-01', '2019-04-01 08:46:40'),
(2, 0, 1, 0, '', '', '', '', NULL, NULL, '', '', '', '', '2019-04-01', '2019-04-01 08:46:40'),
(3, 0, 1, 0, '', '', '', '', NULL, NULL, '', '', '', '', '2019-04-01', '2019-04-01 08:50:02');

-- --------------------------------------------------------

--
-- Table structure for table `income_types`
--

CREATE TABLE `income_types` (
  `id` tinyint(3) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `income_types`
--

INSERT INTO `income_types` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Own business', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'Employed', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 'Unemployed', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `installments`
--

CREATE TABLE `installments` (
  `id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `amount` varchar(255) NOT NULL,
  `date` date NOT NULL,
  `name` varchar(255) NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `interviews`
--

CREATE TABLE `interviews` (
  `id` mediumint(9) NOT NULL,
  `file_id` mediumint(8) UNSIGNED NOT NULL,
  `created_by` int(10) UNSIGNED NOT NULL,
  `date` date NOT NULL,
  `department` varchar(255) NOT NULL,
  `interview_with` varchar(255) NOT NULL,
  `telephone` varchar(255) NOT NULL,
  `employer` varchar(255) NOT NULL,
  `capacity` varchar(255) NOT NULL,
  `employed_since` varchar(255) NOT NULL,
  `gross_salary` varchar(255) NOT NULL,
  `interviewer` varchar(255) NOT NULL,
  `family` varchar(200) DEFAULT NULL,
  `signature` varchar(200) DEFAULT NULL,
  `residence` varchar(200) DEFAULT NULL,
  `assistance_currently_receiving` text NOT NULL,
  `issues_discussed` text NOT NULL,
  `conclusion` text NOT NULL,
  `concluded` varchar(10) NOT NULL DEFAULT 'no',
  `next_date` date NOT NULL,
  `task_allocation` varchar(255) NOT NULL,
  `lawajam` varchar(255) NOT NULL,
  `jamaat_rent` varchar(255) NOT NULL,
  `school_fees` varchar(255) NOT NULL,
  `remarks` text NOT NULL,
  `next_interview` date NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `loans`
--

CREATE TABLE `loans` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `file_id` varchar(255) NOT NULL,
  `code` varchar(255) NOT NULL,
  `currency_id` tinyint(3) UNSIGNED NOT NULL,
  `amount` decimal(10,2) NOT NULL,
  `deadline` date NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `loan_agreements`
--

CREATE TABLE `loan_agreements` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `file_id` mediumint(9) NOT NULL DEFAULT '0',
  `loan_id` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL DEFAULT '',
  `file` varchar(255) NOT NULL DEFAULT '',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `loan_applications`
--

CREATE TABLE `loan_applications` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `file_id` mediumint(8) UNSIGNED NOT NULL,
  `loan_id` varchar(255) NOT NULL,
  `how_did_you_hear` text NOT NULL,
  `bank_creditor` varchar(255) NOT NULL DEFAULT '',
  `credit_limit` varchar(255) NOT NULL DEFAULT '',
  `outstand_balance` varchar(255) NOT NULL DEFAULT '',
  `payment_frequency` varchar(255) NOT NULL DEFAULT '',
  `arreas` varchar(255) NOT NULL DEFAULT '',
  `date_started` date NOT NULL,
  `date_completed` date NOT NULL,
  `referee_name` varchar(255) NOT NULL DEFAULT '',
  `referee_relationship` varchar(255) NOT NULL DEFAULT '',
  `referee_address` varchar(255) NOT NULL DEFAULT '',
  `loan_amount` varchar(255) NOT NULL DEFAULT '',
  `loan_duration` varchar(255) NOT NULL DEFAULT '',
  `loan_purpose` text NOT NULL,
  `preffered_rate` varchar(255) NOT NULL DEFAULT '',
  `affordable_repayment` varchar(255) NOT NULL DEFAULT '',
  `project_cost` varchar(255) NOT NULL DEFAULT '',
  `business_name` varchar(255) NOT NULL,
  `business_address` varchar(255) NOT NULL,
  `business_telephone` varchar(255) NOT NULL,
  `business_fax` varchar(255) NOT NULL,
  `business_email` varchar(255) NOT NULL,
  `business_ownership` varchar(255) NOT NULL,
  `type_of_business` varchar(255) NOT NULL,
  `how_long_trading` varchar(255) NOT NULL,
  `business_registration` varchar(255) NOT NULL,
  `registration_date` date NOT NULL,
  `vat_no` varchar(255) NOT NULL,
  `state_type` varchar(255) NOT NULL,
  `business_place_ownership` varchar(255) NOT NULL,
  `landlord_name` varchar(255) NOT NULL,
  `landlord_address` varchar(255) NOT NULL,
  `landlord_telephone` varchar(255) NOT NULL,
  `landlord_email` varchar(255) NOT NULL,
  `shop_location` varchar(255) NOT NULL,
  `other_sources_of_income` varchar(255) NOT NULL,
  `strength` text NOT NULL,
  `weaknesses` text NOT NULL,
  `opportunities` text NOT NULL,
  `threats` text NOT NULL,
  `family_income_expenditure` text NOT NULL,
  `bank_details` varchar(255) NOT NULL,
  `additional_info` text NOT NULL,
  `declaration_full_name` varchar(255) NOT NULL,
  `declaration_postal_address` varchar(255) NOT NULL,
  `declaration_street` varchar(255) NOT NULL,
  `declaration_date` date NOT NULL,
  `declaration_accept` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `loan_guarantee`
--

CREATE TABLE `loan_guarantee` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `file_id` mediumint(8) UNSIGNED NOT NULL,
  `loan_id` varchar(255) NOT NULL,
  `full_name` varchar(255) NOT NULL,
  `postal_address` varchar(255) NOT NULL,
  `telephone` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `loan_purpose` varchar(255) NOT NULL,
  `borrowers_full_name` varchar(255) NOT NULL,
  `borrowers_postal_address` varchar(255) NOT NULL,
  `loan_amount` varchar(255) NOT NULL,
  `date_of_loan_agreement` date NOT NULL,
  `witness_full_name` varchar(255) NOT NULL,
  `witness_address` varchar(255) NOT NULL,
  `agree` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `loan_payments`
--

CREATE TABLE `loan_payments` (
  `id` int(10) UNSIGNED NOT NULL,
  `loan_id` mediumint(8) UNSIGNED NOT NULL,
  `amount` decimal(10,2) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `luku`
--

CREATE TABLE `luku` (
  `id` mediumint(9) NOT NULL,
  `file_id` mediumint(8) UNSIGNED NOT NULL,
  `title` varchar(255) NOT NULL DEFAULT '',
  `file` varchar(255) NOT NULL DEFAULT '',
  `date` date NOT NULL,
  `currency_id` tinyint(255) UNSIGNED NOT NULL,
  `total_amount` decimal(10,2) NOT NULL,
  `self_contribution` decimal(10,2) UNSIGNED NOT NULL,
  `ssc_contribution` decimal(10,2) NOT NULL,
  `remarks` text NOT NULL,
  `concluded` varchar(10) NOT NULL DEFAULT 'no',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `created_by` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `luku_files`
--

CREATE TABLE `luku_files` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `luku_id` mediumint(8) UNSIGNED NOT NULL,
  `path` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `medicals`
--

CREATE TABLE `medicals` (
  `id` mediumint(9) NOT NULL,
  `file_id` mediumint(8) UNSIGNED NOT NULL,
  `title` varchar(255) NOT NULL DEFAULT '',
  `file` varchar(255) NOT NULL DEFAULT '',
  `date` date NOT NULL,
  `currency_id` tinyint(255) UNSIGNED NOT NULL,
  `total_amount` decimal(10,2) NOT NULL,
  `self_contribution` decimal(10,2) UNSIGNED NOT NULL,
  `ssc_contribution` decimal(10,2) NOT NULL,
  `remarks` text NOT NULL,
  `concluded` varchar(10) NOT NULL DEFAULT 'no',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `created_by` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `medicals`
--

INSERT INTO `medicals` (`id`, `file_id`, `title`, `file`, `date`, `currency_id`, `total_amount`, `self_contribution`, `ssc_contribution`, `remarks`, `concluded`, `created_at`, `updated_at`, `deleted_at`, `created_by`) VALUES
(1, 1, '', '', '2019-10-05', 2, '12.00', '1.00', '2.00', 'dgh', 'no', '2019-05-10 11:32:38', '2019-05-10 11:32:38', NULL, 24);

-- --------------------------------------------------------

--
-- Table structure for table `medical_files`
--

CREATE TABLE `medical_files` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `medical_id` mediumint(8) UNSIGNED NOT NULL,
  `path` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `medical_payment_types`
--

CREATE TABLE `medical_payment_types` (
  `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `medical_payment_types`
--

INSERT INTO `medical_payment_types` (`id`, `name`) VALUES
(1, 'CASH'),
(2, 'NHIF INSURANCE');

-- --------------------------------------------------------

--
-- Table structure for table `others`
--

CREATE TABLE `others` (
  `id` int(11) NOT NULL,
  `full_name` varchar(100) DEFAULT NULL,
  `person_id` int(11) DEFAULT NULL,
  `gender` varchar(10) DEFAULT NULL,
  `date_of_birth` date DEFAULT NULL,
  `occupation` text,
  `relationship_id` int(11) DEFAULT NULL,
  `phone` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `address` varchar(100) DEFAULT NULL,
  `family_assistance` varchar(255) DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `relationship` varchar(100) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `outreach_types`
--

CREATE TABLE `outreach_types` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `num` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `outreach_types`
--

INSERT INTO `outreach_types` (`id`, `name`, `updated_at`, `created_at`, `num`) VALUES
(1, 'US - UMM SALMA', '2019-04-01 05:46:40', '0000-00-00 00:00:00', 1),
(2, 'BF - BAYTUL FATIMIYYAH', '2019-02-27 09:29:49', '0000-00-00 00:00:00', 0),
(3, 'OPH - OLD PEOPLES HOME', '2019-02-27 09:29:55', '0000-00-00 00:00:00', 0),
(4, 'BK - BEWAKHANA (BAYTUL KHADIJA)', '2018-01-29 01:11:30', '0000-00-00 00:00:00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `people`
--

CREATE TABLE `people` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `file_id` int(11) NOT NULL DEFAULT '0',
  `full_name` varchar(255) NOT NULL,
  `date_of_birth` date NOT NULL,
  `passport_number` varchar(255) NOT NULL,
  `nationality` varchar(255) NOT NULL,
  `passport_expiry_date` date NOT NULL,
  `head_of_family` varchar(255) NOT NULL,
  `education_level` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `voter_id` varchar(200) DEFAULT NULL,
  `driver_licence` varchar(200) DEFAULT NULL,
  `national_id` varchar(200) DEFAULT NULL,
  `voter_image` text,
  `driver_image` text,
  `national_image` text,
  `passport_image` text,
  `total_family` int(11) DEFAULT NULL,
  `total_adults` int(11) DEFAULT NULL,
  `total_kids` int(11) DEFAULT NULL,
  `maritual_status` varchar(100) DEFAULT NULL,
  `maritual_status_name` varchar(100) DEFAULT NULL,
  `maritual_status_comment` text
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `people`
--

INSERT INTO `people` (`id`, `file_id`, `full_name`, `date_of_birth`, `passport_number`, `nationality`, `passport_expiry_date`, `head_of_family`, `education_level`, `image`, `created_at`, `updated_at`, `voter_id`, `driver_licence`, `national_id`, `voter_image`, `driver_image`, `national_image`, `passport_image`, `total_family`, `total_adults`, `total_kids`, `maritual_status`, `maritual_status_name`, `maritual_status_comment`) VALUES
(1, 1, 'Marziya Mohamed Bachoo', '1959-03-30', '', '', '1959-03-30', 'Marziya', 'STD 3', '', '2019-04-01 08:46:40', '2019-04-01 08:50:02', '', '', '19580330-11105-00001-13', NULL, NULL, NULL, NULL, 3, 2, 1, 'Widow', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` smallint(5) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Registration', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'Interview', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 'Education', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 'Medical', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 'Loan', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 'Rent', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, 'Dawasco', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(8, 'Luku', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(9, 'Reports', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(10, 'settings', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(11, 'capacity building', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(12, 'Will Attachment', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(13, 'Visit Form', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(15, 'Vouchers', '2019-03-07 00:00:00', '2019-03-07 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `permission_user_type`
--

CREATE TABLE `permission_user_type` (
  `user_type_id` smallint(5) UNSIGNED NOT NULL,
  `permission_id` smallint(5) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `permission_user_type`
--

INSERT INTO `permission_user_type` (`user_type_id`, `permission_id`) VALUES
(1, 1),
(1, 2),
(1, 3),
(1, 4),
(1, 5),
(1, 6),
(1, 7),
(1, 8),
(1, 9),
(1, 11),
(1, 12),
(1, 13),
(1, 15),
(2, 1),
(2, 2),
(2, 3),
(2, 4),
(2, 6),
(2, 7),
(2, 8),
(2, 9),
(2, 12),
(2, 13),
(3, 3),
(3, 6),
(4, 1),
(4, 5),
(4, 13),
(5, 1),
(5, 2),
(5, 3),
(5, 4),
(5, 5),
(5, 6),
(5, 7),
(5, 8),
(5, 9),
(5, 12),
(5, 13),
(6, 1),
(7, 1),
(7, 2),
(7, 5),
(7, 6),
(7, 8),
(7, 11),
(7, 13),
(10, 1),
(10, 2),
(10, 3),
(10, 4),
(10, 5),
(10, 6),
(10, 7),
(10, 8),
(10, 9),
(10, 10),
(10, 11),
(10, 12),
(10, 13),
(10, 15);

-- --------------------------------------------------------

--
-- Table structure for table `rates`
--

CREATE TABLE `rates` (
  `id` tinyint(3) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `relationships`
--

CREATE TABLE `relationships` (
  `id` int(11) NOT NULL,
  `name` varchar(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `relationships`
--

INSERT INTO `relationships` (`id`, `name`) VALUES
(1, 'Father'),
(2, 'Mother'),
(3, 'Brother'),
(4, 'Sister'),
(5, 'Aunt'),
(6, 'Friend'),
(7, 'Uncle'),
(8, 'Other');

-- --------------------------------------------------------

--
-- Table structure for table `remarks`
--

CREATE TABLE `remarks` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `file_id` mediumint(8) UNSIGNED NOT NULL DEFAULT '0',
  `by_member` text,
  `by_head_of_department` text,
  `by_chairman_of_welfare` text,
  `notes` text,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `remarks`
--

INSERT INTO `remarks` (`id`, `file_id`, `by_member`, `by_head_of_department`, `by_chairman_of_welfare`, `notes`, `created_at`, `updated_at`) VALUES
(1, 1, '', '', '', '', '2019-04-01 08:46:40', '2019-04-01 08:46:40');

-- --------------------------------------------------------

--
-- Table structure for table `rent`
--

CREATE TABLE `rent` (
  `id` mediumint(9) NOT NULL,
  `created_by` int(10) UNSIGNED NOT NULL,
  `file_id` mediumint(8) UNSIGNED NOT NULL,
  `title` varchar(255) NOT NULL DEFAULT '',
  `file` varchar(255) NOT NULL DEFAULT '',
  `date` date NOT NULL,
  `currency_id` varchar(255) NOT NULL,
  `total_amount` decimal(10,2) NOT NULL,
  `self_contribution` decimal(10,2) UNSIGNED NOT NULL,
  `ssc_contribution` decimal(10,2) NOT NULL,
  `remarks` text NOT NULL,
  `concluded` varchar(10) NOT NULL DEFAULT 'no',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rent_files`
--

CREATE TABLE `rent_files` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `rent_id` mediumint(8) UNSIGNED NOT NULL,
  `path` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `residence`
--

CREATE TABLE `residence` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `person_id` mediumint(8) UNSIGNED NOT NULL DEFAULT '0',
  `residence_type_id` tinyint(3) UNSIGNED NOT NULL,
  `residence_ownership_type_id` tinyint(3) UNSIGNED NOT NULL,
  `owner_name` varchar(255) NOT NULL,
  `rent_per_month` varchar(255) NOT NULL,
  `private_owner_name` varchar(255) DEFAULT NULL,
  `jamaat_membership_no` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `residence`
--

INSERT INTO `residence` (`id`, `person_id`, `residence_type_id`, `residence_ownership_type_id`, `owner_name`, `rent_per_month`, `private_owner_name`, `jamaat_membership_no`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 1, '', '337,500', '', '', '2019-04-01 08:46:40', '2019-04-01 08:46:40');

-- --------------------------------------------------------

--
-- Table structure for table `residence_ownership_types`
--

CREATE TABLE `residence_ownership_types` (
  `id` tinyint(3) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `residence_ownership_types`
--

INSERT INTO `residence_ownership_types` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'NHC', '2018-02-20 00:00:00', '2018-02-20 00:00:00'),
(2, 'PRIVATE', '2018-02-20 00:00:00', '2018-02-20 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `residence_types`
--

CREATE TABLE `residence_types` (
  `id` tinyint(3) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `residence_types`
--

INSERT INTO `residence_types` (`id`, `name`, `created_at`, `updated_at`) VALUES
(2, 'Own house', '2018-02-20 00:00:00', '2018-02-20 00:00:00'),
(1, 'Rented house', '2018-02-20 00:00:00', '2018-02-20 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `schools`
--

CREATE TABLE `schools` (
  `id` tinyint(3) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `school_assistances`
--

CREATE TABLE `school_assistances` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `file_id` mediumint(8) UNSIGNED NOT NULL,
  `title` varchar(255) NOT NULL DEFAULT '',
  `name_of_student` varchar(255) NOT NULL,
  `gender` varchar(255) NOT NULL,
  `date_of_birth` date NOT NULL,
  `date` date DEFAULT NULL,
  `school_id` varchar(255) NOT NULL,
  `school_name` varchar(255) NOT NULL,
  `grade` varchar(255) NOT NULL,
  `course` varchar(255) NOT NULL DEFAULT '',
  `currency_id` tinyint(3) UNSIGNED NOT NULL,
  `total_amount` decimal(10,2) NOT NULL,
  `self_contribution` decimal(10,2) NOT NULL DEFAULT '0.00',
  `ssc_contribution` decimal(10,2) NOT NULL,
  `attachment` varchar(255) NOT NULL,
  `file` varchar(255) NOT NULL DEFAULT '',
  `agree` tinyint(1) NOT NULL,
  `concluded` varchar(10) NOT NULL DEFAULT 'no',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `created_by` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `simple_loan_agreements`
--

CREATE TABLE `simple_loan_agreements` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `created_by` int(10) UNSIGNED NOT NULL,
  `file_id` mediumint(8) UNSIGNED NOT NULL,
  `loan_code` varchar(20) NOT NULL,
  `currency_id` tinyint(3) UNSIGNED NOT NULL,
  `amount` decimal(10,2) NOT NULL,
  `deadline` date NOT NULL,
  `file` varchar(255) NOT NULL,
  `rate_id` tinyint(3) UNSIGNED NOT NULL,
  `date` date NOT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `simple_loan_payments`
--

CREATE TABLE `simple_loan_payments` (
  `id` int(10) UNSIGNED NOT NULL,
  `simple_loan_code` varchar(50) NOT NULL,
  `remaining` decimal(10,2) NOT NULL,
  `file_id` mediumint(8) UNSIGNED NOT NULL,
  `amount` decimal(10,2) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `created_by` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `spouses`
--

CREATE TABLE `spouses` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `person_id` mediumint(8) UNSIGNED NOT NULL DEFAULT '0',
  `full_name` varchar(255) NOT NULL,
  `date_of_birth` date NOT NULL,
  `occupation` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `spouses`
--

INSERT INTO `spouses` (`id`, `person_id`, `full_name`, `date_of_birth`, `occupation`, `created_at`, `updated_at`) VALUES
(1, 1, 'Marhum Mohammed Bachoo ', '0000-00-00', '', '2019-04-01 08:46:40', '2019-04-01 08:50:02');

-- --------------------------------------------------------

--
-- Table structure for table `students`
--

CREATE TABLE `students` (
  `id` int(11) NOT NULL,
  `student_name` varchar(255) NOT NULL,
  `student_no` varchar(255) NOT NULL,
  `serial_no` varchar(255) NOT NULL,
  `lawajam` varchar(255) NOT NULL,
  `parent_name` varchar(255) NOT NULL,
  `email` varchar(100) NOT NULL,
  `phone_no` varchar(255) NOT NULL,
  `grade_id` int(11) NOT NULL,
  `actual_fees` varchar(255) NOT NULL,
  `quarter` varchar(255) NOT NULL,
  `total_fees` varchar(255) NOT NULL,
  `new_student` varchar(255) DEFAULT NULL,
  `exam_fees` varchar(255) DEFAULT NULL,
  `level` varchar(100) NOT NULL,
  `created_by` varchar(255) NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `upload_files`
--

CREATE TABLE `upload_files` (
  `id` int(11) NOT NULL,
  `file_id` int(10) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `file` varchar(255) NOT NULL,
  `date` date NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `user_type` tinyint(4) NOT NULL,
  `full_name` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `active_file` int(11) DEFAULT NULL,
  `remember_token` varchar(255) NOT NULL DEFAULT '',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `user_type`, `full_name`, `username`, `password`, `active_file`, `remember_token`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 'System Admin', 'admin', '$2y$10$7BK0jiIkmRTQxbgegvWvZ.OLSUG2bf2FrYLE20JdOG8XXIyQQpiJS', NULL, 'x4Y64QWjZKHX3FohsQrWbvNEQdsDBXF8Nob8nA8F4hTU3cwy2zpuhAJrFVs8', '2017-07-31 13:04:00', '2018-02-16 11:47:54', '2018-02-16 11:47:54'),
(9, 1, 'said', '12345', '$2y$10$IOXDHA32BTFTqqYjGiJJo.yQd4t.D5CQJ5yFh/Cv.okY8EuZomCh6', 0, '1tOcUGVVH1DdSs0Kpi7Idi7ygc1LA2ucTV9zskFkvNOfrA7QEPO6X9FxDc1z', '2018-01-31 11:19:16', '2018-03-27 10:46:04', '2018-03-27 10:46:04'),
(22, 5, 'sajida tejani', 'Stejani', '$2y$10$p2YoVtKq9nuzt58s/XWUMuGd4if6CD/6PxemfMMak3nnqL/HCuIS2', NULL, 'LCyljiXDT4QxzxsMypcGZHzavHI376PvhJnzMAT0sOZJ9gSLZFQuofILscAW', '2018-03-01 09:22:22', '2019-03-12 12:35:36', '2019-03-12 12:35:36'),
(12, 7, 'sarf', 'sarf', '$2y$10$teU.ByzHlLUY1UYDnUYKNuMRuKZZh818XAHqeXnEbQ5RKI9u8d8rC', 23, 'nkqwLnh2iiLXXo2jWt6b4fQndvXzcsfwETyFnWwC5kmwJ8ENQuTChSGECqci', '2018-01-31 11:50:28', '2018-02-16 11:48:11', '2018-02-16 11:48:11'),
(27, 7, 'sarf', '222', '$2y$10$YdM/jm7DYYZ8f0daOX.ZNuZn3gPH.FmGUolUV5TIBe/BgPf1h01Ay', 0, 'ufc9IdsivDiSMoY7h47u1nukl3fjqznDKAuQxXq5ZG5bdYissKsisDUfm65a', '2018-03-13 07:26:28', '2018-03-27 10:45:54', '2018-03-27 10:45:54'),
(26, 5, 'outreach', 'outreach', '$2y$10$fkmyPKew/a7B5UI2UR1w3.m5Th/G0aLQBET4Awpu5HOhp8ZytwJVm', 0, 'aZInpSVFkOTUgBPPNeg6tELO8F0CKRb4YWUu14EgP9W2Dzrz5h38E76baQoj', '2018-03-09 14:07:00', '2018-03-09 14:10:33', '2018-03-09 14:10:33'),
(25, 5, 'outreach', 'outreach', '$2y$10$bIHZ4uRHj0DFExb.KAYSlOEbMKs58qaM6d7BTXISFArlnlXh6GRti', NULL, '', '2018-03-09 14:06:59', '2018-03-09 14:10:30', '2018-03-09 14:10:30'),
(24, 10, 'Superadmin', 'superadmin', '$2y$12$T0a/ouNuzGf8fArPz3jI9uzoAR0tI8n0wIGe56g1BqXOLPEF.UrgC', 1, 'mOmmGQlfN0Ex0VUqlls8WkqCJwYCPCFBdwkucMCGcIRl4VoVVXxKwn1D64Xg', '2018-03-09 09:03:46', '2019-05-10 11:32:06', NULL),
(23, 2, 'Sarfaraz Dhalla', 'sarf1', '$2y$10$SKCKjgPm1bmpoOSYOy2eIejZBvVtxWGwXX3ui4Wi4nKBvfg7C.cX.', 0, 'GZ4Gn6ZhfUaqJENRIu4TaYyKZoDHCQ5K0oZM94cnKdVal9XTl3BRTpcpwTHv', '2018-03-09 06:28:33', '2018-03-27 10:45:58', '2018-03-27 10:45:58'),
(21, 1, 'Mohammed', 'Mohammedkhatau', '$2y$10$YIGmtdE3A8cyTajmCxE6b.J8wfVqOdHP0jNrqZq1Y42lQkt9.seBG', NULL, 'BcdIvdHRMj5k0ftKCDAZ9a1K1gt1Ns4esqBrSFrbUBTKeccSAslqLWJYlCCa', '2018-02-27 08:42:42', '2019-03-12 12:35:33', '2019-03-12 12:35:33'),
(28, 3, 'ahmed', 'ahmed', '$2y$10$FFjj9VOC2obMKbkKkOMqCeZKMr9cPXCXPNCaaeZS7nvPx3jRFdc0K', 0, 'DI0SJ6Z2O3BPhtz0xfjMpoHWTq3QxiAbuaM1jHYBuASgLOl8cTcMHo3uH0G9', '2018-03-19 11:56:36', '2018-08-14 08:17:21', '2018-08-14 08:17:21'),
(29, 5, 'shireen', 'shireen', '$2y$10$YpMHCvVRao1yUFqrKNJ3hul41R4cK443OyI05OwueX1QAm6a7kZzu', 0, 'HEyeXguQ8JpNl74F54WN7NYjEneULhuv299EYnZfALJrdS6XwmFJBP3AQlww', '2018-08-14 08:19:08', '2019-03-12 12:35:27', '2019-03-12 12:35:27'),
(30, 1, 'Sarfaraz', 'sarf', '$2y$10$2sUMex3jbpt1zhCdhDkI3OmYcM4ZtZhj8GuKK6fcW4P1.JDOFGzYC', 0, 'D1dSlhDUF5dJ36zWQSOXrO6BncPSmvwlGyvAkAgdlNwz5oQ3tB8jFGvUbMAq', '2019-03-12 12:36:03', '2019-03-12 12:36:58', '2019-03-12 12:36:58'),
(31, 1, 'SAr', 'asdf', '$2y$10$./TyQyRkEUP4mk.OTcP6meYCYn1phJPkGlN1PFIQR8jJqRjKjEUke', NULL, '', '2019-03-12 12:37:13', '2019-03-12 12:38:25', '2019-03-12 12:38:25'),
(32, 1, 'Shireen Jabir', 'Shireen', '$2y$10$VjcJ07cmJsYxGhuibXVQte7dkb7iuTD3t0jnTDTOQdE6.Hv8LU5JW', 0, 'KtyXiyKIqL1tmOSlqvr8eklD41XZ7kuaVpDxef0FgWnsujS4385NFtCHDYtI', '2019-04-01 07:56:57', '2019-04-01 08:54:56', NULL),
(33, 1, 'Sajeda Walji', 'sajeda', '$2y$10$p1OHjddcLmuhxFEI8mKHMeCZB.HSptJ92YFisXEIOrkUp5cNBEt8G', NULL, '', '2019-04-01 07:58:37', '2019-04-01 07:58:37', NULL),
(34, 1, 'Sayeeda Virjee', 'sayeeda', '$2y$10$oDjwRzza0ZxbUOc4LDlbVeT04l6x1pgY.D2JbpikzMh0ozluMt5C.', 0, 'eypEw0xT2tSdImEEx1GY5o8cKDgXZ5rMgjmOG1WsFKSoeQY93DrHuBmyWURG', '2019-04-01 07:59:54', '2019-04-03 07:56:54', NULL),
(35, 1, 'temp', 'temp', '$2y$10$s5XxWu.vIjT2KmLnM11GB.VdbMTTHtuC0X0dMeyPY0aYKq4OZfnrW', 1, 'SjzDTKvaG4Kxy6SdW9aExXS8MTyLHmMRSH9okqMVy1IVmvTFco5dzBqRZhNR', '2019-04-01 08:57:21', '2019-04-01 08:57:45', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_types`
--

CREATE TABLE `user_types` (
  `id` tinyint(3) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `color` varchar(7) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `num` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_types`
--

INSERT INTO `user_types` (`id`, `name`, `color`, `created_at`, `updated_at`, `num`) VALUES
(1, 'Admin', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(2, 'Tier 0', '#357eb3', '0000-00-00 00:00:00', '2019-02-27 13:21:37', 0),
(3, 'Tier 1', '#184c71', '0000-00-00 00:00:00', '2018-06-27 07:46:50', 0),
(4, 'Tier 2', '#072338', '0000-00-00 00:00:00', '2018-06-26 09:15:24', 0),
(5, 'Outreach', '#bf4a5f', '0000-00-00 00:00:00', '2019-04-01 08:46:40', 1),
(6, 'First aid', '#981b31', '0000-00-00 00:00:00', '2018-05-18 11:19:18', 0),
(7, 'Capacity Building', '#5a0c1a', '0000-00-00 00:00:00', '2018-03-13 08:47:29', 0),
(10, 'Superadmin', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `visits`
--

CREATE TABLE `visits` (
  `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `date` date NOT NULL,
  `purpose` text,
  `comment` text,
  `action` text,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `file_id` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `visit_files`
--

CREATE TABLE `visit_files` (
  `id` int(11) NOT NULL,
  `visit_id` int(11) NOT NULL,
  `path` varchar(200) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `vouchers`
--

CREATE TABLE `vouchers` (
  `id` int(11) NOT NULL,
  `created_by` varchar(200) NOT NULL,
  `file_id` int(11) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `amount` varchar(200) DEFAULT NULL,
  `payable_to` varchar(200) DEFAULT NULL,
  `in_respect_of` varchar(200) DEFAULT NULL,
  `amount_in_words` varchar(200) DEFAULT NULL,
  `prepared_by` varchar(200) DEFAULT NULL,
  `authorised` varchar(200) DEFAULT NULL,
  `received` varchar(200) DEFAULT NULL,
  `reference` varchar(200) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `wills`
--

CREATE TABLE `wills` (
  `id` smallint(5) UNSIGNED NOT NULL,
  `file_id` smallint(5) UNSIGNED NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `path` varchar(255) NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `created_by` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `activities_track`
--
ALTER TABLE `activities_track`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `addresses`
--
ALTER TABLE `addresses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `assistances`
--
ALTER TABLE `assistances`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `business_plans`
--
ALTER TABLE `business_plans`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `capacity_building`
--
ALTER TABLE `capacity_building`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `capacity_building_images`
--
ALTER TABLE `capacity_building_images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `children`
--
ALTER TABLE `children`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `currencies`
--
ALTER TABLE `currencies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dawasco`
--
ALTER TABLE `dawasco`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dawasco_files`
--
ALTER TABLE `dawasco_files`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `files`
--
ALTER TABLE `files`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `grades`
--
ALTER TABLE `grades`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `incomes`
--
ALTER TABLE `incomes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `income_types`
--
ALTER TABLE `income_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `installments`
--
ALTER TABLE `installments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `interviews`
--
ALTER TABLE `interviews`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `loans`
--
ALTER TABLE `loans`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `loan_agreements`
--
ALTER TABLE `loan_agreements`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `loan_applications`
--
ALTER TABLE `loan_applications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `loan_guarantee`
--
ALTER TABLE `loan_guarantee`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `loan_payments`
--
ALTER TABLE `loan_payments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `luku`
--
ALTER TABLE `luku`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `luku_files`
--
ALTER TABLE `luku_files`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `medicals`
--
ALTER TABLE `medicals`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `medical_files`
--
ALTER TABLE `medical_files`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `medical_payment_types`
--
ALTER TABLE `medical_payment_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `others`
--
ALTER TABLE `others`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `outreach_types`
--
ALTER TABLE `outreach_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `people`
--
ALTER TABLE `people`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `permission_user_type`
--
ALTER TABLE `permission_user_type`
  ADD UNIQUE KEY `user_id` (`user_type_id`,`permission_id`),
  ADD UNIQUE KEY `user_id_2` (`user_type_id`,`permission_id`);

--
-- Indexes for table `rates`
--
ALTER TABLE `rates`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `relationships`
--
ALTER TABLE `relationships`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `remarks`
--
ALTER TABLE `remarks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rent`
--
ALTER TABLE `rent`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rent_files`
--
ALTER TABLE `rent_files`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `residence`
--
ALTER TABLE `residence`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `residence_ownership_types`
--
ALTER TABLE `residence_ownership_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `residence_types`
--
ALTER TABLE `residence_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `schools`
--
ALTER TABLE `schools`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `school_assistances`
--
ALTER TABLE `school_assistances`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `simple_loan_agreements`
--
ALTER TABLE `simple_loan_agreements`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `simple_loan_payments`
--
ALTER TABLE `simple_loan_payments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `spouses`
--
ALTER TABLE `spouses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `students`
--
ALTER TABLE `students`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `upload_files`
--
ALTER TABLE `upload_files`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_types`
--
ALTER TABLE `user_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `visits`
--
ALTER TABLE `visits`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `visit_files`
--
ALTER TABLE `visit_files`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vouchers`
--
ALTER TABLE `vouchers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wills`
--
ALTER TABLE `wills`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `activities_track`
--
ALTER TABLE `activities_track`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `addresses`
--
ALTER TABLE `addresses`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `assistances`
--
ALTER TABLE `assistances`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `business_plans`
--
ALTER TABLE `business_plans`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `capacity_building`
--
ALTER TABLE `capacity_building`
  MODIFY `id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `capacity_building_images`
--
ALTER TABLE `capacity_building_images`
  MODIFY `id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `children`
--
ALTER TABLE `children`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `currencies`
--
ALTER TABLE `currencies`
  MODIFY `id` tinyint(3) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `dawasco`
--
ALTER TABLE `dawasco`
  MODIFY `id` mediumint(9) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `dawasco_files`
--
ALTER TABLE `dawasco_files`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `files`
--
ALTER TABLE `files`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `grades`
--
ALTER TABLE `grades`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `incomes`
--
ALTER TABLE `incomes`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `income_types`
--
ALTER TABLE `income_types`
  MODIFY `id` tinyint(3) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `installments`
--
ALTER TABLE `installments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `interviews`
--
ALTER TABLE `interviews`
  MODIFY `id` mediumint(9) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `loans`
--
ALTER TABLE `loans`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `loan_agreements`
--
ALTER TABLE `loan_agreements`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `loan_applications`
--
ALTER TABLE `loan_applications`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `loan_guarantee`
--
ALTER TABLE `loan_guarantee`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `loan_payments`
--
ALTER TABLE `loan_payments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `luku`
--
ALTER TABLE `luku`
  MODIFY `id` mediumint(9) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `luku_files`
--
ALTER TABLE `luku_files`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `medicals`
--
ALTER TABLE `medicals`
  MODIFY `id` mediumint(9) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `medical_files`
--
ALTER TABLE `medical_files`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `medical_payment_types`
--
ALTER TABLE `medical_payment_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `others`
--
ALTER TABLE `others`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `outreach_types`
--
ALTER TABLE `outreach_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `people`
--
ALTER TABLE `people`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `rates`
--
ALTER TABLE `rates`
  MODIFY `id` tinyint(3) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `relationships`
--
ALTER TABLE `relationships`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `remarks`
--
ALTER TABLE `remarks`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `rent`
--
ALTER TABLE `rent`
  MODIFY `id` mediumint(9) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rent_files`
--
ALTER TABLE `rent_files`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `residence`
--
ALTER TABLE `residence`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `residence_ownership_types`
--
ALTER TABLE `residence_ownership_types`
  MODIFY `id` tinyint(3) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `residence_types`
--
ALTER TABLE `residence_types`
  MODIFY `id` tinyint(3) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `schools`
--
ALTER TABLE `schools`
  MODIFY `id` tinyint(3) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `school_assistances`
--
ALTER TABLE `school_assistances`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `simple_loan_agreements`
--
ALTER TABLE `simple_loan_agreements`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `simple_loan_payments`
--
ALTER TABLE `simple_loan_payments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `spouses`
--
ALTER TABLE `spouses`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `students`
--
ALTER TABLE `students`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `upload_files`
--
ALTER TABLE `upload_files`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT for table `user_types`
--
ALTER TABLE `user_types`
  MODIFY `id` tinyint(3) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `visits`
--
ALTER TABLE `visits`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `visit_files`
--
ALTER TABLE `visit_files`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `vouchers`
--
ALTER TABLE `vouchers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wills`
--
ALTER TABLE `wills`
  MODIFY `id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
